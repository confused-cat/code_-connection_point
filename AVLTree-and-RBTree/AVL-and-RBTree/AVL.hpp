#pragma once

template<class T>
class AVLTreeNode
{
public:
	AVLTreeNode<T>* _left;//节点左孩子
	AVLTreeNode<T>* _right;//节点右孩子
	AVLTreeNode<T>* _parent;//节点双亲
	T _data; //节点值域
	int _bf; //该节点的平衡因子

	AVLTreeNode(const T& data=T())
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _data(data)
		, _bf(0)
	{}
};

//规定：树中元素唯一
template<class T>
class AVLTree
{
	typedef AVLTreeNode<T> Node;
public:
	AVLTree()
		:_root(nullptr)
	{}

	~AVLTree()
	{
		_Destroy(_root);
	}

	//AVL树节点插入
	//1.按照二叉搜索树的方式插入新节点
	//2.调整节点的平衡因子（平衡因子规定为：右子树高度-左子树高度）
	bool Insert(const T& data)
	{
		if (_root == nullptr)
		{
			//新插入节点即为根节点
			_root = new Node(data);
			return true;
		}
		//1.按照二叉搜索树插入新节点方式往AVL树中插入新节点
		//a.查找带插入节点在树中是否存在
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			parent = cur;
			if (data < cur->_data)
				cur = cur->_left;
			else if (data>cur->_data)
				cur =cur->_right;
			else
				return false;
		}
		//b.走下来表示未找到，进行节点的插入
		cur = new Node(data);
		if (data < parent->_data)
		{
			parent->_left = cur;
		}
		else
		{
			parent->_right = cur;
		}

		cur->_parent = parent;

		//节点插入成功后，其parent的子树高度改变，故需更新parent的平衡因子
		while (parent)
		{
			if (cur == parent->_left)
			{
				parent->_bf--;
			}
			else
			{
				parent->_bf++;
			}

			/////////////////////////////
			if (parent->_bf == 0)//说明节点在插入之前parent有一孩子，插入新节点并未影响整体的树的高度，只是影响parent的平衡因子
				break;
			else if (1==parent->_bf  || -1==parent->_bf )
			{
				//说明以parent为根的二叉树高度增加了一层，需要继续往上更新平衡因子
				cur = parent;
				parent = cur->_parent;
			}
			else //parent的平衡因子为2或-2
			{
				if (parent->_bf == 2)
				{
					//说明其右子树高
					if (cur->_bf == 1)
					{
						RotateLeft(parent);//右子树高且为较高右子树的右侧，故左单旋
					}
					else
					{
						//右子树较高且为较高右子树的左侧，故子树先右单旋再整体左单旋，即右左双旋
						RotateRL(parent);
						//等价于：先RotateRight(parent->_right)再RotateLeft(parent)
					}
				}
				else
				{
					//说明其左子树高
					if (cur->_bf == -1)
					{
						RotateRight(parent);//左子树较高且为较高左子树的左侧，故右单旋
					}
					else
					{
						//左子树较高且为较高左子树的右侧,故子树先左单旋再整体右单旋，即左右双旋
						RotateLR(parent);
						//等价于：先RotateLeft(parent->_left)再RotateRight(parent)
					}
				}
				//在元素插入之前以parent为根的二叉树高度
				//插入元素后进行树的旋转之后以parent为根的二叉树的高度一样
				//故无需继续向上更新
				break;
			}
		}
		return true;
	}

	void Inorder()
	{
		cout << "树的中序遍历为：" << endl;
			_Inorder(_root);
	}
	
	//检测构造出来的树是否是AVL树
	bool IsValidAVLTree()
	{
		return _IsValidAVLTree(_root);
	}

private:
	void _DestroyAVLTree(Node*& root)
	{
		if (root)
		{
			_DestroyAVLTree(root->_left);
			_DestroyAVLTree(root->_right);
			delete root;
			root = nullptr;
		}
	}

	void _Inorder(Node* root)
	{
		if (root)
		{
			_Inorder(root->_left);
			cout << root->_data << " ";
			_Inorder(root->_right);
		}
	}

	//求树的高度
	int _Height(Node* root)
	{
		if (root == nullptr)
			return 0;
		int leftHeight = _Height(root->_left);
		int rightHeight = _Height(root->_right);
		return leftHeight > rightHeight ? leftHeight + 1 : rightHeight + 1;
	}

	bool _IsValidAVLTree(Node* root)
	{
		if (root == nullptr)
			return true;
		//树非空，用平衡因子计算公式进行树的平衡因子的检测
		int leftHeight = _Height(root->_left);
		int rightHeight = _Height(root->_right);
		if (abs(rightHeight - leftHeight) > 1 || ((rightHeight - leftHeight) != root->_bf))
		{
			cout << root->_data << " " << root->_bf << " " << rightHeight - leftHeight << endl;
			//节点处的平衡因子出错，前者为创建的树更新的平衡因子，后者根据平衡因子计算公式进行数据计算
			return false;
		}
		return _IsValidAVLTree(root->_left) && _IsValidAVLTree(root->_right);
	}

	//左单旋
	void RotateLeft(Node* parent)
	{
		Node*subR = parent->_right;
		Node* subRL = subR->_left;

		//进行①指针的指向修改
		parent->_right = subRL;
		//subRL可能为空
		if (subRL)
		{
			subRL->_parent = parent;
		}

		//进行②指针的指向修改
		subR->_left = parent;
		Node* pParent = parent->_parent;
		parent->_parent = subR;
		subR->_parent = pParent;
		
		//parent为根节点时，subR直接为新的根节点
		//parent不为根节点时，parent可能为pParent的左孩子或是右孩子
		if (pParent == nullptr)
		{
			_root = subR;
		}
		else
		{
			if (parent == pParent->_left)
			{
				pParent ->_left = subR;
			}
			else
			{
				pParent->_right = subR;
			}
		}
		//将平衡因子进行订正更新
		parent->_bf = subR->_bf = 0;
	}

	//右单旋
	void RotateRight(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		//进行①指针的指向修改
		parent->_left = subLR;
		//subLR可能为空
		if (subLR)
		{
			subLR->_parent = parent;
		}

		//进行②指针的指向修改
		subL->_right = parent;
		Node* pParent = parent->_parent;
		parent->_parent = subL;
		subL->_parent = pParent;


		//parent为根节点时，subR直接为新的根节点
		//parent不为根节点时，parent可能为pParent的左孩子或是右孩子
		if (pParent == nullptr)
		{
			_root = subL;
		}
		else
		{
			if (parent == pParent->_left)
			{
				pParent->_left = subL;
			}
			else
			{
				pParent->_right = subL;
			}
		}
		//将平衡因子进行订正更新
		parent->_bf = subL -> _bf = 0;
	}

	//左右双旋
	void RotateLR(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;
		int bf = subLR->_bf;

		RotateLeft(parent->_left);
		RotateRight(parent);

		//双旋后进行平衡因子的更新
		if (bf == 1)
		{
			subL->_bf = -1;
		}
		else if (bf == -1)
		{
			parent->_bf = 1;
		}
		else
			;//bf为0的时候不需要更新
	}

	//右左双旋
	void RotateRL(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;
		int bf = subRL->_bf;

		RotateRight(parent->_right);
		RotateLeft(parent);

		if (bf == 1)
			parent->_bf = -1;
		else if (bf==-1)
			subR->_bf = 1;
		else
			;
	}
private:
	AVLTreeNode<T>* _root;
};

void TestAVLTree()
{
	int a[] = { 4, 2, 6, 1, 3, 5, 15, 7, 16, 14 };
	AVLTree<int> t;

	for (auto e : a)
	{
		t.Insert(e);
	}

	t.Inorder();

	if (t.IsValidAVLTree())
	{
		cout << endl;
		cout << "t is valid AVLTree" << endl;
	}
	else
	{
		cout << endl;
		cout << "t is invalid AVLTree" << endl;
	}
}