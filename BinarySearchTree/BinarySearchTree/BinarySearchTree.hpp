#pragma once

template<class T>
struct BSTNode
{
	BSTNode(const T& val = T())
	:_left(nullptr)
	, _right(nullptr)
	, _val(val)
	{}

	BSTNode<T>* _left;
	BSTNode<T>* _right;
	T _val;
};

template<class T>
class BSTree
{
	typedef BSTNode<T> Node;
public:
	BSTree()
		:_root(nullptr)
	{}

	~BSTree()
	{
		DestroyBSTree(_root);
	}

	//数据查找
	Node* Find(const T& val)
	{
		Node* cur = _root;
		while (cur)
		{
			if (cur->_val == val)
				return cur;
			else if (val < cur->_val)
				cur = cur->_left;
			else
				cur = cur->_right;
		}
		return nullptr;
	}

	//数据插入
	bool Insert(const T& val)
	{
		//1.当树为空树，节点直接进行插入，插入节点为根节点
		if (nullptr == _root)
		{
			_root = new Node(val);
			return true;
		}
		//2.当树非空
		//a.查找插入数据在树中是否存在（借助cur在BSTree中查找值为val的节点是否存在）
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			parent = cur;
			if (cur->_val == val)
				return false;
			else if (cur->_val > val)
				cur = cur->_left;
			else
				cur = cur->_right;
		}
		//b.插入新节点
		cur = new Node(val);
		if (parent->_val > val)
			parent->_left = cur;
		else
			parent->_right = cur;
		return true;
	}
	//数据删除
	bool Erase(const T& val)
	{
		//1.当树为空树直接返回
		if (nullptr == _root)
			return false;
		//2.当树非空
		//a.找到值为val的节点
		Node* cur = _root;
		Node* parent = nullptr;
		while (cur)
		{
			if (cur->_val == val)
				break;
			else if (cur->_val > val)
			{
				parent = cur;
				cur = cur->_left;
			}
			else
			{
				parent = cur;
				cur = cur->_right;
			}
		}
		//循环跳出情况，一是cur为空，二是元素找到后跳出循环
		//确认元素是否找到
		if (cur == nullptr)
			return false;
		//b.删除找到的节点
		//对情况进行分类：
		//情况一：欲删除节点只有左孩子或为叶子节点时
		//情况二：欲删除节点只有右孩子时
		//情况三：欲删除节点左右孩子均存在
		Node* del = cur;
		//情况一：欲删除节点只有左孩子或为叶子节点时
		if (cur->_right == nullptr)
		{
			if (parent == nullptr)
			{
				//cur为根节点
				_root = cur->_left;
			}
			else
			{
				//cur不为根节点，即parent不为空
				if (cur == parent->_left)
					parent->_left = cur->_left;
				else
					parent->_right = cur->_left;
			}
		}
		//情况二：欲删除节点只有右孩子时
		else if (cur->_left == nullptr)
		{
			if (parent == nullptr)
			{
				//cur为根节点
				_root = cur->_right;
			}
			else
			{
				//cur不为根节点，即parent不为空
				if (cur == parent->_left)
					parent->_left = cur->_right;
				else
					parent->_right = cur->_right;
			}
		}
		//情况三：欲删除节点左右孩子均存在
		else
		{
			//cur不可直接删除，需在子树中找替代节点进行删除，将情况进行转化为一或二
			//在右子树中找替代节点，找最小的节点
			del = cur->_right;
			parent = cur;
			//在左子树中找最大的节点
			while (del->_left)
			{
				parent = del;
				del = del->_left;
			}
			//将del中值域替换cur中值域
			cur->_val = del->_val;
			//将del节点删除
			if (del == parent->_left)
				parent->_left = del->_right;
			else
				parent->_right = del->_right;
		}
		delete del;
		return true;
	}

	void Inorder()
	{
		cout << "中序遍历：";
		_Inorder(_root);
		cout << endl;
	}
private:
	void _Inorder(Node* root)
	{
		if (root)
		{
			_Inorder(root->_left);
			cout << root->_val << " ";
			_Inorder(root->_right);
		}
	}
	void DestroyBSTree(Node*& root)
	{
		if (root)
		{
			DestroyBSTree(root->_left);
			DestroyBSTree(root->_right);
			delete root;
			root = nullptr;
		}
	}

private:
	BSTNode<T>* _root;
};

void Test()
{
	BSTree<int> t;
	int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
	for (auto e : a)
	{
		//插入节点
		t.Insert(e);
	}

	t.Inorder();

	//插入值为9的节点
	if (!t.Find(9))
	{
		t.Insert(9);
	}
	t.Inorder();

	//删除值为8的节点
	t.Erase(8);
	t.Inorder();

}