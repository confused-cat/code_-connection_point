#pragma once

#include<vector>
#include<assert.h>

namespace bs
{
	template<size_t N>
	class bitset
	{
	public:
		bitset()
			:_bst(N / 8 + 1)
			, _size(0)
		{}
		
		//1.比特位置1
		bitset<N>& set(size_t pos)
		{
			assert(pos < N);
			if (test(pos))
				return *this;

			size_t whichByte = pos / 8;
			size_t whichBit = pos % 8;

			_bst[whichByte] |= (1 << whichBit);
			++_size;

			return *this;
		}
		//2.比特位置0
		bitset<N>& reset(size_t pos)
		{
			assert(pos < N);
			if (!test(pos))
				return *this;

			size_t whichByte = pos / 8;
			size_t whichBit = pos % 8;

			_bst[whichByte] &= ~(1 << whichBit);
			--_size;
			return *this;
		}
		//3.判断比特位是否为1
		bool test(size_t pos)
		{
			assert(pos);
			size_t whichByte = pos / 8;
			size_t whichBit = pos % 8;
			return 0 != (_bst[whichByte] & (1 << whichBit));
		}
		size_t size()
		{
			return _size;
		}
		size_t count()
		{
			return N;
		}
	private:
		std::vector<unsigned char> _bst;
		size_t _size; //表示比特位为1的个数
	};
}

void TestBitSet()
{
	bs::bitset<24> bt;
	int array[] = { 1, 3, 7, 4, 12, 16, 19, 13, 22, 18 };
	for (auto e : array)
	{
		bt.set(e);
	}
	cout << "位图中置1比特位个数：" << bt.size() << endl;
	cout << "位图中总比特位个数：" << bt.count() << endl;

	if (bt.test(12))
	{
		cout << "12 is in bitset." << endl;
	}
	else
	{
		cout << "12 is not in bitset." << endl;
	}
	bt.reset(12);
	cout << endl;
	cout << "将12的比特位进行置0后." << endl;
	if (bt.test(12))
	{
		cout << "12 is in bitset." << endl;
	}
	else
	{
		cout << "12 is not in bitset." << endl;
	}
}