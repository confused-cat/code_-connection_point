#pragma once 
#include"Common.h"
#include"BitSet.hpp"

#include<iostream>
using namespace std;

namespace bs
{
	template<class T, size_t N, class TtoInt1 = BKDRHash, class TtoInt2 = APHash , class TtoInt3 = DJBHash >
	class bloomfilter
	{
	public:
		bloomfilter()
			:_size(0)
		{}
		void Insert(const T& data)
		{
			size_t allbitsize = _bst.count();

			size_t pos1 = TtoInt1()(data) % allbitsize;
			size_t pos2 = TtoInt2()(data) % allbitsize;
			size_t pos3 = TtoInt3()(data) % allbitsize;

			_bst.set(pos1);
			_bst.set(pos2);
			_bst.set(pos3);
			_size++;
		}
		bool Find(const T& data)
		{
			size_t allbitsize = _bst.count();
			size_t pos = TtoInt1()(data) % allbitsize;
			if (!_bst.test(pos))
				return false;
			pos = TtoInt2()(data) % allbitsize;
			if (!_bst.test(pos))
				return false;
			pos = TtoInt3()(data) % allbitsize;
			if (!_bst.test(pos))
				return false;
			//数据可能存在
			return true;
		}
	private:
		bitset<N * 3> _bst;
		size_t _size;
	};
}
void TestBloomFilter()
{
	bs::bloomfilter<string, 100> bf;
	bf.Insert("chinese");
	bf.Insert("english");
	bf.Insert("math");
	bf.Insert("science");
	bf.Insert("geography");

	if (bf.Find("chinese"))
	{
		cout << "chinese 可能存在集合中" << endl;
	}
	else
	{
		cout << "chinese 一定不在集合中" << endl;
	}
}