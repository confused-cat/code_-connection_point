#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
#include<malloc.h>
#include<memory>
using namespace std;

#include"smart_ptr.hpp"

int main()
{
	//TestSmart_ptr1();
	//TestSmart_ptr2();

	//Testauto_ptr1();
	//Testauto_ptr2();
	//Testauto_ptr3();

	//Testunique_ptr1();
	//Testunique_ptr2();

	//Testshared_ptr();
	//Testshared_ptr1();
	//Testshared_ptr2();
	//Testshared_ptr3();

	//智能指针的安全性问题
	//引用计数的安全性问题
	//1.通过加锁来保证引用计数的原子操作过程
	//Testshared_ptr4();
	//Testshared_ptr5();

	//2.shared_ptr的循环引用问题
	Testshared_ptr6();

	system("pause");
	_CrtDumpMemoryLeaks();
	return 0;
}