#pragma once

//////////////////////////////////////////////////////
//smart_ptr
template<class T>
class smart_ptr
{
public:
	//RAII
	smart_ptr(T* ptr = nullptr)
		:_ptr(ptr)
	{}
	~smart_ptr()
	{
		//如果指针管理资源，则将管理的资源释放掉
		if (_ptr)
		{
			delete _ptr;
			_ptr = nullptr;
		}
	}
	//具有指针类似的行为
	T& operator*()
	{
		return *_ptr;
	}
	T* operator->()
	{
		return _ptr;
	}
	//提供原生态指针操作
	T* Get()
	{
		return _ptr;
	}
private:
	T* _ptr;
};

void TestSmart_ptr1()
{
	int* p = new int(1);
	//将申请的空间交给smart_ptr管理
	smart_ptr<int> sp(p);
	smart_ptr<int> sp1(new int(2));
	*sp1 = 100;
}

struct str
{
	int a;
	int b;
	int c;
};
void TestSmart_ptr2()
{
	str *s=new str();
	smart_ptr<str> sp(s);
	s->a = 1;
	s->b = 2;
	s->c = 3;
}

//////////////////////////////////////////////////////
//auto_ptr第一版
namespace auo
{
	template<class T>
	class auto_ptr1
	{
	public:
		//RAII
		auto_ptr1(T* ptr = nullptr)
			:_ptr(ptr)
		{}
		~auto_ptr1()
		{
			if (_ptr)
			{
				delete _ptr;
				_ptr = nullptr;
			}
		}
		//具有指针的行为
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		//解决浅拷贝方式：进行资源的转移
		auto_ptr1(auto_ptr1<T>& ap)
			:_ptr(ap._ptr)
		{
			ap._ptr = nullptr;
		}
		auto_ptr1<T>& operator=(auto_ptr1<T>& ap)
		{
			//先检测不是自己对自己赋值
			if (this != &ap)
			{
				//若当前对象有管理资源，则将资源释放掉
				if (_ptr)
					delete _ptr;
				_ptr = ap._ptr;
				ap._ptr = nullptr;
			}
			return *this;
		}
	private:
		T* _ptr;
	};
}
class A
{
public:
	A()
	{
		cout << "A()" << endl;
	}
	~A()
	{
		cout << "~A()" << endl;
	}
	int a;
	int b;
	int c;
};
void Testauto_ptr1()
{
	auo::auto_ptr1<A> sp(new A());
	sp->a = 1;
	sp->b = 2;
	sp->c = 3;

	auo::auto_ptr1<A> sp1(sp);
	sp1->a = 10;
	sp1->b = 20;
	sp1->c = 30;
	//cout<<sp->a<<endl;

	auo::auto_ptr1<A> sp2;
	sp2 = sp1;
	sp2->a = 100;
	sp2->b = 200;
	sp2->c = 300;
}

//////////////////////////////////////////////////////
//auto_ptr第二版
namespace auo
{
	template<class T>
	class auto_ptr2
	{
	public:
		//RAII
		auto_ptr2(T* ptr = nullptr)
			:_ptr(ptr)
			,_owner(false)
		{
			//资源管理释放权，默认设为false,当其有资源时，设置为true
			if (_ptr)
			{
				_owner = true;
			}
		}
		~auto_ptr2()
		{
			if (_ptr && _owner)
			{
				delete _ptr;
				_ptr = nullptr;
				_owner = false;
			}
		}
		//具有指针的行为
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		//解决浅拷贝方式：进行资源的转移
		auto_ptr2(const auto_ptr2<T>& ap)
			:_ptr(ap._ptr)
			, _owner(ap._owner)
		{
			ap._owner = false;
		}
		auto_ptr2<T>& operator=(const auto_ptr2<T>& ap)
		{
			//先检测不是自己对自己赋值
			if (this != &ap)
			{
				//若当前对象有管理资源，则将资源释放掉
				if (_ptr && _owner)
					delete _ptr;
				_ptr = ap._ptr;
				_owner = ap._owner;
				ap._owner = false;
			}
			return *this;
		}
	private:
		T* _ptr;
		mutable bool _owner;//true:表示当前对象对资源具有资源释放的权力
	};
}
class B
{
public:
	B()
	{
		cout << "B()" << endl;
	}
	~B()
	{
		cout << "~B()" << endl;
	}
	int a;
	int b;
	int c;
};
void Testauto_ptr2()
{
	auo::auto_ptr2<B> sp(new B());
	sp->a = 1;
	sp->b = 2;
	sp->c = 3;

	auo::auto_ptr2<B> sp1(sp);
	sp1->a = 10;
	sp1->b = 20;
	sp1->c = 30;
	sp->a = 1;

	auo::auto_ptr2<B> sp2;
	sp2 = sp1;
	sp2->a = 100;
	sp2->b = 200;
	sp2->c = 300;
	sp1->a = 10;
}

void Testauto_ptr3()
{
	auo::auto_ptr2<B> sp(new B());
	sp->a = 1;
	sp->b = 2;
	sp->c = 3;

	if (1)
	{
		auo::auto_ptr2<B> sp1(sp);
		sp1->a = 10;
		sp1->b = 20;
		sp1->c = 30;
	}
	sp->a = 100;
	sp->b = 200;
	sp->c = 300;
}

//////////////////////////////////////////////////////
//unique_ptr第一版

namespace uni
{
	template<class T>
	class unique_ptr
	{
	public:
		//RAII
		unique_ptr(T* ptr = nullptr)
			:_ptr(ptr)
		{}
		~unique_ptr()
		{
			if (_ptr)
			{
				delete _ptr;
				_ptr = nullptr;
			}
		}
		//具有指针的行为
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		//解决浅拷贝的方式：防拷贝
		unique_ptr(const unique_ptr<T>& ap) = delete;
		unique_ptr<T>& operator=(const unique_ptr<T>& ap) = delete;
	private:
		T* _ptr;
	};
}
class C
{
public:
	C()
	{
		cout << "C()" << endl;
	}
	~C()
	{
		cout << "~C()" << endl;
	}
	int a;
	int b;
};
void Testunique_ptr1()
{
	uni::unique_ptr<C> sp = new C();
	sp->a = 1;
	sp->b = 2;

	//uni::unique_ptr<C> sp1(sp);

	//uni::unique_ptr<C> sp2;
	//sp2 = sp;
}

//////////////////////////////////////////////////////
//unique_ptr第二版
//增加空间释放的多样性，析构函数不再单一化delete

namespace uni
{
	template<class T>
	//默认为new申请的空间，使用delete来进行释放
	class Def
	{
	public:
		void operator()(T*& ptr)
		{
			if (ptr)
			{
				cout << "  delete()" << endl;
				delete ptr;
				ptr = nullptr;
			}
		}
	};

	template<class T,class DF=Def<T>>
	class unique_ptr1
	{
	public:
		//RAII
		unique_ptr1(T* ptr = nullptr)
			:_ptr(ptr)
		{}
		~unique_ptr1()
		{
			if (_ptr)
			{
				//根据用户提供的资源方式去释放
				DF df;
				df(_ptr);
				//DF()(_ptr)
			}
		}
		//具有指针的行为
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		T* Get()
		{
			return _ptr;
		}
		//解决浅拷贝的方式：防拷贝
		//C++11
		unique_ptr1(const unique_ptr1<T>& ap) = delete;
		unique_ptr1<T>& operator=(const unique_ptr1<T>& ap) = delete;
		//C++89
	//private:
		//unique_ptr1(const unique_ptr1<T>& ap);
		//unique_ptr1<T>& operator=(const unique_ptr1<T>& ap);
	private:
		T* _ptr;
	};
}

//malloc申请的空间释放
template<class T>
class Free
{
public:
	void operator()(T*& ptr)
	{
		if (ptr)
		{
			cout << "  Free()" << endl;
			free(ptr);
			ptr = nullptr;
		}
	}
};

//关闭文件指针的空间释放
class Fclose
{
public:
	void operator()(FILE*& ptr)
	{
		if (ptr)
		{
			cout << "  Fclose()" << endl;
			fclose(ptr);
			ptr = nullptr;
		}
	}
};

//申请连续空间的释放
template <class T>
class deleteArray
{
public:
	void operator()(T*& ptr)
	{
		if (ptr)
		{
			cout << "  delete[]" << endl;
			delete[] ptr;
			ptr = nullptr;
		}
	}
};

void Testunique_ptr2()
{
	uni::unique_ptr1<int> sp(new int(10));
	uni::unique_ptr1<int ,Free<int>> sp1((int*)malloc(sizeof(int)));
	uni::unique_ptr1<FILE, Fclose> sp2(fopen("abc.txt", "w"));
}


//////////////////////////////////////////////////////
//shared_ptr：第一版
//普通成员变量，资源的计数有问题，计数并未进行共享起来
namespace sha
{
	template<class T>
	//默认为new申请的空间，使用delete来进行释放
	class Def
	{
	public:
		void operator()(T*& ptr)
		{
			if (ptr)
			{
				cout << "  delete()" << endl;
				delete ptr;
				ptr = nullptr;
			}
		}
	};

	template<class T, class DF = Def<T>>
	class shared_ptr1
	{
	public:
		//RAII
		shared_ptr1(T* ptr = nullptr)
			:_ptr(ptr)
			, _count(0)
		{
			//有资源的话，则使用资源的对象+1
			if (_ptr)
				_count = 1;
		}
		~shared_ptr1()
		{
			//除了自己之外无其他对象使用该资源的话
			//则将资源释放掉
			if (_ptr && --_count==0)
			{
				//根据用户提供的资源方式去释放
				DF df;
				df(_ptr);
				//DF()(_ptr)
			}
		}
		//具有指针的行为
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		T* Get()
		{
			return _ptr;
		}
		//解决浅拷贝的方式：引用计数
		shared_ptr1(shared_ptr1<T>& ap)
			:_ptr(ap._ptr)
			, _count(++ap._count)
		{}
	private:
		T* _ptr;
		int _count;
	};
}

void Testshared_ptr()
{
	//sp与sp1共享资源
	sha::shared_ptr1<int> sp(new int(1));
	sha::shared_ptr1<int> sp1(sp);

	sha::shared_ptr1<int> sp2(new int(1));
}

void Testshared_ptr1()
{
	sha::shared_ptr1<int> sp(new int(1));
	if (1)
	{
		sha::shared_ptr1<int> sp1(sp);
	}

	sha::shared_ptr1<int> sp2(new int(1));
}

//////////////////////////////////////////////////////
//shared_ptr：第二版
//静态资源变量，资源的计数有问题,计数资源的份数对应不上
namespace sha
{
	template<class T>
	//默认为new申请的空间，使用delete来进行释放
	class Deff
	{
	public:
		void operator()(T*& ptr)
		{
			if (ptr)
			{
				cout << "  delete()" << endl;
				delete ptr;
				ptr = nullptr;
			}
		}
	};

	template<class T, class DF = Deff<T>>
	class shared_ptr2
	{
	public:
		//RAII
		shared_ptr2(T* ptr = nullptr)
			:_ptr(ptr)
		{
			//有资源的话，则使用资源的对象+1
			if (_ptr)
				_count = 1;
		}
		~shared_ptr2()
		{
			//除了自己之外无其他对象使用该资源的话
			//则将资源释放掉
			if (_ptr && --_count == 0)
			{
				//根据用户提供的资源方式去释放
				DF df;
				df(_ptr);
				//DF()(_ptr)
			}
		}
		//具有指针的行为
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		T* Get()
		{
			return _ptr;
		}
		//解决浅拷贝的方式：引用计数
		shared_ptr2(shared_ptr2<T>& ap)
			:_ptr(ap._ptr)
		{
			++_count;
		}
	private:
		T* _ptr;
		static int _count;
	};
	template<class T,class DF>
	int shared_ptr2<T, DF>::_count = 0;
}

void Testshared_ptr2()
{
	sha::shared_ptr2<int> sp(new int(1));
	sha::shared_ptr2<int> sp1(sp);
	sha::shared_ptr2<int> sp2(new int(1));
}

//////////////////////////////////////////////////////
//shared_ptr：第三版
namespace sha
{
	template<class T>
	//默认为new申请的空间，使用delete来进行释放
	class De
	{
	public:
		void operator()(T*& ptr)
		{
			if (ptr)
			{
				cout << "  delete()" << endl;
				delete ptr;
				ptr = nullptr;
			}
		}
	};

	template<class T, class DF = De<T>>
	class shared_ptr3
	{
	public:
		//RAII
		shared_ptr3(T* ptr = nullptr)
			:_ptr(ptr)
			, _count(nullptr)
		{
			//有资源的话，则使用资源的对象+1
			if (_ptr)
			{
				_count = new int(1);
			}
		}
		~shared_ptr3()
		{
			Rlease();
		}
		//具有指针的行为
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		//解决浅拷贝的方式：引用计数
		shared_ptr3(const shared_ptr3<T>& ap)
			:_ptr(ap._ptr)
			,_count(ap._count)
		{
			++(*_count);
		}
		shared_ptr3<T>& operator=(const shared_ptr3<T>& ap)
		{
			if (this != &ap)
			{
				//如果this原来有资源，则先将this的资源进行释放掉
				Rlease();
				//两者共享资源
				_ptr = ap._ptr;
				_count = ap._count;
				++(*_count);
			}
			return *this;
		}
	private:
		void Rlease()
		{
			if (_ptr && --(*_count)==0)
			{
				DF df;
				df(_ptr);
				delete _count;
				_count = nullptr;
			}
		}
	private:
		T* _ptr;
		int* _count;
	};
}

void Testshared_ptr3()
{
	//sp与sp1共享资源
	sha::shared_ptr3<int> sp(new int(10));
	sha::shared_ptr3<int> sp1(sp);
	
	//sp2与sp3共享资源
	sha::shared_ptr3<int> sp2(new int(20));
	sha::shared_ptr3<int> sp3(sp2);

	//sp sp1 sp2共享资源
	sp2 = sp1;

	//sp sp1 sp2 sp3共享资源
	sp3 = sp1;
}

//////////////////////////////////////////////////////
//解决引用计数的原子操作性
//计数过程的加锁操作
//shared_ptr：第四版
#include<mutex>
namespace sha
{
	template<class T>
	//默认为new申请的空间，使用delete来进行释放
	class D
	{
	public:
		void operator()(T*& ptr)
		{
			if (ptr)
			{
				cout << "  delete()" << endl;
				delete ptr;
				ptr = nullptr;
			}
		}
	};

	template<class T, class DF = D<T>>
	class shared_ptr4
	{
	public:
		//RAII
		shared_ptr4(T* ptr = nullptr)
			:_ptr(ptr)
			, _count(nullptr)
			, _mutex(nullptr)
		{
			//有资源的话，则使用资源的对象+1
			if (_ptr)
			{
				_count = new int(1);
				_mutex = new mutex;
			}
		}
		~shared_ptr4()
		{
			Rlease();
		}
		//具有指针的行为
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		//解决浅拷贝的方式：引用计数
		shared_ptr4(const shared_ptr4<T>& ap)
			:_ptr(ap._ptr)
			, _count(ap._count)
			, _mutex(ap.mutex)
		{
			AddRef();
		}
		shared_ptr4<T>& operator=(const shared_ptr4<T>& ap)
		{
			if (this != &ap)
			{
				//如果this原来有资源，则先将this的资源进行释放掉
				Rlease();
				//两者共享资源
				_ptr = ap._ptr;
				_count = ap._count;
				_mutex = ap._mutex;
				AddRef();
			}
			return *this;
		}
	private:
		//释放资源
		void Rlease()
		{
			_mutex->lock();
			//标记资源是否被释放了
			bool flag=false;
			if (_ptr && --(*_count) == 0)
			{
				DF df;
				df(_ptr);
				delete _count;
				_count = nullptr;

				flag = true;
			}
			_mutex->unlock();
			//判断资源是否被释放，被释放的话将锁资源也给释放掉
			if (flag == true)
			{
				delete _mutex;
			}
		}
		//给只要有计数的地方都进行加锁操作
		void AddRef()
		{
			_mutex->lock();
			++(*_count);
			_mutex->unlock;
		}
	private:
		T* _ptr;
		int* _count;
		mutex* _mutex;
	};
}
void Testshared_ptr4()
{
	//sp与sp1共享资源
	sha::shared_ptr3<int> sp(new int(10));
	sha::shared_ptr3<int> sp1(sp);

	//sp2与sp3共享资源
	sha::shared_ptr3<int> sp2(new int(20));
	sha::shared_ptr3<int> sp3(sp2);

	//sp sp1 sp2共享资源
	sp2 = sp1;

	//sp sp1 sp2 sp3共享资源
	sp3 = sp1;
}

//////////////////////////////////////////////////////
//解决shared_ptr的循环引用问题
//shared_ptr：第五版
namespace sha
{
	template<class T>
	//默认为new申请的空间，使用delete来进行释放
	class Dee
	{
	public:
		void operator()(T*& ptr)
		{
			if (ptr)
			{
				cout << "  delete()" << endl;
				delete ptr;
				ptr = nullptr;
			}
		}
	};

	template<class T, class DF = Dee<T>>
	class shared_ptr5
	{
	public:
		//RAII
		shared_ptr5(T* ptr = nullptr)
			:_ptr(ptr)
			, _count(nullptr)
			, _mutex(nullptr)
		{
			//有资源的话，则使用资源的对象+1
			if (_ptr)
			{
				_count = new int(1);
				_mutex = new mutex;
			}
		}
		~shared_ptr5()
		{
			Rlease();
		}
		//具有指针的行为
		T& operator*()
		{
			return *_ptr;
		}
		T* operator->()
		{
			return _ptr;
		}
		//解决浅拷贝的方式：引用计数
		shared_ptr5(const shared_ptr5<T>& ap)
			:_ptr(ap._ptr)
			, _count(ap._count)
			, _mutex(ap.mutex)
		{
			AddRef();
		}
		shared_ptr5<T>& operator=(const shared_ptr5<T>& ap)
		{
			if (this != &ap)
			{
				//如果this原来有资源，则先将this的资源进行释放掉
				Rlease();
				//两者共享资源
				_ptr = ap._ptr;
				_count = ap._count;
				_mutex = ap._mutex;
				AddRef();
			}
			return *this;
		}
	private:
		//释放资源
		void Rlease()
		{
			_mutex->lock();
			//标记资源是否被释放了
			bool flag = false;
			if (_ptr && --(*_count) == 0)
			{
				DF df;
				df(_ptr);
				delete _count;
				_count = nullptr;

				flag = true;
			}
			_mutex->unlock();
			//判断资源是否被释放，被释放的话将锁资源也给释放掉
			if (flag == true)
			{
				delete _mutex;
			}
		}
		//给只要有计数的地方都进行加锁操作
		void AddRef()
		{
			_mutex->lock();
			++(*_count);
			_mutex->unlock();
		}
	private:
		T* _ptr;
		int* _count;
		mutex* _mutex;
	};
}

struct Node
{
	sha::shared_ptr5<Node> next;
	sha::shared_ptr5<Node> prev;
	int data;

	Node(int val = 0)
		:next(nullptr)
		, prev(nullptr)
		, data(val)
	{}
	~Node()
	{
		cout << "~Node()" << endl;
	}
};

void Testshared_ptr5()
{
	//定义两个节点
	sha::shared_ptr5<Node> sp1(new Node);
	sha::shared_ptr5<Node> sp2(new Node);

	//给两节点值域赋值
	sp1->data = 10;
	sp2->data = 20;

	//将两节点进行连接起来
	sp1->next = sp2;
	sp2->prev = sp1;
}

#include<memory>
#include<iostream>
using namespace std;
//链表节点
struct ListNode
{
	weak_ptr<ListNode> next;
	weak_ptr<ListNode> prev;
	int data;
	~ListNode()
	{
		cout << "~ListNode()" << endl;
	}
};

void Testshared_ptr6()
{
	//定义两个节点
	std::shared_ptr<ListNode> sp1(new ListNode);
	std::shared_ptr<ListNode> sp2(new ListNode);

	//给两节点值域赋值
	sp1->data = 10;
	sp2->data = 20;

	//将两节点进行连接起来
	sp1->next = sp2;
	sp2->prev = sp1;
}

