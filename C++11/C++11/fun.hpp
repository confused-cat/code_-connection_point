#pragma once

#include<iostream>
using namespace std;

#include<algorithm>
#include<functional>

struct good
{
	string _name;//商品名称
	double _price;//商品价格
	int _eva;//商品评价

	good(const char* str, double price, int eva)
		:_name(str)
		, _price(price)
		, _eva(eva)
	{}
};

//按照商品价格对商品进行升序排序
struct ComPriceLess
{
	bool operator()(const good& g1, const good& g2)
	{
		return g1._price < g2._price;
	}
};
//按照商品价格对商品进行降序排序
struct ComPriceGreater
{
	bool operator()(const good& g1, const good& g2)
	{
		return g1._price > g2._price;
	}
};