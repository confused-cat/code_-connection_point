#define _CRT_SECURE_NO_WARNINGS
#include<iostream>
#include<vector>
#include<string>
#include<list>
#include<map>
#include<assert.h>
#include<string>
using namespace std;

namespace na
{
	class string
	{
	public:
		typedef char* iterator;
		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}
		string(const char* str = "")
			:_size(strlen(str))
			, _capacity(_size)
		{
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}
		// s1.swap(s2)
		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}
		// 拷贝构造
		string(const string& s)
			:_str(nullptr)
		{
			cout << "string(const string& s) --> 深拷贝" << endl;
			string tmp(s._str);
			swap(tmp);
		}
		// 赋值重载
		string& operator=(const string& s)
		{
			cout << "string& operator=(string s) --> 深拷贝" << endl;
			string tmp(s);
			swap(tmp);
			return *this;
		}
		// 移动构造
		string(string&& s)
			:_str(nullptr)
			, _size(0)
			, _capacity(0)
		{
			cout << "string(string&& s) --> 移动构造" << endl;
			swap(s);
		}
		// 移动赋值
		string& operator=(string&& s)
		{
			cout << "string& operator=(string&& s) --> 移动赋值" << endl;
			swap(s);
			return *this;
		}
		~string()
		{
			delete[] _str;
			_str = nullptr;
		}
		void reserve(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;
				_capacity = n;
			}
		}
		void push_back(char ch)
		{
			if (_size >= _capacity)
			{
				size_t newcapacity = _capacity == 0 ? 4 : _capacity * 2;
				reserve(newcapacity);
			}
			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';
		}
		//string operator+=(char ch)
		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}
	private:
		char* _str;
		size_t _size;
		size_t _capacity;
	};
}
namespace na
{
	na::string to_string(int value)
	{
		bool flag = true;
		if (value < 0)
		{
			flag = false;
			value = 0 - value;
		}
		na::string str;
		while (value>0)
		{
			int x = value % 10;
			value /= 10;
			str += ('0' + x);
		}
		if (flag == false)
		{
			str += '-';
		}
		std::reverse(str.begin(), str.end());
		return str;
	}
}

#include"fun.hpp"
#include<iostream>
using namespace std;
//sort头文件
#include<algorithm>
//less和greater头文件,实际为类模板，在类中将（）进行重载了
#include<functional>

#if 0
int main()
{
	//标准库排序实现
	int array[] = { 4, 1, 8, 5, 3, 7, 0, 9, 2, 6 };
	//默认按照升序的方式进行排序
	std::sort(array, array + sizeof(array) / sizeof(array[0]));
	//greater按照升序的方式进行排序
	for (auto e : array)
	{
		cout << e << " ";
	}
	cout << endl;

	std::sort(array, array + sizeof(array) / sizeof(array[0]), greater<int>());
	for (auto e : array)
	{
		cout << e << " ";
	}
	cout << endl;

	//自定义类型的实现
	vector<good> v = { { "苹果", 2.1, 5 }, { "香蕉", 3, 4 }, { "鸭梨", 2.2, 3 } };
	sort(v.begin(), v.end(), ComPriceLess());
	for (auto e : v)
	{
		cout << e._name << endl;
	}
	cout << endl;
	sort(v.begin(), v.end(), ComPriceGreater());
	for (auto e : v)
	{
		cout << e._name << endl;
	}

	//最简单的lambda表达式，无意义
	[]{};
	//省略参数列表和返回值类型，返回值类型由编译器进行推导
	int a = 10;
	int b = 20;
	[=]{return a + 10; };

	//省略返回值类型
	auto f = [&](int c){b = a + c; };
	f(10);
	cout << a << " " << b << endl;

	//捕获x
	int x = 10;
	auto ff = [x](int a)mutable{x *= 2; return a + x; };
	cout << ff(10) << endl;

	system("pause");
	return 0;
}
#endif

#if 0
void(*pf)();
int main()
{
	//两内容及实现一样的lambda表达式 （相当于匿名函数）
	auto f1 = []{cout << "hello world" << endl; };
	auto f2 = []{cout << "hello world" << endl; };

	//lambda表达式拷贝构造一个新对象并调用
	auto f3(f2);
	f3();

	//lambda表达式赋值给相同类型的函数指针并调用
	pf = f2;
	pf();

	//将f2赋值给f1
	//f1 = f2;

	system("pause");
	return 0;
}
#endif

class Rate
{
public:
	Rate(double rate) 
		: _rate(rate)
	{}
	double operator()(double money, int year)
	{
		return money * _rate * year;
	}
private:
	double _rate;
};

#if 0
int main()
{
	double rate = 0.5;
	int a = 10;
	int b = 20;

	auto r1 = [=]{return 0; };
	cout << sizeof(r1) << endl;

	auto r2 = [=]
	{
		cout << a << b << endl;
	};
	cout << sizeof(r2) << endl;

	auto r3 = [rate, a, &b](double money, int year)->double
	{
		return money*rate*year;
	};
	cout << sizeof(r3) << endl;

	system("pause");
	return 0;
}
#endif

template<class F,class T>
T Fun(F f, T x,T y)
{
	static int count = 0;
	cout << "count:" << ++count << endl;
	cout << "count:" << &count << endl;
	return f(x, y);
	cout << endl;
}
int add(int a, int b)
{
	return a + b;
}
class sub
{
public:
	int operator()(int m,int n)
	{
		return m - n;
	}
};

#include<functional>
#if 0
int main()
{
	//函数指针
	int a = 0;
	a = Fun(add, 10, 20);
	cout << a << endl;
	
	//函数对象
	a = Fun(sub(), 30, 40);
	cout << Fun(sub(), 30, 40)<<endl;

	//lambda表达式
	a = Fun([](int c, int d)->int{return c / 2 + d / 2; }, 20, 20);
	cout << a << endl;

	cout << endl;
	//function包装器的使用
	int b = 0;
	//f为包装器的一个对象
	function<int(int, int)> f(add);
	b = Fun(f,10, 20);
	cout << b << endl;

	f = sub();
	b = Fun(f, 30, 40);
	cout << b << endl;

	f = [](int c, int d)->int{return c / 2 + d / 2; };
	b = Fun(f, 20, 20);
	cout << b << endl;

	system("pause");
	return 0;
}
#endif

class Add
{
public:
	static int add1(int a, int b)
	{
		return a + b;
	}
	double add2(double a, double b)
	{
		return a + b;
	}
};
#if 0
int main()
{
	function<int(int, int)> ff = Add::add1;
	ff(10, 20);

	//add1与add2函数类型不一样，编译报错，非静态成员里面有this
	//ff = Add::add2;

	function<double(Add, double, double)> pp = &Add::add2;//包装非静态成员方法的时候，得加上&
	pp(Add(), 10.0, 20.0);//表示用Add()对象调用函数里面的函数add2

	system("pause");
	return 0;
}
#endif

int Plus(int a, int b)
{
	return a + b;
}
class Sub
{
public:
	int sub(int a, int b)
	{
		return a - b;
	}
};
#if 0
int main()
{
	//绑定函数为普通函数
	//绑定函数Plus，参数由绑定预留的第一和第二个参数来传递
	function<int(int, int)> f1 = bind(Plus, placeholders::_1, placeholders::_2);
	//传递1和2
	//placeholders::_1位置就是1，将来传递给Plus函数的第一个参数
	//placeholders::_2位置就是2，将来传递给Plus函数的第二个参数
	cout << f1(1, 2) << endl;

	//f2类型为：function<int(int, int)>
	auto f2 = bind(Plus, 3, 4);
	cout << f2(3, 4) << endl;
	cout << f2() << endl;

	//绑定的函数是类成员函数
	Sub s;
	function<int(int, int)> f3 = bind(&Sub::sub, s, placeholders::_1, placeholders::_2);
	cout << f3(7, 8) << endl;

	//将预留的参数位置进行调换
	function<int(int, int)> f4 = bind(&Sub::sub, s, placeholders::_2, placeholders::_1);
	//9是第二个参数，将来传递给类成员函数sub的第二个参数
	//10是第一个参数，将来传递给类成员函数sub的第一个参数
	cout << f4(9, 10) << endl;

	system("pause");
	return 0;
}
#endif

#include<thread>
// 函数指针
void ThreadFun(int a)
{
	cout << "thread1" << endl;
}
// 函数对象，仿函数
class Funal
{
public:
	void operator()()
	{
		cout << "thread3" << endl;
	}
};
#if 0
int main()
{
	//线程执行函数是函数指针
	thread t1(ThreadFun, 10);
	//执行函数是lambda表达式
	thread t2([]{cout << "thread2" << endl; });
	//执行函数是函数对象
	Funal f;
	thread t3(f);

	//确保线程在主线程结束前结束
	t1.join();
	t2.join();
	t3.join();

	system("pause");
	return 0;
}
#endif

#include<thread>
void ThreadFun1(int& a)
{
	a += 10;
}
void ThreadFun2(int* a)
{
	*a += 10;
}
#if 0
int main()
{
	int x = 10;
	//线程t1修改x，不会影响外部的实参，其为线程栈中的一份拷贝，打印x=10
	thread t1(ThreadFun1, x);
	t1.join();
	cout << x << endl;

	//地址的拷贝
	thread t2(ThreadFun2, &x);
	t2.join();
	cout << x << endl;

	//std::ref()：通过形参改变外部的实参
	thread t3(ThreadFun1, std::ref(x));
	t3.join();
	cout << x << endl;

	system("pause");
	return 0;
}
#endif

#include<mutex>
#include<thread>
#include<atomic>

//定义原子操作变量sum，其操作过程都是原子操作
atomic_long sum{ 0 };

void fun(size_t num)
{
	for (size_t i = 0; i < num; ++i)
	{
		sum++; //自加过程为原子操作
	}
}
#if 0
int main()
{
	cout << "Thread Before: sum=" << sum << endl;

	thread t1(fun, 100000000);
	thread t2(fun, 100000000);

	t1.join();
	t2.join();

	cout << "Thread After: sum=" << sum << endl;

	system("pause");
	return 0;
}
#endif

template<class _Mutex>
class unique_lock
{	// whizzy class with destructor that unlocks mutex
public:
	typedef unique_lock<_Mutex> _Myt;
	typedef _Mutex mutex_type;

	// CONSTRUCT, ASSIGN, AND DESTROY
	unique_lock() _NOEXCEPT
		: _Pmtx(0), _Owns(false)
	{	// default construct
	}

	explicit unique_lock(_Mutex& _Mtx)
		: _Pmtx(&_Mtx), _Owns(false)
	{	// construct and lock
		_Pmtx->lock();
		_Owns = true;
	}

	unique_lock(_Mutex& _Mtx, adopt_lock_t)
		: _Pmtx(&_Mtx), _Owns(true)
	{	// construct and assume already locked
	}

	unique_lock(_Mutex& _Mtx, defer_lock_t) _NOEXCEPT
		: _Pmtx(&_Mtx), _Owns(false)
	{	// construct but don't lock
	}

	unique_lock(_Mutex& _Mtx, try_to_lock_t)
		: _Pmtx(&_Mtx), _Owns(_Pmtx->try_lock())
	{	// construct and try to lock
	}

	template<class _Rep, class _Period>
	unique_lock(_Mutex& _Mtx,
		const chrono::duration<_Rep, _Period>& _Rel_time)
		: _Pmtx(&_Mtx), _Owns(_Pmtx->try_lock_for(_Rel_time))
	{	// construct and lock with timeout
	}

	template<class _Clock, class _Duration>
	unique_lock(_Mutex& _Mtx,
		const chrono::time_point<_Clock, _Duration>& _Abs_time)
		: _Pmtx(&_Mtx), _Owns(_Pmtx->try_lock_until(_Abs_time))
	{	// construct and lock with timeout
	}

	unique_lock(_Mutex& _Mtx, const xtime *_Abs_time)
		: _Pmtx(&_Mtx), _Owns(false)
	{	// try to lock until _Abs_time
		_Owns = _Pmtx->try_lock_until(_Abs_time);
	}

	unique_lock(unique_lock&& _Other) _NOEXCEPT
		: _Pmtx(_Other._Pmtx), _Owns(_Other._Owns)
	{	// destructive copy
		_Other._Pmtx = 0;
		_Other._Owns = false;
	}

	unique_lock& operator=(unique_lock&& _Other) _NOEXCEPT
	{	// destructive copy
		if (this != &_Other)
		{	// different, move contents
			if (_Owns)
				_Pmtx->unlock();
			_Pmtx = _Other._Pmtx;
			_Owns = _Other._Owns;
			_Other._Pmtx = 0;
			_Other._Owns = false;
		}
		return (*this);
	}

	~unique_lock() _NOEXCEPT
	{	// clean up
		if (_Owns)
		_Pmtx->unlock();
	}

	unique_lock(const unique_lock&) = delete;
	unique_lock& operator=(const unique_lock&) = delete;

	// LOCK AND UNLOCK
	void lock()
	{	// lock the mutex
		_Pmtx->lock();
		_Owns = true;
	}

	bool try_lock() _NOEXCEPT
	{	// try to lock the mutex
		_Owns = _Pmtx->try_lock();
		return (_Owns);
	}

	template<class _Rep,
	class _Period>
		bool try_lock_for(const chrono::duration<_Rep, _Period>& _Rel_time)
	{	// try to lock mutex with timeout
			_Owns = _Pmtx->try_lock_for(_Rel_time);
			return (_Owns);
		}

	template<class _Clock,
	class _Duration>
		bool try_lock_until(
		const chrono::time_point<_Clock, _Duration>& _Abs_time)
	{	// try to lock mutex with timeout
			_Owns = _Pmtx->try_lock_until(_Abs_time);
			return (_Owns);
		}

	bool try_lock_until(const xtime *_Abs_time)
	{	// try to lock the mutex until _Abs_time
		_Owns = _Pmtx->try_lock_until(_Abs_time);
		return (_Owns);
	}

	void unlock()
	{	// unlock the mutex
		_Pmtx->unlock();
		_Owns = false;
	}

	// MUTATE
	void swap(unique_lock& _Other) _NOEXCEPT
	{	// swap with _Other
		_STD swap(_Pmtx, _Other._Pmtx);
		_STD swap(_Owns, _Other._Owns);
	}

	_Mutex *release() _NOEXCEPT
	{	// disconnect
		_Mutex *_Res = _Pmtx;
		_Pmtx = 0;
		_Owns = false;
		return (_Res);
	}

	// OBSERVE
	bool owns_lock() const _NOEXCEPT
	{	// return true if this object owns the lock
		return (_Owns);
	}

	explicit operator bool() const _NOEXCEPT
	{	// return true if this object owns the lock
		return (_Owns);
	}

	_Mutex *mutex() const _NOEXCEPT
	{	// return pointer to managed mutex
		return (_Pmtx);
	}

private:
	_Mutex *_Pmtx;
	bool _Owns;
};

//创建两线程，交替打印奇数与偶数
#include<thread>
#include<iostream>
#include<mutex>
#include<memory>
#include<condition_variable>
#include<atomic>
using namespace std;

mutex mtx;
condition_variable c1, c2;
int g = 1;
int n = 101;
void fun1()
{
	while (g<n)
	{
		std::unique_lock<mutex> lock(mtx);
		cout << "thread1:" << g << endl;
		g++;
		//唤醒线程2，阻塞线程1
		c2.notify_one();
		c1.wait(lock);
		lock.unlock();
	}
}
void fun2()
{
	while (g<n)
	{
		std::unique_lock<mutex> locker(mtx);
		cout << "thread2:" << g << endl;
		g++;
		c1.notify_one();
		c2.wait(locker);
		locker.unlock();
	}
}
int main()
{
	thread t1(fun1);
	thread t2(fun2);
	t1.join();
	t2.join();

	system("pause");
	return 0;
}






