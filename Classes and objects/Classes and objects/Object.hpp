#pragma once
#include<iostream>
using namespace std;

//第一个
class Date1
{
public:
	Date1(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	//拷贝构造函数
	Date1(const Date1& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

void Testfunc1()
{
	Date1 d1(2023, 8, 13);
	//使用d1对象拷贝构造d2对象
	Date1 d2(d1);
}

//第二个
class Time1
{
public:
	Time1()
	{
		_hour = 1;
		_minute = 1;
		_second = 1;
	}
	Time1(const Time1& t)
	{
		_hour = t._hour;
		_minute = t._minute;
		_second = t._second;
	}
private:
	int _hour;
	int _minute;
	int _second;
};
class Date2
{
private:
	int _year = 1900;
	int _month = 1;
	int _day = 1;

	Time1 _t;
};

void Testfunc2()
{
	Date2 d1;
	Date2 d2(d1);
}

//第三个
typedef int DataType;
class Stack1
{
public:
	Stack1(size_t capacity = 10)
	{
		_array = (DataType*)malloc(sizeof(DataType) * capacity);
		if (_array == nullptr)
		{
			perror("malloc申请空间失败");
			return;
		}
		_size = 0;
		_capacity = capacity;
	}
	void Push(const DataType& data)
	{
		_array[_size] = data;
		_size++;
	}
	~Stack1()
	{
		if (_array)
		{
			free(_array);
			_array = nullptr;
			_size = 0;
			_capacity = 0;
		}
	}
private:
	DataType* _array;
	size_t _size;
	size_t _capacity;
};

void Testfunc3()
{
	Stack1 s1;
	s1.Push(1);
	s1.Push(2);
	s1.Push(3);
	s1.Push(4);

	//用s1拷贝构造一份s2
	Stack1 s2(s1);
}

//第四个
class Date3
{
public:
	Date3(int year, int month, int day)
	{
		cout << "Date3(int,int,int)" << this << endl;
	}
	Date3(const Date3& data)
	{
		cout << "Date3(const Date& data)" << this << endl;
	}
	~Date3()
	{
		cout << "~Date3()" << this << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

Date3 Test(Date3 d)
{
	Date3 temp(d);
	return temp;
}

void Testfunc4()
{
	Date3 d1(2023, 8, 13);
	Test(d1);
}

//第五个
class Date4
{
public:
	Date4(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	//类成员变量必须公有化
	int _year;
	int _month;
	int _day;
};

bool operator==(const Date4& d1, const Date4& d2)
{
	return d1._year == d2._year && d1._month == d2._month && d1._day == d2._day;
}

void Testfunc5()
{
	Date4 d1(2023, 8, 13);
	Date4 d2(2023, 8, 14);
	cout << (d1 == d2) << endl;
}

//第六个
class Date5
{
public:
	Date5(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}

	// 这里需要注意的是，左操作数是this，指向调用函数的对象
	// 即：bool operator==(Date* this, const Date& d)
	bool operator==(const Date5& d)
	{
		return _year == d._year && _month == d._month && _day == d._day;
	}
private:
	int _year;
	int _month;
	int _day;
};

class Date6
{
public:
	//构造函数
	Date6(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	//拷贝构造函数
	Date6(const Date6& d)
	{
		_year = d._year;
		_month = d._month;
		_day = d._day;
	}
	//赋值运算符重载函数
	Date6& operator=(const Date6& d)
	{
		//检测是否是自己给自己赋值
		if (this != &d)
		{
			_year = d._year;
			_month = d._month;
			_day = d._day;
		}
		return *this;
	}
	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

void Testfunc6()
{
	Date6 d1(2023, 8, 13);
	cout << "d1=";
	d1.Print();
	Date6 d2 = d1;
	cout << "d2=";
	d2.Print();
}


class Date7
{
public:
	//构造函数
	Date7(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
private:
	int _year;
	int _month;
	int _day;
};
void Testfunc7()
{
	Date7 d1(2023, 8, 13);
	Date7 d2 = d1;
}


class Time
{
public:
	Time()
	{
		_hour = 1;
		_minute = 1;
		_second = 1;
	}
	Time& operator=(const Time& t)
	{
		if (this != &t)
		{
			_hour = t._hour;
			_minute = t._minute;
			_second = t._second;
		}
		return *this;
	}
private:
	int _hour;
	int _minute;
	int _second;
};
class Date8
{
public:
	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
private:
	// 基本类型(内置类型)
	int _year = 1900;
	int _month = 1;
	int _day = 1;
	// 自定义类型
	Time _t;
};

void Testfunc8()
{
	Date8 d1;
	cout << "d1=";
	d1.Print();
	Date8 d2;
	d2 = d1;
	cout << "d2=";
	d2.Print();
}

typedef int DataType;
class Stack
{
public:
	Stack(size_t capacity = 10)
	{
		_array = (DataType*)malloc(sizeof(DataType) * capacity);
		if (_array == nullptr)
		{
			perror("malloc申请空间失败");
			return;
		}
		_size = 0;
		_capacity = capacity;
	}
	void Push(const DataType& data)
	{
		_array[_size] = data;
		_size++;
	}
	~Stack()
	{
		if (_array)
		{
			free(_array);
			_array = nullptr;
			_size = 0;
			_capacity = 0;
		}
	}
private:
	DataType* _array;
	size_t _size;
	size_t _capacity;
};


class Date9
{
public:
	Date9(int year = 1900, int month = 1, int day = 1)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	//前置++：返回+1之后的结果
	//this指针指向的对象函数结束之后并不会销毁，用引用方式进行返回结果提高效率
	Date9& operator++()
	{
		_day += 1;
		return *this;
	}
	//后置++：先使用再进行++操作
	// C++规定：后置++重载时多增加一个int类型的参数，但调用函数时该参数不用传递，编译器自动传递
	// 注意：后置++是先使用后+1，因此需要返回+1之前的旧值，故需在实现时需要先将this保存一份，然后给this + 1
	// 而temp是临时对象，因此只能以值的方式返回，不能返回引用
	Date9 operator++(int)
	{
		Date9 temp(*this);
		_day += 1;
		return temp;
	}
	void Print()
	{
		cout << _year << "/" << _month << "/" << _day << endl;
	}
private:
	int _year;
	int _month;
	int _day;
};

void Testfunc9()
{
	Date9 d;
	Date9 d1(2023, 8, 13);
	d = d1++;
	cout << "d=";
	d.Print();
	cout << "d1=";
	d1.Print();

	cout << endl;
	d = ++d1;
	cout << "d=";
	d.Print();
	cout << "d1=";
	d1.Print();
}

class Date10
{
public:
	Date10(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	void Print()
	{
		cout << "Print()" << endl;
		cout << "year:" << _year << endl;
		cout << "month:" << _month << endl;
		cout << "day:" << _day << endl << endl;
	}
	void Print() const
	{
		cout << "Print()const" << endl;
		cout << "year:" << _year << endl;
		cout << "month:" << _month << endl;
		cout << "day:" << _day << endl;
	}
private:
	int _year; 
	int _month; 
	int _day;
};

void Testfunc10()
{
	Date10 d1(2023, 8, 13);
	d1.Print(); 

	const Date10 d2(2023, 8, 14);
	d2.Print();
}

class Date
{
public:
	Date(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}
	//非const成员函数
	void fun1(){}

	//const成员函数
	void fun2()const {}

	//非const成员函数
	void Show()
	{
		fun1();
		fun2();
		cout << _year << "/" << _month << "/" << _day;
	}

	//const成员函数
	void Print() const
	{
		//fun1(); 
		fun2();
		cout << _year << "/" << _month << "/" << _day;
	}
private:
	int _year;
	int _month;
	int _day;
};
void Testfunc11()
{
	Date d1(2023, 8, 13);
	d1.Print();

	const Date d2(2023, 8, 14);
	d2.Print();
}


