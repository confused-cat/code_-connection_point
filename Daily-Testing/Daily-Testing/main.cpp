/*
#include<iostream>
#include<vector>
#include<algorithm>
using namespace std;

//十进制个位数相加 八进制数字个位数相加 十六进制数字个位数相加 是否相等
bool f(int n)
{
	int sum1 = 0;
	int s1 = n;
	while (s1)
	{
		sum1 += s1 % 10;
		s1 /= 10;
	}
	int sum2 = 0;
	int s2 = n;
	while (s2)
	{
		sum2 += s2 % 8;
		s2 /= 8;
	}
	int sum3 = 0;
	int s3 = n;
	while (s3)
	{
		sum3 += s3 % 16;
		s3 /= 16;
	}
	if (sum1 == sum2 && sum2 == sum3)
	{
		return true;
	}
	return false;
}
vector<int> func(int n, int cn)
{
	vector<int> path;
	while (n++ && n < 9999)
	{
		if (path.size() == cn)
			break;
		if (f(n))
		{
			path.push_back(n);
		}
	}
	return path;
}
int main()
{
	int n, cn;
	vector<int> ret;
	while (cin >> n >> cn)
	{
		ret = func(n, cn);
		for (auto e : ret)
		{
			cout << e << endl;
		}
	}
	return 0;
}
*/

/*
#include<iostream>
#include<stack>
#include<vector>
#include<string>
#include<unordered_map>
#include<algorithm>
using namespace std;

//判断括号字符串是否有效
bool func(const string& s)
{
	int n = s.size();
	if (n % 2 != 0)
	{
		return false;
	}
	stack<char> st;
	unordered_map<char, char> m;
	m.insert(make_pair(')', '('));
	m.insert(make_pair(']', '['));
	m.insert(make_pair('}', '{'));

	for (int i = 0; i < n; ++i)
	{
		//当栈为空 或者栈顶元素和当前遍历元素无法匹配就会直接进行入栈操作
		if (s.empty() || st.top()!=m[s[i]])
		{
			st.push(s[i]);
		}
		st.pop();
	}
	return st.empty();
}
int main()
{
	string s;
	while (getline(cin, s))
	{
		if (func(s))
		{
			cout << "TRUE" << endl;
		}
		else
		{
			cout << "FALSE" << endl;
		}
	}
	return 0;
}
*/
