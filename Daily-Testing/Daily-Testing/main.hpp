#include<iostream>
#include<vector>
using namespace std;


void test1()
{
	vector<int> array;
	array.push_back(100);
	array.push_back(300);
	array.push_back(300);
	array.push_back(300);
	array.push_back(300);
	array.push_back(500);

	for (auto it = array.begin(); it != array.end(); ++it)
	{
		if (*it == 300)
		{
			it = array.erase(it);
		}
	}
	for (auto it = array.begin(); it != array.end(); ++it)
	{
		cout << *it << " ";
	}
}

class A
{
public:
	A()
	{
		p();
	}
	virtual void p()
	{
		cout << "A" << endl;
	}
	virtual ~A()
	{
		p();
	}
};
class B :public A
{
public:
	B()
	{
		p();
	}
	void p()
	{
		cout << "B" << endl;
	}
	~B()
	{
		p();
	}
};
void test2()
{
	A* a = new B();
	delete a;
}

class base
{
public:
	base()
	{
		echo();
	}
	virtual void echo()
	{
		cout << "base" << endl;
	}
};
class derived :public base
{
public:
	derived()
	{
		echo();
	}
	virtual void echo()
	{
		cout << "derived" << endl;
	}
};
void test3()
{
	base* b = new derived();
	b->echo();
}
