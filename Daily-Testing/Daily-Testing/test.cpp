#include<iostream>
using namespace std;
#include<vector>

long long func(long long n)
{
	vector<long long> ret(n+1 , 0);
	ret[1] = 1;
	ret[2] = 2;
	ret[3] = 4;
	for (int i = 4; i <= n; ++i)
	{
		ret[i] = ret[i - 1] + ret[i - 2] + ret[i - 3];
	}
	return ret[n];
}
int main()
{
	int n;
	int n2;
	cin >> n;
	//cin >> n2;
	cout << func(n) << endl;
	//cout << func(n2) << endl;
	return 0;
}
