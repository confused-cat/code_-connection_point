#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>

int test1()
{
	char arr1[] = "hello";
	char arr2[] = { 'h','e','l','l','o' };
	char arr3[] = { 'h','e','l','l','o','\0' };

	printf("%d\n", sizeof(arr1));//6
	printf("%d\n", sizeof(arr2));//5
	printf("%d\n", sizeof(arr3));//6

	printf("%s\n", arr1);//hello
	printf("%s\n", arr2);//hello烫烫烫烫hell 没有字符串结尾标志，所以会存在数据溢出
	printf("%s\n", arr3);//hello
}
int test2()
{
	int a[] = { 1,2,3,4 };
	printf("%d\n", sizeof(a));//16
	printf("%d\n", sizeof(a + 0));//4
	printf("%d\n", sizeof(*a));//4
	printf("%d\n", sizeof(a + 1));//4
	printf("%d\n", sizeof(a[1]));//4
	printf("%d\n", sizeof(&a));//4
	printf("%d\n", sizeof(*&a));//16
	printf("%d\n", sizeof(&a + 1));//4
	printf("%d\n", sizeof(&a[0]));//4
	printf("%d\n", sizeof(&a[0] + 1));//4
}

struct Test
{
	int num;//4
	char* pname;//4
	short date;//2
	char a[2];//2
	short b[4];//4
}*p;
int test3()
{
	int p = 0x100000;
	printf("%p\n", p + 0x1);
	printf("%p\n", (unsigned long)p + 0x1);
	printf("%p\n", (unsigned int*)p + 0x1);

}
int test4()
{
	int a[3][2] = { (1,2),(3,4),(5,6) }; //2.4.6 //
	int* p;
	p = a[0];
	printf("%d", p[0]);
}
void test5()
{
	int a = 1;
	int b = 2;
	int c = (a > b, a = b + 10, a, b = a + 1);//逗号表达式
	printf("%d\n", c);//13
}

///////strlen函数只计算\0之前的字符个数
////计数进行实现
//int strlen1(const char* str)
//{
//	int cn = 0;
//	while (*str)
//	{
//		cn++;
//		str++;
//	}
//	return cn;
//}
////递归进行实现
//int strlen2(const char* str)
//{
//	if (*str == '\0')
//		return 0;
//	else
//		return strlen2(str + 1) + 1;
//}
////指针-指针的方式
//int strlen3(const char* str)
//{
//	char* p = str;
//	while (*p != '\0')
//	{
//		p++;
//	}
//	return p - str;
//}
//
////将src中的内容复制到dest内容中
//char* strcpy(char* dest, const char* src)
//{
//	assert(src!=NULL);
//	assert(dest!=NULL);
//	char* ret = dest;
//	while (*src)
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	return ret;
//}
////将src中的内容追加到dest后内容
//char* strcat(char* dest, const char* src)
//{
//	assert(dest!=NULL);
//	assert(src!=NULL);
//	char* ret = dest;
//	//先找到dest的末尾位置
//	while (*dest)
//	{
//		dest++;
//	}
//	while (*src)
//	{
//		*dest = *src;
//		dest++;
//		src++;
//	}
//	return ret;
//}
////比较两个字符串的大小
//int strcmp(const char* str1, const char* str2)
//{
//	assert(str1 != NULL);
//	assert(str2 != NULL);
//	//找到两字符串第一个不一样的字符
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//		{
//			//当其中一个字符为结束标志的时候，就表示两字符串一样
//			return 0;
//		}
//		str1++;
//		str2++;
//	}
//	if (*str1 > *str2)
//	{
//		return 1;
//	}
//	else
//		return -1;
//}

//复制n个字符拷贝复制函数
//char* strncpy(char* dest, const char* src,size_t num)
//{
//	char* ret = dest;
//	assert(dest != NULL);
//	assert(src != NULL);
//	int n = strlen(src);
//	if (num >= n)
//		strcpy(dest, src);
//	while (num && *src)
//	{
//		*dest = *src;
//		num--;
//	}
//	while (num--)
//	{
//		*dest == '\0';
//	}
//	return ret;
//}
////判断str2是否是str1的子串
////返回第一次出现的字符的地址
//const char* strstr(const char* str1, const char* str2)
//{
//	assert(str1);
//	assert(str2);
//	char* ret1 = (char*)str1;
//	char* ret2 = (char*)str2;
//	char* s = NULL;
//	//当要查找的字符串为空时，那么此结果就是NULL
//	if (*str2 == NULL)
//		return NULL;
//	while (*ret1)
//	{
//		s = ret1;
//		ret2 = str2;
//		//找到两者指向相等的地方
//		while (*s && *ret2 && *s == *ret2)
//		{
//			s++;
//			ret2++;
//		}
//		if (*ret2 == '\0')
//			return ret1;
//		ret1++;
//	}
//}
////将含有特殊字符串的字符串进行分割，一次分割函数只能分割一个分隔符，所以想要将分隔符全部分割，就得多次调用strtok函数
//char* strtok(char* str, const char* sep)
//{
//	//静态成员记录每次切割的位置
//	//C语言并没有异常处理机制，得依靠错误码来进行异常情况的处理
//	static char* ret = NULL;
//}
////在src中拷贝复制num字节的数据到dest的内存空间中,不允许两字符串重叠
//void* memcpy(void* dest, const void* src, size_t num)
//{
//	void* temp = dest;
//	assert(dest != NULL);
//	assert(src != NULL);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		(char*)dest = (char*)dest + 1;
//		(char*)src = (char*)src + 1;
//	}
//	return temp;
//}
////在src中拷贝复制num字节的数到dest中去，允许两字符串空间重叠
//void* memmove(void* dest, const char* src, size_t num)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	void* temp = dest;
//	//当dest小于src 或者 dest大于等于src+num的值时，直接从前往后遍历src进行字符的拷贝复制到dest中
//	if (dest <= src || (char*)dest >= (char*)src + num)
//	{
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			(char*)dest = (char*)dest + 1;
//			(char*)src = (char*)src + 1;
//		}
//	}
//	//其它情况：dest大于src，但是间隔距离小于num
//	//从后往前进行拷贝复制
//	else
//	{
//		dest = (char*)dest + num - 1;
//		src = (char*)src + num - 1;
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			(char*)dest = (char*)dest - 1;
//			(char*)src = (char*)src - 1;
//		}
//	}
//	return temp;
//}
////将指针指向的内存块前num个字节数设置为指定值并返回该指针
//void* memset(void* ptr,int value, size_t num)
//{
//	assert(ptr != NULL);
//	void* temp = ptr;
//	while (num--)
//	{
//		*(char*)ptr = value;
//		ptr=(char*)ptr + 1;
//	}
//	return temp;
//}

int fun(int n)
{
	int sum = 0;
	while (n)
	{
		int i = n % 10;
		for (int i = 0; i < 3; ++i)
		{
			i *= i;
		}
		n / 10;
	}
	if (n == sum)
	{
		return 1;
	}
	return 0;
}
void test6()
{
	for (int i = 100; i < 1000; ++i)
	{
		if (fun(i) == 1)
		{
			printf("%d ", i);
		}
	}
}

