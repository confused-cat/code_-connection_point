#include<iostream>
#include<vector>
using namespace std;

int func(int m, int k)
{
    int num = m;
    int sum = 0;
    while (m)
    {
        sum += m / k + m % k;
        num += m / k;
        m = sum;
        if (sum > k)
        {
            continue;
        }
    }
    return num;
}
int main()
{
    int m, k;
    while (cin >> m >> k)
    {
        cout << func(m, k) << endl;
    }
    return 0;
}
