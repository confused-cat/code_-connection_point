#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int main()
{
	int n1;
	int n2;
	scanf("%d %d\n", &n1, &n2);
	int ret = n1 > n2 ? n1 : n2;
	peintf("%d\n", ret);
	return 0;
}