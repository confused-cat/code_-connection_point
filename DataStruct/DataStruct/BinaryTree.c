#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<assert.h>

#include"BinaryTree.h"
#include"Queue.h" //二叉树的层序遍历

//创建一个值域为data的节点
BTNode* CreateNewNode(DataType data)
{
	BTNode* newNode = (BTNode*)malloc(sizeof(BTNode));
	if (newNode == NULL)
	{
		assert(0);
		return NULL;
	}
	newNode->left = NULL;
	newNode->right = NULL;
	newNode->data = data;
	return newNode;
}


//array：用户要创建的二叉树数据集合
//size：数据个数
//pindex：指向外部用来表示array下标的index
//invalid：表示array数组中用来区分节点是否存在子树的标记
BTNode* _CreateBinaryTree(DataType array[], int size, int* pindex,DataType invalid)
{

	//二叉树：空树：直接返回NULL
	//        非空树：创建根节点+递归创建左子树+递归创建右子树
	BTNode* root=NULL;
	//二叉树存在，则创建
	if (*pindex < size && array[*pindex] != invalid)
	{
		//创建根节点
		root = CreateNewNode(array[*pindex]);
		//递归创建左子树
		++(*pindex);
		root->left = _CreateBinaryTree(array, size, pindex, invalid);
		//递归创建右子树
		++(*pindex);
		root->right = _CreateBinaryTree(array, size, pindex, invalid);
	}
	return root;
}

//1.创建二叉树
BTNode* CreateBinaryTree(DataType array[], int size, DataType invalid)
{
	int index = 0;
	return _CreateBinaryTree(array, size, &index, invalid);
}
//二叉树的遍历：
//前序遍历：根左右
//中序遍历：左根右
//后序遍历：左右根

//前序遍历：根左右
void PreOrder(BTNode* root)
{
	//1.树为空树
	if (root == NULL)
		return;
	//2.树非空：先输出根节点的值，递归遍历左子树，递归遍历右子树
	printf("%d ", root->data);
	PreOrder(root->left);
	PreOrder(root->right);
}

//中序遍历：左根右
void InOrder(BTNode* root)
{
	//1.树为空树
	if (root == NULL)
		return;
	//2.树非空：先输出根节点的值，递归遍历左子树，递归遍历右子树
	InOrder(root->left);
	printf("%d ", root->data);
	InOrder(root->right);
}

//后序遍历：左右根
void PostOrder(BTNode* root)
{
	//1.树为空树
	if (root == NULL)
		return;
	//2.树非空：先输出根节点的值，递归遍历左子树，递归遍历右子树
	PostOrder(root->left);
	PostOrder(root->right);
	printf("%d ", root->data);
}

//2.二叉树节点个数
int BinaryTreeSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	//根节点1+左子树节点个数+右子树节点个数
	return 1 + BinaryTreeSize(root->left) + BinaryTreeSize(root->right);
}

//3.二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	if (root == NULL)
		return 0;
	//当一个节点左右孩子都为NULL时
	if (root->left == NULL && root ->right == NULL)
		return 1;
	return BinaryTreeSize(root->left) + BinaryTreeSize(root->right);
}

//4.二叉树高度
int BinaryTreeHeight(BTNode* root)
{
	if (root == NULL)
		return 0;
	//求root子树的高度
	int leftHeight = BinaryTreeHeight(root->left);
	int rightHeight = BinaryTreeHeight(root->right);

	return leftHeight > rightHeight ?
		leftHeight + 1 : rightHeight + 1;
}


//5.二叉树第K层节点个数
int BinaryTreeLevelKSize(BTNode* root,int k)
{
	//树为空或是k不大于0
	if (root == NULL || k <= 0)
	{
		return 0;
	}
	//k=1 就是第一层的节点个数
	if (k == 1)
		return 1;
	//对于root来言是第k层，对于root的左右子树来言是k-1层
	//欲求root第k层的节点个数<=>求root左子树和root右子树的第k-1层的节点个数
	return BinaryTreeLevelKSize(root->left, k - 1) + BinaryTreeLevelKSize(root->right, k - 1);
}


//6.二叉树查找值为data的节点
BTNode* BinaryTreeFind(BTNode* root, DataType data)
{
	BTNode* cur = NULL;
	//树空
	if (root == NULL)
	{
		return NULL;
	}
	if (root->data == data)
		return root;
	//若根节点不是要找的节点，那么就递归到根节点的左子树和右子树中去找
	//先到左子树中去找节点
	cur = BinaryTreeFind(root->left, data);
	if (cur != NULL)
		return cur;
	//表示在左子树中未找到该节点，则到右子树中去找
	cur = BinaryTreeFind(root->right, data);
	return cur;
	
	/*
	cur=BinaryTreeFind(root->left,data) || BinaryTreeFind(root->right,data);
	return cur;
	*/
}

//7.二叉树层序遍历
//利用队列的先进先出
void BinaryTreeLevelOrder(BTNode* root)
{
	Queue q;
	//空树
	if (root == NULL)
		return;
	//树非空
	//借助队列进行层序遍历
	QueueInit(&q);
	QueuePush(&q, root);
	
	//将树的根节点入队列，当队列不为空，循环进行下一操作
	while (!QueueEmpty(&q))
	{
		//a.从对头去一个元素，QueueFront查找只是将对头元素获取到，并没有将对头元素删除
		BTNode* cur = QueueFront(&q);

		//b.将该节点进行遍历
		printf("%d ", cur->data);

		//c.如果cur有左孩子则入队列，有右孩子则入队列
		if (cur->left)
			QueuePush(&q, cur->left);
		if (cur->right)
			QueuePush(&q, cur->right);
		
		//遍历了元素之后，将元素出队列
		QueuePop(&q);
	}
	BinaryTreeDestroy(&q);
	printf("\n");
}

//8.判断二叉树是否为完全二叉树
int BinaryTreeComplete(BTNode* root)
{
	Queue q;
	int flag = 0;//标记第一个不饱和的节点
	int ComplateTree = 1;//函数返回值，是完全二叉树的话 返回1，不是返回0
	
	//空树（是完全二叉树）
	if (root == NULL)
		return 1;
	//树非空
	QueueInit(&q);
	QueuePush(&q, root);
	while (!QueueEmpty(&q))
	{
		BTNode* cur = QueueFront(&q);
		QueuePop(&q);

		//即此节点为不饱和节点
		if (flag)
		{
			//不饱和节点之后的所有节点都不能有孩子
			//故若其左孩子存在 或是 右孩子存在都不是完全二叉树
			if (cur->left || cur->right)
			{
				ComplateTree = 0;
				break;
			}
		}
		else
		{
			//检测cur是否为饱和的节点：即是否左右孩子节点都存在
			if (cur->left && cur->right)
			{
				QueuePush(&q, cur->left);
				QueuePush(&q, cur->right);
			}
			else if (cur->left)
			{
				//只有左孩子没有右孩子
				//按照层序遍历方式找到第一个不饱和的节点
				QueuePush(&q, cur->left);
				flag = 1;
			}
			else if (cur->right)
			{
				//只有右孩子，则一定不是完全二叉树
				ComplateTree = 0;
				break;
			}
			else
			{
				//左右孩子都不存在
				//则该节点为第一个不饱和的节点
				flag = 1;
			}
		}
	}
	QueueDestroy(&q);
	return ComplateTree;
}

//9.二叉树销毁
void BinaryTreeDestroy(BTNode** proot)
{
	assert(proot);
	//空树
	if (*proot == NULL)
		return;
	//非空：根节点+左子树+右子树
	BinaryTreeDestroy(&((*proot)->left));
	BinaryTreeDestroy(&((*proot)->right));

	free(*proot);
	*proot = NULL;
}

void TestBinaryTree()
{
	int array[] = { 1, 2, 3, -1, -1, -1, 4, 5, -1, -1, 6 };
	BTNode* root = CreateBinaryTree(array, sizeof(array) / sizeof(array[0]), -1);

	//前序遍历：
	printf("前序遍历：");
	PreOrder(root);
	printf("\n");
	//中序遍历：
	printf("中序遍历：");
	InOrder(root);
	printf("\n");
	//后序遍历：
	printf("后序遍历：");
	PostOrder(root);
	printf("\n");
	//层序遍历：
	printf("层序遍历：");
	BinaryTreeLevelOrder(root);
	printf("\n");

	printf("节点个数：%d\n", BinaryTreeSize(root));
	printf("第3层节点个数：%d\n", BinaryTreeLevelKSize(root,3));
	printf("二叉树高度：%d\n", BinaryTreeHeight(root));

	if (BinaryTreeFind(root, 3))
	{
		printf("3 is in the binary tree.");
	}
	else
	{
		printf("3 is not in the binary tree.");
	}
	printf("\n");
	if (BinaryTreeFind(root, 7))
	{
		printf("7 is in the binary tree.");
	}
	else
	{
		printf("7 is not in the binary tree.");
	}
	printf("\n");
	int ret = BinaryTreeComplete(root);
	if (ret)
	{
		printf("此树是完全二叉树！");
	}
	else
	{
		printf("此树不是完全二叉树！");
	}
	printf("\n");
}
