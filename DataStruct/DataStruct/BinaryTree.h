#pragma once

//孩子表示法，值域+左孩子+右孩子

typedef int DataType;

//节点定义（孩子表示法）
typedef struct BTNode
{
	DataType data;
	struct BTNode* left;
	struct BTNode* right;
}BTNode;

//1.创建二叉树
BTNode* CreateBinaryTree(DataType array[], int size, DataType invalid);
//二叉树的遍历：
//前序遍历：根左右
//中序遍历：左根右
//后序遍历：左右根

void PreOrder(BTNode* root);

void InOrder(BTNode* root);

void PostOrder(BTNode* root);

//2.二叉树节点个数
int BinaryTreeSize(BTNode* root);

//3.二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root);

//4.二叉树高度
int BinaryTreeHeight(BTNode* root);


//5.二叉树第K层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k);


//6.二叉树查找值为data的节点
BTNode* BinaryTreeFind(BTNode* root, DataType data);

//7.二叉树层序遍历
void BinaryTreeLevelOrder(BTNode* root);

//8.判断二叉树是否为完全二叉树
int BinaryTreeComplete(BTNode* root);

//9.二叉树销毁
void BinaryTreeDestroy(BTNode** proot);

void TestBinaryTree();
