#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include<assert.h>

#include"DList.h"

//动态建立新节点
DListNode* CreateDListNode(DataType data)
{
	DListNode* newNode = (DListNode*)malloc(sizeof(DListNode));
	if (newNode == NULL)
	{
		assert(0);
		return NULL;
	}
	newNode->data = data;
	newNode->prev = NULL;
	newNode->next = NULL;
	return newNode;
}

//1.带头双向循环链表初始化
void DListInit(DListNode** head)
{
	assert(head);
	//当只有头节点时,头节点prev和next指针都指向它自身
	*head = CreateDListNode(0);
	(*head)->next = *head;
	(*head)->prev = *head;
}

//2.带头双向循环链表尾插
void DListPushBack(DListNode* head, DataType data)
{
	//尾插：即在head之前插入新节点
	DListInsert(head, data);
}

//3.带头双向循环链表尾删
void DListPopBack(DListNode* head)
{
	//尾删：即删除节点head->prev
	if (DListEmpty(head))
		return;
	DListErase(head->prev);
}

//4.带头双向循环链表头插
void DListPushFront(DListNode* head, DataType data)
{
	//头插:即在节点head->next之前进行新节点的插入
	DListInsert(head->next, data);
}

//5.带头双向循环链表头删
void DListPopFront(DListNode* head)
{
	if (DListEmpty(head))
		return;
	//头删：即将节点head->next删除
	DListErase(head->next);
}

//6.带头双向循环链表查找元素
DListNode* DListFind(DListNode* head, DataType data)
{
	assert(head);
	DListNode* cur = head->next;
	while (cur != head)
	{
		if (cur->data == data)
			return cur;
		cur = cur->next;
	}
	return NULL;
}

//7.带头双向循环链表插入元素
void DListInsert(DListNode* pos, DataType data)
{
	if (pos == NULL)
		return;
	//1.创建值域为data的节点
	struct DListNode* newNode = CreateDListNode(data);

	//2.在pos前插入新节点
	//a.先将新节点连接到pos节点上
	newNode->next = pos;
	newNode->prev = pos->prev;

	//b.再断开原连接
	newNode->prev->next = newNode;
	pos->prev = newNode;
}

//8.带头双向循环链表删除元素
void DListErase(DListNode* pos)
{
	if (pos == NULL)
		return;
	//删除pos节点
	pos->prev->next = pos->next;
	pos->next->prev = pos->prev;
	
	free(pos);
}

//9.带头双向循环链表有效元素个数
int DListSize(DListNode* head)
{
	DListNode* cur = head->next;
	int size = 0;
	while(cur != head)
	{
		size++;
		cur = cur->next;
	}
	return size;
}

//10.带头双向循环链表判空
int DListEmpty(DListNode* head)
{
	assert(head);
	return head->next == head;
}

//11.带头双向循环链表清空所有有效节点
void DListClear(DListNode* head)
{
	DListNode* cur = head->next;
	while (cur!=head)
	{
		//头删进行处理
		head->next = cur->next;
		free(cur);
		cur = head->next;
	}
	//所有元素清除完以后
	head->prev = NULL;
	head->next = NULL;
}

//12.带头双向循环链表销毁
void DListDestroy(DListNode** head)
{
	assert(head);
	DListClear(*head);
	free(*head);
	*head = NULL;
}

//#############################
void PrintDList(DListNode* head)
{
	DListNode* cur = head->next;
	printf("链表为：");
	while (cur != head)
	{
		printf("%d ", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

void TestDList()
{
	DListNode* head = NULL;
	DListInit(&head);

	//尾插1 2 3 4
	DListPushBack(head, 1);
	DListPushBack(head, 2);
	DListPushBack(head, 3);
	DListPushBack(head, 4);
	printf("size=%d\n", DListSize(head));
	PrintDList(head);//1 2 3 4 

	//头插：5 6 
	DListPushFront(head, 5);
	DListPushFront(head, 6);
	printf("size=%d\n", DListSize(head));
	PrintDList(head);//6 5 1 2 3 4

	//尾删
	DListPopBack(head);
	PrintDList(head);//6 5 1 2 3

	//头删
	DListPopFront(head);
	PrintDList(head);//5 1 2 3

	DListInsert(DListFind(head, 3), 7);
	PrintDList(head);//5 1 2 7 3

	DListErase(DListFind(head, 3));
	PrintDList(head); //5 1 2 7
}