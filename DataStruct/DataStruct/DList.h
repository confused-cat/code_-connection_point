#pragma once

//定义并实现：带头结点的双向循环链表（带头+双向+循环）
typedef int DataType;

//节点定义
typedef struct DListNode
{
	struct DListNode* prev;//指向前一个节点
	struct DListNode* next;//指向后一个节点
	DataType data;  //值域
}DListNode;

//1.带头双向循环链表初始化
void DListInit(DListNode** head);

//2.带头双向循环链表尾插
void DListPushBack(DListNode* head, DataType data);

//3.带头双向循环链表尾删
void DListPopBack(DListNode* head);

//4.带头双向循环链表头插
void DListPushFront(DListNode* head, DataType data);

//5.带头双向循环链表头删
void DListPopFront(DListNode* head);

//6.带头双向循环链表查找元素
DListNode* DListFind(DListNode* head, DataType data);

//7.带头双向循环链表插入元素
void DListInsert(DListNode* pos, DataType data);

//8.带头双向循环链表删除元素
void DListErase(DListNode* pos);

//9.带头双向循环链表有效元素个数
int DListSize(DListNode* head);

//10.带头双向循环链表判空
int DListEmpty(DListNode* head);

//11.带头双向循环链表清空所有有效节点
void DListClear(DListNode* head);

//12.带头双向循环链表销毁
void DListDestroy(DListNode** head);

void TestDList();