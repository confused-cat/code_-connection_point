#include<stdio.h>
#include<assert.h>
#include<malloc.h>
#include<stdlib.h>
#include<string.h>

#include"Heap.h"


int Less(int left,int right)
{
	return left < right;
}
int Greater(int left, int right)
{
	return left>right;
}
void Swap(DataType* left, DataType* right)
{
	int temp = *left;
	*left = *right;
	*right = temp;
}

//调整某一节点所在的树，向下进行调整，直到走到叶子节点处
//堆的向下调整，一般实在构建堆的时候，随意建立的堆，得调整堆，使其满足堆的性质
//堆的根节点，和父节点的下标
//从根开始往下进行调整
//时间复杂度：完全二叉树的高度（logN）
void AdjustDown(Heap* hp, int parent) 
{
	int size = hp->size;
	int* array = hp->array;

	int child = parent * 2 + 1;
	//child默认标记左孩子，因为堆为完全二叉树，有右孩子一定有左孩子，即先有左孩子才有右孩子
	while (child < size)
	{
		//1.
		//当右孩子存在，用数据比较函数，找出左右孩子中较大或较小的那个节点
		//小堆：找出较小的那个节点，和父节点进行比较，若小于父节点，则将较小的那个和父节点进行交换
		//大堆：找出较大的那个节点，和父节点进行比较，若大于父节点，则将较大的那个和父节点进行交换
		if (child + 1 < size && hp->Compare(array[child + 1], array[child]))
			child += 1;
		//2.检测parent是否满足堆的性质
		if (hp->Compare(array[child], array[parent]))
		{
			Swap(&array[child], &array[parent]);
			//元素的调整可能导致子树不满足堆的相关特性，故需要继续往下进行检测
			parent = child;
			child = parent * 2 + 1;
		}
		else
			return;
	}
}

//从某一节点开始向上进行调整，直到根节点位置处
//向上调整法，将一个元素插入到堆中时，默认插入到空间的末尾，然而可能会不满足堆的性质
//从倒数第一个非叶子节点开始向上进行元素的调整
void HeapAdjustUp(Heap* hp, int child)
{
	int* array = hp->array;
	int parent = (child - 1) / 2;

	while (child)
	{
		if (hp->Compare(array[child], array[parent]))
		{
			Swap(&array[child], &array[parent]);
			//元素的调整可能导致子树不满足堆的相关特性，故需要继续往下进行检测
			child = parent;
			int parent = (child - 1) / 2;
		}
		else
			return;
	}
}

//空间的检测和扩容
void heap_CheckCapacity(Heap* hp)
{
	assert(hp);
	if (hp->capacity == hp->size)
	{
		//进行空间的扩容
		//1.申请新空间
		int newcapacity = hp->capacity * 2;
		DataType* temp = (DataType*)malloc(sizeof(DataType)*newcapacity);
		if (temp==NULL)
		{
			assert(0);
			return;
		}

		//2.将旧空间元素拷贝到旧空间中
		memcpy(temp, hp->array, (hp->size)*sizeof(DataType));

		//3.释放旧空间，使用新空间
		free(hp->array);
		hp->array = temp;
		hp->capacity = newcapacity;
	}
}
//1.构建堆
//hp:堆的地址 a:元素空间地址  n:元素个数  Compare:元素的比较方式
void HeapCreate(Heap* hp, DataType* a, int n, COMP Compare) 
{
	hp->array = (DataType*)malloc(sizeof(DataType)*n);
	if (hp->array == NULL)
	{
		assert(0);
		return;
	}
	hp->capacity = n;

	//将数组a内存中的元素拷贝到hp->array中去，拷贝字节数为sizeof(DataType)*n
	memcpy(hp->array, a, sizeof(DataType)*n);

	hp->size = n;
	hp->Compare = Compare;

	//创建堆后，需从倒数第一个非叶子节点的位置开始进行调整
	for (int root = (n - 2) / 2; root >= 0; --root)
	{
		AdjustDown(hp, root);
	}
}

//2.销毁堆
void HeapDestroy(Heap* hp)
{
	assert(hp);
	free(hp->array);
	hp->array = NULL;
	hp->capacity = 0;
	hp->size = 0;
}


//3.堆的插入
void HeapPush(Heap* hp, DataType data)
{
	//在插入元素之前，检测空间是否需要扩容
	heap_CheckCapacity(hp);
	
	//1.元素插入
	hp->array[hp->size] = data;
	hp->size++;

	//2.对元素进行向上调整
	HeapAdjustUp(hp, hp->size - 1);
}

//4.堆的删除
void HeapPop(Heap* hp)
{
	if (HeapEmpty(hp))
		return;
	//1.堆不为空。将堆顶元素和最后一个元素进行交换
	Swap(&hp->array[hp->size - 1], &hp->array[0]);

	//2.将堆中有效元素-1
	hp->size--;

	//3.将堆顶元素进行往下调整
	AdjustDown(hp, 0);
}

//5.堆顶元素的获取
DataType HeapTop(Heap* hp)
{
	assert(hp);
	return hp->array[0];
}

//6.堆中数据的个数
int HeapSize(Heap* hp)
{
	assert(hp);
	return hp->size;
}

//7.堆的判空
int HeapEmpty(Heap* hp)
{
	assert(hp);
	return hp->size == 0;
}
///////////////////////////////////////////////
void PrintfHeapSort(int array[], int size)
{
	printf("数组为：\n");
	for (int i = 0; i < size; ++i)
	{
		printf("%d ", array[i]);
	}
	printf("\n");
}

void TestHeap()
{
	Heap hp;
	int array[] = { 27,15,19, 18, 28, 34, 65, 49, 25, 37 };
	//创建堆
	HeapCreate(&hp, array, sizeof(array) / sizeof(array[0]), Greater);
	printf("有效元素个数：size=%d\n", HeapSize(&hp));
	printf("堆顶元素为：%d\n", HeapTop(&hp));

	HeapPop(&hp);
	HeapPop(&hp);
	printf("有效元素个数：size=%d\n", HeapSize(&hp));
	printf("堆顶元素为：%d\n", HeapTop(&hp));

	HeapDestroy(&hp);

	int arr[] = { 49, 65, 27, 34, 19, 28, 18, 15, 25, 37 };
	HeapSort(arr, sizeof(arr) / sizeof(arr[0]));
	PrintfHeapSort(arr, sizeof(arr) / sizeof(arr[0]));

}

////堆排序
//升序：建大堆，堆排序是在删除的思想上进行排序的
//堆删除：将堆顶元素和最后一个元素进行交换，有效元素减1,调整堆，而堆顶元素为堆中最大的元素
//        依次进行，为一升序的序列
//降序：建小堆

//以建立大堆规则进行堆的调整
void AdjustHeap_to_BigHeap(int array[], int size, int parent)
{
	int child = parent * 2 + 1;
	while (child < size)
	{
		//在左右孩子里面找最大的那个元素
		if (child + 1 < size && array[child + 1] > array[child])
		{
			child += 1;
		}
		//如果孩子节点比父节点大的话，将两者元素进行交换
		if (array[parent] < array[child])
		{
			Swap(&array[child], &array[parent]);

			parent = child;
			child = parent * 2 + 1;
		}
		else
			return;
	}
}
//以建立小堆规则进行堆的调整
void AdjustHeap_to_SmallHeap(int array[], int size, int parent)
{
	int child = parent * 2 + 1;
	while (child < size)
	{
		//在左右孩子里面找最大的那个元素
		if (child + 1 < size && array[child + 1] < array[child])
		{
			child += 1;
		}
		//如果孩子节点比父节点大的话，将两者元素进行交换
		if (array[parent] > array[child])
		{
			Swap(&array[child], &array[parent]);

			parent = child;
			child = parent * 2 + 1;
		}
		else
			return;
	}
}

void HeapSort(int array[], int size)
{
	
	//1.导数第一个非叶子节点进行调整
	int LastNotLeaf = (size - 2) / 2;
	for (int root = LastNotLeaf; root >= 0;--root)
	{
		AdjustHeap_to_BigHeap(array, size, root);
	}
	//2.堆删除思想对堆进行排序
	int end = size - 1;
	while (end)
	{
		Swap(&array[0], &array[end]);
		AdjustHeap_to_BigHeap(array, end, 0);
		end--;
	}
}



