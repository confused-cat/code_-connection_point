#pragma once

//堆：完全二叉树+限制条件（孩子节点都小于或大于它的父节点）
//堆的元素可以用空间连续的数组来进行实现

typedef int DataType;

typedef int(*COMP)(int left, int right); //函数指针

typedef struct Heap
{
	DataType* array; //存储元素的空间
	int capacity;   //空间大小
	int size;       //空间中有效元素个数
	COMP Compare;//堆中元素的比较方式
}Heap;

//1.构建堆
void HeapCreate(Heap* hp, DataType* a, int n, COMP Compare);

//2.销毁堆
void HeapDestroy(Heap* hp);

//3.堆的插入
void HeapPush(Heap* hp, DataType data);

//4.堆的删除
void HeapPop(Heap* hp);

//5.堆顶元素的获取
DataType HeapTop(Heap* hp);

//6.堆中数据的个数
int HeapSize(Heap* hp);

//7.堆的判空
int HeapEmpty(Heap* hp);

void TestHeap();
void HeapSort(int array[], int size);
