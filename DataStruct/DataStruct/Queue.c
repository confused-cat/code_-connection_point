#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<malloc.h>
#include<string.h>

#include"Queue.h"

//1.初始化队列
void QueueInit(Queue* q)
{
	assert(q);
	q->front = NULL;
	q->rear = NULL;
}

//动态创建一个新节点
QNode* CreateQNode(DataType data)
{
	QNode* newNode = (QNode*)malloc(sizeof(QNode));
	if (newNode == NULL)
	{
		assert(0);
		return NULL;
	}
	newNode->data = data;
	newNode->next = NULL;
	return newNode;
}

//2.入队列
void QueuePush(Queue* q, DataType data)
{
	//直接在队尾进行新节点的插入
	QNode* newNode = CreateQNode(data);

	//1.当队列为空，即链表为空
	if (QueueEmpty(q))
	{
		q->front = newNode;
		q->rear = newNode;
	}
	//2.队列不为空
	else
	{
		q->rear->next = newNode;
		q->rear = newNode;
	}
}

//3.出队列
void QueuePop(Queue* q)
{
	assert(q);
	//1.队列为空，直接返回
	if (QueueEmpty(q))
		return;
	//2.队列非空，进行元素出队列
	//a.队列中只有一个节点
	else if (q->front == q->rear)
	{
		free(q->front);
		q->front = NULL;
		q->rear = NULL;
	}
	//b.队列中不止一个节点
	else
	{
		QNode* delNode = q->front;
		q->front = delNode->next;
		free(delNode);
	}
}

//4.获取队头元素
DataType QueueFront(Queue* q)
{
	assert(q);
	return q->front->data;
}

//5.获取队尾元素
DataType QueueBack(Queue* q)
{
	assert(q);
	return q->rear->data;
}

//6.获取队列中有效元素个数
int QueueSize(Queue* q)
{
	assert(q);
	QNode* cur = q->front;
	int size = 0;
	while (cur)
	{
		++size;
		cur = cur->next;
	}
	return size;
}

//7.检测队列是否为空
int QueueEmpty(Queue* q)
{
	assert(q);
	return q->front == NULL;
}

//8.销毁队列
void QueueDestroy(Queue* q)
{
	assert(q);
	QNode* cur = q->front;
	while (cur)
	{
		q->front = cur->next;
		free(cur);
		cur = q->front;
	}
	q->front = NULL;
	q->rear = NULL;
}

void TestQueue()
{
	Queue q;
	QueueInit(&q);

	QueuePush(&q, 1);
	QueuePush(&q, 2); 
	QueuePush(&q, 3); 
	QueuePush(&q, 4);
	QueuePush(&q, 5);
	printf("对头元素：%d\n", QueueFront(&q));
	printf("对尾元素：%d\n", QueueBack(&q));
	printf("size=%d\n", QueueSize(&q));

	QueuePop(&q);
	QueuePop(&q);
	printf("对头元素：%d\n", QueueFront(&q));
	printf("对尾元素：%d\n", QueueBack(&q));
	printf("size=%d\n", QueueSize(&q));
}