#pragma once

typedef int DataType;

//队列中节点定义(不带头节点的单链表)
typedef struct QListNode
{
	struct QListNode* next;
	DataType data;
}QNode;
//队列的结构定义
typedef struct Queue
{
	 QNode* front; //对头指针
	 QNode* rear;  //队尾指针
}Queue;

//1.初始化队列
void QueueInit(Queue* q);

//2.入队列
void QueuePush(Queue* q, DataType data);

//3.出队列
void QueuePop(Queue* q);

//4.获取队头元素
DataType QueueFront(Queue* q);

//5.获取队尾元素
DataType QueueBack(Queue* q);

//6.获取队列中有效元素个数
int QueueSize(Queue* q);

//7.检测队列是否为空
int QueueEmpty(Queue* q);

//8.销毁队列
void QueueDestroy(Queue* q);

void TestQueue();