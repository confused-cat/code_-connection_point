#include<stdio.h>
#include<stdlib.h>
#include<malloc.h>
#include<assert.h>

#include"SList.h"

//动态创建新节点
SListNode* CreateSListNode(DataType x)
{
	SListNode* newNode = (SListNode*)malloc(sizeof(SListNode));
	if (newNode == NULL)
	{
		printf("malloc 空间失败！\n");
		exit(-1);
	}
	else
	{
		newNode->data = x;
		newNode->next = NULL;
	}
	return newNode;
}

//1.单链表尾插
//此处传二级指针原因：即传递头指针的地址，而不是头指针本身
//因为此过程为传值调用，故形参为实参的一份临时拷贝，对形参的改变不会影响到实参
void SListPushBack(SListNode** pplist, DataType x)
{
	assert(pplist);
	//创建一个新节点
	SListNode* newNode = CreateSListNode(x);

	//1.单链表为空
	if (*pplist == NULL)
	{
		//新插入节点为链表首节点
		*pplist = newNode;
	}
	//2.链表不为空
	else
	{
		SListNode* cur = *pplist;
		while (cur->next)
		{
			cur = cur->next;
		}
		cur->next = newNode;
	}
}

//2.单链表尾删
void SListPopBack(SListNode** pplist)
{
	assert(pplist);
	SListNode* cur1 = *pplist;
	//1.链表为空,直接返回
	if (*pplist == NULL)
	{
		return;
	}
	//2.链表只有一个节点
	else if (cur1->next== NULL)
	{
		free(*pplist);
		*pplist = NULL;
	}
	else
	{
		SListNode* cur2 = *pplist;
		while (cur2->next->next)
		{
			cur2 = cur2->next;
		}
		free(cur2->next);
		cur2->next = NULL;
	}
}

//3.单链表头插
void SListPushFront(SListNode** pplist, DataType x)
{
	//创建一值域为x的节点，使该节点next指针域指向第一个节点，再将头指针指向插入节点
	assert(pplist);
	//创建一个新节点
	SListNode* newNode = CreateSListNode(x);
	newNode->next = *pplist;
	*pplist = newNode;
}

//4.单链表头删
void SListPopFront(SListNode** pplist)
{
	//1.链表为空
	if (*pplist == NULL)
	{
		return;
	}
	//2.链表不为空，将头指针指向第二个节点，释放第一个节点
	else
	{
		SListNode* cur = *pplist;
		*pplist = cur->next;
		free(cur);
		cur = NULL;
	}
}

//5.单链表查找元素
SListNode* SListFind(SListNode* plist, DataType x)
{
	assert(plist);
	SListNode* cur = plist;
	while (cur)
	{
		if (cur->data == x)
			return cur;
		cur = cur->next;
	}
	return NULL;
}

//6.单链表在pos位置之后进行节点的插入
//此处：只能在pos节点后进行新节点的插入，因为链表为单向非循环的链表，只能往后遍历节点，不能往前遍历节点
void SListInsertAfter(SListNode* pos, DataType x)
{
	assert(pos);
	SListNode* newNode = CreateSListNode(x);
	newNode->next = pos->next;
	pos->next = newNode;
}

//7.单链表删除pos位置之后的值
//此处同理：只能在pos节点后进行节点的删除
void SListEraseAfter(SListNode* pos)
{
	assert(pos);
	SListNode* cur = pos->next;
	if (cur)
	{
		pos->next = cur->next;
		free(cur);
		cur = NULL;
	}
}

//8.单链表有效元素个数
int SListSize(SListNode* plist)
{
	assert(plist);
	SListNode* cur = plist;
	int size = 0;
	while (cur)
	{
		size++;
		cur = cur->next;
	}
	return size;
}

//9.单链表的销毁
void SListDestroy(SListNode* plist)
{
	assert(plist);
	SListNode* cur = plist;
	while (cur)
	{
		SListNode* p = cur->next;//将下一个节点进行保存下来
		free(cur);
		cur = p;;
	}
	plist = NULL;
}

//打印链表
void PrintSList(SListNode* plist)
{
	SListNode* cur = plist;
	while (cur)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("NULL\n");
}

void TestSList()
{
	SListNode* plist = NULL;

	//尾插1 2 3
	SListPushBack(&plist, 1);
	SListPushBack(&plist, 2);
	SListPushBack(&plist, 3);
	PrintSList(plist);       //1->2->3

	//头插4 5 6
	SListPushFront(&plist, 4);
	SListPushFront(&plist, 5);
	SListPushFront(&plist, 6);
	PrintSList(plist);        //6->5->4->1->2->3

	//尾删一次和头删一次
	SListPopBack(&plist);
	SListPopFront(&plist);
	PrintSList(plist);       //5->4->1->2

	SListNode* pos = SListFind(plist, 4);
	if (pos)
	{
		SListInsertAfter(pos, 3);
	}
	PrintSList(plist);      //5->4->3->1->2

	SListEraseAfter(pos);
	PrintSList(plist);     //5->4->1->2
}