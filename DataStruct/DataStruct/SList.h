#pragma once

//定义并实现：不带头结点的单向非循环链表（无头结点+单向+非循环）
typedef int DataType;

//节点定义
typedef struct SListNode
{
	DataType data; //节点中值域
	struct SListNode* next; //下一个节点的地址
}SListNode;

//1.单链表尾插
void SListPushBack(SListNode** pplist, DataType x);

//2.单链表尾删
void SListPopBack(SListNode** pplist);

//3.单链表头插
void SListPushFront(SListNode** pplist, DataType x);

//4.单链表头删
void SListPopFront(SListNode** pplist);

//5.单链表查找元素
SListNode* SListFind(SListNode* plist, DataType x);

//6.单链表在pos位置之后进行节点的插入
void SListInsertAfter(SListNode* pos, DataType x);

//7.单链表删除pos位置之后的值
void SListEraseAfter(SListNode* pos);

//8.单链表有效元素个数
int SListSize(SListNode* plist);

//9.单链表的销毁
void SListDestroy(SListNode* plist);

void TestSList();