
#include<stdio.h>
#include<malloc.h>
#include<stdlib.h>
#include<assert.h>
#include"SeqList.h"


//1.对顺序表进行初始化
void SeqListInit(SeqList* ps)
{
	assert(ps);
	//默认：将顺序表初始容量置为3
	ps->array = (DataType*)malloc(3 * sizeof(DataType));
	if (ps->array == NULL)
	{
		printf("SeqListInit:malloc failed!");
		exit(0);
	}
	ps->capacity = 3;
	ps->size = 0;
}

//2.销毁顺序表
void SeqListDestroy(SeqList* ps)
{
	assert(ps);
	if (ps->array)
	{
		free(ps->array);
		ps->array = NULL;
		ps->capacity = 0;
		ps->size = 0;
	}
}

//3.检查空间，对空间进行扩容
void CheckCapacity(SeqList* ps)
{
	if (ps->size == ps->capacity)
	{
		//空间不足，需要扩容
		int newCapacity = ps->capacity * 2;

		//1.开辟新空间
		DataType* temp = (DataType*)malloc(newCapacity*sizeof(DataType));
		if (temp == NULL)
		{
			printf("ChackCapacity:malloc failed!");
			exit(0);
		}
		//2.将就空间元素搬移到新空间中去
		for (int i = 0; i < ps->size; ++i)
		{
			temp[i] = ps->array[i];
		}
		//3.释放新空间
		free(ps->array);
		//4.使用新空间，并更新动态顺序表的容量
		ps->array = temp;
		ps->capacity = newCapacity;
	}

	//开辟空间和将旧空间元素搬移到新空间中，可用realloc函数进行一步到位
	//DataType * temp=(DataType*)realloc(ps->array,newCapacity*sizeof(DataType));
}

//4.顺序表尾插
void SeqListPushBack(SeqList* ps, DataType x)
{
	assert(ps);
	//看能否需要扩容
	CheckCapacity(ps);
	ps->array[ps->size] = x;
	ps->size++;
}

//5.顺序表尾删
void SeqListPopBack(SeqList* ps)
{
	if (SeqListEmpty(ps))
	{
		return;
	}
	ps->size--;
}

//6.顺序表查找
size_t SeqListFind(SeqList* ps,DataType x)
{
	assert(ps);
	for (int i = 0; i < ps->size; ++i)
	{
		if (ps->array[i] == x)
		{
			return i;
		}
	}
	return -1;
}

//7.顺序表在pos的位置插入元素
void SeqListInsert(SeqList* ps, int pos, DataType x)
{
	assert(ps);
	if (pos<0 || pos>ps->size)
	{
		printf("SeqListInsert:pos 非法！");
		exit(0);
	}
	CheckCapacity(ps);
	//先将待插入位置后的元素全部往后进行搬移
	for (int i = ps->size - 1; i >= pos; i--)
	{
		ps->array[i + 1] = ps->array[i];
	}
	ps->array[pos] = x;
	ps->size++;
}

//8.顺序表删除pos元素
void SeqListErase(SeqList* ps, int pos)
{
	//1.参数检测
	assert(ps);
	if (pos >= ps->size)
	{
		printf("SeqListErase:pos 非法！");
		exit(0);
	}
	//元素删除，将pos后的元素整体往前进行搬移
	for (int i = pos + 1; i < ps->size; ++i)
	{
		ps->array[i - 1] = ps->array[i];
	}
	ps->size--;
}

//9.获取任意位置的元素
DataType SeqListAt(SeqList* ps, int pos)
{
	assert(ps);
	assert(pos < ps->size);
	return ps->array[pos];
}

//10.获取起始位置的元素
DataType SeqListFront(SeqList* ps)
{
	assert(ps);
	return ps->array[0];
}

//11.获取最后一个元素
DataType SeqListBack(SeqList* ps)
{
	assert(ps);
	return ps->array[ps->size - 1];
}

//12.获取有效元素个数
size_t SeqListSize(SeqList* ps)
{
	assert(ps);
	return ps->size;
}

//13.获取空间容量大小
size_t SeqListCapacity(SeqList* ps)
{
	assert(ps);
	return ps->capacity;
}

//14.检测顺序表是否为空
size_t SeqListEmpty(SeqList* ps)
{
	return ps->size == 0;
}

//############################
void SeqListPrint(SeqList* ps)
{
	assert(ps);
	for (int i = 0; i < ps->size; ++i)
	{
		printf("%d ", ps->array[i]);
	}
	printf("\n");
}

void TestSeqList()
{
	SeqList s;
	SeqListInit(&s);

	SeqListPushBack(&s, 1);
	SeqListPushBack(&s, 2);
	SeqListPushBack(&s, 3);
	SeqListPrint(&s);
	printf("顺序表有效元素个数为：%d\n", SeqListSize(&s));
	printf("顺序表容量为：%d\n", SeqListCapacity(&s));
	printf("顺序表首元素为：%d\n", SeqListFront(&s));
	printf("顺序表末尾元素为：%d\n", SeqListBack(&s));
	printf("顺序表pos位置元素为：%d\n", SeqListAt(&s,2));
	SeqListPrint(&s);
	SeqListDestroy(&s);

}

