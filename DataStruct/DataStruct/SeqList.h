#pragma once

#include<stddef.h>
//定义并实现：动态顺序表

typedef int DataType;
typedef struct SeqList
{
	DataType* array;//指向存储元素的空间，为一数组
	int capacity; //空间容量
	int size;    //有效元素个数
}SeqList;

//1.对顺序表进行初始化
void SeqListInit(SeqList* ps);

//2.销毁顺序表
void SeqListDestroy(SeqList* ps);

//3.检查空间，对空间进行扩容
void CheckCapacity(SeqList* ps);

//4.顺序表尾插
void SeqListPushBack(SeqList* ps, DataType x);

//5.顺序表尾删
void SeqListPopBack(SeqList* ps);

//6.顺序表查找
size_t SeqListFind(SeqList* ps);

//7.顺序表在pos的位置插入元素
void SeqListInsert(SeqList* ps, int pos, DataType x);

//8.顺序表删除pos元素
void SeqListErase(SeqList* ps, int pos);

//9.获取任意位置的元素
DataType SeqListAt(SeqList* ps, int pos);

//10.获取起始位置的元素
DataType SeqListFront(SeqList* ps);

//11.获取最后一个元素
DataType SeqListBack(SeqList* ps);

//12.获取有效元素个数
size_t SeqListSize(SeqList* ps);
//13.获取空间容量大小
size_t SeqListCapacity(SeqList* ps);

//14.检测顺序表是否为空
size_t SeqListEmpty(SeqList* ps);

void TestSeqList();