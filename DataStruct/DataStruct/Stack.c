#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<string.h>
#include<malloc.h>

#include"Stack.h"

//1.初始化栈
void StackInit(Stack* ps)
{
	assert(ps);
	//将初始化空间设置为3个元素大小
	ps->array = malloc(sizeof(DataType)* 3);
	if (ps->array == NULL)
	{
		printf("malloc 空间失败！\n");
		return;
	}
	ps->capacity = 3;
	ps->size = 0;
}

//检测空间是否需要扩容
void ChangeCapacity(Stack* ps)
{
	assert(ps);
	if (ps->capacity == ps->size)
	{
		//空间扩容
		//1.开辟新空间
		int newcapacity = ps->capacity * 2;
		DataType* temp = malloc(sizeof(DataType)*newcapacity);
		if (temp == NULL)
		{
			printf("malloc 空间失败！\n");
			return;
		}

		//2.将旧空间元素拷贝到新空间中去
		memcpy(temp, ps->array, sizeof(DataType)*ps->size);

		//3.释放旧空间
		free(ps->array);

		//4.使用新空间
		ps->array = temp;
		ps->capacity = newcapacity;
	}
}
//2.入栈
void StackPush(Stack* ps,DataType data)
{
	//在元素入栈之前查看是否需要扩容
	ChangeCapacity(ps);
	ps->array[ps->size] = data;
	ps->size++;
}

//3.出栈
void StackPop(Stack* ps)
{
	if (StackEmpty(ps))
		return;
	ps->size--;
}

//4.获取栈顶元素
DataType StackTop(Stack* ps)
{
	assert(ps);
	return ps->array[ps->size - 1];
}

//5.获取栈中有效元素个数
int StackSize(Stack* ps)
{
	assert(ps);
	return ps->size;
}

//6.检测栈是否为空
int StackEmpty(Stack* ps)
{
	assert(ps);
	return ps->size == 0;
}

//7.销毁栈
void StackDestroy(Stack* ps)
{
	assert(ps);
	free(ps->array);
	ps->array = NULL;
	ps->capacity = 0;
	ps->size = 0;
}

void TestStack()
{
	Stack s;
	StackInit(&s);

	StackPush(&s, 1);
	StackPush(&s, 2);
	StackPush(&s, 3);
	StackPush(&s, 4);
	StackPush(&s, 5);
	printf("栈顶元素为：%d\n", StackTop(&s));
	printf("size=%d\n", StackSize(&s));

	StackPop(&s);
	StackPop(&s);
	StackPop(&s);
	printf("栈顶元素为：%d\n", StackTop(&s));
	printf("size=%d\n", StackSize(&s));
}