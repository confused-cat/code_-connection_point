#pragma once

typedef int DataType;

typedef struct Stack
{
	DataType* array;
	int capacity;//栈容量
	int size;//栈中有效元素个数
}Stack;

//1.初始化栈
void StackInit(Stack* ps);

//2.入栈
void StackPush(Stack* ps, DataType data);

//3.出栈
void StackPop(Stack* ps);

//4.获取栈顶元素
DataType StackTop(Stack* ps);

//5.获取栈中有效元素个数
int StackSize(Stack* ps);

//6.检测栈是否为空
int StackEmpty(Stack* ps);

//7.销毁栈
void StackDestroy(Stack* ps);

void TestStack();