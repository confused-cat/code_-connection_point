
#include<stdio.h>
#include<stdlib.h>
#include"SeqList.h"

int main()
{
	//1.动态顺序表
	TestSeqList();

	//2.单向不带头节点的非循环链表
	TestSList();

	//3.带头节点的双向循环链表
	TestDList();

	//4.栈
	TestStack();

	//5.队列
	TestQueue();

	//6.堆
	TestHeap();

	//7.二叉树
	TestBinaryTree();

	system("pause");
	return 0;
}