#pragma once
#include<iostream>
#include<list>
#include<memory>
using namespace std;

//抽象的构建类Transform->变形金刚
class Transform
{
public:
	virtual void move() = 0;
};
//具体的构建类Car
class Car :public Transform
{
public:
	Car()
	{
		cout << "变形金刚是一辆车" << endl;
	}
	void move()
	{
		cout << "在陆地上进行移动" << endl;
	}
};

//抽象装饰类
class Changer :public Transform
{
public:
	Changer(shared_ptr<Transform> trans)
	{
		this->transform = trans;
	}
	void move()
	{
		transform->move();
	}
private:
	shared_ptr<Transform> transform;
};

//具体装饰类Robot
class Robot :public Changer
{
public:
	Robot(shared_ptr<Transform> trans)
		:Changer(trans)
	{
		cout << "变成机器人" << endl;
	}
	void say()
	{
		cout << "然后说话" << endl;
	}
};

//具体装饰类AirPlane
class AirPlane :public Changer
{
public:
	AirPlane(shared_ptr<Transform> trans)
		:Changer(trans)
	{
		cout << "变成飞机" << endl;
	}
	void say()
	{
		cout << "然后在天空飞翔" << endl;
	}
};

void TestDecorators()
{
	shared_ptr<Transform> p1 = make_shared<Car>();
	p1->move();
	cout << "-------------------------" << endl;

	shared_ptr<Robot> p2 = make_shared<Robot>(p1);
	p2->move();
	p2->say();
	cout << "-------------------------" << endl;

	shared_ptr<AirPlane> p3 = make_shared<AirPlane>(p2);
	p3->move();
	p3->say();
}

