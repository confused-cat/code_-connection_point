#pragma once
#include<iostream>
#include<string>
#include<list>
using namespace std;

class Subject;

//观察者基类（内部实例化了被观察的对象sub）
class Observer
{
protected:
	string name;
	Subject* sub;
public:
	Observer(string name, Subject* sub)
	{
		this->name = name;
		this->sub = sub;
	}
	virtual void update() = 0;
};
//观察者派生类
class PlayerObserver :public Observer
{
public:
	PlayerObserver(string name, Subject* sub)
		:Observer(name, sub)
	{}
	void update();
};
//观察者派生类
class NBAObserver :public Observer
{
public:
	NBAObserver(string name, Subject* sub)
		:Observer(name, sub)
	{}
	void update();
};

//被观察者基类（内部存放了所有的观察者对象，以便状态发生变化时，给观察者发送通知）
class Subject
{
protected:
	list<Observer*> observers;
public:
	string action; //被观察者的状态
public:
	virtual void attach(Observer*) = 0;
	virtual void detach(Observer*) = 0;
	virtual void notify() = 0;
};
class Secretary :public Subject
{
public:
	void attach(Observer* obe)
	{
		observers.push_back(obe);
	}
	void detach(Observer* obe)
	{
		auto it = observers.begin();
		while (it != observers.end())
		{
			if ((*it) == obe)
			{
				observers.erase(it);
				return;
			}
			++it;
		}
	}
	void notify()
	{
		auto it = observers.begin();
		while (it != observers.end())
		{
			(*it)->update();
			++it;
		}
	}
};

void PlayerObserver::update()
{
	cout << name << "收到消息：" << sub->action << endl;
	if (sub->action == "老师来了")
	{
		cout << "停止玩耍，马上回到座位学习！" << endl;
	}
}
void NBAObserver::update()
{
	cout << name << "收到消息：" << sub->action << endl;
	if (sub->action == "老师来了")
	{
		cout << "停止看球赛，马上回到座位学习！" << endl;
	}
}

void TestObserver()
{
	Subject* teacher = new Secretary();
	Observer* aa = new NBAObserver("aa", teacher);
	Observer* bb = new NBAObserver("bb", teacher);
	Observer* cc = new PlayerObserver("cc", teacher);

	teacher->attach(aa);
	teacher->attach(bb);
	teacher->attach(cc);

	teacher->action = "在批改作业";
	teacher->notify();
	cout << endl;

	teacher->action = "老师来了";
	teacher->notify();
}
