#pragma once
#include<iostream>
#include<algorithm>
using namespace std;

/*
* 工厂模式：
*  1.简单工厂模式：建立一个工厂类，对实现了同一个接口的一些类进行实例的创建
*                实际上是由一个工厂类根据传入的参数动态决定应该创建哪一个
*                产品类（这些产品类继承自一个父类或接口）
*  2.抽象工厂模式：对工厂也进行抽象，每个工厂只能生产特定的产品
*/

//1.简单工厂模式：
#include<thread>

//产品类（抽象类，不可实例化）
class Product
{
public:
	Product(){}
	virtual void show() = 0;
};
//产品A
class productA :public Product
{
public:
	productA(){}
	void show()
	{
		cout << "product A create" << endl;
	}
	~productA(){}
};
//产品B
class productB :public Product
{
public:
	productB() {}
	void show()
	{
		cout << "product B create" << endl;
	}
	~productB() {}
};
//工厂类
class simpleFactory
{
public:
	simpleFactory(){}
	Product* product(const string str)
	{
		if (str == "productA")
			return new productA();
		if (str == "productB")
			return new productB();
		return nullptr;
	}
};

void TestProduct1()
{
	//创建工厂
	simpleFactory obj;
	//创建产品
	Product* pro;
	pro = obj.product("productA");
	pro->show();
	delete pro;

	pro = obj.product("productB");
	pro->show();
	delete pro;
}

//2.抽象工厂模式：
//产品类（抽象类，不能实例化）
class PProduct
{
public:
	PProduct(){}
	virtual void show() = 0;
};
//产品A
class PProductA :public PProduct
{
public:
	PProductA() {}
	void show()
	{
		cout << "product A create" << endl;
	}
};
//产品B
class PProductB :public PProduct
{
public:
	PProductB() {}
	void show()
	{
		cout << "product B create" << endl;
	}
};
//工厂类（抽象类，不能实例化）
class FFactory
{
public:
	virtual PProduct* createProduct() = 0;
};
//工厂类A，只生产产品A
class FactoryA :public FFactory
{
public:
	PProduct* createProduct()
	{
		PProduct* pro = nullptr;
		pro = new PProductA();
		return pro;
	}
};
//工厂类B，只生产产品B
class FactoryB :public FFactory
{
public:
	PProduct* createProduct()
	{
		PProduct* pro = nullptr;
		pro = new PProductB();
		return pro;
	}
};

void TestProduct2()
{
	PProduct* product = nullptr;
	auto myfactoryA = new FactoryA();
	product = myfactoryA->createProduct();//工厂A生产产品A
	product->show();
	delete product;

	auto myfactoryB = new FactoryB();
	product = myfactoryB->createProduct();//工厂B生产产品B
	product->show();
	delete product;
}

