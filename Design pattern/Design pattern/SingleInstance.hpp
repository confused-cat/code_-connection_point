#pragma once
#include<iostream>
#include<algorithm>
using namespace std;

/*
*  单例模式：保证一个类仅有一个实例，并提供访问它的访问全局点
   典型模式：懒汉模式+饿汉模式
   适用场景：
		（1）系统只需要一个实例对象或考虑到资源消耗的太大而只允许创建一个对象
		（2）客户调用类的单个实例只允许使用一个公共访问点，除了访问点之外不允许通过其他方式访问该实例

*/

//一、饿汉模式(线程安全)：在类定义时就进行实例化
//在最开始的时候静态对象就已经创建完成，设计方法是类中包含一个静态成员指针
//该指针指向该类的一个对象
//提供一个公有的静态成员方法，返回该对象指针，构造函数私有化
class SingleInstance1
{
public:
	static SingleInstance1* GetInstance()
	{
		static SingleInstance1 s;
		return &s;
	}
	~SingleInstance1() {}
private:
	//涉及到创建对象的函数都进行私有化
	//1.空构造
	SingleInstance1()
	{
		cout << "SingleInstance1() 饿汉" << endl;
	}
	//2.拷贝构造
	SingleInstance1(const SingleInstance1& s)
	{}
	//3.赋值运算符重载
	SingleInstance1& operator=(const SingleInstance1& s)
	{
		return *this;
	}
};

void TestSingleInstance1()
{
	SingleInstance1* s1 = SingleInstance1::GetInstance();
}


#include<mutex>

//二、懒汉模式：线程安全需要进行加锁操作
//只有要用的时候才去实例化对象，即该单例类第一次被引用的时候将自己初始化
//如：写诗拷贝、晚绑定等
class SingleInstance2
{
public:
	static SingleInstance2* GetInstance()
	{
		if (ins == nullptr)
		{
			_mutex.lock();
			if (ins == nullptr)
				ins = new SingleInstance2();
			_mutex.unlock();
		}
		return ins;
	}
	~SingleInstance2() {}
	static mutex _mutex;  //互斥锁
private:
	//涉及到创建对象的函数都进行私有化
	SingleInstance2()
	{
		cout << "SingleInstance2() 懒汉" << endl;
	}
	SingleInstance2(const SingleInstance2& s) {}
	SingleInstance2& operator=(const SingleInstance2& s)
	{
		return *this;
	}
	//静态成员
	static SingleInstance2* ins;
};
//静态变量需要定义
SingleInstance2* SingleInstance2::ins = nullptr;
mutex SingleInstance2::_mutex;

void TestSingleInstance2()
{
	SingleInstance2* s2 = SingleInstance2::GetInstance();
	delete s2;
}