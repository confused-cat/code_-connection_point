#include<iostream>
using namespace std;

/* 
*   设计模式：
* 1.分类：
*    （1）创造型模式：单例模式、工厂模式、建造者模式、原型模式
*    （2）结构型模式：适配器模式、桥接模式、外观模式等
*    （3）行为型模式：命令模式、迭代器模式、组合模式等
* 2.常见集中设计模式：
*    （1）单例模式
*    （2）工厂模式
*    （3）简单工厂模式
*    （4）抽象工厂模式
*    （5）观察者模式
*    （6）装饰模式
*/

#include"SingleInstance.hpp"
#include"Product.hpp"
#include"Observe.hpp"
#include"Decorators.hpp"

int main()
{
	//1.单例模式
	//TestSingleInstance1(); 
	//TestSingleInstance2();

	//2.工厂模式
	//TestProduct1(); //简单工厂模式
	//TestProduct2(); //抽象工厂模式

	//3.观察者模式
	//TestObserver();

	//4.装饰器模式
	//TestDecorators();
	
	return 0;
}