#include<iostream>
#include<string>
#include<stack>
#include<malloc.h>
#include<vector>
#include<list>
using namespace std;


//题目一：给出一个仅包含字符'(', ')', '{', '}', '['和']', 的字符串，判断给出的字符串是否是合法的括号序列。
//注意：括号必须以正确的顺序关闭，"()", "([{}])"和"()[]{}"都是合法的括号序列，但"(]"和"([)]"不合法。

bool fun(const string& s, int n)
{
	if (s[0] == ')' || s[0] == '}' || s[0] == ']' || n % 2 != 0)
	{
		return false;
	}
	stack<char> st;
	st.push(s[0]);
	for (int i = 1; i < n; ++i)
	{
		if (st.empty())
		{
			st.push(s[i]);
		}
		else
		{
			if (st.top() == '(' && s[i] == ')')
				st.pop();
			else if (st.top() == '{' && s[i] == '}')
				st.pop();
			else if (st.top() == '[' && s[i] == ']')
				st.pop();
			else
			{
				st.push(s[i]);
			}
		}
	}
	return st.empty();
}

//1 1 2 3 3
//1 2 3
struct ListNode
{
	int nVal;
	ListNode* pNext;
};

//题目二：请从已经排序好的链表中删除重复的节点，保证每个节点只出现一次。
ListNode* DeleteNode(ListNode* head)
{
	ListNode* pre = head;
	if (head == nullptr)
		return nullptr;
	ListNode* cur = head->pNext;
	while (cur)
	{
		if (cur->nVal == head->nVal)
		{
			cur = cur->pNext;
			head->pNext = cur;
		}
		else
		{
			head = cur;
			cur = head->pNext;
		}
	}
	return pre;
}


//题目三：给定一个由0和1组成的二维矩阵，找出每个元素到最近的0的距离。两个相邻元素间的距离为1。
//输入
//[[0, 0, 0],
//[0, 1, 0],
//[0, 0, 0]]
//输出
//[[0, 0, 0],
//[0, 1, 0],
//[0, 0, 0]]
//
//示例2
//输入
//[[0, 0, 0],
//[0, 1, 0],
//[1, 1, 1]]
//输出
//[[0, 0, 0],
//[0, 1, 0],
//[1, 2, 1]]
vector<vector<int>> function(const vector<vector<int>>& v, int m, int n)
{
	//第一个参数表示0 1矩阵，m行，n列数
	vector<vector<int>> ret(m, vector<int>(n, 0));
	for (int i = 0; i < m; ++i)
	{
		for (int j = 0; j < n; ++j)
		{

		}
	}
}

int main()
{
	//1
	string s("([{}])");
	int n = s.size();
	if (fun(s, n))
	{
		cout<<"YES"<<endl;
	}
	else
	{
		cout<<"NO"<<endl;
	}

	//2
	list<ListNode> L;

	return 0;
}



