//哈希桶开散列实现
#pragma once 

#include<iostream>
#include<vector>
#include<string>
#include<assert.h>
#include"Common.h"

using namespace std;

template<class T>
struct HashNode
{
	T _data;
	HashNode<T>* _next;

	HashNode(const T& data = T())
		:_data(data)
		,_next(nullptr)
	{}
};

//解决问题，采用除留余数法来作为哈希函数，那当数据类型不为int时又如何处理
//数据转换，处理T是整型家族的类型数据：char/short/int/long/long long
template<class T>
class TtoIntDef
{
public:
	size_t operator()(const T& data)
	{
		return data;
	}
};

//template<class T, class KOFD, class TtoInt = TtoIntDef<T>>
//class HashBucket;

//模板参数：1.T->存储元素类型
//          2.KOFD->如map中将k提取出来进行元素比较
//          3.TtoInt->将存储的元素转化为整型可以哈希函数进行取模操作
//template<class T, class KOFD, class TtoInt>

template<class T, class TtoInt = TtoIntDef<T>>
class HashBucket;

template<class T, class TtoInt>
class HashBucket
{
public:
	typedef HashNode<T> Node;
	
	HashBucket(size_t capacity = 10)
		:_table(capacity)
		, _size(0)
	{}

	~HashBucket()
	{
		Clear();
	}
	
	//哈希桶中元素唯一
	//元素插入
	bool InsertUnique(const T& data)
	{
		//首先检测一下空间是否需要扩容
		CheckCapacity();
		
		//1.计算哈希地址（即桶号）
		size_t bucketNo = HashFunc(data);
		//2.确保桶中元素唯一，若桶中已有元素data则错误返回，没有的话插入
		Node* cur = _table[bucketNo];
		while (cur)
		{
			if (data == cur->_data)
				return false;
			cur = cur->_next;
		}
		//3.进行新节点的插入
		cur = new Node(data);
		cur->_next = _table[bucketNo];
		_table[bucketNo] = cur;
		++_size;
		return true;
	}
	//元素删除
	size_t EraseUnique(const T& data)
	{
		//1.计算哈希地址（即桶号）
		size_t bucketNo = HashFunc(data);
		//2.在此桶中找到元素进行删除
		Node* cur = _table[bucketNo];
		Node* prev = nullptr;
		while (cur)
		{
			if (data == cur->_data)
			{
				if (cur == _table[bucketNo])
				{
					//删除元素为桶中首节点
					_table[bucketNo] = cur->_next;
				}
				else
				{
					prev -> _next = cur->_next;
				}
				delete cur;
				--_size;
				return 1;
			}
			else
			{
				prev = cur;
				cur = cur->_next;
			}
		}
		return 0;
	}

	//哈希桶中元素不唯一
	//元素插入
	bool InsertEqual(const T& data)
	{
		//首先检测一下空间是否需要扩容
		CheckCapacity();

		//1.计算哈希地址（即桶号）
		size_t bucketNo = HashFunc(data);

		//2.直接进行新节点的插入
		Node* cur = new Node(data);
		cur->_next = _table[bucketNo];
		_table[bucketNo] = cur;
		++_size;
		return true;
	}
	//元素删除(返回值为删除元素个数)
	size_t EraseEqual(const T& data)
	{
		//1.计算哈希地址（即桶号）
		size_t bucketNo = HashFunc(data);
		size_t oldsize = _size;
		//2.在此桶中找到值为data的元素全部进行删除
		Node* cur = _table[bucketNo];
		Node* prev = nullptr;
		while (cur)
		{
			if (data == cur->_data)
			{
				if (prev==nullptr)
				{
					//删除元素为桶中首节点
					_table[bucketNo] = cur->_next;
					delete cur;
					--_size;
					cur = _table[bucketNo];
				}
				else
				{
					prev = > _next = cur->_next;
					delete cur;
					--_size;
					cur = prev->_next;
				}
			}
			else
			{
				prev = cur;
				cur = cur->_next;
			}
		}
		return oldsize - _size;
	}

	//计算桶的个数
	size_t BucketCount()const
	{
		return _table.capacity();
	}

	//计算某个桶中的元素个数
	size_t BucketSize(size_t bucketNo)const
	{
		assert(bucketNo < BucketCount());
		Node* cur = _table[bucketNo];
		size_t size = 0;
		while (cur)
		{
			size++;
			cur = cur->_next;
		}
		return size;
	}

	//计算某个元素所在的桶号
	size_t BucketNo(const T& data)
	{
		return HashFunc(data);
	}

	//查找元素是否存在在桶中
	bool Find(const T& data)
	{
		//1.计算元素所在的桶号
		size_t bucketNo = HashFunc(data);
		//2.检测元素是否存在
		Node* cur = _table[bucketNo];
		while (cur)
		{
			if (cur->_data == data)
				return true;
			cur = cur->_next;
		}
		return false;
	}

	//表中有效元素个数
	size_t Size()const
	{
		return _size;
	}

	void Clear()
	{
		for (size_t i = 0; i < _table.capacity(); ++i)
		{
			Node* cur = _table[i];//获取桶中首节点
			while (cur)
			{
				_table[i] = cur->_next;
				delete cur;
				cur = _table[i];
			}
		}
		_size = 0;
	}

	void Swap(HashBucket<T,TtoInt>& ht)
	{
		_table.swap(ht._table);
		std::swap(_size, ht._size);
	}

	void PrintHash()const
	{
		for (size_t i = 0; i < _table.capacity(); ++i)
		{
			cout << "table[" << i << "]:";
			Node* cur = _table[i];
			while (cur)
			{
				cout << cur->_data << "----->";
				cur = cur->_next;
			}
			cout << "NULL" << endl;
		}
		cout << endl;
	}
private:
	//哈希函数(除留余数法)
	size_t HashFunc(const T& data)const
	{
		TtoInt t2int;
		return t2int(data) % _table.capacity();
	}
	//空间检测
	void CheckCapacity()
	{
		//如果表中有效元素等于空间容量，进行空间2倍数扩容
		if (_size == _table.capacity())
		{
			//HashBucket<T> newHT(_size * 2);
			HashBucket<T,TtoInt> newHT(GetNextPrime(_size));
			
			//将旧桶中元素往新桶中进行插入（不可直接搬移，因为扩容哈希函数也改变了）
			for (size_t i = 0; i < _table.capacity(); ++i)
			{
				Node* cur = _table[i];
				//将i号桶对应的链表中每个节点插入到新桶中去
				while (cur)
				{
					//1.将cur从桶上取出来
					_table[i] = cur->_next;
					//2.将cur插入到新桶中去，先计算cur在新桶中的哈希地址再进行插入
					size_t newBucketNo = newHT.HashFunc(cur->_data);
					cur->_next = newHT._table[newBucketNo];
					newHT._table[newBucketNo] = cur;
					newHT._size++;
					//3.从_table[i]中取下一个节点
					cur = _table[i];
					_size--;
				}
			}
			this->Swap(newHT);
		}
	}
private:
	vector<Node*> _table;//哈希表中存储哈希节点
	size_t _size;        
};


void TestHashBucket1()
{
	HashBucket<int> ht;
	ht.InsertUnique(1);
	ht.InsertUnique(2);
	ht.InsertUnique(3);
	ht.InsertUnique(4);
	ht.InsertUnique(11);
	ht.InsertUnique(12);
	ht.InsertUnique(13);
	ht.InsertUnique(14);
	ht.InsertUnique(44);
	ht.InsertUnique(54);
	cout << "size=" << ht.Size() << endl;
	ht.PrintHash();

	size_t i=ht.EraseUnique(44);
	if (i)
	{
		cout << "44 is not in the bucket." << endl;
	}
	else
	{
		cout << "44 is in the bucket." << endl;
	}
	cout << "size=" << ht.Size() << endl;

	size_t bucketNo = ht.BucketNo(1);
	cout << "1所在的桶号为：" << bucketNo << endl;
	
	ht.Clear();
	cout << "size=" << ht.Size() << endl;
	cout << "桶的个数为：" << ht.BucketCount() << endl;
}

//数据转换，处理T为string类型的数据，字符串哈希算法来进行处理
class StrtoInt
{
public:
	size_t operator()(const string& s)
	{
		const char* str = s.c_str();
		unsigned int seed = 131;
		unsigned int hash = 0;
		while (*str)
		{
			hash = hash*seed + (*str++);
		}
		return (hash & 0x7FFFFFFF);
	}
};

void TestHashBucket2()
{
	HashBucket<string, StrtoInt> ht;
	ht.InsertUnique("chinese");
	ht.InsertUnique("math");
	ht.InsertUnique("science");
	ht.InsertUnique("art");

	cout << "size=" << ht.Size() << endl;
	ht.PrintHash();

	size_t i = ht.EraseUnique("chinese");
	if (i)
	{
		cout << "chinese is not in the bucket." << endl;
	}
	else
	{
		cout << "chinese is in the bucket." << endl;
	}
	cout << "size=" << ht.Size() << endl;

	size_t bucketNo = ht.BucketNo("art");
	cout << "art所在的桶号为：" << bucketNo << endl;
}