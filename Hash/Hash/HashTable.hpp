//哈希表闭散列实现
#pragma once 

#include<iostream>
#include<vector>
#include<string>
#include<assert.h>

//数据状态
enum State
{
	EM, //空
	EX, //存在
	DEL //删除
};

//表中元素唯一
template<class K,class V>
class HashTable
{
	struct Elem
	{
		pair<K, V> _val;
		State _state;
	};
public:
	HashTable(size_t capacity = 10)
		:_table(capacity)
		, _size(0)
	{
		for (size_t i = 0; i < capacity; ++i)
		{
			_table[i]._state = EM;
		}
	}
	//插入元素
	bool Insert(const pair<K,V>& data)
	{
		//1.检测哈希表空间是否足够
		_CheckCapacity();
		
		//2.哈希函数计算哈希地址
		size_t hashAddr = HashFunc(data.first);
		//3.找下一个空位置来进行插入
		while (_table[hashAddr]._state != EM)
		{
			if (_table[hashAddr]._state == EX && _table[hashAddr]._val.first == data.first)
				return false;
			hashAddr++;
			//找空位置走到表末尾后得从头开始进行空位置的查找
			if (hashAddr == _table.capacity())
				hashAddr = 0;
		}
		//4.找到空位置，元素插入
		_table[hashAddr]._state = EX;
		_table[hashAddr]._val = data;
		_size++;
		return true;
	}
	//查找元素
	int Find(const K& data)
	{
		//1.计算元素哈希地址
		size_t hashAddr = HashFunc(data);
		while (_table[hashAddr]._state != EM)
		{
			if (_table[hashAddr]._state == EX && _table[hashAddr]._val.first == data)
				return hashAddr;
			hashAddr++;
		}
		return -1;
	}
	//删除元素
	bool Erase(const K& data)
	{
		int index = Find(data);
		if (index !=-1)
		{
			_table[index]._state = DEL;
			_size--;
			return true;
		}
		return false;
	}
	size_t Size()
	{
		return _size;
	}
	void Swap(HashTable<K,V>& ht)
	{
		_table.swap(ht._table);
		std::swap(_size, ht._size);
	}
private:
	void _CheckCapacity()
	{
		//当载荷因子达到0.7时，便进行扩容处理
		if (_size * 10 / _table.capacity() >= 7)
		{
			HashTable<K,V> newHT(GetNextPrime(_table.capacity()));
			for (size_t i = 0; i < _table.capacity(); ++i)
			{
				if (_table[i]._state == EX)
					newHT.Insert(_table[i]._val);
			}
			Swap(newHT);
		}
	}
	size_t HashFunc(const K& data)
	{
		return data%_table.capacity();
	}
private:
	vector<Elem> _table;//哈希表中存储哈希节点
	size_t _size;   //哈希表中有效元素个数
};

void TestHashTable()
{
	HashTable<int,int> ht;
	ht.Insert(make_pair(1, 1));
	ht.Insert(make_pair(2, 1));
	ht.Insert(make_pair(5, 3));
	ht.Insert(make_pair(7, 2));
	ht.Insert(make_pair(3, 1));
	//查找2是否在哈希表中
	if (ht.Find(2)!=-1)
	{
		cout << "2 is in the HashTable." << endl;
	}
	else
	{
		cout << "2 is not in the HashTable." << endl;
	}
	cout << "元素个数为：" << ht.Size() << endl;
	//删除2后再查找2是否在哈希表中
	if (ht.Erase(2))
	{
		cout << "删除2成功." << endl;
		if (ht.Find(2) !=-1)
		{
			cout << "删除后找到" << endl;
		}
		else
		{
			cout << "删除2后未找到" << endl;
		}
	}
	else
	{
		cout << "删除2失败." << endl;
	}
	cout << "元素个数为：" << ht.Size() << endl;
}