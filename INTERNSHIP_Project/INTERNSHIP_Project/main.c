//#define _CRT_SECURE_NO_WARNINGS

/*
#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
int main()
{
	int n1=0;
	int n2=0;
	printf("请输入两个要比较的数字：\n");
	scanf("%d %d", &n1, &n2);
	int ret = 0;
	ret = n1 > n2 ? n1 : n2;
	printf("两数字中较大的数字为：");
	printf("%d\n", ret);

	return 0;
}
*/
//#include <stdio.h>

/*/
int main()
{
	int i = 0;
	for (i = 0; i < 10; i++)
	{
		if (i = 5)
			printf("%d ", i);
	}
	return 0;
}
*/

////将三个整数数按从大到小输出
//#include<stdio.h>
//int main()
//{
//	int a, b, c;
//	scanf("%d %d %d", &a, &b, &c);
//
//	if (a >= b)
//	{
//		if (b >= c)
//		{
//			printf("%d %d %d\n", a, b, c);
//		}
//		else
//		{
//			if (a > c)
//			{
//				printf("%d %d %d\n", a, c, b);
//			}
//			else
//				printf("%d %d %d\n", c, a, b);
//		}
//	}
//	else
//	{
//		if (a >= c)
//		{
//			printf("%d %d %d\n", b, a, c);
//		}
//		else
//		{
//			if (b >= c)
//			{
//				printf("%d %d %d\n", b, c, a);
//			}
//			else
//				printf("%d %d %d\n", c, b, a);
//		}
//	}
//	return 0;
//}

////打印1 - 100之间所有3的倍数的数字
//#include<stdio.h>
//int main()
//{
//	int i = 1;
//	for (int i = 1; i < 100; ++i)
//	{
//		if (i % 3 == 0)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

////求10 个整数中最大值
//#include<stdio.h>
//int main()
//{
	////创建数据个数为10的数组array
	//int array[10] = { 0 };
	//for (int i = 0; i < 10; ++i)
	//{
	//	scanf("%d", &array[i]);
	//}
//	//maxnum用来记录数组中最大的值，初始化为数组第一个元素的值
//	int maxnum = array[0];
//	//遍历数组，进行maxnum的更新
//	for (int i = 1; i < 10; ++i)
//	{
//		if (array[i] > maxnum)
//		{
//			maxnum = array[i];
//		}
//		//if语句也可以简化为:
//		//maxnum=max(maxnum,array[i]);
//	}
//	printf("%d", maxnum);
//	return 0;
//}

////打印100-200之间的素数
//#include<stdio.h>
//#include<stdbool.h>
//#include<math.h>
////判断一个数是否为素数
//bool func(int n)
//{
//	if (n == 1)
//		return true;
//	for (int i = 2; i <=n/2; ++i)
//	{
//		if (n % i == 0)
//		{
//			return false;
//		}
//	}
//	return true;
//}
//int main()
//{
//	for (int i = 101; i < 200; ++i)
//	{
//		if (func(i))
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

////1000-2000年的闰年
//#include<stdio.h>
//#include<stdbool.h>
//bool func(int year)
//{
//	if ((year % 400 == 0) || ((year % 100 != 0) && (year % 4 == 0)))
//	{
//		return true;
//	}
//	return false;
//}
//int main()
//{
//	int n = 1000;
//	for (int i = n; i < 2000; ++i)
//	{
//		if (func(i))
//		{
//			printf("%d ",i);
//		}
//	}
//	return 0;
//}

//计算1/1-1/2+1/3-1/4+1/5 …… + 1/99 - 1/100 的值，打印出结果
//#include<stdio.h>
//int main()
//{
//	int i = 0;
//	double sum = 0;
//	for (i = 1; i <= 100; ++i)
//	{
//		//奇数项为正
//		if (i % 2 != 0)
//		{
//			sum += 1.0 / i;
//		}
//		else
//		{
//			sum -= 1.0 / i;
//		}
//	}
//	printf("%lf", sum);
//	return 0;
//}

//给定两个数，求这两个数的最大公约数
//例如：
//输入：20 40
//输出：20
//#include<stdio.h>
//int main()
//{
//	int a, b;
//	scanf("%d%d", &a, &b);
//	int max, min;
//	//max表示较大的数
//	max = a > b ? a : b;
//	//min表示较小的数
//	min = a > b ? b : a;
//	//当两个数相等时，两数的最大公约数就是它们本身
//	if (a == b)
//	{
//		printf("%d\n", a);
//	}
//	//用范围在 2-min 的数去整除a，b
//	//若存在数字 i，有 (max%i==0)&&(min%i==0)成立
//	//那么i就是a,b的最大公约数
//	int i = min;
//	while (min--)
//	{
//		if ((max % i == 0) && (min % i == 0))
//		{
//			printf("%d\n", i);
//			break;
//		}
//	}
//	return 0;
//}

//编写程序数一下 1到 100 的所有整数中出现多少个数字9
/*
解题思路：
1. 数字范围都是一位数和两位数，只需要确定个位或者十位的数字是否为9即可
*/
//#include<stdio.h>
/*
解题思路：
1. 给一个循环从1遍历到100，拿到每个数据后进行一下操作
2.  
a.通过 % 的方式取当前数据的个位，检测个位数据是否为9
如果是，给计数器加1
b.通过 / 的方式取当前数据的十位，检测十位数据是否是9，
如果是，给计数器加1
循环一直继续，直到所有的数据检测完，所有9的个数已经统计在count计数中。
*/
//int main()
//{
//	int count = 0;
//	for (int i = 1; i < 100; ++i)
//	{
//		if (i % 10 == 9)
//		{
//			count++;
//		}
//		if (i / 10 == 9)
//		{
//			count++;
//		}
//	}
//	printf("%d\n", count);
//	return 0;
//}



////在屏幕上输出9*9乘法口诀表
//#include<stdio.h>
//int main()
//{
	//int i = 1;
	//int j = 1;
	//while(i<10)
	//{
	//	//每一轮循环j都是从1开始进行增长
	//	j = 1;
	//	while(j<10)
	//	{
	//		if (j == i)
	//		{
	//			printf("%d x %d = %d\n", j, i, i * j);
	//			break;
	//		}
	//		printf("%d x %d = %d\t", j, i, i * j);
	//		j++;
	//	}
	//	i++;
	//}
//	return 0;
//}

////猜数字游戏
///*
//解题思路：
//1.创建一个随机数，然后输入猜测的数字
//2.当猜测数字大于生成的随机数，提示玩家猜大了，再猜
//3.当猜测数字小于生成的随机数，提示玩家猜小了，再猜
//4.当猜测数字等于生成的随机数，提示猜对
//5.
//*/
//#define _CRT_SECURE_NO_WARNINGS
//#include <stdlib.h>
//#include <time.h>
//#include <stdio.h>
//void menu()
//{
//	printf("********************************\n");
//	printf("*******     1. play      *******\n");
//	printf("*******     0. exit      *******\n");
//	printf("********************************\n");
//}
//void game()
//{
//	//1.生成随机数
//	int ret = rand() % 100 + 1;//生成0-100之间的随机数
//	int num = 0;
//	//2.猜数字
//	while (1)
//	{
//		printf("请猜数字:>");
//		scanf("%d", &num);
//		if (num == ret)
//		{
//			printf("恭喜你，猜对了\n");
//			break;
//		}
//		else if (num > ret)
//		{
//			printf("猜测数字过大，请重新猜\n");
//		}
//		else
//		{
//			printf("猜测数字过小，请重新猜\n");
//		}
//	}
//}
//int main()
//{
//	int input = 0;
//	//srand函数：C语言中用于生成伪随机数的函数，可以用来初始化随机数生成器
//	//以确保在程序的不同运行过程中产生不同的随机数
//	srand((unsigned int)time(NULL));//使用当前时间作为随机数种子
//	do
//	{
//		menu();
//		printf("请选择:>");
//		scanf("%d", &input);
//		switch (input)
//		{
//		case 1:
//			game();
//			break;
//		case 0:
//			printf("退出游戏\n");
//			break;
//		default:
//			printf("选择错误\n");
//			break;
//		}
//	} while (input);
//	return 0;
//}




//
///*
//二分查找：
//1.找到数组的中间位置
//2.查看数组中间位置元素array[mid]和待查找元素key是否相等
//  a.相等，返回下标，跳出循环
//  b.array[mid]>key，就转到数组左半侧继续进行二分查找
//  c.array[mid]<key，就转到数组有半侧继续进行二分查找
//3.如果找到返回下标，没找到打印没找到提示信息
//
//*/
////二分查找
////在数组 arr 中查找值 n
////找到返回 n 在数组中的下标
////没找到返回-1
//#include<stdio.h>
//int main()
//{
//	int array[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	printf("请输入要查找的值：\n");
//	int number;
//	scanf("%d", &number);
//	int left = 0;
//	int right = sizeof(array) / sizeof(array[0]);
//
//	while (left <= right)
//	{
//		int mid = (left + right) / 2;
//		if (array[mid] > number)
//		{
//			//数组中间的值大于当前查找的值，说明值在左半侧，需要修改右边界
//			right = mid - 1;
//		}
//		else if(array[mid]<number)
//		{
//			//数组中间的值小于当前查找的值，说明值在右半侧，需要修改左边界
//			left = mid + 1;
//		}
//		else
//		{
//			//两者相等,打印下标
//			printf("找到了，%d 的下标为：",number);
//			printf("%d\n", mid);
//			break;
//		}
//	}
//	//循环结束，表示没有找到该值
//	if (left > right)
//	{
//		printf("找不到\n");
//	}
//	return 0;
//}
//
// 
//实现一个函数is_prime，判断一个数是不是素数
//利用上面实现的is_prime函数，打印100到200之间的素数

////当数的因子只有1和它本身，那么该数就是一个素数
///*
//解题思路：
//
//*/
//#include<stdio.h>
//#include<stdbool.h>
//#include<math.h>
//
//bool is_prime(int n)
//{
//	//sqrt函数：对数进行开根操作 
//	for (int i = 2; i < sqrt(n); ++i)
//	{
//		if (n % i == 0)
//		{
//			return false;
//		}
//	}
//	return true;
//}
//int main()
//{
//	for (int i = 101; i < 200; ++i)
//	{
//		if (is_prime(i))
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//#include<stdio.h>
//#include<stdbool.h>
///*
//判断闰年：
//1.可以被400整除的年一定是闰年
//2.可以被4整除 但是 不能被100整除的年也一定是闰年
//*/
//bool func(int year)
//{
//	if ((year % 400 == 0) || ((year % 4 == 0) && (year % 100 != 0)))
//	{
//		return true;
//	}
//	return false;
//}
//int main()
//{
//	int year;
//	printf("请输入要判断的年份：");
//	scanf("%d", &year);
//	if (func(year))
//	{
//		printf("%d是闰年\n", year);
//	}
//	else
//	{
//		printf("%d不是闰年\n", year);
//	}
//	return 0;
//}

//实现一个函数来交换两个整数的内容
/*
方法一：值传递
借助一个临时变量来进行二者数据的交换
方法二：指针传递，即传地址
*/
//#include<stdio.h>
//void swap1(int left, int right)
//{
//	int temp = left;
//	left = right;
//	right = temp;
//}
//int main()
//{
//	int a = 10;
//	int b = 20;
//	swap1(a, b);
//	printf("a=%d b=%d", a, b);
//	return 0;
//}

//实现一个函数，打印乘法口诀表，口诀表的行数和列数自己指定
//如：输入9，输出9 * 9口诀表，输出12，输出12 * 12的乘法口诀表
//#include<stdio.h>
//void func(int n)
//{
//	int i = 1;
//	int j = 1;
//	while (i <= n)
//	{
//		//每一轮循环j都是从1开始进行增长
//		j = 1;
//		while (j <= n)
//		{
//			if (j == i)
//			{
//				printf("%d x %d = %d\n", j, i, i * j);
//				break;
//			}
//			printf("%d x %d = %d\t", j, i, i * j);
//			j++;
//		}
//		i++;
//	}
//}
//int main()
//{
//	int num;
//	scanf("%d", &num);
//	func(num);
//	return 0;
//}

//递归方式实现打印一个整数的每一位
/*
思路：
递归函数执行：递归函数每执行一次就将数字的个位数上的数字打印
递归结束条件：当数递归到等于0时，结束递归，进行递归回退
*/
//#include<stdio.h>
//void func(int num)
//{
//	if (num == 0)
//	{
//		return;
//	}
//	printf("%d ", num % 10);
//	func(num / 10);
//}
//int main()
//{
//	int num;
//	scanf("%d", &num);
//	func(num);
//	return 0;
//}

//#include<stdio.h>
///*
//思路：
//1.递归实现：
//每一个数的阶乘=前一个数的阶乘*该数，当等于1时，1!=1
//如：5!=5!*4!
//递归结束：当n=1时递归结束，进行回退
//例子：计算 4!
//      递归：4!=4*3! --> 3!=3*2! --> 2!=2*1! --> 1!=1
//	  回退：1!=1 --> 2!=2*1=2 --> 3!=3*2=6 --> 4!=4*6=24
//2.非递归实现：
//直接一个for循环将数进行相乘即可
//*/
////递归实现阶乘
//int Re_factorial(int n)
//{
//	if (n == 1)
//	{
//		return 1;
//	}
//	return n * Re_factorial(n - 1);
//}
////非递归实现阶乘
//int Non_Re_factorial(int n)
//{
//	int sum = 1;
//	for (int i = 1; i <= n; ++i)
//	{
//		sum *= i;
//	}
//	return sum;
//}
//int main()
//{
//	int num;
//	scanf("%d", &num);
//	int ret1=Re_factorial(num);
//	printf("递归 %d!=%d\n", num, ret1);
//	int ret2 = Non_Re_factorial(num);
//	printf("非递归 %d!=%d\n", num, ret2);
//	return 0;
//}

//递归和非递归分别实现strlen
///*
//思路：
//1.递归实现：
//	a.定义一个递归函数，接收一个字符串参数
//	b.检查字符串第一个字符是否为'\0'
//		不是'\0'，则继续调用该函数，直到遇到字符'\0'
//	c.每次递归调用时，计数器加1
//	d.遇到字符'\0'时，计数器的值就是字符串的有效长度
//2.非递归实现：
//遍历字符串，遇到'\0'则计数结束，否则计数加1
//*/
////递归实现
//int Re_strlen(char* str)
//{
//	if (*str == '\0')
//		return 0;
//	return 1 + Re_strlen(str + 1);
//}
////非递归实现
//int Non_Re_strlen(char* str)
//{
//	int count = 0;
//	while (*str)
//	{
//		count++;
//		str += 1;
//	}
//	return count;
//}
//#include<stdio.h>
//int main()
//{
//	char array[] = "hello";
//	int ret1 = Re_strlen(array);
//	printf("递归实现：%d\n", ret1);
//	int ret2 = Non_Re_strlen(array);
//	printf("非递归实现：%d\n", ret2);
//}

//编写一个函数 reverse_string(char* string)（递归实现）
//实现：将参数字符串中的字符反向排列，不是逆序打印。
//要求：不能使用C函数库中的字符串操作函数。

///*
//思路：
//1.将字符串第一个字符保存到临时变量中
//2.将字符串最后一个字符赋值到字符串第一个字符的位置
//3.将字符串最后一个字符的位置置为'\0'
//4.中间字符串若长度大于1的话，进行递归操作字符串
//5.将之后赋值为'\0'的位置用临时变量进行填充
//*/
//#include<stdio.h>
//#include<string.h>
//void reverse_string(char* string)
//{
//	//1.先计算出来字符串的长度
//	int len = strlen(string);
//	//2.将字符串第一个字符保存下来
//	char temp = *string;
//	//3.将最后一个字符放置到第一个字符的位置
//	*string = *(string + len - 1);
//	*(string + len - 1) = '\0';
//	//4.当中间字符串长度大于1时，进行递归操作
//	if (strlen(string + 1) >= 2)
//	{
//		reverse_string(string + 1);
//	}
//	//5.将保存的临时变量将修改为'\0'的字符填充
//	*(string + len - 1) = temp;
//}
//int main()
//{
//	char array[] = "hello";
//	reverse_string(array);
//	printf("%s", array);
//	return 0;
//}

//写一个递归函数DigitSum(n)，输入一个非负整数，返回组成它的数字之和
//例如，调用DigitSum(1729)，则应该返回1 + 7 + 2 + 9，它的和是19
//输入：1729，输出：19

///*
//思路：
//1.函数内部检测该非负整数是否为0，若为0，则返回0
//2.非负整数不为0，对整数取模操作获取整数的个位数，再将整数除以10，去除个位数
//3.将个位数与递归调用，其中递归调用中传入2中获取的整数，将二者结果相加
//4.返回结果
//*/
//#include<stdio.h>
//int DigitSum(int n)
//{
//	if (n == 0)
//	{
//		return 0;
//	}
//	return n % 10 + DigitSum(n / 10);
//}
//int main()
//{
//	int num;
//	scanf("%d", &num);
//	printf("%d", DigitSum(num));
//	return 0;
//}

//编写一个函数实现n的k次方，使用递归实现
///*
//思路：
//任何数的0次方为1
//1.n的k次方即k个n相乘
//2.当k=0时，结果为1
//3.当k≠0时，n的k次方就是n与n的k-1次方的乘积
//4.依次递归计算下去，直到k值为0，递归结束
//*/
//#include<stdio.h>
//int func(int n, int k)
//{
//	if (k == 0)
//	{
//		return 1;
//	}
//	return n * func(n, k - 1);
//}
//int main()
//{
//	int n;
//	int k;
//	scanf("%d %d", &n, &k);
//	int ret = func(n, k);
//	printf("%d", ret);
//	return 0;
//}

//递归和非递归分别实现求第n个斐波那契数
//例如：
//输入：5  输出：5
//输入：10， 输出：55
//输入：2， 输出：1

///*
//思路：
//斐波那契数:第一项为1 第二项为1 之后的每一项都是它前面两项的和 1 1 2 3 5...
//1.递归实现：
// a.本题我们只要找到递推公式就可以
// b.斐波那契数列是前两项都为1，从第三项开始每一项的数值都是前两项之和
// c.func(n)=func(n-1)+func(n-2)
//2.非递归实现：
//for循环从第三项开始求解，结束是循环到第n项
//*/
//#include<stdio.h>
//
////递归实现
//int func1(int n)
//{
//	if (n == 1 || n==2)
//	{
//		return 1;
//	}
//	return func1(n - 1) + func1(n - 2);
//}
////非递归实现
//int func2(int n)
//{
//	if (n == 1 || n == 2)
//	{
//		return 1;
//	}
//	int n1 = 1;
//	int n2 = 1;
//	int ret = 0;
//	for (int i = 3; i <= n; ++i)
//	{
//		ret = n1 + n2;
//		n1 = n2;
//		n2 = ret;
//	}
//	return ret;
//}
//int main()
//{
//	int num;
//	scanf("%d", &num);
//	int ret1 = func1(num);
//	int ret2 = func2(num);
//	printf("递归求解：%d\n", ret1);
//	printf("非递归求解：%d\n", ret2);
//	return 0;
//}


////升序排序
//void BubbleSort(int array[], int size)
//{
//	//外层循环控制需要进行几趟循环，即循环趟数
//	for (int i = 0; i < size-1; ++size)
//	{
//		//内层循环进行数组的排序操作
//		for (int j = 1; j < size-i; ++j)
//		{
//			if (array[j-1] > array[j])
//			{
//				int temp = array[j - 1];
//				array[j - 1] = array[j];
//				array[j] = temp;
//			}
//		}
//	}
//}
//int main()
//{
//	int arr[10] = { 9,8,7,6,5,4,3,2,1,0 };
//	sort(arr, 10);
//	for (int i = 0; i < 10; ++i)
//	{
//		printf("%d ", arr[i]);
//	}
//	return 0;
//}

////实现函数init() 初始化数组为全0
//void init(int array[], int size)
//{
//	for (int i = 0; i < size; ++i)
//	{
//		array[i] = 0;
//	}
//}
////实现print()  打印数组的每个元素
//void print(int array[], int size)
//{
//	for (int i = 0; i < size;++i)
//	{
//		printf("%d ", array[i]);
//	}
//}
////实现reverse()  函数完成数组元素的逆置
//void reverse(int array[], int size)
//{
//	int left = 0;
//	int right = size - 1;
//	while (left <= right)
//	{
//		swap(&array[left], &array[right]);
//		left++;
//		right--;
//	}
//}


/*
思路：
分别遍历A与B数组，将两数组中的内容进行交换即可
*/
////将数组A中的内容和数组B中的内容进行交换（数组一样大）
//void SwapArrayNum(int A[], int B[], int size)
//{
//	int left = 0;
//	while (left < size)
//	{
//		int temp = A[left];
//		A[left] = B[left];
//		B[left] = temp;
//	}
//}

//编程实现：两个int（32位）整数m和n的二进制表达中，有多少个位(bit)不同？
//输入例子 :
//1999 2299
//输出例子 : 7
///*
//思路：
//要计算两个整数的位的不同的个数，那么可以使用按位异或运算符来进行操作
//按位运算符：当两个数的位不同，进行按位异或的结果为1
//1. 将两数字进行按位异或
//2. 计算异或之后的数值中位为1的个数
//3. 输出结果
//*/
//#include<stdio.h>
//int calc_diff_bit(int m, int n)
//{
//	//将m和n进行按位异
//	int tmp = m ^ n;
//	int count = 0;
//	while (tmp)
//	{
//		//按位与，计算数二进制中1的个数
//		tmp = tmp & (tmp - 1);
//		count++;
//	}
//	return count;
//}
//int main()
//{
//	int n1, n2;
//	while (scanf("%d %d", &n1, &n2) == 2)
//	{
//		int ret = calc_diff_bit(n1, n2);
//		printf("%d", ret);
//	}
//	return 0;
//}

//获取一个整数二进制序列中所有的偶数位和奇数位，分别打印出二进制序列
///*
//思路：
//1. 提取所有的奇数位，如果该位是1，输出1，是0则输出0
//2. 以同样的方式提取偶数位置
//
//
// 检测num中某一位是0还是1的方式：
//   1. 将num向右移动i位
//   2. 将移完位之后的结果与1按位与，如果：
//	  结果是0，则第i个比特位是0
//	  结果是非0，则第i个比特位是1
//*/
//void Printbit(int num)
//{
//	for (int i = 31; i >= 1; i -= 2)
//	{
//		printf("%d ", (num >> i) & 1);
//	}
//	printf("\n");
//
//	for (int i = 30; i >= 0; i -= 2)
//	{
//		printf("%d ", (num >> i) & 1);
//	}
//	printf("\n");
//}

//写一个函数返回参数二进制中 1 的个数
//比如： 15    0000 1111    4 个 1
/*
思路：
1. 先计算出数的二进制数
   a. 求一个数的二进制数：除2取余 逆序
2. 根据二进制数统计二进制中1的个数

循环将数右移一位和1进行按位与操作，只有该位为1时结果才为1
结果为1，计数+1
*/

//写一个函数打印arr数组的内容，不使用数组下标，使用指针。
//arr是一个整形一维数组

///*
//思路：
//1. 创建一个指针执行数组首元素
//2. for循环对指针进行+1操作访问数组元素
//*/
//#include<stdio.h>
//int main()
//{
//	int arr[10] = { 1,2,3,4,5,6,7,8,9,10 };
//	int* ptr = arr;
//	for (int i = 0; i < 10; ++i)
//	{
//		printf("%d ", *(ptr + i));
//	}
//	return 0;
//}

//写一个函数，可以逆序一个字符串的内容
///*
//思路：
//1. 创建两个char类型的指针，分别指向字符串的首元素和末尾元素
//2. 一个从前往后进行访问呢元素，一个从后往前进行访问元素
//3. 将两个指针指向的内容进行交换操作
//4. 逆序成功
//*/
//void reverse_string(char* str)
//{
//	//创建left指针指向str的首元素
//	//创建right指针指向str的末尾元素
//	char* left = str;
//	char* right = str+strlen(str) - 1;
//	//交换两个指针指向的内容
//	while (left < right)
//	{
//		char temp = *left;
//		*left = *right;
//		*right = temp;
//		//left指针往后进行移动
//		//right指针往前进行移动
//		left++;
//		right--;
//	}
//}
//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char array[] = "hello";
//	reverse_string(array);
//	for (int i = 0; i < strlen(array); ++i)
//	{
//		printf("%c ", array[i]);
//	}
//	return 0;
//}

//求Sn = a + aa + aaa + aaaa + aaaaa的前5项之和，其中a是一个数字，
//例如：2 + 22 + 222 + 2222 + 22222

///*
//思路：
//1. 每一项等于前一项数据乘以10与a的和
//如：
//	a.2
//	b.2*10+2=22
//	c.22*10+2=222
//	d.222*10+2=2222
//	e.2222*10+2=22222
//
//	a.a
//	b.a*10+a=aa
//	c.aa*10+a=aaa
//	d.aaa*10+a=aaaa
//	e.aaaa*10+a=aaaaa
//2. 创建一个变量进行和的计算
//*/
//#include<stdio.h>
//int main()
//{
//	int a;
//	scanf("%d", &a);
//	int i = 5;
//	int sum = 0;
//	while (i--)
//	{
//		sum += a;
//		a = a * 10 + a;
//	}
//	printf("%d", sum);
//	return 0;
//}

//求出0～100000之间的所有“水仙花数”并输出。
//“水仙花数”是指一个n位数，其各位数字的n次方之和确好等于该数本身，如 : 153＝1 ^ 3＋5 ^ 3＋3 ^ 3，则153是一个“水仙花数”

/*
思路：
此题的关键在于只要知道判断一个数据是否为水仙花数的方式，问题就迎刃而解。
假定给定一个数据data，具体检测方式如下：
1. 求取data是几位数
2. 获取data中每个位置上的数据，并对其进行立方求和
3. 对data中每个位上的数据立方求和完成后，在检测其结果是否与data相等即可，
	相等：则为水仙花数
	否则：不是
*/
//#include<stdio.h>
//#include<math.h>
//#include<stdbool.h>
//int main()
//{
//	int i = 0;
//	for (i = 0; i <= 99999; i++)
//	{
//		int count = 1;
//		int tmp = i;
//		int sum = 0;
//		//判断i是否为水仙花数
//		//1. 求判断数字的位数
//		while (tmp / 10)
//		{
//			count++;
//			tmp = tmp / 10;
//		}
//
//		//2. 计算每一位的次方和
//		tmp = i;
//		while (tmp)
//		{
//			sum += pow(tmp % 10, count);
//			tmp = tmp / 10;
//		}
//
//		//3. 判断
//		if (sum == i)
//			printf("%d ", i);
//	}
//	return 0;
//}

///*
//思路：
//仔细观察图形，可以发现，此图形中是由空格和*按照不同个数的输出组成的。
//
//上三角：先输出空格，后输出*，每行中
//	空格：从上往下，一行减少一个
//	*：2*i+1的方式递增
//
//下三角：先输出空格，后输出*，每行中
//	空格：从上往下，每行多一个空格
//	*： 从上往下，按照2*(line-1-i)-1的方式减少，其中：line表示总共有多少行
//按照上述方式，将上三角和下三角中的空格和*分别输出即可
//*/
//int main()
//{
//	int line = 0;
//	int i = 0;
//	scanf("%d", &line);//7
//	//打印上半部分
//	for (i = 0; i < line; i++)
//	{
//		//打印一行
//		//打印空格
//		int j = 0;
//		for (j = 0; j < line - 1 - i; j++)
//		{
//			printf(" ");
//		}
//		//打印*
//		for (j = 0; j < 2 * i + 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	//打印下半部分
//	for (i = 0; i < line - 1; i++)
//	{
//		//打印一行
//		int j = 0;
//		for (j = 0; j <= i; j++)
//		{
//			printf(" ");
//		}
//		for (j = 0; j < 2 * (line - 1 - i) - 1; j++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}

////////////结构体的声明/////////////
//喝汽水，1瓶汽水1元，2个空瓶可以换一瓶汽水，给20元，可以喝多少汽水（编程实现）
/*
思路：
1. 初始有20块钱，就可以买20瓶汽水，就有20个空瓶
2. 空瓶进行换汽水，就可以有（空瓶子/20）瓶汽水
	空瓶数量就是：（兑换汽水的数量）+（兑换汽水之后剩余的空瓶子数量）
					即：（空瓶子/2）+（空瓶子%2）
3. 当空瓶子的数量大于1时，重复2的操作
*/
//#include<stdio.h>
//int main()
//{
//	int money = 20;//初始的钱
//	int sum = 20;//喝的汽水数量
//	int emptybottle = 20;//空瓶子的数量
//	while (emptybottle>1)
//	{
//		sum += emptybottle / 2;
//		emptybottle = (emptybottle / 2) + (emptybottle % 2);
//	}
//	printf("%d", sum);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	int i = 0;
//	int arr[] = { 1,2,3,4,5,6,7,8,9,10 };
//	for (i = 0; i <= 12; i++)
//	{
//		arr[i] = 0;
//		printf("hello bit\n");
//	}
//	return 0;
//}

//模拟实现库函数strlen
////方式一：计数器方式进行计数
//int my_strlen(const char* str)
//{
//	int count = 0;
//	while (*str)
//	{
//		count++;
//		str++;
//	}
//	return count;
//}
////方式二：不创建临时变量计数器，使用递归
//int my_strlen(const char* str)
//{
//	if (*str == '\0')
//		return 0;
//	else
//	{
//		return 1 + my_strlen(str + 1);
//	}
//}
////方式三：指针-指针方式
//int my_strlen(const char* str)
//{
//	char* ptr = str;
//	while (*ptr!='\0')
//	{
//		ptr++;
//	}
//	return ptr - str;
//}
//
//模拟实现strcpy
//char* my_strcpy(char* dest, const char* src)
//{
//	char* ret = dest;
//	//对dest和src进行断言
//	assert(dest != NULL);
//	assert(src != NULL);
//	while (*dest++=*src++)
//	{
//		;
//	}
//	return ret;
//}

//输入一个整数数组，实现一个函数，
//来调整该数组中数字的顺序使得数组中所有的奇数位于数组的前半部分，
//所有偶数位于数组的后半部分

///*
//思路：
//1. 给定两个下标left和right，left放在数组的起始位置，right放在数组中最后一个元素的位置
//2. 循环进行一下操作
// a. 如果left和right表示的区间[left, right]有效，进行b，否则结束循环
// b. left从前往后找，找到一个偶数后停止
// c. right从后往前找，找到一个奇数后停止
// d. 如果left和right都找到了对应的数据，则交换，继续a，
//*/
//void swap_arr(int arr[], int sz)
//{
//	int left = 0;
//	int right = sz - 1;
//	int tmp = 0;
//
//
//	while (left < right)
//	{
//		// 从前往后，找到一个偶数，找到后停止
//		while ((left < right) && (arr[left] % 2 == 1))
//		{
//			left++;
//		}
//
//		// 从后往前找，找一个奇数，找到后停止
//		while ((left < right) && (arr[right] % 2 == 0))
//		{
//			right--;
//		}
//
//		// 如果偶数和奇数都找到，交换这两个数据的位置
//		// 然后继续找，直到两个指针相遇
//		if (left < right)
//		{
//			tmp = arr[left];
//			arr[left] = arr[right];
//			arr[right] = tmp;
//		}
//	}
//}


/*
思路：
将每个人的说辞给变化成条件，判断条件是否成立即可
*/
//#include<stdio.h>
//int main()
//{
//	int killer = 0;
//	//分别假设凶手是a,b,c,d,看谁是凶手时满足3个人说了真话，一个人说了假话
//	for (killer = 'a'; killer <= 'd'; killer++)
//	{
//		if ((killer != 'a') + (killer == 'c') + (killer == 'd') + (killer != 'd') == 3)
//			printf("凶手是：%c", killer);
//	}
//	return 0;
//}

//#include <stdio.h>
//int checkData(int* p)
//{
//	int tmp[7] = { 0 }; //标记表，实际是哈希表的思路。一开始每个元素都是0。
//
//	int i;
//	for (i = 0; i < 5; i++)
//	{
//		if (tmp[p[i]]) //如果这个位置的标记已经是1，则代表重复，直接返回0。
//		{
//			return 0;
//		}
//		tmp[p[i]] = 1; //如果不是，则给这个位置标记为1。
//	}
//	return 1; //全部标记完毕也没有出现重复的情况，代表OK。
//}
//
//int main()
//{
//	int p[5]; //0 1 2 3 4分别代表a b c d e
//
//	for (p[0] = 1; p[0] <= 5; p[0]++)
//	{
//		for (p[1] = 1; p[1] <= 5; p[1]++)
//		{
//			for (p[2] = 1; p[2] <= 5; p[2]++)
//			{
//				for (p[3] = 1; p[3] <= 5; p[3]++)
//				{
//					for (p[4] = 1; p[4] <= 5; p[4]++) //五层循环遍历
//					{
//						//这里是五个人的描述，由于比较表达式只有0和1两个结果，如果要两个条件有且只有一个为真，则可以用比较表达式的值总和为1的方式直接判定。别忘了还要判定不能并列。
//						if ((p[1] == 2) + (p[0] == 3) == 1 && //B第二，我第三
//							(p[1] == 2) + (p[4] == 4) == 1 && //我第二，E第四
//							(p[2] == 1) + (p[3] == 2) == 1 && //我第一，D第二
//							(p[2] == 5) + (p[3] == 3) == 1 && //C最后，我第三
//							(p[4] == 4) + (p[0] == 1) == 1 && //我第四，A第一
//							checkData(p) //不能并列
//							)
//						{
//							for (int i = 0; i < 5; i++)
//							{
//								printf("%d ", p[i]);
//							}
//							putchar('\n');
//						}
//					}
//				}
//			}
//		}
//	}
//
//	return 0;
//}


/*
思路：
1. 观察杨辉三角规律我们可得，每一行的第一个数据和最后一个数据都是1
2. 每一行的元素个数是上一行的元素个数加1，其中第一行的元素个数和值都是1
3. 其余的值data[i][j]=data[i-1][j]+data[i-1][j-1]
4. 定义一个二维数组进行计算填表即可
*/
//void yangHuiTriangle(int n)
//{
//	int data[30][30] = { 1 }; //第一行直接填好，播下种子
//
//	int i, j;
//
//	for (i = 1; i < n; i++) //从第二行开始填
//	{
//		data[i][0] = 1; //每行的第一列都没有区别，直接给1，保证不会越界。
//		for (j = 1; j <= i; j++) //从第二列开始填
//		{
//			data[i][j] = data[i - 1][j] + data[i - 1][j - 1]; //递推方程
//		}
//	}
//
//	for (i = 0; i < n; i++) //填完打印
//	{
//		for (j = 0; j <= i; j++)
//		{
//			printf("%d ", data[i][j]);
//		}
//		putchar('\n');
//	}
//}

/*
改进：
实际上，我们在计算每一行的时候只会用到前一行的相关数据
所以就没必要将所有的数据都进行保存下来，只需要将上一行的数据进行保存
以便可以求出下一行的数据，这样可以大大减少空间的开销
*/

//有一个数字矩阵，矩阵的每行从左到右是递增的，矩阵从上到下是递增的，请编写程序在这样的矩阵中查找某个数字是否存在。
//要求：时间复杂度小于O(N)

/*
思路：
杨氏矩阵，元素从左到右进行递增，元素从上到下进行递增
所以右上角的元素是一行中最大的，一列中最小的
左下角的元素是一行中最小的，是一列中最大的
所以我们可以从右上角或者左下角开始查找
比如：从右上角开始查找的时候，右上角的元素比我们要查找元素小，我们就可以去掉右上角元素所在的这一行；
右上角的元素比我们要查找的元素大，我们就可以去掉右上角元素所在的这一列
然后依然找右上角的元素继续和要查找的元素与比较
这样每一次比较去掉一行或者去掉一列
这个查找效率是高于遍历数组元素的，所以时间复杂度是小于O(N)，也满足题目要求。
*/
#include <stdio.h>

int findnum(int a[][3], int x, int y, int f) //第一个参数的类型需要调整
{
	int i = 0, j = y - 1; //从右上角开始遍历
	while (j >= 0 && i < x)
	{
		if (a[i][j] < f) //比我大就向下
		{
			i++;
		}
		else if (a[i][j] > f) //比我小就向左
		{
			j--;
		}
		else
		{
			return 1;
		}
	}
	return 0;
}

//实现一个函数，可以左旋字符串中的k个字符
///*
//思路：
//每次将字符串左旋一次，左旋k次即可
//改进：
//1. 但是这种循环得一次一次的进行，可以借助一个临时变量，将左旋之后的字符串保存下来
//再拷贝到原字符串上去
//2. 可以将字符串进行分割，分别将字符串进行逆置，然后再拼接起来
//*/
//void leftRound(char* src, int time)
//{
//	int i, j, tmp;
//	int len = strlen(src);
//	time %= len; //长度为5的情况下，旋转6、11、16...次相当于1次，7、12、17...次相当于2次，以此类推。
//	for (i = 0; i < time; i++) //执行k次的单次平移
//	{
//		tmp = src[0];
//		for (j = 0; j < len - 1; j++) //单次平移
//		{
//			src[j] = src[j + 1];
//		}
//		src[j] = tmp;
//	}
//}
////版本一：
//void leftRound(char* src, int time)
//{
//	int len = strlen(src);
//	int pos = time % len; //断开位置的下标
//	char tmp[256] = { 0 }; //更准确的话可以选择malloc len + 1个字节的空间来做这个tmp
//
//	strcpy(tmp, src + pos); //先将后面的全部拷过来
//	strncat(tmp, src, pos); //然后将前面几个接上
//	strcpy(src, tmp); //最后拷回去
//}
////版本二：
//void reverse_part(char* str, int start, int end) //将字符串从start到end这一段逆序
//{
//	int i, j;
//	char tmp;
//
//	for (i = start, j = end; i < j; i++, j--)
//	{
//		tmp = str[i];
//		str[i] = str[j];
//		str[j] = tmp;
//	}
//}
//
//void leftRound(char* src, int time)
//{
//	int len = strlen(src);
//	int pos = time % len;
//	reverse_part(src, 0, pos - 1); //逆序前段
//	reverse_part(src, pos, len - 1); //逆序后段
//	reverse_part(src, 0, len - 1); //整体逆序
//}

//模仿qsort的功能实现一个通用的冒泡排序

///*
//模仿qsort的功能实现一个通用的冒泡排序
//*/
//void Swap(char* buf1, char* buf2, int width)//一次交换一对字节
//{
//	int i = 0;
//	for (i = 0; i < width; i++)
//	{
//		char temp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = temp;
//		buf1++;
//		buf2++;
//	}
//}
////两个数据的比较规则
//int cmp_int(const void* e1, const void* e2)
//{
//	return *(int*)e1 - *(int*)e2;
//}
//void bubbleSort(void* arr, size_t num, size_t width, int (*cmp)(const void*, const void*))//使用回调函数实现通用的冒泡排序函数
//{
//	for (int i = 0; i < num - 1; i++)
//	{
//		//控制排序的趟数
//		for (int j = 0; j < num - 1 - i; j++)
//		{
//			//进行每一趟数据的交换
//			if (cmp((char*)arr + j * width, (char*)arr + (j + 1) * width) > 0)
//			{
//				Swap((char*)arr + j * width, (char*)arr + (j + 1) * width, width);
//
//			}
//		}
//	}
//}
//
//#include<stdio.h>
//#include<string.h>
//struct student
//{
//	char name[20];
//	int age;
//};
////结构体里面字符数组的比较方法
//int com_student_name(const void* e1, const void* e2)
//{
//	return strcmp(((struct student*)e1)->name, ((struct student*)e2)->name);
//}
////结构体里面年龄的比较方法
//int com_student_age(const void* e1, const void* e2)
//{
//	return ((struct student*)e1)->age - ((struct student*)e2)->age;
//}
//int main()
//{
//	//测试整形数据
//	int arr[] = { 10,9,8,7,6,5,4,3,2,1 };
//	int len = sizeof(arr) / sizeof(arr[0]);
//	bubbleSort(arr, len, sizeof(arr[0]), cmp_int);
//	for(int i = 0; i < len; ++i)
//	{
//		printf("%d ", arr[i]);
//	}
//	//测试结构体类型数据
//
//	struct student s[3] = { {"zhangsan",18},{"lisi",20},{"wanger",19} };
//	int n = sizeof(s) / sizeof(s[0]);
//	//1. 姓名
//	bubbleSort(s, n, sizeof(s[0]),com_student_name);
//	for (int i = 0; i < n; ++i)
//	{
//		printf("%s ", s[i].name);
//	}
//	//2. 年龄
//	bubbleSort(s, n, sizeof(s[0]),com_student_age);
//	for (int i = 0; i < n; ++i)
//	{
//		printf("%d ", s[i].age);
//	}
//	return 0;
//}


//判断一个字符串是否是另一个字符串的左旋与右旋而来
//int findRound(const char* src, char* find)
//{
//	char tmp[256] = { 0 }; //用一个辅助空间将原字符串做成两倍原字符串
//	strcpy(tmp, src); //先拷贝一遍strcat(tmp, src); 
//	//再连接一遍
//	return strstr(tmp, find) != NULL; //看看找不找得到
//}

//模拟实现strstr
///*
//字符串查找函数
//思路：
//const char* strstr(const char* str1,const char* str2);
//表示判断str2是否是str1的子串，若是，函数返回str1字符串从str2第一次出现的位置开始到str1结束的字符串
//不是则返回NULL
//1. 首先对两个字符串进行断言处理
//2. 首先在str1中查找str2的第一个字符，判断str2长度的字符是否都相等
//	相等就返回该字符处的地址
//	不相等就继续往下进行匹配
//*/
//
//const char* strstr(const char* str1, const char* str2)
//{
//	//对两个字符串进行断言操作
//	assert(str1 != NULL);
//	assert(str2 != NULL);
//
//	char* temp1 = str1;
//	char* temp2 = str2;
//	char* s = NULL;
//	if (*str2 == '\0')
//	{
//		return NULL;
//	}
//	while (*temp1)
//	{
//		s = temp1;
//		temp2 = str2;
//		//找到两字符串字符相等的地方,然后进行str2长度的字符判断
//		while (*s && *temp2 && (*s == *temp2))
//		{
//			s++;
//			temp2++;
//		}
//		if (*temp2 == '\0')
//		{
//			//说明字符匹配到了末尾 表示字符全部匹配成功
//			return temp1;
//		}
//		//否则匹配不成功，继续进行匹配
//		temp1++;
//	}
//}

//

//模拟实现strcmp
///*
//字符串比较函数：
//int strcmp(const char* str1,const char* str2);
//判断两字符串大小：
//	若str1更大的话就返回一个大于0的数字
//	若两个字符串相等的话就返回0
//	若str1字符串小于str2的话就返回一个小于0的数
//
//思路：
//从两个字符串的第一个字符开始比较，若相等，则继续比较
//否则则比较第二个字符，依次进行下去，直到有字符不同或者达到终止字符
//其中，字符的比较是以字符的ascii码值来进行比较
//*/
//int strcmp(const char* str1, const char* str2)
//{
//	assert(str1 != NULL);
//	assert(str2 != NULL);
//	while (*str1 == *str2)
//	{
//		if (*str1 == '\0')
//		{
//			return 0;
//		}
//		str1++;
//		str2++;
//	}
//	if (*str1 > *str2)
//	{
//		return 1;
//	}
//	else if (*str1 < *str2)
//	{
//		return -1;
//	}
//}

//模拟实现strcpy
///*
//字符串复制函数：
//char* strcpy(char* dest,char* src);
//
//思路：
//直接将字符串src中的内容复制到dest中即可
//*/
//char* strcpy(char* dest, char* src)
//{
//	char* ret = dest;
//	assert(dest != NULL);
//	assert(src != NULL);
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}

//模拟实现memcpy
///*
//内存复制函数：
//void* memcpy(void* dest,const void* src,size_t num);
//作用：将num个字节的数据从源指针指向的位置直接复制到目标指向的内存块
//返回：返回目标指针指向的字符串
//
//思路：
//直接将字符串src中的内容复制到dest中即可
//*/
//void* memcpy(void* dest, const void* src,size_t num)
//{
//	void* temp = dest;
//	//断言，对字符串进行判空处理
//	assert(dest != NULL);
//	assert(src != NULL);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return temp;
//}

//模拟实现memmove
///*
//void* memmove(void* dest, const void* src, size_t num);
//作用：将num字节的数据从源指向的空间位置复制到了目标指向的内存块
//复制就像使用了中间缓冲区一样，允许目标空间内容和源空间内容重叠
//返回：返回目标指针指向的字符串
//
//思路：
//1. 当两空间不重叠的时候就和memcpy的处理一样，直接将字符进行复制即可
//2. 当二者空间产生重叠时
//	a.dest<src，直接将src空间内容从前往后进行复制到dest中
//	b.dest>src，将src中的内容从后往前进行复制到dest中
//*/
//void* memmove(void* dest, const void* src, size_t num)
//{
//	//对两字符串进行断言操作
//	assert(dest != NULL);
//	assert(src != NULL);
//	void* temp = dest;
//	//当dest小于等于src，或者二者没有产生重叠情况，就直接从前往后进行遍历即可
//	if (dest <= src || (char*)dest >= ((char*)src + num))
//	{
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	//当src>dest时，从后往前进行字符复制
//	else
//	{
//		dest = (char*)dest + num - 1;
//		src = (char*)src + num - 1;
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest - 1;
//			src = (char*)src - 1;
//		}
//	}
//	return temp;
//}

// memmove函数使用char*指针进行增加，是因为字符型char在内存中占用的空间最小
//大多数编程语言中，字符型数据数据的首地址可以直接被访问，使用字符型指针
//移动内存时，可以确保数据在内存中的顺序不被改变，从而保证数据的完整性

//#pragma pack(8)
//#include<stdio.h>
//typedef struct 
//{
//	int a;
//	char b;
//	short c;
//	short d;
//}AA_t;
//int main()
//{
//	printf("%d", sizeof(AA_t));
//	return 0;
//}

//atoi的规则是：跳过不可见字符，碰到负号或者数字开始转换，转换到非数字字符为止
/*
atoi函数：将str指向的字符串转化为整型数字
int atoi(const char* str);

思路：
1. 设置一个全局的枚举变量state，将其初始化为非法的INVAILD
   将该值作为判断该字符串是否转换成功的标志
2. 若str为空指针时候是错误的，使用断言检测字符串是否为空
3. 字符串中有空格和tab等符号的话，需要跳过这些字符，借用isspace函数
4. 在整数型之前如果有'-'的话，那么转换后的数字就是负数，没有就默认为正数
   设置一个标志flag，用来判断该函数是正数还是负数
*/
//
//#include<stdio.h>
//#include<assert.h>
//#include<ctype.h>
//#include<stdlib.h>
//enum State
//{
//	VAILD,
//	INVAILD
//};
//enum State state = INVAILD;//创造变量默认为非法
//int my_atoi(const char* str)
//{
//	对字符串进行断言操作
//	assert(str);
//	如果字符串为0话就是异常情况，直接返回0
//	if (*str == '\0')
//	{
//		return 0;
//	}
//	判断空格或者tab的情况，直接掠过
//	while (isspace(*str))
//	{
//		str++;
//	}
//	为正数的时候，flag标志就为1，负数的时候，flag标志就为-1
//	int flag = 1;
//	if (*str == '+')
//	{
//		flag = 1;
//		str++;
//	}
//	else if (*str == '-')
//	{
//		flag = -1;
//		str++;
//	}
//	long long ret = 0;
//	while (*str != '\0')
//	{
//		if (isdigit(*str))
//		{
//			ret = ret * 10 + flag * (*str - '0');//减去字符0，才是数字0
//			if (ret > INT_MAX || ret < INT_MIN)
//			{
//				return 0;
//			}
//		}
//		else
//		{
//			return (int)ret;//强制类型转化为int（函数的返回值是int）
//		}
//		str++;
//	}
//	if (*str == '\0')
//	{
//		state = VAILD; //正常转换完了，到末尾的 \0
//	}
//	return (int)ret;
//}
//int main()
//{
//	char arr[20] = "1234";
//	int ret = my_atoi(arr);
//	if (state == VAILD)
//	{
//		printf("合法转换:%d\n", ret);
//	}
//	else if (state == INVAILD)
//	{
//		printf("非法转换:%d\n", ret);
//	}
//	return 0;
//}


//char* strncat(char* dest, const char* src, size_t num)
//{
//	char* temp = dest;
//	assert(dest != NULL);
//	assert(src != NULL);
//	while (*dest)
//	{
//		dest++;
//	}
//	while (num--)
//	{
//		*dest++ = *src++;
//	}
//	return temp;
//}
//char* strncpy(char* dest, const char* src,size_t num)
//{
//	char* temp = dest;
//	assert(dest != NULL);
//	assert(src != NULL);
//	while (num && *src)
//	{
//		*dest++ = *src++;
//		num--;
//	}
//	//当num大于字符串的长度的时候，那么就是用'\0'来进行填充
//	while (num)
//	{
//		*dest++ = '\0';
//		num--;
//	}
//	return temp;
////}
//
//一个数组中只有两个数字是出现一次，其他所有数字都出现了两次。
//编写一个函数找出这两个只出现一次的数字。
//例如：
//有数组的元素是：1，2，3，4，5，1，2，3，4，6
//只有5和6只出现1次，要找出5和6

/*
思路：
方法一：
1. 我们可以采用暴力法进行数组的循环遍历
2. 对于一个数data，遍历数组，查找数组中是否还有其它值等于data
3. 如果有相等的话，那么数data就出现了两次
   如果没有相等的话，那么数data就出现了一次
4. 继续重复2与3步骤，直到找到只出现了两次的数据

时间复杂度：O（N^2）
空间复杂度：O（1）
*/
//void GetUnpairedTwo1(int* arr,int start,int end)
//{
//	int size = end - start + 1;
//	for (int i = 0; i < size; ++i)
//	{
//		for (int j = i + 1; j < size; ++j)
//		{
//			if (arr[i] == arr[j])
//			{
//				printf("%d", arr[i]);
//			}
//		}
//	}
//}

/*
方法二：
	对于两个数来说，我们将数组进行排序操作后
	出现两次的数据一定是连在一起的
	所以我们只需要看数data的前一个数是否和data相等
	相等就出现了两次，不相等就出现了两次
时间复杂度：O（N^2）
空间复杂度：O（1）
*/

/*
方法三：
知识点：任何数异或它自己都为0,即 data^data=0，0与任何数异或都等于任何数,即 data^0=data
	实际上，该题的初级问题是数组中只有一个出现一次的数字，其余都是出现了两次
	对于只出现一次的数字的话，采用异或的话：
1. 将数组数据先和0进行异或
2. 再和数组数据进行异或
3. 最终的结果就是只出现一次的数据

那么对于只出现一次的两个数据的话，就可以根据只出现一次的思路来进行：
1. 将数组分为两部分
	a.从头到尾异或数组中的每一个数字，得到结果就是两个只出现一次的两数的异或结果data
	b.两数字不同，所以异或的结果中至少有一位是为1的
	c.先找到第一个二进制位是否为1的位置为依据将数据分为两组，这样带查找的两个数一定在两个子数组中
	d.再找两个变量，分别异或两组数，即可找到这两个数
2. 分别采用只有一个只出现一次的数据的思想找出只出现一次的两个数据
3. 将数据进行返回即可
*/
//
//#include<stdio.h>
//#include<stdbool.h>
////根据结果的二进制右数第一个1来对数组进行分组，在该位上是0的分一组，不是0的分一组
////arr为数组集合 len为数组长度 ptr1和ptr2为分别指向两个单独数的指针
//bool GetUnpairedTwo(int* arr,int len, int* ptr1, int* ptr2)
//{
//	int ret = 0;
//	//将数组所有数据异或一遍
//	for (int i = 0; i < len; ++i)
//	{
//		ret ^= arr[i];
//	}
//	//找到异或结果ret右数的第一个为1的位置
//	int pos = 1;
//	while ((pos & ret) ==0) //两位异或为1，表示该位为0，那么继续左移找到第一个1的位置
//	{
//		//pos进行左移
//		pos <<= 1;
//	}
//	//以每个数字的pos位是否为1进行数组的分配依据
//	*ptr1 = 0;
//	*ptr2 = 0;
//	for (int i = 0; i < len; ++i)
//	{
//		//当数据的pos处的位置为0，那么就将该位也为0的数据进行异或操作,最终*ptr1的结果就是该分组中只出现一次的数
//		if (((arr[i] & pos) == 0))
//		{
//			*ptr1 ^= arr[i];
//		}
//		//当数据的pos处的位置为1，那么就将为0的数据进行异或操作，最终*ptr2的结果就是该分组中只出现一次的数
//		else
//		{
//			*ptr2 ^= arr[i];
//		}
//	}
//	return true;
//}
//int main()
//{
//	int array[] = { 1,2,3,4,5,1,2,3,4,6 };
//	int len = sizeof(array) / sizeof(array[0]);
//	int arg1, arg2;
//	if (GetUnpairedTwo(array, len, &arg1, &arg2))
//	{
//		printf("该数组中只出现了一次的两个数据分别是：%d %d", arg1, arg2);
//	}
//	else
//	{
//		printf("没找到");
//	}
//}



//写一个宏，可以将一个整数的二进制位的奇数位和偶数位交换
///*
//思路：
//随便给出一个32个bit位的二进制数字，观察发现：
//1. 我们可以将其奇数位的二进制位都保留，然后偶数位制成0，再右移一位，这样奇数位就全部去到偶数位位置
//	保留数位：要保留的位按位与1，奇数位 &上010101010101（十六进制数为55555555），偶数位 &上101010101010（十六进制数为aaaaaaaa）
//	          不保留的位按位与0
//2. 再将其偶数位保留，奇数位制成0，左移一位，这样全部偶数位就去到奇数位位置
//3. 再将得到的两个数字相加，即可实现交换
//*/
//#define SWAP_INT(n) (n=(n&0xaaaaaaaa>>1)+(n&0x55555555<<1))

//写一个宏，计算结构体中某变量相对于首地址的偏移，并给出说明
/*
思路：
1. 因为是求偏移量 所以假设结构体的首地址是0开始 
2. 把他转换成结构体指针类型，在用->取得他的成员
3. 前面加了一个 & 就取得这个成员的地址
4. 最后在强制转换成unsigned long, 这样就得到了偏移量
*/
//#define  STRUCT_OFFSET(id, element)  ((unsigned long)(&(( struct id*)0)->element))

//#include<stdio.h>
//int main()
//{
//	char arr[] = "bit";
//	char arr1[] = { 'b','i','t' };//没有有效字符串结尾标志
//	char arr2[] = { 'b','i','t','\0' };
//	printf("%s\n", arr);
//	printf("%s\n", arr1);
//	printf("%s\n", arr2);
//	return 0;
//}

//辗转相除法
//例子：18和24的最大公约数
//第一次：a = 18  b = 24  c = a % b = 18 % 24 = 18
//循环中：a = 24   b = 18
//第二次：a = 24   b = 18  c = a % b = 24 % 18 = 6
//循环中：a = 18   b = 6
//第三次：a = 18   b = 6   c = a % b = 18 % 6 = 0

//int func(int a, int b)
//{
//	if (a == b)
//		return a;
//	int c = a % b;
//	while (c)
//	{
//		
//	}
//}

//int Count_Digit(const int N, const int D) 
//{
//	int m;
//	if (N < 0)
//		m = -N;
//	else m = N;
//
//	if (N == 0 && D == 0)
//		return 1;
//
//	int count = 0;
//	while (m) 
//	{
//		if (m % 10 == D)
//			count++;
//		m /= 10;
//	}
//	return count;
//}

//int Search_Bin(SSTable T, KeyType k)
//{
//	int low = 1;
//	int high = T.length;
//	int mid;
//	while (low < high)  //终止条件
//	{
//		int mid = (low + high) / 2;
//		if (T.R[mid].key == k)
//			return mid;  //返回位置
//		else if (T.R[mid].key < k)
//			low = mid + 1;  //更新low
//		else if (T.R[mid].key > k)
//			high = mid - 1;  //更新high
//	}
//
//	return 0;
//}
//#include <stdio.h>
//int main()
//{
//	int a, b;
//	scanf("%d %d", &a, &b);
//	if (a > b)
//	{
//		printf("a>b");
//	}
//	printf("a<b");
//	return 0;
//}

//1、因为long数据类型表示的数字范围到2,147,483,647，而12!=479,001,600，所以从12为分界点，后面数字阶乘结果比较大，适合用数组存储。
// long表示的数据最大为：
// 2,147,483,647（10位）
// 12!=479,001,600（9位）
// 13!=6,227,020,800（10位）
// 2^32=4,294,967,296
//2、采用数组方法存储，设置进位、位数、临时值。
//3、临时值一定要每一位都与第i个元素相乘。

//void Print_Factorial(const int N) {
//	long sum = 1;
//	if (N >= 0 && N <= 12) {
//		for (int i = 0; i <= N; i++) {
//			if (i == 0) {
//				sum = 1;
//			}
//			else {
//				sum = sum * i;
//			}
//		}
//		printf("%d\n", sum);
//	}
//	else if (N > 12 && N <= 1000) {
//		int Num[3000] = { 0 };  
//		//确保保存最终运算结果的数组足够大：
//		//1-9相乘最多有9位，10-99相乘最多有2*90=180位，100-999相乘最多有3*900=2700位，1000是4*1=4位，总计2893，最好数组取大一下
//		int i, j, k, n;
//		k = 1;  //位数
//		n = 0;  //进位
//		Num[0] = 1;   //将结果先初始化为1
//		int temp;  //阶乘的任一元素与临时结果的某位的乘积结果
//
//		for (i = 2; i <= N; i++)  //开始阶乘，阶乘元素从2开始
//		{  //和平时乘法方法相同，将临时结果的每位与阶乘元素相乘
//
//			for (j = 0; j < k; j++)
//			{
//				temp = Num[j] * i + n;  //相应阶乘中的一项与当前所得临时结果的某位相乘（加上进位）
//				Num[j] = temp % 10;  //更新临时结果的位上信息
//				n = temp / 10;   //看是否有进位
//			}
//
//			while (n != 0)
//			{  //如果有进位
//				Num[k] = n % 10;  //新加一位，添加信息。位数增1
//				k++;
//				n = n / 10;   //看还能不能进位
//			}
//		}
//
//		for (i = k - 1; i >= 0; i--)
//		{
//			printf("%d", Num[i]);
//		}
//		printf("\n");
//	}
//	else 
//	{
//		printf("Invalid input\n");
//	}
//}

//5位运动员参加了10米台跳水比赛，有人让他们预测比赛结果：
//A选手说：B第二，我第三；
//B选手说：我第二，E第四；
//C选手说：我第一，D第二；
//D选手说：C最后，我第三；
//E选手说：我第四，A第一；
//比赛结束后，每位选手都说对了一半，请编程确定比赛的名次。

/*
1. 将所有的情况进行列举出来，每个人都有五种可能
2. 每个人都用一个for循环来进行列举
3. 根据每个人的说辞，说对一半进行判断
4. 列举出来的情况可能会有重复情况
5. 对重复结果进行筛除
*/
//#include<stdio.h>
//int main()
//{
//	int a, b, c, d, e;
//	for (a = 1; a <= 5; ++a)
//	{
//		for (b = 1; b <= 5; ++b)
//		{
//			for (c = 1; c <= 5; ++c)
//			{
//				for (d = 1; d <= 5; ++d)
//				{
//					for (e = 1; e <= 5; ++e)
//					{
//						//每个人都说对了一半
//						if ((b == 2 && a != 3) || (b != 2 && a == 3) == 1)
//						{
//							if ((b == 2 && e != 4) || (b != 2 && e == 4)==1)
//							{
//								if ((c == 1 && d != 2) || (c != 1 && d == 2) == 1)
//								{
//									if ((c == 5 && d != 3) || (c != 5 && d == 3)==1)
//									{
//										if ((e == 4 && a != 1) || (e != 4 && a == 1) == 1)
//										{
//											//求出来的结果有重复，我们将数据筛选出来即可
//											//每个数都是在1，2，3，4，5中且不重复，所以值的乘积是一定等于120的
//											if (a * b * c * d * e == 120)
//											{
//												printf("a=%d b=%d c=%d d=%d e=%d", a, b, c, d, e);
//											}
//										}
//									}
//								}
//							}
//						}
//					}
//				}
//			}
//		}
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int a = 10;
//	int b = 20;
//	return 0;
//}

//void swap(char* buf1, char* buf2, int width)
//{
//	int i = 0;
//	for (i = 0; i < width; ++i)
//	{
//		//进行逐字节的数据交换
//		char temp = *buf1;
//		*buf1 = *buf2;
//		*buf2 = temp;
//		buf1++;
//		buf2++;
//	}
//}
////数据比较规则
//int cmp_int(const void* a, const void* b)
//{
//	//转化为要比较的数据类型的指针
//	return *(int*)a - *(int*)b;
//}
//void sort(void* arr, size_t num, size_t width, int (*cmp_int)(const void*, const void*))
//{
//	//冒泡排序
//	//控制排序的趟数
//	for (int i = 0; i < num - 1; ++i)
//	{
//		//对每一趟进行数据排序
//		for (int j = 0; j < num - 1 - i; ++j)
//		{
//			if (cmp_int((char*)arr + j * width, (char*)arr + (j + 1) * width) > 0)
//			{
//				swap((char*)arr + j * width, (char*)arr + (j + 1) * width,width);
//			}
//		}
//	}
//}

//qsort：C语言中用来对数组进行排序的函数
/*
1. 函数原型：
void qsort(void* arr,size_t num,size_t len,int (*cmp)(const void* a,const void* b));
2. 参数列表：
arr:待排序的数据列表
num:数组的大小
len:数组中元素的大小
cmp:数组中数据比较大小方式
*/

//const char* strstr(const char* str1,const char* str2)
//{
//	assert(str1 != NULL);
//	assert(str2 != NULL);
//
//	char* temp1 = str1;
//	char* temp2 = str2;
//	char* s = NULL;
//	//当要判断的字符串为空的时候那么就直接返回空
//	if (*str2 == '\0')
//	{
//		return NULL;
//	}
//	while (*temp1)
//	{
//		//将临沭字符指向当前要开始遍历的字符处
//		s = temp1;
//		//每次循环都重新将temp2指向str2的字符初始处
//		temp2 = str2;
//		//找到s指向的字符处和temp2指向的字符处字符一致，然后进行str2长度的字符判断
//		while (*s == *temp2 && *s && *temp2)
//		{
//			s++;
//			temp2++;
//		}
//		//如果相等的话那么temp一定走到了str2的末尾
//		//进行temp判断
//		if (*temp2 == '\0')
//		{
//			return temp1;
//		}
//		//否则的话，就继续往后进行字符匹配
//		temp1++;
//	}
//}

//#include<assert.h>
//void assert(int expression);
//当程序运行时，如果 expression 的值为假（即 0），assert() 函数会向标准错误输出一条出错信息
//然后通过调用 abort() 终止程序运行。如果 expression 的值为真，assert() 函数不做任何操作

//char* strcat(char* dest,const char* src)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	char* ret = dest;
//	while (*dest)
//	{
//		dest++;
//	}
//	while (*src)
//	{
//		*dest++ = *src++;
//	}
//	return ret;
//}

//int strcmp(const char* str1, const char* str2)
//{
//	assert(str1 != NULL);
//	assert(str2 != NULL);
//	while (*str1 == *str2)
//	{
//		if (*str2 == '\0')
//			return 0;
//		str1++;
//		str2++;
//	}
//	if (*str1 > *str2)
//	{
//		return 1;
//	}
//	else if (*str1 < *str2)
//	{
//		return -1;
//	}
//}
//
//char* strcmp(char* dest, const char* src)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	char* ret = dest;
//	while (*dest++ = *src++)
//	{
//		;
//	}
//	return ret;
//}


//void* memmove(void* dest, const void* src,size_t num)
//{
//	assert(dest != NULL);
//	assert(src != NULL);
//	void* temp = dest;
//	// dest src
//	if (dest <= src || (char*)dest >= (char*)src + num)
//	{
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest + 1;
//			src = (char*)src + 1;
//		}
//	}
//	//当src小于dest的时候，就得从后往前进行字符替换
//	else
//	{
//		dest = (char*)dest + num - 1;
//		src = (char*)src + num - 1;
//		while (num--)
//		{
//			*(char*)dest = *(char*)src;
//			dest = (char*)dest - 1;
//			src = (char*)src - 1;
//		}
//	}
//	return temp;
//}

//void memcpy(void* dest, const void* src,size_t num)
//{
//	void* ret = dest;
//	assert(dest != NULL);
//	assert(src != NULL);
//	while (num--)
//	{
//		*(char*)dest = *(char*)src;
//		dest = (char*)dest + 1;
//		src = (char*)src + 1;
//	}
//	return ret;
//}


//#include<stdio.h>
//int main()
//{
//	unsigned char pub[4];
//	struct A
//	{
//		unsigned char a;
//		unsigned char b : 1;
//		unsigned char c : 2;
//		unsigned char d : 3;
//	}*ptr;
//	ptr = (struct A*)pub;
//	memset(pub, 0, 4);
//	ptr->a = 2;
//	ptr->b = 3;
//	ptr->c = 4;
//	ptr->d = 5;
//	printf("%02x %02x %02x %02x\n", pub[0], pub[1], pub[2], pub[3]);
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	FILE* pf;
//	pf = fopen("hello.txt", "wb");
//	fputs("This is an apple.", pf);
//	fseek(pf, 9, SEEK_SET);
//	fputs(" sam", pf);
//	fclose(pf);
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	FILE* pf;
//	long len = 0;
//	pf = fopen("hello.txt", "rb");
//	if (pf == NULL)
//	{
//		perror("Error opening file.");
//	}
//	fseek(pf, 0, SEEK_END);
//	len = ftell(pf);
//	fclose(pf);
//	printf("the size of file:%ld bytes.\n", len);
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int n;
//	FILE* pf;
//	char buf[27];
//	pf = fopen("hello.txt", "w+");
//	if (pf == NULL)
//	{
//		perror("Error opening file.");
//	}
//	for (n = 'A'; n <= 'Z'; ++n)
//	{
//		fputc(n, pf);
//	}
//	//将文件指针回到文件的起始位置
//	rewind(pf);
//	fread(buf, 1, 26, pf);
//	fclose(pf);
//	buf[26] = '\0';
//	puts(buf);
//	return 0;
//}
//
//#include<stdio.h>
//#include<stdlib.h>
//int main()
//{
//	int c;
//	FILE* pf = fopen("hello.txt", "r");
//	if (pf == NULL)
//	{
//		perror("Error opening file.");
//		return EXIT_FAILURE;
//	}
//	//fgetc:当读取失败的时候或者遇到文件结束的时候，都会返回EOF
//	//标准C I/O循环读取文件
//	while ((c = fgetc(pf)) != EOF)
//	{
//		putchar(c);
//	}
//	//对结束原因进行判断
//	if (ferror(pf))
//	{
//		puts("文件读取错误.");
//	}
//	else if (feof(pf))
//	{
//		puts("成果读取到文件的末尾而结束.");
//	}
//	fclose(pf);
//	return 0;
//}

//#include<stdio.h>
//enum { SIZE = 5 };
//int main(void)
//{
//	double a[SIZE] = { 1.0,2.0,3.0,4.0,5.0 };
//	double b = 0.0;
//	size_t ret_code = 0;
//
//	FILE* fp = fopen("hello.bin", "wb"); // 必须用二进制模式
//	fwrite(a, sizeof(*a), SIZE, fp); // 写 double 的数组
//	fclose(fp);
//
//	fp = fopen("hello.bin", "rb");
//	// 读 double 的数组
//	while ((ret_code = fread(&b, sizeof(double), 1, fp)) >= 1)
//	{
//		printf("%lf\n", b);
//	}
//	if (feof(fp)) 
//	{
//		printf("Error reading hello.bin: unexpected end of file\n");
//	}
//	else if (ferror(fp)) 
//	{
//		perror("Error reading hello.bin");
//	}
//	fclose(fp);
//	fp = NULL;
//	return 0;
//}

//#include<stdio.h>
//void main() 
//{
//	int a;
//	float b;
//	double c;
//	scanf("%d %f %lf", &a, &b, &c);
//	printf("%d %f %lf", a, b, c);
//	return 0;
//}

//#include <stdio.h>
//#include <stdlib.h>
//#include <math.h>
//int main()
//{
//	int n;
//	int temp;
//	int sz[32];
//	while (scanf_s("%d", &n) != EOF)
//	{
//		int arr[1000] = { 0 };
//		int i = 0;
//		for (i = 0; i < n; i++) 
//		{
//			scanf_s("%d", &arr[i]);
//			int sum = 0;
//			int j = 0;
//			while (arr[i]) 
//			{
//				sum += pow(2, j) * (arr[i] % 10);
//				arr[i] = arr[i] / 10;
//				j++;
//			}
//			printf("%d\n", sum);
//		}
//
//
//	}
//	return 0;
//}


//#include<stdio.h>
//int main()
//{
//	int n;
//	scanf("%d", &n);
//	while (n--)
//	{
//		int b;
//		scanf("%d", &b);
//		printf("%d", b);
//	}
//	return 0;
//}

//本题要求实现一个函数，统计给定区间内的三位数中有两位数字相同的完全平方数（如144、676）的个数。
//函数接口定义：
//int search(int n);
//其中传入的参数int n是一个三位数的正整数（最高位数字非0）。函数search返回[101, n]区间内所有满足条件的数的个数。
//裁判测试程序样例：


//int func(int N)
//{
//
//	int ret = N;
//	int a[10];//定义存储每一位数字的数组
//	int i = 0;
//	int j = 0;
//	while (ret >= 10)
//	{
//		a[j++] = ret % 10;//将除最高位之外的每一位数字存储到数组中
//		ret = ret / 10;
//	}
//	a[j] = ret;//最后将最高的那位数字存储到数组中
//	int x = (int)sqrt(N);//将N开平方时候强转成int
//	int y = x * x;//计算x的平方
//	for (int m = 0; m <= j; m++)
//	{
//		for (int n = m + 1; n <= j; n++)
//		{
//			//两次循环数组，注意n是从m+1开始的，也就是只与其后边的比较即可
//			if (a[m] == a[n])//如果出现了一组相同的数字
//			{
//				if (y == N)//然后判断N是否是完全平方数
//				{
//					return 1;//直接就返回1
//				}
//				else
//				{
//					return 0;
//				}
//			}
//		}
//	}
//	return 0;
//}
//int search(int n)
//{
//	int count = 0;
//	for (int i = 101; i < n; ++i)
//	{
//		if (func(i))
//		{
//			count++;
//		}
//	}
//	return count;
//}

//#include<stdio.h>
//#include<math.h>
//int search(int n)
//{
//	int count = 0;
//	int m;
//	for (int i = 101; i <= n; i++)
//	{
//		int g, s, b;
//		g = i % 10;
//		s = (i / 10) % 10;
//		b = i / 100;
//		if ((g == s && g != b) || (g == b && g != s) || (s == b && s != g))
//		{
//			m = (int)sqrt(i);
//			if (m*m == i)
//			{
//				count++;
//			}
//		}
//	}
//	return count;
//}
//int main()
//{
//	int number;
//	
//	scanf("%d", &number);
//	printf("count=%d\n", search(number));
//
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int num;
//	while (1)
//	{
//		//输入一个数据 输出它对应的ASCII码值
//		scanf("%d", &num);
//		printf("%c", num);
//	}
//	return 0;
//}

//

//#include<stdio.h>
//int main()
//{
//	int N,M;
//	scanf("%d", &N);
//	int sum = 0;
//	for (int i = 1; i <= N; i++)
//	{
//		scanf("%d", &M);
//		sum = 0;
//		for (int j = 1; j <= M; j++)
//		{
//			int p;
//			scanf("%d", &p);
//			sum = sum + p;
//			printf("%d", sum);
//		}
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int n = 0;
//	scanf("%d", &n);
//	int sum = 0;
//	int p;
//	while (n)
//	{
//		int m = 0;
//		scanf("%d", &m);
//		sum = 0;
//		for (int i = 0; i < m; ++i)
//		{
//			scanf("%d", &p);
//			sum += p;
//		}
//		printf("%d\n", sum);
//	}
//	return 0;
//}

//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int x1 = 0;
//	int y1 = 0;
//	int x2 = 0;
//	int y2 = 0;
//	scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
//	while (1)
//	{
//		if (x1 == 0 && x2 == 0 && y1 == 0 && y2 == 0)
//		{
//			break;
//		}
//		double s = sqrt((x1 - x2) * (x1 - x2) * 1.0 + (y1 - y2)*(y1 - y2) * 1.0);
//		printf("%.2f", s);
//		//再将下一组数据给获取一下
//		scanf("%d%d%d%d", &x1, &y1, &x2, &y2);
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int arr[4] = { 0 };
//	scanf("%d%d%d%d", &arr[0], &arr[1], &arr[2], &arr[3]);
//	while (1)
//	{
//		if (arr[0] == 0 && arr[1] == 0 && arr[2] == 0 && arr[3] == 0)
//		{
//			break;
//		}
//
//		for (int i = 0; i < 4; ++i)
//		{
//			for (int j = i + 1; j < 4; ++i)
//			{
//				if (arr[i] > arr[j])
//				{
//					//进行两数据的交换
//					int temp = arr[i];
//					arr[i] = arr[j];
//					arr[j] = temp;
//				}
//			}
//		}
//		//打印排序好的数据
//		for (int i = 0; i < 4; ++i)
//		{
//			printf("%d ", arr[i]);
//		}
//		//输入下组数据
//		scanf("%d%d%d%d", &arr[0], &arr[1], &arr[2], &arr[3]);
//	}
//	return 0;
//}

//#include<stdio.h>
//union Date
//{
//	struct 
//	{
//		unsigned char day;
//		unsigned char month;
//		unsigned short year;
//	};
//	unsigned int value;
//};
//int cmp(union Date d1, union Date d2)
//{
//	if (d1.value > d2.value)
//	{
//		return 1;
//	}
//	else if (d1.value < d2.value)
//	{
//		return -1;
//	}
//	else
//	{
//		return 0;
//	}
//	return -2;
//}
//int main()
//{
//	union Date d1 = { 1,2,2023 };
//	union Date d2 = { 2,1,2022 };
//
//	union Date d3 = { 20,1,1 };
//	printf("%d\n", d3.value);
//
//	printf("%p\n", &d1);
//	printf("%p\n", &d1.day);
//	printf("%d\n", d1.value);
//
//	printf("%p\n", &d2);
//	printf("%p\n", &d2.day);
//	printf("%d\n", d2.value);
//
//	int sum = cmp(d1, d2);
//	printf("%d", sum);
//
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int sum = 0;
//	//循环从2开始 因为1的因子是它本身 不是真因子
//	for (int i = 2; i < 1000; ++i)
//	{
//		//判断i是否是完数
//		int num = 0;
//		for (int j = 1; j <i; ++j)
//		{
//			if (i % j == 0)
//			{
//				num = num + j;
//			}
//		}
//		//如果i的所有因子之和等于i的话 那么i就是完数 那么就计算进sum之中
//		if (num == i)
//		{
//			sum += i;
//		}
//
//	}
//	printf("%d", sum);
//	return 0;
//}

//
//#include<stdio.h>
//int main()
//{
//	printf("%-7d\n", 10);
//	printf("%07d\n", 10);
//
//	printf("%4.3f", 3.1415);
//	printf("\n");
//	printf("%-4.3f",3.1415);
//	return 0;
//}

//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char arr[] = { 'b','i','t' };
//	printf("%d", strlen(arr));
//	return 0;
//}

//#include<stdio.h>
//#include<string.h>
//char a[128];
//int main() 
//{
//	int len, n = 0;
//	double score = 0.0;
//	while (gets(a)) 
//	{
//		len = strlen(a);
//		for (int i = 0; i < len; i += 2) 
//		{
//			switch (a[i]) 
//			{
//			case 'A':
//				score += 4.0; 
//				break;
//			case 'B':
//				score += 3.0; 
//				break;
//			case 'C':
//				score += 2.0; 
//				break;
//			case 'D':
//				score += 1.0; 
//				break;
//			case 'F':
//				score += 0.0; 
//				break;
//			default: 
//				n++; break;
//			}
//		}
//		n ? printf("Unknown\n") : printf("%.2f\n", score / (double)((len + 1) / 2));
//		n = 0;
//		score = 0.0;
//	}
//	return 0;
//}

//#include<stdio.h>
//void change(int* p)
//{
//	*p = *p / 2;
//}
//int main()
//{
//	int i;
//	int* p;
//	scanf("%d", &i);
//	p = &i;
//	change(p);
//	printf("%d", i);
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int i = 1;
//	sizeof(i++);
//	printf("%d\n", i);
//	return 0;
//}
//
//#include<stdio.h>
//#include<string.h>
//int validateData(char str[])
//{
//	int i, flag = 0;
//	for (i = 0; str[i] != '\0' && flag == 0; i++) {
//		if (str[i] >= '0' && str[i] <= '9' || str[i] >= 'a' && str[i] <= 'z' || str[i] >= 'A' && str[i] <= 'Z')
//			flag = 0;
//		else
//			flag = 1;
//	}
//	if (flag == 0)
//		return 1;
//	return 0;
//}
//
//void splitData(char str1[][1000], char str[])
//{
//	int i, j, k = 0;
//	for (i = 0; str[k] != '\0'; i++) {
//		for (j = 0; str1[i][j] != ' '; j++) {
//			str1[i][j] = str[k];
//			k++;
//		}
//		str1[i][j] = ' ';
//		k++;
//	}
//}
//
//void showReverseData(char str1[][1000])
//{
//	int len = sizeof(str1) / sizeof(str1[0]);
//	for (int i = len - 1; i >= 0; i--) {
//		for (int j = 0; str1[i][j] != ' '; j++) {
//			printf("%c", str1[i][j]);
//		}
//		printf(" ");
//	}
//}
//
//int main()
//{
//	char str[1000], str1[1000][1000];
//	gets(str);
//	int op = validateData(str);
//	if (op == 1) 
//	{
//		splitData(str1, str);
//		showReverseData(str1);
//	}
//	else
//		printf("Wrong Format");
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int a, b, c;
//	int t;
//	scanf("%d %d %d", &a, &b, &c);
//	//a c b
//	if (a > c)
//	{
//		t = c;
//		c = a;
//		a = t;
//	}
//	if (b > c)
//	{
//		t = c;
//		c = b;
//		b = t;
//	}
//	if (a + b <= c)
//		printf("error");
//	else if (a * a + b * b == c * c)
//		printf("%d", 0.5 * a * b);
//	else if (a == c && b == c)
//		printf("%d", 3 * a);
//	else
//		printf("other");
//	return 0;
//
//}


//#include<stdio.h>
//#include<math.h>
//int main()
//{
//	int a, b, c;
//	scanf("%d %d %d", &a, &b, &c);
//	if (a + b > c && a + c > b && b + c > a)
//	{
//		double s, ar;
//		s = (double)(a + b + c) / 2.0;
//		ar = (double)sqrt(s * (s - a) * (s - b) * (s - c));
//		printf("area = %.2f; perimeter = %.2f", ar, s);
//	}
//	else 
//	{
//		printf("These sides do not correspond to a valid triangle");
//	}
//	return 0;
//}

//#include<stdio.h>
//int main() 
//{
//	int h;
//	//输入要求第几项
//	scanf("%d", &h);
//	float a = 0;//表示第三项的分子
//	float b = 3;//表示第一项的分母
//	float s = 0;
//	float temp = 2;
//	float ret = 2;//将第一项的分子给保存上
//	for (int i=1;i<h;++i) 
//	{
//		//处理第一项
//		if (i == 1)
//		{
//			s += temp / b;
//			temp = 3;
//			b += 2;
//			continue;
//		}
//		//处理第二项
//		if (i == 2)
//		{
//			s = (double)2 / 3;
//			s += temp / b;
//			b += 2;
//			continue;
//		}
//		//i>=3的情况
//		s += a / b;
//		a = temp + ret;
//		ret = temp;
//		temp = a;
//		b += 2;
//		
//	}
//	printf("%.2f", s);
//	return 0;
//}

//#include<stdio.h>
//int scan_pint(void)
//{
//	int tmp;
//	do {
//		printf("请输入一个正整数：");
//		scanf("%d", &tmp);
//		if (tmp <= 0)
//			puts("请不要输入非正整数。");
//	} while (tmp > 0);
//	return tmp;
//}
//int rev_int(int num)
//{
//	int tmp = 0;
//	if (num > 0) {
//		do {
//			tmp = tmp * 10 + num % 10;
//			num /= 10;
//		} while (num > 0);
//	}return tmp;
//}
//int main(void)
//{
//	int nx = scan_pint();
//	printf("%d\n", rev_int(nx));
//	return 0;
//}

//
//#include<stdio.h>
//int main()
//{
//	int N;
//	int a, b, c, m;
//	int n[101] = { 0 };
//
//	scanf("%d", &N);
//
//	for (a = 0; a < N; a++)
//	{
//		scanf("%d", &b);
//		for (c = 0; c < b; c++)
//		{
//			scanf("%d", &m);
//			n[m]++;
//		}
//	}
//	int t = n[0];
//	int p = 0;
//	for (m = 1; m < 101; m++)
//	{
//
//		if (n[m] >= t)
//		{
//			t = n[m];
//			p = m;
//		}
//	}
//	printf("%d %d", p, n[p]);
//	return 0;
//
//}


//#include<stdio.h>
//int main()
//{
//	int n;
//	scanf_s("%d", &n);
//	float sum = 0;
//	if (n == 1)
//	{
//		sum = 2 * 1.0 / 3;
//		printf("%.2f\n", sum);
//	}
//	else if (n == 2)
//	{
//		sum = 2 * 1.0 / 3 + 3 * 1.0 / 5;
//		printf("%.2f\n", sum);
//	}
//	else
//	{
//		float a1 = 2;
//		float a2 = 3;
//		float b1 = 7;
//		sum = 2 * 1.0 / 3 + 3 * 1.0 / 5;
//		for (int i = 3; i <= n; ++i)
//		{
//			sum += (a1 + a2) * 1.0 / b1;
//
//			float temp = a1;
//			a1 = a2;
//			a2 = temp + a1;
//			b1 += 2;
//		}
//		printf("%.2f", sum);
//	}
//
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int n, r;
//	int i, m;
//	int a = 0;
//	scanf("%d%d", &n, &r);
//	for (i = 1; i <= n; i++)
//	{
//		m = n % i;
//		{
//			if (m == r)
//				a += 1;
//		}
//	}
//	printf("%d", a);
//}

//
//#include<stdio.h>
//main()
//{
//	int a = 2;
//	int b = 5;
//	printf("a=%%d,b=%%d\n", a, b);
//	//return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int n, t=0, m=0, i;
//	int a[1000] = { 0 };
//	int b[1000] = { 0 };
//	scanf("%d", &n);
//	for (i = 0; i < n; i++)
//	{
//		scanf("%d", &a[i]);
//	}
//	for (i = 0; i < n; i++)
//	{
//		{
//			if (a[i + 1] == a[i])
//			{
//				t = t + a[i];
//			}
//			else
//			{
//				t = t + a[i];
//				b[i] = t;
//				t = 0;
//			}
//		}
//	}
//
//	for (i = 0; i < n; i++)
//	{
//		if (b[i] > m)
//		{
//			m = b[i];
//		}
//	}
//	printf("%d", m);
//	return 0;
//}

//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//int main()
//{
//	char x[32];
//	int i = 0;
//	int a = 1;
//	int sum = 0;
//	fgets(x, sizeof(x), stdin);
//	while (i < strlen(x) - 1)
//	{
//		if (x[i] == 'a' || x[i] == 'e' || x[i] == 'i' || x[i] == 'o' || x[i] == 'u' || x[i] == 'A' || x[i] == 'E' || x[i] == 'I' || x[i] == 'O' || x[i] == 'U')
//		{
//			sum += a;
//		}
//		i++;
//	}
//	printf("%d", sum);
//
//
//	return 0;
//}

//#include <stdio.h>
//int main()
//{
//	long long x;
//	long long a;
//	int sz[10] = { 0 };
//	scanf("%lld", &x);
//	while (x > 0)
//	{
//		a = x;
//		a %= 10;
//		sz[a]++;
//		x /= 10;
//
//	}
//	for (int i = 0; i < 10; i++)
//		printf("%3d", i);
//	printf("\n");
//	for (int m = 0; m < 10; m++)
//		printf("%3d", sz[m]);
//	printf("\n");
//	return 0;
//}

//#include<stdio.h>
//#include<string.h>
//int main()
//{
//	char str1[20]; char str2[20]; char str3[20]; char str4[20]; char str5[20];
//	char str[5][20]; char str0[20];
//	gets(str1); gets(str2); gets(str3); gets(str4); gets(str5);
//	strcpy(str[0], str1); strcpy(str[1], str2); strcpy(str[2], str3); strcpy(str[3], str4); strcpy(str[4], str5);
//	for (int i = 0; i < 5; ++i)
//	{
//		for (int j = i+1; j < 5; ++j)
//		{
//			if (strlen(str[i]) > strlen(str[j]))
//			{
//				strcpy(str0, str[i]);
//				strcpy(str[i], str[j]);
//				strcpy(str[j], str0);
//			}
//		}
//	}
//	printf("\n");
//	for (int m = 4; m >=0; --m)
//	{
//		int len = strlen(str[m]);
//		for (int n = len - 1; n >= 0; --n)
//		{
//			printf("%c", str[m][n]);
//		}
//		printf("\n");
//	}
//	return 0;
//}



//#include<stdio.h>
//int main()
//{
//	int a, b;
//	scanf("%d %d", &a, &b);
//	int y = 0;
//	for (int x = a; x <= b; ++x)
//	{
//		y = x * x;
//		if (y == x)
//		{
//			printf("%d\n", x);
//		}
//		//i=25 y=625
//		else
//		{
//			int cn = 1;
//			int ret = 0;
//			int temp = 1;
//			while (y >=10)
//			{
//				temp = y % 10;
//				ret = temp * cn + ret;
//				cn *= 10;
//				y /= 10;
//				if (ret == x)
//				{
//					printf("%d\n", x);
//				}
//			}
//		}
//	}
//	return 0;
//}
//#include<stdio.h>
//int main()
//{
//	int a, b;
//	scanf("%d%d", &a, &b);
//	for (int i = a; i <= b; ++i)
//	{
//		if (i % 400 == 0 || (i % 100 != 0 && i % 4 == 0))
//		{
//			printf("%d",i);
//			break;
//		}
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int x = 10; int y = 10;
//	for (int i = 0; x > 8; y = ++i)
//	{
//		printf("%d %d", x--, y);
//	}
//	return 0;
//}\

//#include<stdio.h>
//int fun(int x)
//{
//	if ((x % 4 == 0 && x % 100 != 0) || (x % 400 == 0))
//	{
//		return 1;
//	}
//	return 0;
//}
//int main()
//{
//	int year = 0;
//	scanf("%d", &year);
//	if (fun(year))
//	{
//		printf("是闰年");
//	}
//	else
//	{
//		printf("不是闰年");
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int arr[3] = { 0 };
//	for (int k = 0; k < 3; ++k)
//	{
//		scanf("%d", &arr[k]);
//	}
//	for (int i=0;i<3;++i)
//	{
//		for (int j = i + 1; j < 3; ++j)
//		{
//			if (arr[j] < arr[i])
//			{
//				int ret = arr[i];
//				arr[i] = arr[j];
//				arr[j] = ret;
//			}
//		}
//	}
//	for (int m = 0; m < 3; ++m)
//	{
//		printf("%d ", arr[m]);
//	}
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	int a = 0;
//	int b = 2;
//	if (a == 1)
//		if (b == 2)
//			printf("1\n");
//		else
//			printf("2\n");
//	return 0;
//}

//#include<stdio.h>
//int main()
//{
//	//long long f[1000000] = { 0 };
//	long long f1 = 1; long long f2 = 1; long long f = 0;
//	long long i = 0;
//	// f1 f2 f3
//	long long a = 0;long long b = 0;long long n = 0;
//	scanf("%lld%lld%lld", &a, &b, &n);
//	while (1)
//	{
//		if (a == 0 && b == 0 && n == 0)
//		{
//			break;
//		}
//		if (n == 1 || n == 2)
//		{
//			printf("%lld\n", f1);
//		}
//		for(i=3;i<n+1;++i)
//		{
//			long long f= (a * f1 + b * f2) % 7;
//			printf("%lld\n", f);
//			f1 = f2;
//			f2 = f;
//		}
//		scanf("%lld%lld%lld", &a, &b, &n);
//	}
//	return 0;
//}
//
//#include<stdio.h>
//double CancleFee(double price)
//{
//	double s, a;
//	s = price * 0.05;
//	a = s - (int)(s)*1.0;
//	if (a < 0.25) {
//		s = s - a;
//	}
//	else if (a > 0.25 && a < 0.75) {
//		s = s - a + 0.5;
//	}
//	else if (a > 0.75) {
//		s = s - a + 1.0;
//	}
//	return s;
//}
//int main() {
//	double p = 0;
//	scanf("%lf", &p);
//	while (1) 
//	{
//
//		double t = CancleFee(p);
//		printf("%.1f\n", t);
//		scanf("%lf", &p);
//	}
//	return 0;
//}
//int main()
//{
//	double sin = 0;
//	double t = 1;
//	int fu = -1;
//
//	float x = 0;
//	int n = 0;
//	scanf("%f %d", &x, &n);
//
//	for (int i = 1; i <= 2 * n - 1; i = i + 2)
//	{
//		t = 1;
//		fu *= -1;
//		for (int j = 1; j <= i; ++j)
//		{
//			t *= j;
//		}
//		sin += fu * pow(x, i) / t;
//	}
//	printf("%.7f", sin);
//	return 0;
//}

//#include <stdio.h>
//int main() 
//{
//	int a = 1, b = 1, c = 1;
//
//	while (scanf("%d %d %d", &a, &b, &c) != EOF) 
//	{
//		if (a + b > c && a + c > b && b + c > a) 
//		{
//			if (a == b || b == c || a == c) 
//			{
//				if (a == b && b == c)
//				{
//					printf("等边\n");
//					break;
//				}
//				printf("等腰\n");
//			}
//			else 
//			{
//				printf("普通\n");
//			}
//		}
//		else 
//		{
//			printf("不是\n");
//		}
//	}
//	return 0;
//}

//
//#include<stdio.h>
//int main() {
//	int a[2000], n = 0, sum1 = 0, sum2 = 0;
//	while (1)
//	{
//		scanf("%d", &a[n]);
//		sum1 = sum1 + a[n];
//
//		n++;
//		if (getchar() == '\n')
//		{
//			break;
//		}
//	}
//
//	for (int j = 0; j < n + 1; j++) {
//		sum2 = sum2 + j;
//	}
//	printf("%d", sum2 - sum1);
//	return 0;
////}
//#include<stdio.h>
//int main()
//{
//	int i, j;
//	for (i = 100; i < 200; ++i)
//	{
//		for (j = 2; j < i; ++j)
//		{
//			if (i % j == 0)
//				break;
//		}
//		if (j >= i)
//		{
//			printf("%d ", i);
//		}
//	}
//	return 0;
//}

//#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//#include<string.h>
//void upCopy(char* new, char* old) {
//	int i, j = 0;
//	for (i = 0; *(old + i) != '\0'; i++)
//	{
//		if (*(old + i) >= 'A' && *(old + i) <= 'Z');
//		{
//			*(new + j) = *(old + i);
//			j++;
//		}
//		*(new + j) = '\0';
//	}
//}
//int main()
//{
//	char chs1[10] = "hELa";
//	char chs2[10] = { 0 };
//	upCopy(chs2, chs1);
//	printf("复制之前的字符串为：");
//	puts(chs1);
//	printf("复制之后的字符串为：");
//	puts(chs2);
//	return 0;
//}

//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include<math.h>
//int main()
//{
//	int n;
//	scanf("%d", &n);
//	while (n--)
//	{
//		char str[32] = { 0 };
//		int i = 0;
//		while (i<32)
//		{
//			char c;
//			scanf("%c", &c);
//			if (c == '\n')
//			{
//				break;
//			}
//			str[i] = c;
//			++i;
//		}
//		int len = strlen(str);
//		int sum = 1;
//		for (int j = len - 1; i >= 0; --j)
//		{
//			int ret = str[i] * pow(2, len - 1 - j);
//			sum += ret;
//		}
//		printf("%d\n", sum);
//	}
//	return 0;
//}
//
//#include<stdio.h>
//int main()
//{
//	int i, j;
//	int a[3][3] = { {1,2,3},{4,5,6}, {7,8,9} };
//
//
//	printf("before:\n");
//	for (i = 0; i < 3; ++i)
//	{
//		for (j = 0; j < 3; ++j)
//		{
//			printf("%d ", a[i][j]);
//		}
//		printf("\n");
//	}
//	for (i = 0; i < 3; ++i)
//	{
//		for (j = 0; j < i; ++j)
//		{
//			int ret = a[i][j];
//			a[i][j] = a[j][i];
//			a[j][i] = ret;
//		}
//	}
//	printf("after:\n");
//	for (i = 0; i < 3; ++i)
//	{
//		for (j = 0; j < 3; ++j)
//		{
//			printf("%d ", a[i][j]);
//		}
//		printf("\n");
//	}
//}

//#define _CRT_SECURE_NO_WARNINGS
//#include<stdio.h>
//
//void tiao();
//void avg();
//void print();
//char name[5][10];
//double score[5][5] = { 0 };
//double a[5][5] = { 0 };
//double aver[5] = { 0 };
//int i, j;
//
//int main()
//{
//    for (i = 0; i < 5; i++)
//    {
//        //输入人的姓名
//        scanf("%s", name[i]);
//        for (j = 0; j < 5; j++)
//        {
//            //
//            scanf("%lf", &score[i][j]);
//        }
//    }
//    
//    for (i = 0; i < 5; i++)
//    {
//        for (j = 0; j < 10; j++)
//        {
//            a[i][j] = score[i][j];
//        }
//    }
//    tiao();
//    avg();
//    print();
//    return 0;
//}
//void tiao()
//{
//    int t = 0;
//    double k;
//    for (i = 0; i < 5; i++)
//    {
//        for (t = 0; t < 5; t++)
//        {
//            for (j = 0; j < 4 - t; j++)
//            {
//                if (a[i][j + 1] > a[i][j]) {
//                    k = a[i][j + 1];
//                    a[i][j + 1] = a[i][j];
//                    a[i][j] = k;
//                }
//            }
//        }
//    }
//}
//void avg()
//{
//    double sum = 0;
//    for (i = 0; i < 5; i++)
//    {
//        sum += a[i][1] + a[i][2] + a[i][3];
//        aver[i] = sum / 3;
//        sum = 0;
//    }
//}
//void print()
//{
//    double t1, t2;
//    int n;
//    char m;
//    for (i = 0; i < 5 - 1; i++)
//    {
//        for (j = 0; j < 4 - i; j++)
//        {
//            if (aver[j + 1] > aver[j])
//            {
//                t1 = aver[j];
//                aver[j] = aver[j + 1];
//                aver[j + 1] = t1;
//                for (n = 0; n < 10; n++)
//                {
//                    m = name[j][n];
//                    name[j][n] = name[j + 1][n];
//                    name[j + 1][n] = m;
//                }
//                for (n = 0; n < 5; n++)
//                {
//                    t2 = score[j][n];
//                    score[j][n] = score[j + 1][n];
//                    score[j + 1][n] = t2;
//                }
//            }
//        }
//    }
//    for (i = 0; i < 5; i++)
//    {
//        printf("%10s %6.1f %6.1f %6.1f %6.1f %6.1f %6.2f %5d\n", name[i], score[i][0], score[i][1], score[i][2], score[i][3], score[i][4], aver[i], i + 1);
//    }
//}

//#include<stdio.h>
//#define N 5
//void find_two_largest(int a[], int n, int* largest, int* second_largest)
//{
//	//数据排序
//	for (int i = 0; i < n-1; i++)
//	{
//		for (int j = i+1; j < n; j++)
//		{
//			if (a[i] < a[j])
//			{
//				int temp = a[i];
//				a[i] = a[j];
//				a[j] = temp;
//			}
//		}
//	}
//	*largest = a[0];
//	*second_largest = a[1];
//	if (a[0] == a[1])
//	{
//		int k = 1;
//		while (a[k] == a[1])
//		{
//			k++;
//		}
//		*second_largest = a[k];
//	}
//	return;
//}
//
//int main()
//{
//	int a[N] = { 0 };
//	//int len = 0;
//	int largest;
//	int second_largest;
//	//scanf("%d", &len);
//	for (int i = 0; i < N; i++)
//	{
//		scanf("%d", &a[i]);
//	}
//	find_two_largest(a, N, &largest, &second_largest);
//	printf("%d %d\n", largest, second_largest);
//	return 0;
//}

#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>
#include"game.h"
void game()
{
	char a[rs][cs] = { 0 };
	char b[rs][cs] = { 0 };
	char e[rs][cs] = { 0 };
	kkk(a, rs, cs, '0');
	kkk(b, rs, cs, '*');
	le(a);
	uuu(a, e);
	print(b);
	nnn(a, b, e);
}
void menu()
{
	while (1)
	{
		printf("*************扫雷游戏**************\n");
		printf("***********************************\n");
		printf("************* 1.play **************\n");
		printf("************* 0.exit **************\n");
		printf("***********************************\n");
		printf("请选择->>:\n");
		int a;
		scanf_s("%d", &a);
		if (a == 0)
		{
			printf("游戏已退出");
			break;
		}
		else if (a == 1)
		{
			printf("游戏开始\n");
			game();
		}
		else
		{
			printf("输入错误，请重新选择\n");
		}
	}
}
int main()
{
	menu();
	return 0;
}