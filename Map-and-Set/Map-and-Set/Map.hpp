#pragma once 

#include"RBTree.hpp"
namespace ma
{
	template<class K,class V>
	class map
	{
		typedef pair<K, V> KV;
		struct KOFKV
		{
			const K& operator()(const KV& data)const
			{
				return data.first;
			}
		};
		typedef RBTree<KV, KOFKV> RBT;

	public:
		//此处需加上typename
		//因为静态成员变量是通过 类名::静态成员方式来访问，故编译器不确定iterator是否是RBT中的类型
		//而typename明确地告诉编译器iterator是RBT中的一个类型，而不是静态成员变量
		typename typedef  RBT::iterator iterator;
	public:
		map()
			:_t()
		{}

		//Iterator
		iterator begin()
		{
			return _t.Begin();
		}
		iterator end()
		{
			return _t.End();
		}
		//Capacity
		bool empty()const
		{
			return _t.Empty();
		}
		size_t size()const
		{
			return _t.Size();
		}
		//Element access
		V& operator[](const K& x)
		{
			//make_pair(x,V()) 构造默认的键值对
			return (*(insert(make_pair(x, V())).first)).second;
		}

		pair<iterator, bool> insert(const KV& data)
		{
			return _t.Insert(data);
		}
		iterator find(const K& data)
		{
			return _t.Find(KV(data, V()));
		}
		void swap(map<K, V>& m)
		{
			_t.Swap(m._t);
		}
		void clear()
		{
			_t.Clear();
		}
	private:
		RBT _t;
	};
}
//map测试函数
void TestMap()
{
	cout << "map:" << endl;
	ma::map<string, string> m;
	m.insert(make_pair("apple", "苹果"));
	m.insert(make_pair("orange", "橙子"));
	m.insert(make_pair("pear", "鸭梨"));
	m.insert(make_pair("watermelon", "西瓜"));

	cout << m.size() << endl;
	cout << m["orange"] << endl;
	auto it = m.begin();
	while (it != m.end())
	{
		cout << it->first << "---->" << it->second << endl;
		++it;
	}
	cout << endl;

	if (m.find("orange") != m.end())
	{
		cout << "orange is in map." << endl;
	}
	else
	{
		cout << "orange is not in map." << endl;
	}
}