#include<iostream>
#include<string>
#include<utility> //pair的头文件
using namespace std;

#pragma once

enum Color
{
	RED,
	BLACK
};
template<class T>
class RBTreeNode
{
public:
	RBTreeNode<T>* _left;
	RBTreeNode<T>* _right;
	RBTreeNode<T>* _parent;
	T _data;
	Color _color;

	RBTreeNode(const T& data = T(), Color color = RED)  //在初始化节点的时候，将节点初始化为红色
		:_left(nullptr)
		, _right(nullptr)
		, _parent(nullptr)
		, _data(data)
		, _color(color)
	{}
};

//封装红黑树迭代器的类
template<class T,class Ref,class Ptr>
class RBTreeIterator
{
	typedef RBTreeNode<T> Node;
	typedef RBTreeIterator<T, Ref, Ptr> Self;
public:
	RBTreeIterator(Node* pNode = nullptr)
		:_pNode(pNode)
	{}

	//迭代器需有指针类似的操作
	//解引用* 和指向->
	Ref operator*()
	{
		return _pNode->_data;
	}
	Ptr operator->()
	{
		return &(operator*());
	}
	//迭代器可比较
	bool operator==(const Self& s)const
	{
		return _pNode == s._pNode;
	}
	bool operator!=(const Self& s)const
	{
		return _pNode != s._pNode;
	}
	//迭代器可移动
	//++a
	Self& operator++()
	{
		Increment();
		return *this;
	}
	//a++
	Self operator++(int)
	{
		Self temp(*this);
		Increment();
		return temp;
	}
	Self& operator--()
	{
		Decrement();
		return *this;
	}
	Self operator--(int)
	{
		Self temp(*this);
		Decrement();
		return temp;
	}
private:
	void Increment()
	{
		//迭代器进行往后移动
		//要么根节点，要么右子树最左侧的节点（右子树最小节点）
		if (_pNode->_right)
		{
			//_pNode右子树存在，到右子树中找最左侧的节点
			_pNode = _pNode->_right;
			while (_pNode->_left)
			{
				_pNode = _pNode->_left;
			}
		}
		else //若右子树存在，再往双亲里去找
		{
			Node* parent = _pNode->_parent;
			while (parent->_right == _pNode)
			{
				_pNode = parent;
				parent = _pNode->_parent;
			}
			//特殊：根节点没有右子树，迭代器恰好在根的位置
			//_pNode->_parent=头节点，而_pNode的右指针域是指向自己的
			if (_pNode->_right != parent)
			{
				_pNode = parent;
			}
		}
	}
	void Decrement()
	{
		//迭代器往前移动
		//要么根节点，要么左子树最右侧的节点（左子树最大的节点）
		//当迭代器在end的位置
		if (_pNode->_parent->_parent = _pNode && _pNode->_color == RED)
		{
			_pNode = _pNode->_right;
		}
		else if (_pNode->_left)
		{
			//_pNode左子树存在，在左子树中找最大的（最右侧节点）
			_pNode = _pNode->_left;
			while (_pNode->_right)
			{
				_pNode = _pNode->_right;
			}
		}
		else
		{
			//左子树存在的情况下再往双亲里面去找
			Node* parent = _pNode->_parent;
			while (_pNode = parent->_left)
			{
				_pNode = parent;
				parent = _pNode->_parent;
			}
			_pNode = parent;
		}
	}
private:
	Node* _pNode;
};
//规定：树中元素唯一
//红黑树封装map 和 set
//T表示往红黑树当中存储的元素类型，map中T为键值对，set中T为k值
//参数模板中加一参数KOFT：从T中将比较值K值提取出来
template<class T,class KOFT>
class RBTree
{
public:
	typedef RBTreeNode<T> Node;
	typedef RBTreeIterator<T, T&, T*> iterator;

public:
	RBTree()
	{
		_head = new Node();
		_head->_parent = nullptr;
		_head->_left = _head;
		_head->_right = _head;
		_size = 0;
	}
	~RBTree()
	{
		Destroy(_head->_parent);
	}
	
	iterator Begin()
	{
		return iterator(_head->_left);
	}
	iterator End()
	{
		return iterator(_head);
	}
	bool Empty()const
	{
		return _head->_parent == nullptr;
	}
	size_t Size()const
	{
		return _size;
	}

	//红黑树中节点的插入
	pair<iterator,bool> Insert(const T& data)
	{
		Node* newNode = nullptr;
		//1.按照二叉搜索树的方式进行节点的插入
		Node*& root = _head->_parent;
		//树为空，插入节点为树的根
		if (root == nullptr)
		{
			newNode = root = new Node(data, BLACK);
			root->_parent = _head;
		}
		else
		{
			//树非空，查找插入节点元素是否存在
			Node* cur = root;
			Node* parent = _head;
			KOFT koft;
			while (cur)
			{
				parent = cur;
				if (koft(cur->_data) <koft(data))
				{
					cur = cur->_right;
				}
				else if (koft(cur->_data)>koft(data))
				{
					cur = cur->_left;
				}
				else
				{
					return make_pair(iterator(cur),false);
				}
			}
			//进行节点的插入
			newNode = cur = new Node(data);
			if (koft(parent->_data) > koft(data))
			{
				parent->_left = cur;
			}
			else
			{
				parent->_right = cur;
			}
			cur->_parent = parent;

			//2.验证红黑树的性质若遭到破坏则对树进行调整
			//节点插入成功可能会违反不能有连在一起的红色节点
			//即新节点颜色为红色，parent颜色也为红
			while (parent != _head && parent->_color == RED)
			{
				Node* pParent = parent->_parent;

				//parent可能为pParent的左孩子或是右孩子
				if (parent == pParent->_left)
				{
					//情况一：cur为红，parent为红，pParent为黑，uncle存在且为红
					//处理：将parent和uncle颜色改为黑，将pParent颜色改为红,继续往上调整
					Node* uncle = pParent->_right;
					if (uncle && uncle->_color == RED)
					{
						parent->_color = BLACK;
						uncle->_color = BLACK;
						pParent->_color = RED;
						cur = pParent;
						parent = cur->_parent;
					}
					else
					{
						//叔叔节点不存在 || 树树节点存在且颜色为黑
						if (parent->_right == cur)
						{
							//情况三: cur为红，parent为红，pParent为黑，uncle不存在或uncle存在且为黑
							//处理：若parent为pParent的左孩子，cur为parent的右孩子，则针对parent做左单旋转
							//若parent为g的右孩子，cur为parent的左孩子，则针对parent做右单旋转，转化为情况二
							RotateLeft(parent);
							swap(parent, cur);
						}
						parent->_color = BLACK;
						pParent->_color = RED;
						RotateRight(pParent);
					}
				}
				else
				{
					Node* uncle = pParent->_left;
					if (uncle && uncle->_color == RED)
					{
						parent->_color = BLACK;
						uncle->_color = BLACK;
						pParent->_color = RED;
						cur = pParent;
						parent = cur->_parent;
					}
					else
					{
						//叔叔节点不存在 || 树树节点存在且颜色为黑
						if (parent->_left == cur)
						{
							RotateRight(parent);
							swap(cur, parent);
						}
						parent->_color = BLACK;
						pParent->_color = RED;
						RotateLeft(pParent);
					}
				}

			}
		}
		_head->_left = MostLeftNode();    //树的左子树最左侧的节点
		_head->_right = MostRightNode();  //树的右子树最右侧的节点

		//根节点必须为黑
		_head->_parent->_color = BLACK;
		++_size;
		return make_pair(iterator(newNode), true);
	}
	//查找元素
	iterator Find(const T& data)
	{
		Node* cur = _head->_parent;
		KOFT koft;
		while (cur)
		{
			if (koft(data) == koft(cur->_data))
			{
				return iterator(cur);
			}
			else if (koft(data) < koft(cur->_data))
			{
				cur = cur->_left;
			}
			else
			{
				cur = cur->_right;
			}
		}
		return End();
	}
	//清空元素
	void Clear()
	{
		Destroy(_head->_parent);
		_head->_left = _head;
		_head->_right = _head;
		_size = 0;
	}
	//节点交换
	void Swap(RBTree<T,KOFT>& t)
	{
		std::swap(_head->_parent, t._head->_parent);
	}

	void Inorder()
	{
		cout << "树的中序遍历为：";
		_Indrder(_head->_parent);
		cout << endl;
	}

	bool IsValidRBTree()
	{
		Node* root = _head->_parent;
		if (root == nullptr)
		{
			return true;
		}
		//验证性质b:根节点必须为黑色
		if (root->_color == RED)
		{
			cout << "违反了性质b:根节点颜色为黑色" << endl;
			return false;
		}
		//验证性质性质d:每条路径上黑色节点数一样

		//获取单条路径中黑色节点的个数
		size_t blackNodeCount = 0;
		Node* cur = root;
		while (cur)
		{
			if (cur->_color == BLACK)
				blackNodeCount++;
			cur = cur->_left;
		}

		//逐一与每条路径中黑色节点数进行比较
		size_t pathBlackCount = 0;
		return _IsValidRBTree(root, pathBlackCount, blackNodeCount);
	}

private:
	//树的销毁
	void Destroy(Node*& root)
	{
		if (root)
		{
			Destroy(root->_left);
			Destroy(root->_right);
			delete root;
			root = nullptr;
		}
	}
	//中序遍历
	void _Indrder(Node* root)
	{
		if (root)
		{
			_Indrder(root->_left);
			cout << root->_data << " ";
			_Indrder(root->_right);
		}
	}
	//红黑树的检测
	bool _IsValidRBTree(Node* root, size_t pathBlackCount, size_t blackNodeCount)
	{
		if (root == nullptr)
		{
			return true;
		}
		if (root->_color == BLACK)
			pathBlackCount++;

		Node* parent = root->_parent;
		if (parent != _head && parent->_color == RED && root->_color == RED)
		{
			cout << "违反了性质c:不能有连在一起的红色节点" << endl;
			return false;
		}
		if (root->_left == nullptr && root->_right == nullptr)
		{
			//说明此路径已走到尾,当前路径中黑色节点的个数为pathBlackCount
			if (pathBlackCount != blackNodeCount)
			{
				cout << "违反性质d:路径中黑色节点个数需相同" << endl;
				return false;
			}
		}
		return _IsValidRBTree(root->_left, pathBlackCount, blackNodeCount) && _IsValidRBTree(root->_right, pathBlackCount, blackNodeCount);
	}
	//左单旋
	void RotateLeft(Node* parent)
	{
		Node* subR = parent->_right;
		Node* subRL = subR->_left;

		parent->_right = subRL;
		if (subRL)
		{
			subRL->_parent = parent;
		}
		subR->_left = parent;
		Node* pParent = parent->_parent;
		parent->_parent = subR;
		subR->_parent = pParent;
		if (pParent == _head)
		{
			_head->_parent = subR;
		}
		else
		{
			if (parent == pParent->_left)
				pParent->_left = subR;
			else
				pParent->_right = subR;
		}
	}
	//右单旋
	void RotateRight(Node* parent)
	{
		Node* subL = parent->_left;
		Node* subLR = subL->_right;

		parent->_left = subLR;
		if (subLR)
		{
			subLR->_parent = parent;
		}
		subL->_right = parent;
		Node* pParent = parent->_parent;
		subL->_parent = pParent;
		parent->_parent = subL;
		if (pParent == _head)
		{
			_head->_parent = subL;
		}
		else
		{
			if (parent == pParent->_left)
				pParent->_left = subL;
			else
				pParent->_right = subL;
		}
	}

	//找最左侧的节点
	Node* MostLeftNode()
	{
		Node* root = _head->_parent;
		if (root == nullptr)
		{
			return _head;
		}
		Node* cur = root;
		while (cur->_left)
		{
			cur = cur->_left;
		}
		return cur;
	}
	//找最最右侧的节点
	Node* MostRightNode()
	{
		Node* root = _head->_parent;
		if (root == nullptr)
		{
			return _head;
		}
		Node* cur = root;
		while (cur->_right)
		{
			cur = cur->_right;
		}
		return cur;
	}
private:
	Node* _head;  //定义一个头结点
	size_t _size;
};
//RBTree测试函数
void TestRBTree()
{
	cout << "RBTree:" << endl;
	struct KOFInt
	{
		int operator()(const int data)const
		{
			return data;
		}
	};
	RBTree<int, KOFInt> t;
	int a[] = { 16, 3, 7, 11, 9, 26, 18, 14, 15 };
	for (auto e : a)
	{
		t.Insert(e);
	}
	t.Inorder();

	auto it = t.Begin();
	while (it !=t.End())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	if (t.IsValidRBTree())
	{
		cout << "t is valid tree" << endl;
	}
	else
	{
		cout << "t is invalid tree" << endl;
	}

	cout << t.Size() << endl;
	if (t.Find(18) != t.End())
	{
		cout << "18 is in the RBTree." << endl;
	}
	else
	{
		cout << "18 is not in the RBTree." << endl;
	}
	if (!t.Empty())
	{
		t.Clear();
	}
	cout << t.Size() << endl;
}