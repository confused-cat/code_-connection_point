#pragma once 

#include"RBTree.hpp"
namespace se
{
	template<class K>
	class set
	{
		struct KOFK
		{
			const K& operator()(const K& data)const
			{
				return data;
			}
		};
		typedef RBTree<K, KOFK> RBT;

	public:
		//此处需加上typename
		//因为静态成员变量是通过 类名::静态成员方式来访问，故编译器不确定iterator是否是RBT中的类型
		//而typename明确地告诉编译器iterator是RBT中的一个类型，而不是静态成员变量
		typename typedef  RBT::iterator iterator;
	public:
		set()
			:_t()
		{}

		//Iterator
		iterator begin()
		{
			return _t.Begin();
		}
		iterator end()
		{
			return _t.End();
		}
		//Capacity
		bool empty()const
		{
			return _t.Empty();
		}
		size_t size()const
		{
			return _t.Size();
		}
		//Element access
		pair<iterator, bool> insert(const K& data)
		{
			return _t.Insert(data);
		}
		iterator find(const K& data)
		{
			return _t.Find(data);
		}
		void swap(set<K>& s)
		{
			_t.Swap(s._t);
		}
		void clear()
		{
			_t.Clear();
		}
	private:
		RBT _t;
	};
}
//set测试函数
void TestSet()
{
	cout << "set:" << endl;
	se::set<string> s;
	s.insert("apple");
	s.insert("orange");
	s.insert("pear");
	s.insert("watermelon");

	cout << s.size() << endl;
	auto it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	if (s.find("orange") != s.end())
	{
		cout << "orange is in map." << endl;
	}
	else
	{
		cout << "orange is not in map." << endl;
	}
}