#pragma once
#include<iostream>
using namespace std;

void Test1()
{
	//1.动态申请一个int类型的空间
	int* ptr1 = new int;

	//2.动态申请一个int类型的空间并初始化为2
	int* ptr2 = new int(2);

	//3.动态申请5个int类型的空间
	int* ptr3 = new int[5];

	//4.动态申请5个int类型的空间并初始化
	int* ptr4 = new int[5]{ 1,2,3,4,5 };

	delete ptr1;
	delete ptr2;
	delete[] ptr3;
	delete[] ptr4;
}

class A
{
public:
	A(int a=0)
		:_a(a)
	{
		cout << "A():" << this << endl;
	}
	~A()
	{
		cout << "~A()" << this << endl;
	}
private:
	int _a;
};
void Test2()
{
	//1. malloc & free
	A* ptr1 = (A*)malloc(sizeof(A));
	free(ptr1);

	//2. new & delete
	A* ptr2 = new A();
	delete ptr2;

	//3. malloc/free & new/delete 操作内置类型
	int* ptr3 = (int*)malloc(sizeof(int));
	free(ptr3);
	
	int* ptr4 = new int;
	delete ptr4;

	//4.
	A* ptr5 = (A*)malloc(sizeof(A) * 10);
	free(ptr5);

	A* ptr6 = new A[10];
	delete[] ptr6;
}

class B
{
public:
	B(int b = 0)
		:_b(b)
	{
		cout << "B():" << this << endl;
	}
	~B()
	{
		cout << "~B()" << this << endl;
	}
private:
	int _b;
};
void Test3()
{
	B* ptr = new B(10);
	//...
	delete ptr;
}
void Test4()
{
	B* ptr = new B[10];
	//...
	delete[] ptr;
}
void Test5()
{
	int* ptr = new int[10];
	delete[] ptr;
	_CrtDumpMemoryLeaks();
}

