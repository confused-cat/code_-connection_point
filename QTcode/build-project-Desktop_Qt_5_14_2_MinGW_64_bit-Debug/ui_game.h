/********************************************************************************
** Form generated from reading UI file 'game.ui'
**
** Created by: Qt User Interface Compiler version 5.14.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GAME_H
#define UI_GAME_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCommandLinkButton>
#include <QtWidgets/QDialog>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_game
{
public:
    QPushButton *pushButton;
    QCommandLinkButton *commandLinkButton;

    void setupUi(QDialog *game)
    {
        if (game->objectName().isEmpty())
            game->setObjectName(QString::fromUtf8("game"));
        game->resize(1002, 602);
        game->setCursor(QCursor(Qt::PointingHandCursor));
        game->setMouseTracking(false);
        game->setTabletTracking(false);
        game->setStyleSheet(QString::fromUtf8("url:(:/res/two.jfif)"));
        game->setModal(false);
        pushButton = new QPushButton(game);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(390, 400, 191, 81));
        pushButton->setStyleSheet(QString::fromUtf8("font: 25pt \"\345\215\216\346\226\207\350\241\214\346\245\267\";"));
        commandLinkButton = new QCommandLinkButton(game);
        commandLinkButton->setObjectName(QString::fromUtf8("commandLinkButton"));
        commandLinkButton->setGeometry(QRect(570, 520, 121, 41));
        commandLinkButton->setStyleSheet(QString::fromUtf8("font: 10pt \"\345\215\216\346\226\207\346\245\267\344\275\223\"\n"
"rgb(255, 0, 127)"));

        retranslateUi(game);

        QMetaObject::connectSlotsByName(game);
    } // setupUi

    void retranslateUi(QDialog *game)
    {
        game->setWindowTitle(QCoreApplication::translate("game", "Dialog", nullptr));
        pushButton->setText(QCoreApplication::translate("game", "\345\216\206\345\217\262\346\210\230\347\273\251", nullptr));
        commandLinkButton->setText(QCoreApplication::translate("game", "\350\277\224\345\233\236\346\270\270\346\210\217\345\244\247\345\216\205", nullptr));
    } // retranslateUi

};

namespace Ui {
    class game: public Ui_game {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GAME_H
