#include "QWidget.h"
#include "ui_mainwindow.h"
#include"game.h"
#include"room.h"
#include<QIcon>
#include<QPushButton>
#include <QFont>
#include<QSound>//添加音效的类


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    //第一个界面设置
    //设置界面参数
    this->setFixedSize(800,600);//界面大小
    this->setWindowTitle("贪吃蛇");//设置图片标题
    this->setWindowIcon(QIcon(":/res/xiao.jpg"));//设置页面图标


    //进行开始游戏的按钮设置
    QPushButton* stb=new QPushButton("开始游戏",this);
    //移动按钮位置
    stb->move(300,400);
    stb->resize(200,90);//窗口大小
    //改变开始游戏的字体大小和字体
    QFont front("华文行楷",25);
    stb->setFont(front);
    stb->setFlat(true);//将按钮设置为透明
    //将该页面和按钮关联起来，使其跳转到游戏大厅页面
    game* g=new game(this);
    connect(stb,&QPushButton::clicked,[=](){
        //将界面关闭进入游戏房间
        this->hide();
        QSound::play(":/res/music.wav");
        g->show();
        //设置游戏房间界面的大小
        g->setFixedSize(800,600);
        g->setGeometry(this->geometry());//将游戏房间页面设置为和此页面一样

    });


}
MainWindow::~MainWindow()
{
    delete ui;
}

//开始游戏时界面的显示，即绘图
void MainWindow::paintEvent(QPaintEvent* event)
{
    //定义画家然后开画
    QPainter p(this);//1.定义画家
    QPixmap pix(":/res/snake.jpg");//2.画板设备
    p.drawPixmap(0,0,this->width(),this->height(),pix);//3.进行绘制
}




