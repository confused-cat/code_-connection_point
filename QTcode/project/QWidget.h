#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include<QPaintEvent>//绘画头文件
#include<QPainter>//画家
#include<QPushButton>
#include<QPixmap>
#include<QIcon>
#include<QSound>//添加音效的类
#include"game.h"//添加第二个界面的头文件
#include"room.h"


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


//绘画事件函数声明
public:
    void paintEvent(QPaintEvent* event);


private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
