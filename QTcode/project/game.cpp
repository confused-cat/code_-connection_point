#include "game.h"
#include "ui_game.h"
#include<QPushButton>
#include"game.h"
#include"room.h"
#include<QIcon>
#include<QPushButton>
#include <QFont>
#include <QDebug>
#include<QSound>//添加音效的类
#include<QWidget>
#include<QPen>
#include <QTextEdit>
#include <QDateTime>
#include<QFile>

game::game(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::game)
{
    ui->setupUi(this);
    //第二个界面设置
    //设置界面参数
    this->setFixedSize(800,600);//界面大小
    this->setWindowTitle("进行游戏模式的选择吧~~");
    this->setWindowIcon(QIcon(":/res/xiao.jpg"));

    /////////////////////////////进行四个按钮的添加
    //1.第一个按钮：简单模式
    QPushButton* simplebu=new QPushButton("简单模式",this);
    simplebu->move(100,100);//移动坐标
    simplebu->resize(200,90);//窗口大小
    QFont front1("华文行楷",25);
    simplebu->setFont(front1);//设置字体格式
    //将按钮和游戏房间界面进行关联起来
    room* ro1=new room;//实例化一个room对象
    //将room对象和按钮之间关联起来
    connect(simplebu,&QPushButton::clicked,[=](){
        //将界面关闭进入游戏房间
        this->hide();
        QSound::play(":/res/music.wav");//表示音效
        ro1->show();
        //设置游戏房间界面的大小
        ro1->setFixedSize(800,600);
        ro1->setGeometry(this->geometry());//将游戏房间页面设置为和此页面一样

        //设置游戏的时间长短,简单模式设置为500
        ro1->setTimeout(500);
    });


    //2.第二个按钮：正常模式
    QPushButton* normalbu=new QPushButton("正常模式",this);
    normalbu->move(200,200);//移动坐标
    normalbu->resize(200,90);//窗口大小
    QFont front2("华文行楷",25);
    normalbu->setFont(front2);//设置字体格式
    //将按钮和游戏房间界面进行关联起来
    room* ro2=new room;//实例化一个room对象
    //将room对象和按钮之间关联起来
    connect(normalbu,&QPushButton::clicked,[=](){
        //将界面关闭进入游戏房间
        this->hide();
        QSound::play(":/res/music.wav");//表示音效
        ro2->show();
        //设置游戏房间界面的大小
        ro2->setFixedSize(800,600);
        ro2->setGeometry(this->geometry());//将游戏房间页面设置为和此页面一样

        //设置游戏的时间长短,简单模式设置为400
        ro2->setTimeout(300);
    });

    //3.第三个按钮：困难模式
    QPushButton* hardbu=new QPushButton("困难模式",this);
    hardbu->move(300,300);//移动坐标
    hardbu->resize(200,90);//窗口大小
    QFont front3("华文行楷",25);
    hardbu->setFont(front3);//设置字体格式
    //将按钮和游戏房间界面进行关联起来
    room* ro3=new room;//实例化一个room对象
    //将room对象和按钮之间关联起来
    connect(hardbu,&QPushButton::clicked,[=](){
        //将界面关闭进入游戏房间
        this->hide();
        QSound::play(":/res/music.wav");//表示音效
        ro3->show();
        //设置游戏房间界面的大小
        ro3->setFixedSize(800,600);
        ro3->setGeometry(this->geometry());//将游戏房间页面设置为和此页面一样

        //设置游戏的时间长短,简单模式设置为250
        ro3->setTimeout(100);
    });

    //4.第四个按钮：历史战绩
    //第一个历史战绩是用ui来进行完成的，自动生成槽函数
    //进行当前页面的隐藏
    //然后跳出记录战绩的窗口

    //5. 第五个按钮：返回游戏大厅
    //利用ui界面手动添加，然后转到槽函数自动生成
    //在槽函数定义中进行程序的逻辑处理
}
game::~game()
{
    delete ui;
}

//游戏选择时界面的显示，即绘图
void game::paintEvent(QPaintEvent* event)
{
    //定义画家
    QPainter p(this);
    QPixmap pix(":/res/two.jfif");
    p.drawPixmap(0,0,this->width(),this->height(),pix);
}

//历史战绩按钮自动生成的槽函数定义
void game::on_pushButton_clicked()
{
    //进行该界面的关闭
    this->hide();
    QSound::play(":/res/music.wav");//表示音效
    //创建一个窗口：时间+战绩+...
    QWidget* newwin=new QWidget;
    //设置窗口的相关属性
    newwin->setFixedSize(300,150);//界面大小
    newwin->setWindowTitle("来瞅瞅你的战绩吧~~");//设置图片标题
    newwin->setWindowIcon(QIcon(":/res/xiao.jpg"));//设置页面图标

    //将窗口设置背景图片
    QPixmap bg(":res/back.jpg");//加载资源
    bg=bg.scaled(newwin->size(),Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
    QPalette pa;
    pa.setBrush(QPalette::Background,bg);
    newwin->setPalette(pa);

    ///////////////将游戏得分从文件中读出来然后显示到窗口中去
    //只读方式打开文件
    QFile file("D:/AllCode/QTcode/goal.txt");//文件位置
    file.open(QIODevice::ReadOnly);
    //创建文本流对象并读取文本第一行数据作为整形数据
    QTextStream readin(&file);
    int goal=readin.readLine().toInt();


    //将编辑文本写于新窗口中
    QTextEdit* ed=new QTextEdit(newwin);
    QFont font("方正舒体",18,QFont::ExtraLight,false);
    ed->setFont(font);
    ed->setFixedSize(300,150);
    //将文本框背景和边框设置为透明的
    ed->setStyleSheet("background-color: rgb(255, 255, 255, 60);");
    ed->setWindowFlags(Qt::FramelessWindowHint);
    //获取系统时间进行显示
    QDateTime nowtime=QDateTime::currentDateTime();
    //设置时间显示格式
    QString time=nowtime.toString("yyyy-MM-dd hh:mm:ss");
    ed->setText("时间：");
    ed->append(time);
    ed->append("得分：");
    ed->append(QString::number(goal));
    newwin->show();

}

//在游戏选择模式选择界面返回到游戏大厅界面的按钮自动生成的槽函数
void game::on_commandLinkButton_clicked()
{
    //进行该界面的关闭
    this->close();
    QSound::play(":/res/music.wav");//表示音效
    //将界面返回到游戏初始界面
    //实例化一个第一个界面的对象，将界面进行显示
    MainWindow* qw=new MainWindow(this);
    qw->show();
}
