#ifndef GAME_H
#define GAME_H

#include <QDialog>
#include <QMainWindow>
#include<QPaintEvent>//绘画头文件
#include<QPainter>//画家
#include<QPushButton>//添加按钮时按钮的头文件
#include<QPixmap>//画板设备
#include<QIcon>
#include <QDebug>
#include<QSound>//添加音效的类
#include<QWidget>

namespace Ui {
class game;
}

class game : public QDialog
{
    Q_OBJECT

public:
    explicit game(QWidget *parent = nullptr);
    ~game();

private:
    Ui::game *ui;

//绘画事件函数声明
public:
    void paintEvent(QPaintEvent* event);


private slots:
    void on_pushButton_clicked();//自动生成槽进行历史战绩按钮的页面跳转实现
    void on_commandLinkButton_clicked();//自动生成槽进行游戏返回按钮的页面跳转实现
};

#endif // GAME_H
