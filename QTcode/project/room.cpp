#include "room.h"
#include<QPushButton>
#include"game.h"
#include<QIcon>
#include<QPushButton>
#include <QFont>
#include <QtDebug>
#include<QSound>//添加音效的类
#include<QWidget>
#include<QPen>//Qt中的画笔类
#include<QPixmap>
#include<stdlib.h>
#include<QBitmap>
#include<QPointF>
#include<QtDebug>
#include<QBitmap>
#include<QTimer>//定时器
#include<QList>//链表
#include<QRectF>//链表容器
#include<QFile>//文件
#include<QMessageBox>//消息类
#include<QFile>

room::room(QWidget *parent) : QWidget(parent)
{
    //游戏大厅界面
    this->setFixedSize(800,600);//界面大小
    this->setWindowTitle("开始游戏");
    this->setWindowIcon(QIcon(":/res/xiao.jpg"));

    //进行蛇的初始化
    snakeList.push_back(QRectF(this->width()*0.5,this->height()*0.5,smovewidth,smoveheight));
    //进行两次向上移动
    MoveUp();
    MoveUp();

    //创建食物节点
    createnewfood();

    //使用定时器将蛇移动起来
    timer=new QTimer(this);
    connect(timer,&QTimer::timeout,[=]()
    {
       int cn=1;
       if(snakeList.front().intersects(food))
       {
           createnewfood();
           cn++;

       }
       QSound::play(":res/eatmusic.wav");//进行音乐播放
       while(cn--)
       {
           switch(movedirect)
           {
           case SnakeMove::up:
               MoveUp();
               break;
           case SnakeMove::down:
               MoveDown();
               break;
           case SnakeMove::left:
               MoveLeft();
               break;
           case SnakeMove::right:
               MoveRight();
               break;
           default:
               //qDebug<<"非法移动";
               break;
           }
       }
       //删除尾节点
       snakeList.pop_back();
       update();
    });


    /////////////////////////进行按钮的添加：开始、暂停、退出
    //开始按钮
    QPushButton* start=new QPushButton("开始",this);
    start->move(725,50);//移动坐标
    start->resize(50,25);//窗口大小
    QFont front1("华文行楷",10);
    start->setFont(front1);//设置字体格式
    connect(start,&QPushButton::clicked,[=](){
        //将游戏设置为开始,并且将定时器进行计数
        startgame=true;
        timer->start(movetimeout);


        //开始游戏进行时的音乐
        sound=new QSound(":res/playingmusic.wav");
        sound->play();
        sound->setLoops(-1);//让音乐循环播放器起来
    });
    //暂停按钮
    QPushButton* stop=new QPushButton("暂停",this);
    stop->move(725,75);//移动坐标
    stop->resize(50,25);//窗口大小
    QFont front2("华文行楷",10);
    stop->setFont(front2);//设置字体格式
    connect(stop,&QPushButton::clicked,[=](){
        //将游戏设置为暂停,并且将定时器停止计数
        startgame=false;
        timer->stop();

        //开始游戏暂停时的音乐暂停
        sound->stop();
        sound->stop();
    });

    ///////////////退出按钮弹出一个消息对话框
    //退出按钮创建
    QPushButton* cancel=new QPushButton("退出",this);
    cancel->move(725,100);//移动坐标
    cancel->resize(50,25);//窗口大小
    QFont front3("华文行楷",10);
    cancel->setFont(front3);//设置字体格式
    //消息对话框
    QMessageBox* mess=new QMessageBox(this);
    mess->setWindowTitle("退出游戏界面提示~~");
    mess->setWindowIcon(QIcon(":/res/xiao.jpg"));
    mess->setIcon(QMessageBox::Question);//question表示问好图标
    //mess->setTitle("确定退出游戏吗？");
    mess->setText(QString("确定退出游戏吗？"));

    QPushButton* ok=new QPushButton("OK");
    QPushButton* can=new QPushButton("Cancel");
    //将按钮添加到消息界面
    mess->addButton(ok,QMessageBox::AcceptRole);
    mess->addButton(can,QMessageBox::RejectRole);
    connect(cancel,&QPushButton::clicked,[=](){
        //对话框显示
        mess->show();

        mess->exec();//时间轮询，始终显示
        //进行按钮的检测,当点击ok就将所有的界面关闭，当点击cancel时就将消息框给关闭
        if(mess->clickedButton()==ok)
        {
            //进行当前页面
            this->close();
        }
        else
        {
            //消息界面进行关闭
            mess->close();
        }
    });

    ////////////////////////控制蛇移动的图标设置
    //上键
    QPushButton* icon1=new QPushButton(this);
    icon1->setIcon(QIcon(":res/up.png"));
    icon1->move(40,520);
    connect(icon1,&QPushButton::clicked,[=](){
        if(movedirect!=SnakeMove::down)
        {
            movedirect=SnakeMove::up;
        }
        //进行音效的播放
        QSound::play(":/res/music.wav");//表示音效
    });

    //下键
    QPushButton* icon2=new QPushButton(this);
    icon2->setIcon(QIcon(":res/down.png"));
    icon2->move(40,562);
    connect(icon2,&QPushButton::clicked,[=](){
        if(movedirect!=SnakeMove::up)
        {
            movedirect=SnakeMove::down;
        }
        //进行音效的播放
        QSound::play(":/res/music.wav");//表示音效
    });
    //左键
    QPushButton* icon3=new QPushButton(this);
    icon3->setIcon(QIcon(":res/left.png"));
    icon3->move(15,540);
    connect(icon3,&QPushButton::clicked,[=](){
        if(movedirect!=SnakeMove::right)
        {
            movedirect=SnakeMove::left;
        }
        //进行音效的播放
        QSound::play(":/res/music.wav");//表示音效
    });
    //右键
    QPushButton* icon4=new QPushButton(this);
    icon4->setIcon(QIcon(":res/right.png"));
    icon4->move(65,540);
    connect(icon4,&QPushButton::clicked,[=](){
        if(movedirect!=SnakeMove::left)
        {
            movedirect=SnakeMove::right;
        }
        //进行音效的播放
        QSound::play(":/res/music.wav");//表示音效
    });
}

//////////////////////游戏选择时界面的显示，即绘图
void room::paintEvent(QPaintEvent* event)
{
    //定义画家，将画家这个人放在当前窗口
    QPainter p(this);
    QPixmap pix(":/res/back.jpg");
    p.drawPixmap(0,0,this->width(),this->height(),pix);

    /////////////////////绘制蛇头
    //方向不同的时候分别绘制处不同方向的图片
    if(movedirect==SnakeMove::up)
    {
        pix.load(":res/snakeup.png");
    }
    else if(movedirect==SnakeMove::down)
    {
        pix.load(":res/snakedown.png");
    }
    else if(movedirect==SnakeMove::left)
    {
        pix.load(":res/snakeleft.png");
    }
    else
    {
        pix.load(":res/snakeright.png");
    }

    ///////获取蛇的头节点
    auto snakehead=snakeList.front();
    //进行蛇的头节点的绘画
    p.drawPixmap(snakehead.x(),snakehead.y(),snakehead.width(),snakehead.height(),pix);

    ////////绘制蛇的主体
    pix.load(":res/snakebody.png");
    for(int i=1;i<snakeList.size()-1;++i)
    {
        auto node=snakeList.at(i);
        p.drawPixmap(node.x(),node.y(),node.width(),node.height(),pix);
    }

    ///////////绘制蛇尾
    auto snaketail=snakeList.back();
    p.drawPixmap(snaketail.x(),snaketail.y(),snaketail.width(),snaketail.height(),pix);

    //绘制食物节点，将生成的食物显示出来
    pix.load(":res/snakebody.png");
    p.drawPixmap(food.x(),food.y(),food.width(),food.height(),pix);


    ////////////////将用户实时成绩进行显示
    QPen pen;
    QBrush brush;
    pen.setColor(Qt::black);
    QFont font("方正舒体",18,QFont::ExtraLight,false);
    //定义一个画家
    p.setPen(pen);
    p.setFont(font);
    p.drawText(20,20,QString("当前得分：")+QString("%1").arg(snakeList.size()));

    ///////////////////////将游戏的得分给写入到goal文件当中去
    int goal=snakeList.size();//将蛇的长度记为得分
    QFile file("D:/AllCode/QTcode/goal.txt");
    if(file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream out(&file);
        int num=goal;
        out<<num;//将int类型数据写入到文件当中去
        file.close();
    }

    /////////////判断蛇是否撞上自己，游戏是否失败
    if(checkFail())
    {
        //如果游戏失败，进行效果图的渲染和播放音乐
        pen.setColor(Qt::red);
        p.setPen(pen);//画家拿到画笔
        QFont font("华文行楷",18,QFont::ExtraLight,false);
        //画家去绘制游戏失败的效果图
        p.setFont(font);//进行字体格式的额设置
        p.drawText(this->width()*0.5,this->height()*0.5,QString("游戏结束咯！"));

        //进行失败音乐的播放
        QSound::play(":/res/fail.wav");
        //定时器停止计时，音乐结束播放
        timer->stop();
        sound->stop();
        sound->stop();
    }
}

/////////////////////////进行游戏是否开始的设置
void room::startGame()
{
    startgame=true;//将游戏设置为开始游戏
    timer->start(movetimeout);//计时器开始进行计时
}

/////////////////////蛇的方向移动/////////////////
//蛇向上移动的方法
void room::MoveUp()
{
    //蛇节点是一个方块，两个坐标确定节点位置
    QPointF lefttop;
    QPointF rightbottom;

    auto snode=snakeList.front();//获取头节点
    int headx=snode.x();//获取头节点的x坐标
    int heady=snode.y();//获取头节点的y坐标

    //当蛇节点跑出了界面，当节点的纵坐标小于0的时候就表示蛇撞墙了
    if(heady<0)
    {
        //重新绘制一下左上角和右下角的坐标
        //从窗口下面出来，x坐标不变，y坐标等于当前坐标-节点的高度
        lefttop=QPointF(headx,this->height()-smoveheight);//将蛇从下面表示出来
    }
    else
    {
        //重新绘制一下左上角和右下角的坐标
        lefttop=QPointF(headx,heady-smoveheight);
    }
    //右下角坐标表示
    rightbottom=lefttop+QPointF(smovewidth,smoveheight);

    //将重新绘制的节点头插到链表中
    snakeList.push_front(QRectF(lefttop,rightbottom));

}
//蛇向下移动的方法
void room::MoveDown()
{
    //蛇节点是一个方块，两个坐标确定节点位置
    QPointF lefttop;
    QPointF rightbottom;

    auto snode=snakeList.front();//获取头节点
    int headx=snode.x();//获取头节点的x坐标
    int heady=snode.y();//获取头节点的y坐标

    //当蛇节点跑出了界面：即当前左上角的坐标高度加上节点的高度大于当前窗口的高度时
    if(heady+smoveheight>this->height())
    {
        //重新绘制一下左上角和右下角的坐标
        //从窗口上面出来，x坐标不变，y坐标等于节点的高度
        lefttop=QPointF(headx,0);
    }
    else
    {
        //重新绘制一下左上角和右下角的坐标
        lefttop=QPointF(headx,heady+smoveheight);
    }
    //右下角坐标表示
    rightbottom=lefttop+QPointF(smovewidth,smoveheight);

    //将重新绘制的节点头插到链表中
    snakeList.push_front(QRectF(lefttop,rightbottom));
}
//蛇向左移动的方法
void room::MoveLeft()
{
    //蛇节点是一个方块，两个坐标确定节点位置
    QPointF lefttop;
    QPointF rightbottom;

    auto snode=snakeList.front();//获取头节点
    int headx=snode.x();//获取头节点的x坐标
    int heady=snode.y();//获取头节点的y坐标
    //当坐标
    if(headx<0)
    {
        //重新绘制坐标，窗口从右边出来
        lefttop=QPointF(this->width()-smovewidth,heady);
    }
    else
    {
        lefttop=QPointF(headx-smovewidth,heady);
    }
    //右下角坐标表示
    rightbottom=lefttop+QPointF(smovewidth,smoveheight);

    //将重新绘制的节点头插到链表中
    snakeList.push_front(QRectF(lefttop,rightbottom));
}
//蛇向右移动的方法
void room::MoveRight()
{
    //蛇节点是一个方块，两个坐标确定节点位置
    QPointF lefttop;
    QPointF rightbottom;

    auto snode=snakeList.front();//获取头节点
    int headx=snode.x();//获取头节点的x坐标
    int heady=snode.y();//获取头节点的y坐标
    //当坐标
    if(headx+smovewidth>this->width())
    {
        //重新绘制坐标，窗口从右边出来
        lefttop=QPointF(0,heady);
    }
    else
    {
        lefttop=QPointF(headx+smovewidth,heady);
    }
    //右下角坐标表示
    rightbottom=lefttop+QPointF(smovewidth,smoveheight);

    //将重新绘制的节点头插到链表中
    snakeList.push_front(QRectF(lefttop,rightbottom));
}

///////////////////创建食物节点的函数
void room::createnewfood()
{
    //随机生成食物节点
    food=QRect(qrand()%this->width()+
               smovewidth,qrand()%this->height()+
               smoveheight,smovewidth,smoveheight);

}
///////////////控制蛇移动的时间，以此来区分三种模式的不同
void room::setTimeout(int time)
{
    //将传入的时间赋值给蛇的移动时间
    movetimeout=time;
}

//判断蛇是否进行相交，即蛇头和蛇的某一个节点相等，就表明自己撞到自己了
bool room::checkFail()
{
    for(int i=0;i<snakeList.size();++i)
    {
        for(int j=i+1;j<snakeList.size();++j)
        {
            if(snakeList.at(i)==snakeList.at(j))
            {
                return true;
            }
        }
    }
    return false;
}

































