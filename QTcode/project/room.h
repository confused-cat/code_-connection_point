#ifndef ROOM_H
#define ROOM_H

#include <QWidget>
#include <QDialog>
#include <QMainWindow>
#include<QPaintEvent>//绘画头文件
#include<QPainter>//画家
#include<QPushButton>//添加按钮时按钮的头文件
#include<QPixmap>//画板设备
#include<QIcon>
#include<QPointF>
#include<stdlib.h>
#include<QBitmap>
#include<QTimer>//定时器
#include<QList>//链表
#include<QMessageBox>//消息类
#include<QRectF>//链表容器
#include"QWidget.h"



//将蛇移动的四个方向进行枚举出来
enum SnakeMove
{
    up=0,
    down,
    left,
    right
};


class room : public QWidget
{
    Q_OBJECT
public:
    explicit room(QWidget *parent = nullptr);


public:
    //绘画事件函数声明
    void paintEvent(QPaintEvent* event);

    //进行游戏是否开始的设置
    void startGame();

    //蛇的移动
    //蛇向上移动的方法
    void MoveUp();
    //蛇向下移动的方法
    void MoveDown();
    //蛇向左移动的方法
    void MoveLeft();
    //蛇向右移动的方法
    void MoveRight();

    //进行食物节点的随机创建
    void createnewfood();

    //进行蛇速度移动函数的定义
    void setTimeout(int time);

    //判断蛇是否自己撞到自己了
    bool checkFail();

public:
    QList<QRectF> snakeList;//蛇身表示：链表
private:
    //设置蛇的移动方向,将蛇的默认初始方向设置为上
    SnakeMove movedirect=SnakeMove::up;

    bool startgame=false;//bool类型表示游戏是否开始

    QTimer* timer;//设置定时器进行游戏开始和暂停的控制

    //蛇身表示：链表
    //QList<QRectF> snakeList;

    //表示食物节点
    QRectF food;

    //定义蛇移动的时间,默认移动时间为200秒，蛇移动的宽和高
    const int smovetime=200;
    const int smovewidth=20;//每个节点宽度
    const int smoveheight=20;//每个节点高度

    //蛇移动时间的快慢,然后对于不同模式下蛇都有不同的移动速度，就是根据他的时间来进行恒定
    int movetimeout=smovetime;//将初始的设置时间设置为smovetime

    //设置一个声音类，在开始和暂停按钮的时候进行音乐的控制
    QSound* sound;

//public:
    //一次游戏结束后蛇的长度记录下来
    //int gameoverslength=3;

signals:

};

#endif // ROOM_H
