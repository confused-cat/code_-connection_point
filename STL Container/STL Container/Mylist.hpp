#pragma once

#include<iostream>
using namespace std;
#include<assert.h>
#include<string.h>

namespace lis
{
	//节点定义
	template<class T>
	struct ListNode
	{
		ListNode(const T& value=T())
			:prev(nullptr)
			,next(nullptr)
			,val(value)
		{}
		ListNode* prev;
		ListNode* next;
		T val;
	};

	//双向链表正向迭代器
	template<class T,class Ptr,class Ref>
	class ListIterator
	{
	public:
		typedef ListNode<T> Node;
		typedef ListIterator<T, Ptr, Ref> Self;
	public:
		//给反向迭代器进行使用
		typedef Ptr Ptr;
		typedef Ref Ref;
	public:
		ListIterator(Node* pNode=nullptr)
			:_pNode(pNode)
		{}
		////////////////////////////
		//支持解引用和指向
		Ref operator*()
		{
			return _pNode->val;
		}
		Ptr operator->()
		{
			return &(_pNode->val);
		}
		/////////////////////////
		//可移动
		Self& operator++()
		{
			_pNode = _pNode->next;
			return *this;
		}
		Self operator++(int)
		{
			Self temp(*this);
			_pNode = _pNode->next;
			return temp;
		}
		Self& operator--()
		{
			_pNode = _pNode->prev;
			return *this;
		}
		Self operator--(int)
		{
			Self temp(*this);
			_pNode = _pNode->prev;
			return temp;
		}
		//////////////////////////
		//可进行比较
		bool operator==(const Self& s)const
		{
			return _pNode == s._pNode;
		}
		bool operator!=(const Self& s)const
		{
			return _pNode != s._pNode;
		}
		Node* _pNode;
	};
	//双向链表反向迭代器
	template<class Iterator>
	class ListReverseIterator
	{
	public:
		typename typedef Iterator::Ref Ref;
		typename typedef Iterator::Ptr Ptr;
		typedef ListReverseIterator<Iterator> Self;
	public:
		ListReverseIterator(Iterator it)
			:_it(it)
		{}
		//////////////////////////////
		//解引用和指向
		Ref operator*()
		{
			Iterator temp = _it;
			--temp;
			return *temp;
		}
		Ptr operator->()
		{
			return &(_it->_pNode->val);
		}
		/////////////////////////////
		//能够进行移动
		Self& operator++()
		{
			--_it;
			return *this;
		}
		Self operator++(int)
		{
			_it--;
			return *this;
		}
		Self& operator--()
		{
			++_it;
			return *this;
		}
		Self operator--(int)
		{
			_it++;
			return *this;
		}
		////////////////////////////
		//可进行比较
		bool operator==(const Self& s)const
		{
			return _it==s._it;
		}
		bool operator!=(const Self& s)const
		{
			return _it!=s._it;
		}
		Iterator _it;
	};

	template<class T>
	class mylist
	{
	public:
		typedef ListNode<T> Node;
	public:
		typedef ListIterator<T, T*, T&> iterator;//正向迭代器
		typedef ListIterator<T, const T*, const T&> const_iterator;//正向迭代器

		typedef ListReverseIterator<iterator> reverse_iterator;//反向迭代器
		typedef ListReverseIterator<const_iterator> const_reverse_iterator;//反向迭代器
	public:
		//空构造
		mylist()
		{
			CreateHeadNode();
		}
		//n个值为val值的构造
		mylist(int n, const T& val = T())
		{
			CreateHeadNode();
			for (int i = 0; i < n; ++i)
			{
				push_back(val);
			}
		}
		//迭代器进行构造
		template<class Iterator>
		mylist(Iterator first, Iterator last)
		{
			CreateHeadNode();
			while (first != last)
			{
				push_back(*first);
				++first;
			}
		}
		//拷贝构造函数
		mylist(const mylist<T>& L)
		{
			CreateHeadNode();
			for (auto e : L)
			{
				push_back(e);
			}
		}
		//赋值运算符重载函数
		mylist<T> operator=(mylist<T> L)
		{
			this->swap(L);
			return *this;
		}
		~mylist()
		{
			clear();
			delete[] _head;
			_head = nullptr;
		}
		////////////////////////////////////////
		//iterator
		//正向迭代器
		iterator begin()
		{
			return iterator(_head->next);
		}
		iterator end()
		{
			return iterator(_head);
		}
		const_iterator begin()const
		{
			return const_iterator(_head->next);
		}
		const_iterator end()const
		{
			return const_iterator(_head);
		}
		//反向迭代器
		reverse_iterator rbegin()
		{
			return reverse_iterator(end());
		}
		reverse_iterator rend()
		{
			return reverse_iterator(begin());
		}
		const_reverse_iterator rbegin()const
		{
			return const_reverse_iterator(end());
		}
		const_reverse_iterator rend()const
		{
			return const_reverse_iterator(begin());
		}
		////////////////////////////////////////////
		//capacity
		size_t size()
		{
			size_t cn = 0;
			Node* cur = _head->next;
			while (cur != _head)
			{
				++cn;
				cur = cur->next;
			}
			return cn;
		}
		bool empty()
		{
			return _head == _head->next;
		}
		void resize(size_t newsize, const T& val = T())
		{
			size_t oldsize = size();
			if (newsize < oldsize)
			{
				//将原来链表中的个数减为newsize
				for (size_t i = newsize; i < oldsize; ++i)
				{
					pop_back();
				}
			}
			else
			{
				//将原来链表中的个数增加到newsize
				for (size_t i = oldsize; i < newsize; ++i)
				{
					push_back(val);
				}
			}
		}
		///////////////////////////////////////////
		//access
		T& front()
		{
			return _head->next->val;
		}
		T& back()
		{
			return _head->prev->val;
		}
		/////////////////////////////////////////////
		//modify
		void push_front(const T& val)
		{
			//在迭代器begin()之前插入节点值为val
			insert(begin(), val);
		}
		void pop_front()
		{
			if (empty()) return;
			erase(begin());
		}
		void push_back(const T& val)
		{
			insert(end(), val);
		}
		void pop_back()
		{
			if (empty()) return;
			auto pos = end();
			--pos;
			erase(pos);
		}
		iterator insert(iterator Ipos, const T& val)
		{
			Node* pos = Ipos._pNode;
			Node* newNode = new Node(val);
			//在pos前插入节点newNode
			newNode->next = pos;
			newNode->prev = pos->prev;
			newNode->prev->next = newNode;
			pos->prev = newNode;

			return newNode;
		}
		iterator erase(iterator Ipos)
		{
			Node* pos = Ipos._pNode;
			if (pos == _head)
				return pos;
			//将pos节点从链表中拆除下来
			pos->prev->next = pos->next;
			pos->next->prev = pos->prev;

			Node* retNode = pos->next;
			delete pos;
			return retNode;
		}
		void clear()
		{
			Node* cur = _head->next;
			//头删法
			while (cur != _head)
			{
				_head->next = cur->next;
				delete cur;
				cur = _head->next;
			}
			_head->next = _head;
			_head->prev = _head;
		}

		void swap(mylist<T>& L)
		{
			std::swap(_head, L._head);
		}
	private:
		void CreateHeadNode()
		{
			_head = new Node();
			_head->next = _head;
			_head->prev = _head;
		}
	private:
		Node* _head;
	};

	template<class T>
	void PrintMylist(mylist<T>& L)
	{
		for (auto e : L)
		{
			cout << e << " ";
		}
		cout << endl;
	}
}

void TestMylist1()
{
	lis::mylist<int> L1;
	lis::mylist<int> L2(10, 5);

	int array[] = { 1,2,3,4,5 };
	lis::mylist<int> L3(array, array + sizeof(array) / sizeof(array[0]));

	lis::mylist<int> L4(L3);

	auto it = L2.begin();
	while (it != L2.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	lis::PrintMylist(L3);
	lis::PrintMylist(L4);
}
void TestMylist2()
{
	lis::mylist<int> L;
	L.push_back(1);
	L.push_back(2);
	L.push_back(3);
	L.push_back(4);
	L.push_back(5);
	lis::PrintMylist(L);

	cout << L.size() << endl;
	cout << L.front() << endl;
	cout << L.back() << endl;

	L.pop_back();
	L.pop_back();//1 2 3
	cout << L.size() << endl;
	cout << L.front() << endl;
	cout << L.back() << endl;

	L.push_front(0);//0 1 2 3
	cout << L.size() << endl;
	cout << L.front() << endl;
	cout << L.back() << endl;

	L.pop_front();//1 2 3
	cout << L.size() << endl;
	cout << L.front() << endl;
	cout << L.back() << endl;
}
void TestMylist3()
{
	lis::mylist<int> L;
	L.push_back(1);
	L.push_back(2);
	L.push_back(3);
	L.push_back(4);
	L.push_back(5);
	lis::PrintMylist(L);

	L.resize(10, 8);
	lis::PrintMylist(L);

	L.resize(5);
	lis::PrintMylist(L);

	L.clear();
	if (L.empty())
	{
		cout << "L is empty" << endl;
	}
	else
	{
		cout << "L is not empty" << endl;
	}
}
void TestMylist4()
{
	lis::mylist<int> L;
	L.push_back(1);
	L.push_back(2);
	L.push_back(3);
	L.push_back(4);
	L.push_back(5);
	lis::PrintMylist(L);
	//正向迭代器
	auto it = L.begin();
	while (it != L.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;
	//反向迭代器
	auto rit = L.rbegin();
	while (rit != L.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
}
