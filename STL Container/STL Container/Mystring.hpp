#define _CRT_SECURE_NO_WARNINGS

#include<iostream>
using namespace std;

/*
string浅拷贝问题：
当类中一旦涉及到资源的管理，如申请空间
该类的 拷贝构造函数+赋值运算符重载函数+析构函数 一定得显式实现，不然编译器会按照浅拷贝的方式生成一份默认的成员函数

浅拷贝：
将一个对象里面的内容原封不动的拷贝到另一个对象中，导致多个对象共享同一份资源
之后同一份资源会被多次释放而引起程序崩溃
*/

#if 0
class String
{
public:
	String(const char* str = "")
	{
		if (str == nullptr)
			str = "";
		//给新对象申请空间，然后将内容拷贝一份到新空间中去
		_str = new char[strlen(str) + 1];
		strcpy(_str, str);
	}
	//编译器按照浅拷贝的方式生成的拷贝构造大致如下：
	String(const String& s)
		:_str(s._str)
	{}
	//编译器按照浅拷贝的方式生成默认赋值运算符重载大致如下：
	String& operator=(const String& s)
	{
		_str = s._str;
		return *this;
	}
	~String()
	{
		if (_str)
		{
			delete[] _str;
			_str = nullptr;
		}
	}
private:
	char* _str;
};

/*
浅拷贝解决方式：
    1.深拷贝
	2.写时拷贝
*/

//深拷贝：
//1.传统版
class String
{
public:
	//构造函数
	String(const char* str = "")
	{
		if (str == nullptr)
			str = "";
		_str = new char[strlen(str) + 1];
		strcpy(_str, str);
	}
	//拷贝构造函数
	String(const String& s)
		:_str(new char[strlen(s._str) + 1])
	{
		strcpy(_str, s._str);
	}
	//赋值运算符重载函数
	String& operator=(const String& s)
	{
		//判断是否是给自己赋值
		if (this != &s)
		{
			//确保当前对象没有空间和内容
			delete[] _str;
			_str = new char[strlen(s._str) + 1];
			strcpy(_str, s._str);

			char* temp = new char[strlen(s._str) + 1];
			strcpy(temp, s._str);
			delete[] _str;
			_str = temp;
		}
		return *this;
	}
	//析构函数
	~String()
	{
		if (_str)
		{
			delete[] _str;
			_str = nullptr;
		}
	}
private:
	char* _str;
};

//2.现代版
class String
{
public:
	//构造函数
	String(const char* str = "")
	{
		if (str == nullptr)
			str = "";
		_str = new char[strlen(str) + 1];
		strcpy(_str, str);
	}
	//拷贝构造函数
	String(const String& s)
		:_str(nullptr)
	{
		String temp(s._str);
		swap(_str, temp._str);
	}
	//赋值运算符重载函数
	String& operator=(String s)
	{
		swap(_str, s._str);
		return *this;
	}
	//析构函数
	~String()
	{
		if (_str)
		{
			delete[] _str;
			_str = nullptr;
		}
	}
private:
	char* _str;
};
#endif



#define _CRT_SECURE_NO_WARNINGS
#include<assert.h>

//模拟实现string
namespace str
{
	class mystring
	{
	public:
		typedef char* iterator;
	public:
		//构造函数
		mystring(const char* str = "")
		{
			if (str == nullptr)
				str = "";
			_size = strlen(str);
			_capacity = _size;
			_str = new char[_capacity+1];
			strcpy(_str, str);
		}
		//拷贝构造函数
		mystring(const mystring& s)
		{
			mystring temp(s._str);
			this->swap(temp);
		}
		//n个值为val的构造
		mystring(size_t n, char val)
		{
			_str = new char[n + 1];
			memset(_str, val, n);
			_str[n] = '\0';

			_size = n;
			_capacity = n;
		}
		//赋值运算符重载函数
		mystring& operator=(mystring s)
		{
			if (_str)
			{
				delete[] _str;
				_str = nullptr;
				_capacity = 0;
				_size = 0;
			}
		}
		void swap(mystring& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}
		///////////////////////////////////////////
		//迭代器
		iterator begin()
		{
			return _str;
		}
		iterator end()
		{
			return _str + _size;
		}
		///////////////////////////////////////////
		//capacity
		size_t size()
		{
			return _size;
		}
		size_t length()
		{
			return _size;
		}
		size_t capacity()
		{
			return _capacity;
		}
		bool empty()const
		{
			return _size == 0;
		}
		void clear()
		{
			_size = 0;
		}
		void resize(size_t newsize, char val)
		{
			size_t oldsize = size();
			//当新元素个数大于原来元素的个数时，得对空间进行扩容，且扩容后的空间内容用val来进行填充
			if (newsize > oldsize)
			{
				//有效元素个数大于当前空间的容量时，得对空间进行扩容到newsize
				if (newsize > capacity())
					reserve(newsize);
				memset(_str + _size, val, newsize - _size);
			}
			_size = newsize;
			_str[_size] = '\0';
		}
		void resize(size_t newsize)
		{
			//当未指明要用哪一个字符进行填充时，用'\0'
			resize(newsize, '\0');
		}
		void reserve(size_t newcapacity)
		{
			size_t oldcapacity = capacity();
			if (newcapacity > oldcapacity)
			{
				//1.申请新空间
				char* temp = new char[newcapacity + 1];
				//2.拷贝元素
				strncpy(temp, _str, _size);
				//3.释放旧空间
				delete[] _str;
				//4.使用新空间
				_str = temp;

				_capacity = newcapacity;
				_str[_size] = '\0';
			}
		}
		////////////////////////////////////////////
		//access
		char& operator[](size_t index)
		{
			assert(index < _size);
			return _str[index];
		}
		char& at(size_t index)
		{
			//如果越界的话，抛出out_of_range异常
			return _str[index];
		}
		///////////////////////////////////////////
		//modify
		void push_back(char ch)
		{
			*this += ch;
		}
		//字符串后后缀一个字符
		mystring& operator+=(char ch)
		{
			if (_size == _capacity)
			{
				reserve((size_t)_capacity * 2);
			}
			_str[_size] = ch;
			++_size;
			_str[_size] = '\0';
			return *this;
		}
		//字符串后后缀一段连续空间的字符
		mystring& operator+=(const char* str)
		{
			size_t needspace = strlen(str);
			size_t leftspace = capacity() - size();
			//当剩余的空间小于所需的空间时，得进行扩容
			if (needspace > leftspace)
				reserve(capacity() + needspace);

			//将str的内容追加到_str后面
			strcat(_str, str);
			_size += needspace;
			_str[_size] = '\0';

			return *this;
		}
		//字符串后后缀一个字符串
		mystring& operator+=(const mystring& s)
		{
			*this += s.c_str();
			return *this;
		}
		mystring& append(const char* str)
		{
			*this += str;
			return *this;
		}
		mystring& append(const mystring& s)
		{
			*this += s;
			return *this;
		}
		mystring& insert(size_t pos, const mystring& s);
		mystring& erase(size_t pos = 0, size_t n = npos);
		//////////////////////////////////////////////
		const char* c_str()const
		{
			return _str;
		}
		size_t find(char ch, size_t pos = 0)const
		{
			if (pos >= _size)
				return npos;
			for (size_t i = pos; i < _size; ++i)
			{
				if (_str[i] == ch)
					return i;
			}
			return npos;
		}
		size_t rfind(char ch, size_t pos = npos)
		{
			if (pos >= _size)
				pos = _size - 1;
			for (int i = pos; i >= 0; --i)
			{
				if (_str[i] == ch)
					return i;
			}
			return npos;
		}
		mystring substr(size_t pos = 0, size_t n = npos)const
		{
			if (pos >= _size)
				return mystring();
			//从pos到字符串末尾剩余字符个数
			n = min(n, _size - pos);
			
			mystring temp(n, '\0');
			size_t index = 0;
			for (size_t i = pos; i < pos + n; ++i)
			{
				temp[index++] = _str[i];
			}
			return temp;
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;

		const static size_t npos = -1;
		friend ostream& operator<<(ostream& out,  mystring& s);
	};
	ostream& operator<<(ostream& out, mystring& s)
	{
		for (size_t i = 0; i < s.size(); ++i)
		{
			cout << s[i];
		}
		return out;
	}
}

void TestMystring1()
{
	str::mystring s1;
	str::mystring s2("hello");
	str::mystring s3(10, 'a');
	str::mystring s4(s3);
	cout << s2 << endl;
	cout << s3 << endl;
	cout << s4 << endl;

	for (size_t i = 0; i < s2.size(); ++i)
	{
		cout << s2[i] << " ";
	}
	cout << endl;

	for (auto e : s3)
		cout << e;
	cout << endl;

	auto it = s4.begin();
	while (it != s4.end())
	{
		cout << *it;
		++it;
	}
	cout << endl;
}
void TestMystring2()
{
	str::mystring s("hello");
	s.reserve(15);

	cout << s.size() << endl;
	cout << s.length() << endl;
	cout << s.capacity() << endl;

	//将有效元素个数增多
	s.resize(10, '?');//只增加元素，不进行扩容
	cout << s.size() << endl;
	cout << s.capacity() << endl;

	s.resize(20, 'a');//增加元素，并对空间进行扩容
	cout << s.size() << endl;
	cout << s.capacity() << endl;

	//将有效元素个数减少
	s.resize(12);
	s.resize(5);
	cout << s.size() << endl;
	cout << s.capacity() << endl;

	//空间只能扩大并不可进行缩小
	s.reserve(100);
	s.reserve(40);
	cout << s.size() << endl;
	cout << s.capacity() << endl;
}
void TestMystring3()
{
	str::mystring s;
	s.push_back('h');
	s += 'e';
	s += "llo";
	s += ' ';
	s.append("world");
	cout << s << endl;
}
void TestMystring4()
{
	str::mystring s("hello world mystring.txt");
	size_t pos = s.find('o');
	cout << pos << endl;

	pos = s.find('o', pos + 1);
	cout << pos << endl;

	pos = s.find('o', pos + 1);
	cout << pos << endl;

	pos = s.rfind('.');
	str::mystring ret = s.substr(pos + 1);//txt
	cout << ret << endl;
}
