#pragma once

#include<iostream>
using namespace std;
#include<assert.h>
#include<string.h>

namespace vec
{
	template<class T>
	class myvector
	{
	public:
		typedef T* iterator;
	public:
		//空构造，将底层的指针全部置为空
		myvector()
			:start(nullptr)
			,finish(nullptr)
			,endofstorage(nullptr)
		{}
		//n个值为val的构造
		myvector(int n, const T& val = T())
		{
			start = new T[n];
			for (int i = 0; i < n; ++i)
			{
				start[i] = val;
			}
			finish = start + n;
			endofstorage = finish;
		}
		//迭代器进行构造
		template<class Iterator>
		myvector(Iterator first, Iterator last)
		{
			auto it = first;
			//统计[first,last)区间的元素个数
			size_t n = 0;
			while (it != last)
			{
				++it;
				n++;
			}
			start = new T[n];
			finish = start;
			while (first != last)
			{
				*finish = *first;
				++first;
				++finish;
			}
			endofstorage = finish;
		}
		//拷贝构造函数
		myvector(myvector<T>& v)
		{
			size_t n = v.size();
			start = new T[n];
			for (size_t i = 0; i < n; ++i)
			{
				start[i] = v[i];
			}
			finish = start + n;
			endofstorage = finish;
		}
		//赋值运算符重载函数
		myvector<T>& operator=(vec::myvector<T> v)
		{
			this->swap(v);
			return *this;
		}
		//析构函数
		~myvector()
		{
			if (start)
			{
				delete[] start;
				start = finish = endofstorage = nullptr;
			}
		}
		/////////////////////////////////////////////////////
		//iterator
		iterator begin()
		{
			return start;
		}
		iterator end()
		{
			return finish;
		}
		////////////////////////////////////////////////////
		//capacity
		size_t size()const
		{
			return finish - start;
		}
		size_t capacity()
		{
			return endofstorage - start;
		}
		bool empty()const
		{
			return start == finish;
		}
		void resize(size_t newsize, const T& val = T())
		{
			size_t oldsize = size();
			if (newsize > oldsize)
			{
				//将有效元素个数增加到newsize,先检测空间是否足够
				size_t cap = capacity();
				if (newsize > cap)
					reserve(newsize);
				//多出来的元素用val填充
				for (size_t i = oldsize; i < newsize; ++i)
				{
					start[i] = val;
				}
			}
			finish = start + newsize;
		}
		void reserve(size_t newcapacity)
		{
			size_t oldcapacity = capacity();
			if (newcapacity > oldcapacity)
			{
				//开辟新空间
				T* temp = new T[newcapacity];
				//拷贝元素到新空间
				if (start)
				{
					//将旧空间元素赋值到新空间中去
					for (size_t i = 0; i < size(); ++i)
						temp[i] = start[i];
					//释放旧空间
					delete[] start;
				}
				//使用新空间
				size_t sz = size();
				start = temp;
				finish = start + sz;
				endofstorage = start + newcapacity;
			}
		}
		//////////////////////////////////////////////////
		//access
		T& front()
		{
			return *start;
		}
		T& back()
		{
			return *(finish - 1);
		}
		T& operator[](size_t index)
		{
			assert(index < size());
			return start[index];
		}
		T& at(size_t index)
		{
			if (index >= size())
			{
				throw out_of_range("myvetcor at method:index out_of_range");
			}
			return start[index];
		}
		////////////////////////////////////////////////
		//modify
		void push_back(const T& val)
		{
			//先检测空间是否足够
			if (finish == endofstorage)
				reserve(capacity() * 2 + 3);
			*finish = val;
			++finish;
		}
		void pop_back()
		{
			if (empty()) return;
			--finish;
		}
		//任意位置的插入和删除，时间复杂度O(n)
		iterator insert(iterator pos, const T& val)
		{
			//检测pos的合法性,范围在[begin,end)
			if (pos<begin() || pos>end())
				return end();
			//检测空间是否足够
			if (finish == endofstorage)
				reserve(capacity() * 2);
			auto it = finish - 1;
			while (it >= pos)
			{
				*(it + 1) = *it;
				--it;
			}
			*pos = val;
			++finish;
			return pos;
		}
		//删除迭代器处的元素
		iterator erase(iterator pos)
		{
			if (empty()) return end();
			if (pos < begin() || pos >= end())
				return end();
			auto it = pos + 1;
			while (it != end())
			{
				*(it - 1) = *it;
				++it;
			}
			--finish;
			return pos;
		}
		iterator erase(iterator first, iterator last)
		{
			auto it = first;
			auto pos = last;
			size_t n = last - first;

			while (pos != end())
			{
				*it = *pos;
				++it;
				++pos;
			}
			finish -= n;
			return first;
		}
		void clear()
		{
			erase(begin(), end());
		}
		//将两者的底层空间进行交换即可
		void swap(myvector<T>& v)
		{
			std::swap(start, v.start);
			std::swap(finish, v.finish);
			std::swap(endofstorage, v.endofstorage);
		}
	private:
		iterator start;//指向空间首地址，表征第一个元素
		iterator finish;//最后一个有效元素的后一个位置
		iterator endofstorage;//指向空间的末尾
	};
}
template<class T>
void PrintVector(vec::myvector<T>& v)
{
	for (size_t i = 0; i < v.size(); ++i)
	{
		cout << v[i] << " ";
	}
	cout << endl;
}

void TestMyvector1()
{
	vec::myvector<int> v1;
	vec::myvector<int> v2(10, 5);

	int array[] = { 0,1,2,3,4,5 };
	vec::myvector<int> v3(array, array + sizeof(array) / sizeof(array[0]));

	vec::myvector<int> v4(v3);

	for (size_t i = 0; i < v2.size(); ++i)
	{
		cout << v2[i] << " ";
	}
	cout << endl;

	for (auto e : v3)
	{
		cout << e << " ";
	}
	cout << endl;

	auto it = v4.begin();
	while (it != v4.end())
	{
		cout << *it << " ";
		++it;
	}
}
void TestMyvector2()
{
	vec::myvector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);

	cout << v.size() << endl;
	cout << v.capacity() << endl;

	v.resize(7, 6);
	PrintVector(v);

	v.resize(20, 8);
	PrintVector(v);

	v.resize(15);
	PrintVector(v);
	v.resize(8);
	PrintVector(v);
}

void TestMyvector3()
{
	vec::myvector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.push_back(5);
	v.push_back(6);

	cout << v.front() << endl;
	cout << v.back() << endl;
	cout << v[4] << endl;
	cout << v.at(5) << endl;

	v.insert(v.begin(), 7);
	PrintVector(v);

	v.erase(v.begin());
	PrintVector(v);

	v.erase(v.begin(), v.begin() + 3);
	PrintVector(v);

	v.clear();
	if (v.empty())
	{
		cout << "v is empty" << endl;
	}
	else
	{
		cout << "v is not empty" << endl;
	}
}

class Data
{
public:
	Data(int year = 1900, int month = 1, int day = 1)
		:_year(year)
		, _month(month)
		, _day(day)
	{}
private:
	int _year;
	int _month;
	int _day;
};
void TestMyvector4()
{
	vec::myvector<Data> v;
	v.push_back(Data(2023, 7, 20));
	v.push_back(Data(2023, 7, 21));
	v.push_back(Data(2023, 7, 22));
	v.push_back(Data(2023, 7, 23));

	cout << v.size() << endl;
}
class String
{
public:
	String(const char* str = "")
	{
		if (str == nullptr)
			str = "";
		_str = new char[strlen(str) + 1];
		strcpy(_str, str);
	}
	~String()
	{
		if (_str)
		{
			delete[] _str;
			_str = nullptr;
		}
	}
	String(const String& s)
		:_str(new char[strlen(s._str) + 1])
	{
		strcpy(_str, s._str);
	}
	String& operator=(const String& s)
	{
		if (this != &s)
		{
			char* temp = new char[strlen(s._str) + 1];
			strcpy(temp, s._str);

			delete[] _str;
			_str = temp;
		}
		return *this;
	}
private:
	char* _str;
};
void TestMyvector5()
{
	vec::myvector<String> v;
	v.push_back("11");
	v.push_back("22");
	cout << v.size() << endl;
}

#if 0
void TestMyvector6()
{
	vec::myvector<vec::myvector<int>> vv(5, vec::myvector<int>(6, 8));
	for (size_t i = 0; i < vv.size(); ++i)
	{
		for (size_t j = 0; j < vv[i].size(); ++j)
		{
			cout << vv[i][j] << " ";
		}
		cout << endl;
	}
}
#endif