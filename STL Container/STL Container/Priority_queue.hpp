#pragma once
#pragma once
#include<iostream>
using namespace std;

#include<vector>

//模拟实现priority_queue
//优先级队列(堆)
namespace pri
{
	//升序
	template<class T>
	class Less
	{
	public:
		bool operator()(const T& left, const T& right)
		{
			return left < right;
		}
	};
	//降序
	template<class T>
	class Greater
	{
	public:
		bool operator()(const T& left, const T& right)
		{
			return left > right;
		}
	};

	template<class T,class Container=std::vector<T>,class Compare=Less<T>>
	class priority_queue
	{
	public:
		priority_queue()
		{}
		template<class Iterator>
		priority_queue(Iterator first, Iterator last)
			:_con(first, last)
		{
			//从倒数第一个非叶子节点位置，倒着往前将每个节点向下进行调整，一直调整到根的位置
			int lastNotleaf = (_con.size() - 2) / 2;
			for (int i = lastNotleaf; i >= 0; --i)
			{
				AdjustDown(i);
			}
		}
		void push(const T& val)
		{
			_con.push_back(val);
			//对整棵树进行调整
			AdjustUp();
		}
		void pop()
		{
			if (empty())
				return;
			//将堆顶元素和最后一个元素进行交换
			std::swap(_con.front(), _con.back());
			//将堆中元素减1，即将最后一个元素进行删除
			_con.pop_back();
			//将堆顶元素进行往下调整
			AdjustDown(0);
		}
		const T& top()const
		{
			return _con[0];
		}
		size_t size()
		{
			return _con.size();
		}
		bool empty()
		{
			return _con.empty();
		}
	private:
		//时间复杂度：O(logN)
		void AdjustDown(size_t parent)
		{
			size_t sz = size();
			size_t child = parent * 2 + 1;

			while (child < sz)
			{
				//找到两个孩子中较大的孩子
				Compare com;
				if (child + 1 < sz && com(_con[child], _con[child + 1]))
					child += 1;
				//检测双亲是否满足堆的特性
				if (com(_con[parent], _con[child]))
				{
					swap(_con[parent], _con[child]);
					parent = child;
					child = parent * 2 + 1;
				}
				else
				{
					return;
				}
			}
		}
		void AdjustUp()
		{
			size_t child = size() - 1;
			size_t parent = (child - 1) / 2;

			Compare com;
			while (child)
			{
				if (com(_con[parent], _con[child]))
				{
					std::swap(_con[parent], _con[child]);
					child = parent;
					parent = (child - 1) / 2;
				}
				else
				{
					return;
				}
			}
		}
	private:
		Container _con;
	};
}
void TestMyPriorityQueue1()
{
	int array[] = { 5,9,2,1,8,3,7,4,0,6 };
	pri::priority_queue<int> q(array, array + sizeof(array) / sizeof(array[0]));

	cout << q.size() << endl;
	cout << q.top() << endl;

	q.pop();
	cout << q.size() << endl;
	cout << q.top() << endl;

	q.push(9);
	cout << q.size() << endl;
	cout << q.top() << endl;
}
void TestMyPriorityQueue2()
{
	int array[] = { 5,9,2,1,8,3,7,4,0,6 };
	pri::priority_queue<int> q;
	for (auto e : array)
	{
		q.push(e);
	}
	cout << q.size() << endl;
	cout << q.top() << endl;

	q.pop();
	cout << q.size() << endl;
	cout << q.top() << endl;

	q.push(9);
	cout << q.size() << endl;
	cout << q.top() << endl;
}