#pragma once
#include<iostream>
using namespace std;

#include<deque>
#include<list>

namespace que
{
	//队列的模拟实现，实际上是对list的封装
	//实际上STL内部都使用了deque
	template<class T, class Container = std::deque<T>>
	class queue
	{
	public:
		queue()
		{}
		void push(const T& val)
		{
			c.push_back(val);
		}
		void pop()
		{
			if (empty())
				return;
			c.pop_front();
		}
		T& front()
		{
			return c.front();
		}
		T& back()
		{
			return c.back();
		}
		size_t size()
		{
			return c.size();
		}
		bool empty()
		{
			return c.empty();
		}
	private:
		list<T> c;
	};
}
void TestMyqueue()
{
	que::queue<int> q;
	q.push(1);
	q.push(2);
	q.push(3);
	q.push(4);
	q.push(5);

	cout << q.size() << endl;
	cout << q.front() << endl;
	cout << q.back() << endl;

	q.pop();
	q.pop();
	cout << q.size() << endl;
	cout << q.front() << endl;
	cout << q.back() << endl;
}