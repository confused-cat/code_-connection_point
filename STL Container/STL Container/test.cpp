#define _CRT_SECURE_NO_WARNINGS
#include"Mystring.hpp"
#include"Myvector.hpp"
#include"Mylist.hpp"
#include"Stack.hpp"
#include"Queue.hpp"
#include"Priority_queue.hpp"

int main()
{
	//1.string
	//TestMystring1();
	//TestMystring2();
	//TestMystring3();
	//TestMystring4();

	//2.vector
	//TestMyvector1();
	//TestMyvector2();
	//TestMyvector3();
	//TestMyvector4();
	//TestMyvector5();
	//TestMyvector6(); //����

	//3.list
	//TestMylist1();
	//TestMylist2();
	//TestMylist3();
	//TestMylist4();

	//4.stack
	//TestMystack();

	//5.queue
	//TestMyqueue();

	//6.priority_queue
	//TestMyPriorityQueue1();
	//TestMyPriorityQueue2();

	return 0;
}