//实现json的序列化和反序列化

#include<cstring>
#include<iostream>
#include<jsoncpp/json/json.h>
#include<sstream>

//json数据序列化
void serialize(Json::Value& value,std::string& body)
{
    Json::StreamWriterBuilder swb;
    swb["commentStyle"]="None";//将格式缩进置为空
    
    Json::StreamWriter* sw=swb.newStreamWriter();
    //int waite(Value const& root,std::ostream* sout)//const引用和输入参数
    std::stringstream ss;
    int ret=sw->write(value,&ss);
    if(ret!=0)
    {
        std::cout<<"serialize error\n"<<std::endl;
        delete sw;
        return;
    }
    body=ss.str();
    return;
}
//数据解析：数据反序列化
bool unserialize(std::string& body,Json::Value& value)
{
    Json::CharReaderBuilder crb;

    Json::CharReader* cr=crb.newCharReader();
    std::string err;
    bool ret=cr->parse(body.c_str(),body.c_str()+body.size(),&value,&err);
    if(ret==false)
    {
        std::cout<<"unserialize error\n";
        return false;
    }
    return true;
}
int main()
{
    Json::Value val;
    val["姓名"]="小明";
    val["年龄"]=18;
    val["成绩"].append(30);
    val["成绩"].append(40);
    val["成绩"].append(50);
    std::string body;
    serialize(val,body);
    std::cout<<body<<std::endl;

    Json::Value stu;
    unserialize(body,stu);
    std::cout<<stu["姓名"].asString()<<std::endl;
    std::cout<<stu["年龄"].asInt()<<std::endl;
    if(stu["成绩"].isArray())
    {
        for(int i=0;i<stu["成绩"].size();i++)
        {
            std::cout<<stu["成绩"][i].asFloat()<<std::endl;
        }
    }

    return 0;
}
