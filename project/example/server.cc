#include<iostream>
#include<string>
#include"mongoose.h"

#define WWWROOT "./wwwroot"

void handler(struct mg_connection *c,int ev,void *ev_data,void *fn_data)
{
    if(ev==MG_EV_HTTP_MSG)
    {
        struct mg_http_message *hm=(struct mg_http_message*)ev_data;
        if(mg_http_match_uri(hm,"/hello"))
        {
            std::string body;
            body="<html><body><h1>Hello World</h1></body></html>";
            mg_http_reply(c,200,"","%s",body.c_str());
        }
        else
        {
            struct mg_http_serve_opts opts={.root_dir=WWWROOT};
            mg_http_serve_dir(c,hm,&opts);
        }
    }

}

int main()
{
    struct mg_mgr mgr;
    mg_mgr_init(&mgr);
    mg_http_listen(&mgr,"0.0.0.0:10000",handler,&mgr);
    for(;;)
    {
        mg_mgr_poll(&mgr,1000);
    }
    return 0;
}

