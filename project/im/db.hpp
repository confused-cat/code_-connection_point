#ifndef __M_DB_H__
#define __M_DB_H__ 
#include<iostream>
#include<cassert>
#include<memory>
#include<mutex>
#include<jsoncpp/json/json.h>
#include"util.hpp"

namespace im 
{

    //一、 数据库用户信息表的封装
    //新增用户（id,用户名，昵称，密码，创建时间）
    //用户登录，密码修改，昵称修改，用户修改，用户信息的获取（通过用户名获取用户信息+通过用户获取用户信息）
    class UserTable
    {
        private:
            MYSQL* _mysql;
            std::mutex _mutex;//在进行数据查询时，多个线程访问数据库，若两个线程都用了UserTable实例化出来的对象，
                              //一个查询信息，一个插入信息，若两个使用同一个句柄，

        public:
            UserTable(const std::string &host,const std::string &user,const std::string &pass,const std::string &db,int port)//初始化操作句柄
            {
                _mysql=MysqlUtil::mysql_create(host,user,pass,db,port);
                assert(_mysql!=NULL);//直接断言操作句柄一定可获取，即可以访问数据库
            }

            ~UserTable()//释放数据库操作句柄
            {
                MysqlUtil::mysql_destroy(_mysql);
            }
            
            bool insert(const Json::Value& user)//1.新增用户
            {
                //id 用户名 昵称 密码 创建时间
                //id、username、nickname、password、ctime
                //组织一个sql语句来执行
#define INSERT_USER "insert user values(null,'%s','%s',MD5('%s'),now());"
                char sql[4096]={0};
                //将语句进行格式化之后将语句组织存到sql空间中去
                snprintf(sql,4095,INSERT_USER,
                        user["username"].asCString(),
                        user["nickname"].asCString(),
                        user["password"].asCString());
                //执行语句
                return MysqlUtil::mysql_exec(_mysql,sql);
            }
            
            bool check_login(const Json::Value& user)//2.登录验证
            {
#define LOGIN_CHECK "select id from user where username='%s' and password=MD5('%s');"
                char sql[4096]={0};
                snprintf(sql,4095,LOGIN_CHECK,
                        user["username"].asCString(),
                        user["password"].asCString());
                MYSQL_RES* res;
                {
                    std::unique_lock<std::mutex> lock(_mutex);
                    bool ret=MysqlUtil::mysql_exec(_mysql,sql);
                    if(ret==false)
                    {
                        return false;
                    }
                    res=mysql_store_result(_mysql);
                    if(res==NULL)
                    {
                        std::cout<<"store result failed:"<<mysql_error(_mysql)<<std::endl;
                        return false;
                    }
                    int num_row=mysql_num_rows(res);
                    if(num_row!=1)
                    {
                        mysql_free_result(res);
                        return false;
                    }
                    mysql_free_result(res);
                    return true;
                }
            }

            bool updata_password(const Json::Value& user)//3.密码修改
            {
#define MOD_PASSWD_BY_NAME "update user set password=MD5('%s') where username='%s' and password=MD5('%s');"
                char sql[4096]={0};

                //检测一下要查询的 用户名+密码 是否存在
                if(user["username"].asString().empty() || user["password"].asString().empty())
                {
                    std::cout<<"updata_password data error!!!"<<std::endl;
                    return false;
                }
                snprintf(sql,4095,MOD_PASSWD_BY_NAME,
                        user["new_password"].asCString(),
                        user["username"].asCString(),
                        user["old_password"].asCString()); 
                return MysqlUtil::mysql_exec(_mysql,sql);
            }

            bool updata_nickname(const Json::Value& user)//4.昵称修改
            {
#define MOD_NIKENAME_BY_NAME "update user set nickname='%s' where username='%s' and password=MD5('%s');"
                char sql[4096]={0};
                if(user["nickname"].asString().empty() || user["username"].asString().empty() || user["password"].asString().empty())
                {
                    std::cout<<"updata_nickname data error!!!"<<std::endl;
                    return false;
                }
                snprintf(sql,4095,MOD_NIKENAME_BY_NAME,
                        user["nickname"].asCString(),
                        user["username"].asCString(),
                        user["password"].asCString());
                return MysqlUtil::mysql_exec(_mysql,sql);

            }

            bool remove(int id)//5.删除用户
            {
#define DELETE_USER "delete from user where id=%d;"
                char sql[4096]={0};
                snprintf(sql,4095,DELETE_USER,id);
                return MysqlUtil::mysql_exec(_mysql,sql);
            }
            bool select_by_id(int id,Json::Value *user)
            {
#define SELECT_BY_ID "select username,nickname from user where id=%d;"
                char sql[4096]={0};
                snprintf(sql,4095,SELECT_BY_ID,id);
                MYSQL_RES* res;//结果集
                {
                    std::unique_lock<std::mutex> lock(_mutex);
                    bool ret=MysqlUtil::mysql_exec(_mysql,sql);
                    if(ret==false)
                    {
                        return false;
                    }
                    res=mysql_store_result(_mysql);
                    if(res==NULL)
                    {
                        std::cout<<"store result failed:"<<mysql_error(_mysql)<<std::endl;
                        return false;
                    }   
                }
                int num_row=mysql_num_rows(res);//结果集的行数
                   if(num_row!=1)
                   {
                       std::cout<<"query result error\n";
                       return false;
                   }
                   MYSQL_ROW row=mysql_fetch_row(res);
                  (*user)["id"]=id;
                   (*user)["username"]=row[0];
                   (*user)["nickname"]=row[1];
                   //释放结果集
                   mysql_free_result(res);
                   return true;

            }

            bool select_by_name(const std::string& name,Json::Value* user)//7.通过用户名获取用户信息
            {
#define SELECT_BY_NAME "select id,nickname from user where username='%s';"
                char sql[4096]={0};
                snprintf(sql,4095,SELECT_BY_NAME,name.c_str());
                MYSQL_RES* res;//结果集
                {
                    std::unique_lock<std::mutex> lock(_mutex);
                    bool ret=MysqlUtil::mysql_exec(_mysql,sql);
                    if(ret==false)
                    {
                        return false;
                    }
                    res=mysql_store_result(_mysql);
                    if(res==NULL)
                    {
                        std::cout<<"store result failed:"<<mysql_error(_mysql)<<std::endl;
                        return false;
                    }   
                }
                int num_row=mysql_num_rows(res);//结果集的行数
                   if(num_row!=1)
                   {
                       std::cout<<"query result error\n";
                       return false;
                   }
                   MYSQL_ROW row=mysql_fetch_row(res);
                  (*user)["id"]=std::stoi(row[0]);
                   (*user)["username"]=name;
                   (*user)["nickname"]=row[1];
                   //释放结果集
                   mysql_free_result(res);
                   return true;
            }
    };


    //二、数据库聊天历史信息表 oldmassage表
    //表包括：新增聊天消息+获取历史聊天消息
    class MassageTable 
    {
        private:
            MYSQL* _mysql;
            std::mutex _mutex;

        public:
            MassageTable(const std::string &host,const std::string &user,const std::string &pass,const std::string &db,int port)
            {
                _mysql=MysqlUtil::mysql_create(host,user,pass,db,port);
                assert(_mysql!=NULL);
            }
            ~MassageTable()
            {
                MysqlUtil::mysql_destroy(_mysql);
            }
            bool insert(const Json::Value& msg)//新增聊天信息
            {
                //id,userid,msg,ctime
#define INSERT_MSG "insert oldmessage values(null,%d,'%s',now());"
                char sql[4096]={0};
                snprintf(sql,4095,INSERT_MSG,
                        msg["userid"].asInt(),
                        msg["msg"].asCString());
                return MysqlUtil::mysql_exec(_mysql,sql);
            }
            bool select_part(int sec,Json::Value *msg)//获取sec秒内的历史聊天消息
            {
#define SELECT_PART_MSG "select id,userid,msg,ctime from oldmessage where timestampdiff(second,ctime,now())<%d;"
                char sql[4096]={0};
                snprintf(sql,4095,SELECT_PART_MSG,sec);
                MYSQL_RES* res;//结果集
               {
                  std::unique_lock<std::mutex> lock(_mutex);                                      bool ret=MysqlUtil::mysql_exec(_mysql,sql);
                 if(ret==false)
                       {
                           return false;
                       }
                       res=mysql_store_result(_mysql);
                       if(res==NULL)
                       {
                           std::cout<<"store result failed:"<<mysql_error(_mysql)<<std::endl;                                                                      
                           return false;
                       }   
                   }

                   int num_row=mysql_num_rows(res);
                   for(int i=0;i<num_row;++i)
                   {
                       MYSQL_ROW row=mysql_fetch_row(res);
                       Json::Value m;
                       m["id"]=std::stoi(row[0]);
                       m["userid"]=std::stoi(row[1]);
                       m["msg"]=row[2];
                       m["ctime"]=row[3];
                       msg->append(m);
                   }
                   mysql_free_result(res);
                   return true;
            }
    };

}


#endif 
