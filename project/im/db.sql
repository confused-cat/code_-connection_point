
create database if not exists im_system;

drop table user if exists;
drop table oldmessage if exists;

use im_system;
/*在 im_system 数据库中创建两张表：用户信息表user+聊天信息表oldmassage.*/
/*创建用户信息表（id,用户名，昵称，密码，创建时间）*/

create table if not exists user(
    id int primary key auto_increment,  /*id设置为主键且为自增属性*/
   username varchar(32) unique key not null,
    nickname varchar(32) unique key not null,
    password varchar(32) unique key not null,
    ctime datetime
);

/*创建聊天信息表*/

create table if not exists oldmessage(
    id int primary key auto_increment,/*auto_increment对主键建立id自增*/
    userid int,
    msg varchar(255),
    ctime datetime 
);
