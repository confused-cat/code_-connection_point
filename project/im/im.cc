
#include<iostream>
#include<string>
#include"util.hpp"
#include"db.hpp"
#include"server.hpp"

#define HOST "127.0.0.1"
#define USER "root"
#define PASS ""
#define DB "im_system"
#define PORT 3306

void user_table_test()
{
    im::UserTable *ut=new im::UserTable(HOST,USER,PASS,DB,PORT); //用UserTable表实例化出一个对象出来
    Json::Value user; //组织一条Json数据，进行用户的信息插入，即用户进行注册

    //插入新增用户小吴
    //user["username"]="xiaowu";
    //user["nickname"]="小吴"；
    //user["password"]="456456";
    //ut->insert(user);
    
    //2.将小吴的密码改为123123
    //user["password"]="456456";
   // user["old_password"]="456456";
   // user["new_password"]="123123";
   // ut->updata_password(user);
   ////////////////////////////////// 此处遗留问题：在用户进行注册时，其密码不可重复

    //3.将小华的昵称改为小花
    //user["nickname"]="小花";
    //user["username"]="xiaohua";
    //user["password"]="123123";
   // ut->updata_nickname(user);
   ///////////////////////////////// 此处遗留问题：在用户进行注册的时候其昵称不可重复
   
    //4.将一开始注册的用户小花进行移除
    //ut->remove(1);
    
    //5.查询小花的用户信息,此时小吴的用户id=3
    Json::Value root;
    ut->select_by_id(3,&root);
    std::string body;
    im::JsonUtil::serialize(root,&body);
    std::cout<<body<<std::endl;

    //6.通过用户名来进行获取
    //Json::Value root;
   // ut->select_by_name("xiaowu",&root);
    //std::string body;
    //im::JsonUtil::serialize(root,&body);
    //std::cout<<body<<std::endl;

}
void message_table_test()
{
    im::MassageTable *mt=new im::MassageTable(HOST,USER,PASS,DB,PORT);
    //1.insert new message into oldmessage
    //Json::Value mess;
    //mess["userid"]=2;
    //mess["msg"]="干饭干饭！";
    //mt->insert(mess);
    
    //2.
    //Json::Value root;
    //mt->select_part(1200,&root);
    //std::string body;
    //im::JsonUtil::serialize(root,&body);
    //std::cout<<body<<std::endl;
}

void server_test(int port)
{
    im::Server server(HOST,USER,PASS,DB,PORT);//PORT:数据库端口
    server.Start(port);//服务器监听绑定端口
}
int main()
{
    //user_table_test();
    //message_table_test();
    server_test(9000);
    return 0;
}
