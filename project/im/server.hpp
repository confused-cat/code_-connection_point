#include<iostream>
#include<assert.h>
#include<string>
#include"mongoose.h"
#include"db.hpp"
#include"util.hpp"
#include"session.hpp"

namespace im 
{

    class Server
    {
        private:
            SessionManager *_ss_manager;
            UserTable *_user_table;
            MassageTable* _msg_table;
            struct mg_mgr _mgr;
            std::string _rootdir="./wwwroot";
        private:

            static bool user_add(struct mg_connection* c,struct mg_http_message *hm,Server *srv)//新增用户请求处理函数
            {
                std::string body(hm->body.ptr,hm->body.len);
                Json::Value user_json;
                Json::Value res_json;
                 bool ret=JsonUtil::unserialize(body,&user_json);//将数据进行反序列化
                 if(ret==false)
                   {
                    //如果数据解析失败，则将错误原因序列化进行返回
                    res_json["result"]=false;
                    res_json["rease"]="用户数据解析失败！";
                    std::string body1;
                    JsonUtil::serialize(res_json,&body1);
                    mg_http_reply(c,400,NULL,"%s",body1.c_str());
                    return false;
                   }
                     UserTable *table=srv->get_user_table();//将数据库操作句柄获取到，将序列化成功的数据插入数据库
                    ret=table->insert(user_json);
                    if(ret==false)
                    {
                        res_json["result"]=false;
                        res_json["reason"]="向用户表中插入新增用户信息失败！";
                        std::string body1;
                        JsonUtil::serialize(res_json,&body1);
                        mg_http_reply(c,500,NULL,"%s",body1.c_str());
                        return false;
                    }
                    res_json["result"]=true;
                    res_json["reason"]="新增用户成功";
                    std::string body1;
                    JsonUtil::serialize(res_json,&body1);
                    mg_http_reply(c,200,NULL,"%s",body1.c_str());
            }

            static bool user_login(struct mg_connection* c,struct mg_http_message *hm,Server *srv)//登录验证处理函数
            {
                //1.从请求正文中获取提交的用户名，昵称和密码
                std::string body(hm->body.ptr,hm->body.len);
                //2.将数据进行反序列化
                Json::Value user_json;//保存输入数据的反序列化数据
                Json::Value res_json;//保存结果的数据
                bool ret=JsonUtil::unserialize(body,&user_json);//将数据进行反序列化
                if(ret==false)
                {
                    res_json["result"]=false;
                    res_json["reason"]="用户数据解析失败！";
                    std::string body1;
                    JsonUtil::serialize(res_json,&body1);
                    mg_http_reply(c,400,NULL,"%s",body1.c_str());
                    return false;
                }
                //3.在数据库中进行验证
                UserTable *table=srv->get_user_table();//将数据库操作句柄获取到，将序列化成功的数据插入数据库
                ret=table->check_login(user_json);
                if(ret==false)
                {
                    res_json["result"]=false;
                    res_json["reason"]="用户名密码错误！";
                    std::string body1;
                    JsonUtil::serialize(res_json,&body1);
                    mg_http_reply(c,401,NULL,"%s",body1.c_str());
                    return false;
                }
                //4.为登录成功的客户端新建会话
                SessionManager* ssm=srv->get_ss_manager();//获取会话的操作句柄
                uint64_t ssid=ssm->insert(user_json);//将为用户建立的会话进行返回，将其保存在cookie中

                //设置cookie： Set-Cookie:SSID=1
                std::string cookie="Set-Cookie: SSID="+std::to_string(ssid)+"\r\n";
          
                //5.返回200（成功状态码）
                res_json["result"]=true;
                res_json["reason"]="用户登录成功！";//username login success.
                std::string body1;
                JsonUtil::serialize(res_json,&body1);
                mg_http_reply(c,200,cookie.c_str(),"%s",body1.c_str());
            }
            static bool user_passwd_mod(struct mg_connection* c,struct mg_http_message* hm,Server* srv)//密码修改请求处理函数
            {
                std::string body(hm->body.ptr,hm->body.len);
                Json::Value user_json;
                Json::Value res_json;
                //将输入的数据进行反序列化，将用户名和密码昵称给解析出来和数据库中进行对比
                bool ret=JsonUtil::unserialize(body,&user_json);
                if(ret==false)
                {
                    res_json["result"]=false;
                    res_json["reason"]="用户数据解析失败！";
                    std::string body1;
                    JsonUtil::serialize(res_json,&body1);//将错误数据进行序列化后返回
                    mg_http_reply(c,401,NULL,"%s",body1.c_str());
                    return false;
                }
                //数据反序列化成功
                UserTable *table=srv->get_user_table();
                ret=table->updata_password(user_json);
                if(ret==false)
                {
                    res_json["result"]=false;
                    res_json["reason"]="用户密码修改失败！";
                    std::string body1;
                    JsonUtil::serialize(res_json,&body1);
                    mg_http_reply(c,401,NULL,"%s",body1.c_str());
                    return false;
                }
                res_json["result"]=true;
                res_json["reason"]="用户密码修改成功！";
                std::string body1;
                JsonUtil::serialize(res_json,&body1);
                mg_http_reply(c,200,NULL,"%s",body1.c_str());
            }

            static bool user_nickname_mod(struct mg_connection* c,struct mg_http_message* hm,Server* srv)//昵称修改请求处理函数
            {
                std::string body(hm->body.ptr,hm->body.len);
                Json::Value user_json;
                Json::Value res_json;
                //将输入的数据进行反序列化然后和数据库user表中数据进行对比，之后进行序列化数据返回
                bool ret=JsonUtil::unserialize(body,&user_json);
                if(ret==false)
                {
                    res_json["result"]=false;
                    res_json["reason"]="用户数据解析失败！";
                    std::string body1;
                    JsonUtil::serialize(res_json,&body1);
                    mg_http_reply(c,401,NULL,"%s",body1.c_str());
                    return false;
                }
                UserTable *table=srv->get_user_table();
                ret=table->updata_nickname(user_json);
                if(ret==false)
                {
                    res_json["result"]=false;
                    res_json["reason"]="用户昵称修改失败！";
                    std::string body1;
                    JsonUtil::serialize(res_json,&body1);
                    mg_http_reply(c,401,NULL,"%s",body1.c_str());
                    return false;
                }
                res_json["result"]=true;
                res_json["reason"]="用户密码修改成功！";
                std::string body1;
                JsonUtil::serialize(res_json,&body1);
                mg_http_reply(c,200,NULL,"%s",body1.c_str());
            }
           static bool get_user_msg(struct mg_connection* c,struct mg_http_message* hm,Server* srv)//获取用户信息请求处理
           {
               struct mg_str *cookie=mg_http_get_header(hm,"Cookie");
               if(cookie==NULL)
               {
                   std::cout<<"没有获取到cookie\n";
                   mg_http_reply(c,401,"","%s","401 Unauthorized");
                   return false;
               }
               std::string cookie_str(cookie->ptr,cookie->len);
               std::cout << cookie_str << std::endl;
               size_t pos=cookie_str.find("=");
               std::string ssid_str=cookie_str.substr(pos+1);
               //uint64_t ssid=stol(ssid_str);
               uint64_t ssid=std::stol(ssid_str); 

               SessionManager* ssm=srv->get_ss_manager();

               if(ssm->exists(ssid)==false)
               {
                   std::cout<<"没有对应的session\n";
                   mg_http_reply(c,401,"","%s","401 Unauthorized");
                   return false;
               }
               Json::Value user;
               ssm->get_user_session(ssid,&user);

               UserTable* table=srv->get_user_table();
               Json::Value new_user;
               table->select_by_name(user["username"].asString(),&new_user);
               std::string user_str;
               JsonUtil::serialize(new_user,&user_str);
                                                                                                                                                                                       
               //将登录成功创建的会话进行移除，重新创建一新的会话，保证每次的session都是最新的
               ssm->remove(ssid);
               ssid=ssm->insert(new_user);
               std::string header="Content-Type:application/json\r\n"; 
               header+="Set-Cookie: SSID"+std::to_string(ssid)+"\r\n";
               mg_http_reply(c,200,header.c_str(),"%s",user_str.c_str());
           }
            static bool del_user(struct mg_connection* c,struct mg_http_message* hm,Server* srv)//用户删除请求处理
            {
                std::string body(hm->body.ptr,hm->body.len);
                Json::Value res_json;
                Json::Value user_json;
                JsonUtil::unserialize(body,&user_json);
                int idd=user_json["id"].asInt();
                UserTable* table=srv->get_user_table();
                bool ret=table->remove(idd);
                if(ret==false)
                {
                    std::string body1;
                    res_json["result"]=false;
                    res_json["reason"]="用户删除出错，请重新操作！";
                    JsonUtil::serialize(res_json,&body1);
                    mg_http_reply(c,401,NULL,"%s",body1.c_str());
                }
                res_json["result"]=true;
                res_json["reason"]="用户删除成功！";
                std::string body1;
                JsonUtil::serialize(res_json,&body1);
                mg_http_reply(c,200,NULL,"%s",body1.c_str());
            }
            static bool get_history_msg(struct mg_connection* c,struct mg_http_message* hm,Server* srv)//get histroy message  
            {
                struct mg_str* cookie=mg_http_get_header(hm,"Cookie");
                if(cookie==NULL)
                {
                    std::cout<<"没有获取到cookie\n";
                    mg_http_reply(c,401,"","%s","401 Unauthorized");
                    return false;
                }
                std::string cookie_str(cookie->ptr,cookie->len);
                size_t pos=cookie_str.find("=");
                std::string ssid_str=cookie_str.substr(pos+1);
                uint64_t ssid=std::stol(ssid_str);
                SessionManager *ssm=srv->get_ss_manager();
                if(ssm->exists(ssid)==false)
                {
                    std::cout<<"没有对应的session\n";
                    mg_http_reply(c,401,"","%s","401 Unauthorized");
                    return false; 
                }

                MassageTable *table=srv->get_msg_table();
                Json::Value msgs;
                bool ret=table->select_part(86400,&msgs);
                if(ret==false)
                {
                    Json::Value res_json;
                    res_json["result"]=false;
                    res_json["reason"]="获取历史消息失败！";
                    std::string body;
                    JsonUtil::serialize(res_json,&body);
                    mg_http_reply(c,500,NULL,"%s",body.c_str());
                }
                //获取历史聊天消息成功
                std::string body;
                JsonUtil::serialize(msgs,&body);
                
                //Json::Value user;
                //ssm->get_user_session(ssid,&user);
                //将会话删除，再重新建立一个会话
                //ssm->remove(ssid);
                //ssid=ssm->insert(user);
                std::string header="Content-Type:application/json\r\n";
                mg_http_reply(c,200,header.c_str(),"%s",body.c_str());
            }
            static void broadcast_msg(struct mg_mgr *mgr,const std::string &msg)//广播消息数据
            {
                struct mg_connection *conn=mgr->conns;
                for(;conn!=NULL;conn=conn->next)
                {
                    if(conn->is_websocket)
                    {
                        mg_ws_send(conn,msg.c_str(),msg.size(),WEBSOCKET_OP_TEXT);//发送websocket消息
                    }
                }
            }
            //业务处理函数
            static void handler(struct mg_connection* c,int ev,void* ev_data,void* fn_data)//ev:当前链接触发的请求事件；ev_data:事件请求的数据；fn_data:服务器的操作句柄对象
            {
                Server* srv=(Server*)fn_data;
                if(ev==MG_EV_HTTP_MSG)//http协议连接阶段
                {
                    struct mg_http_message* hm=(struct mg_http_message*)ev_data;
                    
                    std::string method(hm->method.ptr,hm->method.len);//请求方式
                    std::string uri(hm->uri.ptr,hm->uri.len); //资源路径
                    
                    if(method=="POST" && uri=="/user")//1.新增用户请求（method:post && uri:/user）
                    {
                        user_add(c,hm,srv);
                    }
                    else if(method=="POST" && uri=="/login")//2.登录验证（method:post && uri:/login）
                    {
                        user_login(c,hm,srv);
                    }
                    else if(method=="PUT" && uri=="/user/passwd")//3.密码修改（method:put && uri:/user/passwd）
                    {
                        user_passwd_mod(c,hm,srv);
                    }
                    else if(method=="PUT" && uri=="/user/nickname")//4.昵称修改（method:put && uri:/user/nickname）
                    {
                        user_nickname_mod(c,hm,srv);
                    }
                    else if(method=="DELETE" && uri=="/user")//5.用户删除（method:delete && uri:/user）
                    {
                        del_user(c,hm,srv);
                    }
                    else if(method=="GET" && uri=="/user")//6.获取用户信息（method:get && uri:/user）
                    {
                        get_user_msg(c,hm,srv);//此处通过用户id来进行获取，因id唯一
                    }
                    else if(method=="GET" && uri=="/message")//7.get history message 
                    {
                        get_history_msg(c,hm,srv);
                    }
                    else if(method=="GET" && uri=="/ws")//8.协议切换请求，将协议切换为websocket协议
                    {
                        //获取连接的cookie字段
                        struct mg_str *cookie=mg_http_get_header(hm,"Cookie");
                        if(cookie==NULL)//即直接跳过登录验证获取到了首页
                        {
                            mg_http_reply(c,401,"","%s","-----401 Unauthorized");
                            return;
                        }
                        std::string cookie_str(cookie->ptr,cookie->len);
                        size_t pos=cookie_str.find("=");
                        std::string ssid_str=cookie_str.substr(pos+1);//将session截取出来作为cookie
                        uint64_t ssid=std::stol(ssid_str);//将字符串转换为 long int 
      
                        SessionManager *ssm=srv->get_ss_manager();//获取session的操作句柄
                        if(ssm->exists(ssid)==false)
                        {
                            mg_http_reply(c,401,"","%s","#####401 Unauthorized");
                            return;
                        }
                        mg_ws_upgrade(c,hm,NULL);//将给定的http连接升级到websocket连接
                        ssm->remove(ssid);
                    }
                    else 
                    {
                        //除以上请求外，统一认为其为静态资源请求
                        //独立于这些请求以外的请求处理为返回404，找不到返回404
                        struct mg_http_serve_opts opts={.root_dir=srv->rootdir().c_str()};
                        //让连接的处理到静态资源根目录下找对应的文件来进行相应的响应
                        //找不到，返回404
                        mg_http_serve_dir(c,hm,&opts);
                    }
                }
                else if(ev==MG_EV_WS_OPEN)//websocket握手成功
                {
                    //websocket握手成功处理放于前端进行处理
                    
                    //即用户登录成功后进入到了聊天室，此时应 广播"xxx 进入了聊天室"
                    //1.进行数据的广播：XXX上线了
                    //std::unordered_map<struct mg_connection*,uint64_t> *conn_ss=srv->get_conn_session();
                    //auto it=conn_ss->find(c);
                    //if(it==conn_ss->end())
                    //{
                        //return;
                    //}
                    //uint64_t ssid=it->second;
                    //2.移除session
                    //SessionManager* ssm=srv->get_ss_manager();
                    //bool ret=ssm->exists(it->second);
                    //if(ret!=false)
                    //{
                        //Json::Value user;
                        //ssm->get_user_session(ssid,&user);
                        //std::string msg=user["nickname"].asString()+"上线了！";
                        //broadcast_msg(srv->get_mgr(),msg);
                    //}
                }
                else if(ev==MG_EV_WS_MSG)//websocket数据通信，即客户端发送了一条聊天消息
                {
                    //客户端发送了一条聊天消息，将这条消息进行广播，给每个客户端都发送此条消息
                    struct mg_ws_message* wsmsg=(struct mg_ws_message*)ev_data;
                    std::string msg(wsmsg->data.ptr,wsmsg->data.len);
                    std::cout<<msg<<std::endl;

                    broadcast_msg(srv->get_mgr(),msg);//广播数据消息
                    
                    Json::Value msg_json;
                    JsonUtil::unserialize(msg,&msg_json);
                    srv->get_msg_table()->insert(msg_json);
                }
                else if(ev==MG_EV_CLOSE)//通信连接断开
                {
                    //连接的关闭断开放于前端进行实现
                    
                    //1.获取连接对应的session
                    //std::unordered_map<struct mg_connection*,uint64_t> *conn_ss=srv->get_conn_session();
                    //auto it=conn_ss->find(c);
                    //if(it==conn_ss->end())
                    //{
                       // return;
                    //}
                    //uint64_t ssid=it->second;
                    //2.移除session
                    //SessionManager *ssm=srv->get_ss_manager();
                    //bool ret=ssm->exists(it->second);
                    //if(ret==true)
                    //{
                       // Json::Value user;
                        //ssm->get_user_session(ssid,&user);
                        //std::string msg=user["nickname"].asString()+"下线了！";
                        //broadcast_msg(srv->get_mgr(),msg);
                        //ssm->remove(it->second);
                    //}
                    //3.将连接与session的关系信息，并从关系表中进行删除
                    //ssm->remove(it->second);//将会话中ssid进行删除
                    //conn_ss->erase(c);//然后在map映射表中进行ssid的删除
                }
            }
public:
            Server(const std::string &host,const std::string &user,const std::string &pass,const std::string &db,int port)
            {
                _ss_manager=new SessionManager();
                _user_table=new UserTable(host,user,pass,db,port);
                _msg_table=new MassageTable(host,user,pass,db,port);
                mg_mgr_init(&_mgr);
                //初始化服务器的操作句柄
            }
            
            struct mg_mgr* get_mgr()//获取服务器操作句柄
            {
                return &_mgr;
            }
            const std::string& rootdir()//获取私有成员静态资源根目录
            {
                return _rootdir;
            }
            UserTable *get_user_table()//获取私有成员数据库操作表
            {
                return _user_table;
            }
            MassageTable* get_msg_table()//获取私有成员数据历史消息操作表
            {
                return _msg_table;
            }
            SessionManager *get_ss_manager()//获取私有成员中的客户端ssid
            {
                return _ss_manager;
            }
            //开始进行监听操作
            void Start(int port)
            {
                //开始对事件进行连续监听
                std::string address="0.0.0.0:"+std::to_string(port);
                mg_http_listen(&_mgr,address.c_str(),handler,this);
                for(;;)
                {
                    mg_mgr_poll(&_mgr,1000);
                }
            }

    };
}
