#ifndef __M_SESSION_H_
#define __M_SESSION_H_ 
#include<iostream>
#include<string>
#include<memory>
#include<mutex>
#include"util.hpp"
#include"mongoose.h"
#include<unordered_map>
namespace im 
{
    class SessionManager
    {
        private:
            uint64_t _next_id;
            std::mutex _mutex;
            std::unordered_map<uint64_t,Json::Value> _ss;//用户登录成功即为其建立会话映射表，用map容器来进行承载，会话映射表
        public:
            SessionManager()
                :_next_id(1)
            {}
            uint64_t insert(const Json::Value &user)//为登录的用户进行新增会话
            {
                std::unique_lock<std::mutex> lock(_mutex);
                int ssid=_next_id;
                _ss.insert(std::make_pair(_next_id,user));
                _next_id++;
                return ssid;
            }
            bool get_user_session(uint64_t ssid,Json::Value *user)//获取用户的session
            {
                auto it=_ss.find(ssid);//在_ss中查找用户的session
                if(it==_ss.end())//走到end()表示没找到
                {
                    return false;
                }
                *user=it->second;
                return true;
            }
            bool exists(uint64_t ssid)//判断session是否存在
            {
                auto it=_ss.find(ssid);
                if(it==_ss.end())
                {
                    return false;
                }
                return true;
            }
            void remove(uint64_t ssid)//移除会话 session id
            {
                _ss.erase(ssid);
            }
    };
}

#endif 
