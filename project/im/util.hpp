//实用工具类的封装
//1.Json的序列化和反序列化
//2.字符串的分割

#ifndef _M_UTIL_H_ //避免重命名
#define _M_UTIL_H_


#include<iostream>
#include<memory>
#include<jsoncpp/json/json.h>
#include<sstream>
#include<string>
#include<mysql/mysql.h>


//为避免和库中命名空间矛盾，自己命名一个命名空间
namespace im
{
    //一、Json工具类的封装
    class JsonUtil
    {
        //json的序列化和反序列化
        public:
            //1.json，数据序列化
            static bool serialize(Json::Value& value,std::string *body)
            {
                Json::StreamWriterBuilder swb;
                //Json::StreamWriter* sw=swb.newStreamWriter();
                //int waite(Value const& root,std::ostream* sout)//const引用和输入参数
                std::unique_ptr<Json::StreamWriter> sw(swb.newStreamWriter()); //采用智能指针
                std::stringstream ss;
                int ret=sw->write(value,&ss);
                if(ret!=0)
                {
                    std::cout<<"serialize error\n"<<std::endl;
                   // delete sw;
                    return false;
                }
                *body=ss.str();
                return true;
            }

            //2.json数据反序列化，将json格式的字符串解析出各个数据对象
            static bool unserialize(std::string& body,Json::Value *value)
            {
                Json::CharReaderBuilder crb;
                //Json::CharReader* cr=crb.newCharReader();
                //此处还可借用智能指针
                std::unique_ptr<Json::CharReader> cr(crb.newCharReader());
                std::string err;
                bool ret=cr->parse(body.c_str(),body.c_str()+body.size(),value,&err);
                if(ret==false)
                {
                    std::cout<<"unserialize error\n";
                    return false;
                }
                return true;
            }
    
    };

    //二、对数据库本身访问操作的封装
    //初始化句柄（初始化，与服务器连接，设置字符集）、执行语句、关闭操作句柄
    class MysqlUtil
    {
        public:
        //1.创建初始化，连接服务器，设置字符表集，选择要操作的数据库
        static MYSQL* mysql_create(const std::string &host,const std::string &user,const std::string &pass,const std::string &db,int port)
        {
            
            //初始化句柄
            MYSQL* mysql=mysql_init(NULL);
            if(mysql==NULL)
            {
                std::cout<<"mysql init failed\n";
                return NULL;
            }
            //连接服务器，已选择指定的数据库db_jwq
            if(mysql_real_connect(mysql,host.c_str(),user.c_str(),pass.c_str(),db.c_str(),port,NULL,0)==NULL)
            {
                std::cout<<"connect server failed:"<<mysql_error(mysql)<<std::endl;
                mysql_close(mysql);
                return NULL;
            }
            //设置字符集
            mysql_set_character_set(mysql,"utf8");
            return mysql;
        }

        static bool mysql_exec(MYSQL* mysql,const std::string &sql)
        {
            //2.mysql中对数据库中表操作的执行语句
            int ret=mysql_query(mysql,sql.c_str());
            if(ret!=0)
            {
                std::cout<<sql<<std::endl;
                std::cout<<"query failed:"<<mysql_error(mysql)<<std::endl;
                //执行语句失败将失败原因打印出来
                return false;
            }
            return true;
        }

        static void mysql_destroy(MYSQL* mysql)
        {
            //3.销毁数据库
            if(mysql==NULL)
            {
                return;
            }
            mysql_close(mysql);
        }
    }; 
    
}

#endif 
