//mysql 数据的相关操作
//对数据库本身操作的封装（①连接并初始化数据库+②执行数据操作的执行语句+③关闭句柄）
#include<iostream>
#include<mysql/mysql.h>
using namespace std;

int main()
{
  //1.初始化句柄
  MYSQL* mysql=mysql_init(NULL);
  if(mysql==NULL)
  {
    std::cout<<"mysql init failed!\n";
    return -1;
  }

  //2.连接服务器
  //mysql_real_connect(mysql,host,user,pass,dbname,port,socket,flag)
  if(mysql_real_connect(mysql,"127.0.0.1","root","","im_system",0,NULL,0)==NULL)
  {
    std::cout<<"connect server failed:"<<mysql_error(mysql)<<std::endl;
    mysql_close(mysql);
    return -1;
  }

  //3.设置字符集
  mysql_set_character_set(mysql,"utf8");
  //若未进行数据库的选择需进行数据库的选择
  // 选择数据库，执行相关的SQL语句
  //mysql_select_db(mysql,"im_system")
  
  //3.数据的增删改查
  //const char* sql="insert int tbstu values(null,'孙八',19,98,99,now());";
  //const char* sql="update tbstu set age=1 where sn=2";
  //const char* sql="delete from tbstu where name='李四';";
  const char* sql="select from user;";
  int ret=mysql_query(mysql,sql);
  if(ret!=0)
  {
    std::cout<<sql<<std::endl;
    std::cout<<"query failed:"<<mysql_error(mysql)<<std::endl;
    return -1;
  }

  //4.保存结果集到本地
  MYSQL_RES* res=mysql_store_result(mysql);
  if(res==NULL)
  {
    std::cout<<"store result failed:"<<mysql_error(mysql)<<std::endl;
    return -1;
  }

  //6.获取结果集中数据条数
  int num_row=mysql_num_rows(res);
  int num_col=mysql_num_fields(res);
   //根据行数和列数，遍历结果集
   for(int i=0;i<num_row;++i)
   {
     MYSQL_ROW row=mysql_fetch_row(res);
     for(int j=0;j<num_col;++j)
     {
       printf("%s\n",row[j]);
     }
     printf("\n");
   }

   //7.释放结果集
   mysql_free_result(res);
   mysql_close(mysql);
  return 0;
}

//拿到数据库操作句柄，便可对数据库访问操作中两张数据表进行操作
