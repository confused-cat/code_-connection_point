#include<iostream>
using namespace std;
#include<websocketpp/config/asio_no_tls.hpp>
#include<websocketpp/server.hpp>
typedef websocketpp::server<websocketpp::config::asio> wsserver_t;

class test_server
{
private:
    wsserver_t _server;
private:
    void onopen(websocketpp::connection_hdl hdl)
    {
        wsserver_t::connection_ptr conn=_server.get_con_from_hdl(hdl);
        websocketpp::http::parser::request req=conn->get_request();
        cout<<"websocket:"<<req.get_uri()<<"连接建立"<<endl;
    }
    void onclose(websocketpp::connection_hdl hdl)
    {
        wsserver_t::connection_ptr conn=_server.get_con_from_hdl(hdl);
        websocketpp::http::parser::request req=conn->get_request();
        cout<<"websocket:"<<req.get_uri()<<"连接关闭"<<endl;
    }
    void onmessage(websocketpp::connection_hdl hdl,wsserver_t::message_ptr msg)
    {
        wsserver_t::connection_ptr conn=_server.get_con_from_hdl(hdl);
        websocketpp::http::parser::request req=conn->get_request();
        cout<<"websocket:"<<req.get_uri()<<"msg:"<<msg->get_payload()<<endl;
        conn->send("hello world");
    }
    void onhttp(websocketpp::connection_hdl hdl)
    {
        wsserver_t::connection_ptr conn=_server.get_con_from_hdl(hdl);
        websocketpp::http::parser::request req=conn->get_request();
        cout<<req.get_method()<<endl;
        cout<<req.get_uri()<<endl;
        cout<<conn->get_request_body()<<endl;
        cout<<conn->get_request_header("connection")<<endl;
        string body="HELLO MYBRO";
        conn->set_status(websocketpp::http::status_code::ok);
        conn->set_body(body);
        conn->append_header("Content_Type","text/html");
    }
public:
    test_server()
    {
        _server.set_access_channels(websocketpp::log::alevel::none);
        _server.set_close_handler(bind(&test_server::onclose,this,placeholders::_1));
        _server.set_open_handler(bind(&test_server::onopen,this,placeholders::_1));
        _server.set_message_handler(bind(&test_server::onmessage,this,placeholders::_1,placeholders::_2));
        _server.set_http_handler(bind(&test_server::onhttp,this,placeholders::_1));
        _server.init_asio();
        _server.set_reuse_addr(true);
    }
    void start(int port=9000)
    {
        _server.listen(port);
        _server.start_accept();
        _server.run();
    }
};
int main()
{
    test_server srv;
    srv.start();
    return 0;
}