//服务器搭建
#include<iostream>
using namespace std;
#include<string>
#include<websocketpp/server.hpp>
#include<websocketpp/config/asio_no_tls.hpp>

typedef websocketpp::server<websocketpp::config::asio> wsserver_t;

void Print(const string& str)
{
    cout<<str<<endl;
}
//回调函数处理相应的不同业务
void http_callback(wsserver_t* srv,websocketpp::connection_hdl hdl)
{
    //给客户端返回一个"hello world"的页面
    wsserver_t::connection_ptr conn=srv->get_con_from_hdl(hdl);
    cout<<"body:"<<conn->get_request_body()<<endl;
    websocketpp::http::parser::request req=conn->get_request();
    cout<<"method:"<<req.get_method()<<endl;
    cout<<"uri:"<<req.get_uri()<<endl;

    string body="<html><body><h1>hello world</h1></body></html>";
    conn->set_body(conn->get_request_body());
    conn->set_status(websocketpp::http::status_code::ok);
    wsserver_t::timer_ptr tp=srv->set_timer(5000,bind(Print,"fighting"));
    //定时任务的取消，会导致定时任务立即被执行
    tp->cancel();
}
void wsopen_callback(wsserver_t* srv,websocketpp::connection_hdl hdl)
{
    cout<<"websocket 握手成功"<<endl;
}
void wsclose_callback(wsserver_t* srv,websocketpp::connection_hdl hdl)
{
    cout<<"连接断开"<<endl;
}
void wsmsg_callback(wsserver_t* srv,websocketpp::connection_hdl hdl,wsserver_t::message_ptr msg)
{
    wsserver_t::connection_ptr conn=srv->get_con_from_hdl(hdl);
    cout<<"wsmsg:"<<msg->get_payload()<<endl;
    string rsp="client say:"+msg->get_payload();
    conn->send(rsp,websocketpp::frame::opcode::text);
}
int main()
{
    //1.实例化server对象
    wsserver_t wssrv;
    //2.设置日志等级
    wssrv.set_access_channels(websocketpp::log::alevel::none);
    //3.初始化asio调度器
    wssrv.init_asio();
    wssrv.set_reuse_addr(true);
    //4.设置回调函数
    wssrv.set_http_handler(bind(http_callback,&wssrv,placeholders::_1));
    wssrv.set_open_handler(bind(wsopen_callback,&wssrv,placeholders::_1));
    wssrv.set_close_handler(bind(wsclose_callback,&wssrv,placeholders::_1));
    wssrv.set_message_handler(bind(wsmsg_callback,&wssrv,placeholders::_1,placeholders::_2));
    //5.设置监听端口
    wssrv.listen(9000);
    //6.开始获取新连接
    wssrv.start_accept();
    //7.启动服务器
    wssrv.run();
    return 0;
}