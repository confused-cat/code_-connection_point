#include "gobang.hpp"

#define HOST "127.0.0.1"
#define PORT 3306
#define USER "root"
#define PASS "Jwt@20040810"
#define DBNAME "gobang"

//数据库测试
void mysql_test() 
{
    MYSQL *mysql = gobang::mysql_util::mysql_create(HOST, USER, PASS, DBNAME, PORT);
    const char *sql = "insert stu values(null, '小黑', 18, 53, 68, 87);";
    bool ret = gobang::mysql_util::mysql_exec(mysql, sql);
    if (ret == false) {
        return ;
    }
    gobang::mysql_util::mysql_destroy(mysql);
}
//json数据测试
void json_test() 
{
    Json::Value root;
    std::string body;
    root["姓名"] = "小明";
    root["年龄"] = 18;
    root["成绩"].append(98);
    root["成绩"].append(88.5);
    root["成绩"].append(78.5);
    gobang::json_util::serialize(root, body);
    DLOG("%s", body.c_str());
    
    Json::Value val;
    gobang::json_util::unserialize(body, val);
    std::cout << "姓名:" << val["姓名"].asString()  << std::endl;
    std::cout << "年龄:" << val["年龄"].asInt()  << std::endl;
    int sz = val["成绩"].size();
    for (int i = 0; i < sz; i++) 
    {
        std::cout << "成绩: " << val["成绩"][i].asFloat() << std::endl;
    }
}

//字符串切割测试
void str_test() 
{
    std::string str = ",...,,123,234,,,,,345,,,,";
    std::vector<std::string> arry;
    gobang::string_util::split(str, ",", arry);
    for (auto s : arry) 
    {
        DLOG("%s", s.c_str());
    }
}

//文件读取测试
void file_test()
{
    std::string filename = "./makefile";
    std::string body;
    gobang::file_util::read(filename, body);

    std::cout << body << std::endl;
}

//数据库类测试
void db_test()
{
    gobang::user_table ut(HOST, USER, PASS, DBNAME, PORT);
    Json::Value user;
    user["username"] = "xiaoming";
    //user["password"] = "123456";
    bool ret = ut.insert(user);
    //std::string body;
    //json_util::serialize(user, body);
    //std::cout << body << std::endl;
}
//线上持久连接测试
void online_test()
{
    gobang::online_manager om;
    wsserver_t::connection_ptr conn;
    uint64_t uid = 2;
    om.enter_game_room(uid, conn);
    if (om.is_in_game_room(uid)) 
    {
        DLOG("IN GAME HALL");
    }else 
    {
        DLOG("NOT IN GAME HALL");
    }
    om.exit_game_room(uid);
    if (om.is_in_game_room(uid)) 
    {
        DLOG("IN GAME HALL");
    }else 
    {
        DLOG("NOT IN GAME HALL");
    }

}
int main()
{
    gobang::gobang_server _server(HOST, USER, PASS, DBNAME, PORT);
    _server.start(9000);
    return 0;
}