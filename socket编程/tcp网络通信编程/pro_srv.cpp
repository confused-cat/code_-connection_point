//多进程版本

#include <iostream>
#include <cassert>
#include<signal.h>//信号机制避免僵尸子进程的出现
#include "tcp_socket.hpp"

//创建进程执行流
void create_worker(TcpSocket new_sock,std::string& cli_ip,uint16_t cli_port)
{
    pid_t pid = fork();
    if (pid < 0)
    {
        new_sock.Close();
        perror("fork error");
        return;
    }
    // 父进程，直接进行返回
    if (pid > 0)
    {
        
        //父进程并不使用该套接字，进行关闭
        new_sock.Close();
        return;
    }
    // 剩下的就是子进程,进行循环的接收数据
    //而当接收数据或是发送数据失败时，子进程就退出了
    //不能让其与父进程执行相同的代码
    while (1)
    {
        std::string buf;
        bool ret = new_sock.Recv(&buf);
        if (ret == false)
        {
            new_sock.Close();
            exit(-1);
        }
        std::cout << cli_ip << ":" << cli_port << " say: " << buf << std::endl;

        buf.clear();
        std::cout << "server say: ";
        fflush(stdout);
        std::cin >> buf; // 从键盘获取数据
        ret = new_sock.Send(buf);
        if (ret == false)
        {
            new_sock.Close();
            exit(-1);
        }
    }
    exit(-1);
}
int main(int argc, char *argv[])
{
    // 服务端流程：
    //  通过运行参数，获取服务端绑定的地址和端口: ./tcp_srv 192.168.153.2 9000
    if (argc != 3)
    {
        printf("Incorrect operation format!!\nUsage: ./tcp_srv 192.168.153.129 9000\n");
        return -1;
    }
    signal(SIGCHLD,SIG_IGN);//显式忽略子进程退出信号通知，避免子进程称为僵尸进程

    std::string ip = argv[1];
    uint16_t port = std::stoi(argv[2]);

    TcpSocket lsn_sock;
    // 1.创建套接字
    assert(lsn_sock.Socket() != false);
    // 2.为套接字绑定地址信息
    assert(lsn_sock.Bind(ip, port));
    // 3.开始监听
    assert(lsn_sock.Listen());
    while (1)
    {
        // 4.获取新建连接
        TcpSocket new_sock; // 局部变量，一次循环之后就被析构了
        std::string cli_ip;
        uint16_t cli_port;
        bool ret = lsn_sock.Accept(&new_sock, &cli_ip, &cli_port);
        if (ret == false)
        {
            // 因为一次客户端新建连接的获取失败而让程序退出是不合理的，故失败的话继续处理下一个
            continue;
        }
        std::cout << "new client: " << cli_ip << ":" << cli_port << std::endl;

        // 获取新建连接之后，就创建子进程进行之后的连接通信
        create_worker(new_sock,cli_ip,cli_port);
    }
    // 6.关闭套接字
    lsn_sock.Close();
    return 0;
}