//多线程版本

#include <iostream>
#include<pthread.h>
#include <cassert>
#include<signal.h>//信号机制避免僵尸子进程的出现
#include "tcp_socket.hpp"

//创建线程
void *thread_entry(void* arg)
{
    //传址操作 TcpSocket *new_sock=(TcpSocket*)argc
    TcpSocket *new_sock=(TcpSocket*)arg;
    while(1)
    {
        //使用新建连接收发数据
        std::string buf;
        bool ret=new_sock->Recv(&buf);
        if(ret==false)
        {
            new_sock->Close();
            break;
        }
        std::cout<<"client say: "<<buf<<std::endl;

        buf.clear();
        std::cout<<"server say: ";
        fflush(stdout);
        std::cin>>buf;
        new_sock->Send(buf);
        if(ret==false)
        {
            new_sock->Close();
            break;
        }
    }
    delete new_sock;
    return nullptr;
}

void create_worker(TcpSocket *sock)
{
    //int pthread_create(pthread_t* tid,pthread_attr_t* attr,void*(routine)(void*),void* arg)
    pthread_t tid;
    int ret=pthread_create(&tid,NULL,thread_entry,(void*)sock);
    if(ret!=0)
    {
        printf("pthread create error");
        sock->Close();
        delete sock;
        return;
    }
    //使用分离操作，退出后自动回收资源
    pthread_detach(tid);
    return;
}

int main(int argc, char *argv[])
{
    // 服务端流程：
    //  通过运行参数，获取服务端绑定的地址和端口: ./tcp_srv 192.168.153.2 9000
    if (argc != 3)
    {
        printf("Incorrect operation format!!\nUsage: ./tcp_srv 192.168.153.129 9000\n");
        return -1;
    }
    signal(SIGCHLD,SIG_IGN);//显式忽略子进程退出信号通知，避免子进程称为僵尸进程

    std::string ip = argv[1];
    uint16_t port = std::stoi(argv[2]);

    TcpSocket lsn_sock;
    // 1.创建套接字
    assert(lsn_sock.Socket() != false);
    // 2.为套接字绑定地址信息
    assert(lsn_sock.Bind(ip, port));
    // 3.开始监听
    assert(lsn_sock.Listen());
    while (1)
    {
        // 4.获取新建连接
        TcpSocket *new_sock=new TcpSocket;
        std::string cli_ip;
        uint16_t cli_port;
        bool ret = lsn_sock.Accept(new_sock, &cli_ip, &cli_port);
        if (ret == false)
        {
            // 因为一次客户端新建连接的获取失败而让程序退出是不合理的，故失败的话继续处理下一个
            continue;
        }
        std::cout << "new client: " << cli_ip << ":" << cli_port << std::endl;

        // 获取新建连接之后，就创建子进程进行之后的连接通信
        create_worker(new_sock);
    }
    // 6.关闭套接字
    lsn_sock.Close();
    return 0;
}