#include<iostream>
#include<cassert>
#include"tcp_socket.hpp"

int main(int argc,char* argv[])
{
     //客户端流程
    //通过运行参数，获取服务端绑定的地址和端口: ./tcp_srv 192.168.153.2 9000
    if(argc!=3)
    {
        printf("Incorrect operation format!!\nUsage: ./tcp_srv 192.168.153.129 9000\n");
        return -1;
    }
    std::string ip=argv[1];
    uint16_t port=std::stoi(argv[2]);
    
    //1.创建套接字
    TcpSocket cli_sock;
    assert(cli_sock.Socket()!=false);
    //2.为套接字绑定地址信息（不推荐）
    //3.向服务端发起连接请求
    assert(cli_sock.Connect(ip,port));
    //4.循环收发数据
    while(1)
    {
        std::string buf;
        std::cout<<"client say: ";
        fflush(stdout);
        std::cin>>buf;
        assert(cli_sock.Send(buf));

        buf.clear();
        assert(cli_sock.Recv(&buf));
        std::cout<<"server say: "<<buf<<std::endl;
    }
    //5.关闭套接字
    cli_sock.Close();
    return 0;
}