#include <iostream>
#include <string>
#include <cassert>
#include <unistd.h>
#include <sys/socket.h> //套接字接口头文件
#include <netinet/in.h> //地质结构类型以及协议类型宏文件
#include <arpa/inet.h>  //字节序转换接口文件

#define MAX_BACKLOG 128
class TcpSocket
{
private:
    int _sockfd;
public:
    TcpSocket()
        : _sockfd(-1) {}
    ~TcpSocket(){}
    bool Socket()
    {
        _sockfd=socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
        if(_sockfd<0)
        {
            perror("socket error");
            return false;
        }
        return true;
    }
    bool Bind(const std::string &ip,uint16_t port)
    {
        struct sockaddr_in addr;
        addr.sin_family=AF_INET;
        addr.sin_port=htons(port);
        addr.sin_addr.s_addr=inet_addr(ip.c_str());//将IP地址转换为整型IP地址
        socklen_t len=sizeof(struct sockaddr_in);
        int ret=bind(_sockfd,(struct sockaddr*)&addr,len);
        if(ret<0)
        {
            perror("bind error");
            return false;
        }
        return true;
    }
    bool Listen(int backlog=MAX_BACKLOG)
    {
        int ret=listen(_sockfd,backlog);
        if(ret<0)
        {
            perror("listen error");
            return false;
        }
        return true;
    }
    bool Accept(TcpSocket *new_sock,std::string *cli_ip=NULL,uint16_t *cli_port=NULL)
    {
        //int accept(int sockfd,struct sockaddr* peer,socklen_t len)
        struct sockaddr_in peer;
        socklen_t len=sizeof(struct sockaddr_in);
        int newfd=accept(_sockfd,(struct sockaddr*)&peer,&len);
        if(newfd<0)
        {
            perror("accept error");
            return false;
        }
        new_sock->_sockfd=newfd;//将获取的新建连接描述符赋值给外部传入的TcpSocket对象
        if(cli_ip!=NULL)
        {
            //将获取的客户端IP地址转换为字符串点分十进制IP地址赋值给cli_ip
            *cli_ip=inet_ntoa(peer.sin_addr);
        }
        if(cli_port!=NULL)
        {
            //将获取的客户端IP地址转换为字符串点分十进制IP地址赋值给cli_ip
            *cli_port=ntohs(peer.sin_port);
        }
        return true;
    }
    
    //在获取新建连接的同时，获取新建连接对应的客户端地址信息
    bool Recv(std::string *body)
    {
        //ssize_t recv(int _sockfd,void* buf,int len,int flag)
        char temp[1024]={0};
        ssize_t ret=recv(_sockfd,temp,1023,0);//flag为0，表示默认阻塞
        //recv返回值>0表示接收的实际数据长度；=0表示连接断开；<0表示出错
        if(ret<0)
        {
            perror("recv error");
            return false;
        }
        else if(ret==0)
        {
            printf("connect broken\n");
            return false;
        }
        body->assign(temp,ret);//从temp中截取ret长度的数据放到body对象中去
        return true;
    }
    bool Send(const std::string &body)
    {
        //ssize_t send(int sockfd,void* data,int len,int flag)
        ssize_t ret=send(_sockfd,body.c_str(),body.size(),0);//flag为0,表示默认阻塞发送，发送缓冲区满了就阻塞
        if(ret<0)
        {
            perror("send error");
            return false;
        }
        return true;
    }
    bool Connect(const std::string &ip,uint16_t port)
    {
        //int connect(int sockfd,struct sockaddr* srvaddr,socklen_t len)
        struct sockaddr_in addr;
        addr.sin_family=AF_INET;
        addr.sin_port=htons(port);
        addr.sin_addr.s_addr=inet_addr(ip.c_str());
        socklen_t len=sizeof(struct sockaddr_in);
        int ret=connect(_sockfd,(struct sockaddr*)&addr,len);
        if(ret<0)
        {
            perror("connect error");
            return false;
        }
        return true;
    }
    bool Close()
    {
        if(_sockfd!=-1)
        {
            close(_sockfd);
            _sockfd=-1;
        }
        return true;
    }
};