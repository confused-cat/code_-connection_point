#include<iostream>
#include<cassert>
#include "tcp_socket.hpp"

int main(int argc, char *argv[])
{
    //服务端流程：
    // 通过运行参数，获取服务端绑定的地址和端口: ./tcp_srv 192.168.153.2 9000
    if (argc != 3)
    {
        printf("Incorrect operation format!!\nUsage: ./tcp_srv 192.168.153.129 9000\n");
        return -1;
    }
    std::string ip = argv[1];
    uint16_t port = std::stoi(argv[2]);

    TcpSocket lsn_sock;
    // 1.创建套接字
    assert(lsn_sock.Socket() != false);
    // 2.为套接字绑定地址信息
    assert(lsn_sock.Bind(ip, port));
    // 3.开始监听
    assert(lsn_sock.Listen());
    while (1)
    {
        // 4.获取新建连接,因为客户端会有很多，所以需要循环进行不断获取
        TcpSocket new_sock; // 局部变量，一次循环之后就被析构了
        std::string cli_ip;
        uint16_t cli_port;
        bool ret = lsn_sock.Accept(&new_sock, &cli_ip, &cli_port);
        if (ret == false)
        {
            // 因为一次客户端新建连接的获取失败而让程序退出是不合理的，故失败的话继续处理下一个
            continue;
        }
        std::cout << "new client: " << cli_ip << ":" << cli_port << std::endl;
        while (1)
        {
            // 5.获取新建连接收发数据
            std::string buf;
            ret = new_sock.Recv(&buf);
            if (ret == false)
            {
                new_sock.Close();
                break;
            }
            std::cout << cli_ip << ":" << cli_port << " say: " << buf << std::endl;

            buf.clear();
            std::cout << "server say: ";
            fflush(stdout);
            std::cin >> buf; // 从键盘获取数据
            ret=new_sock.Send(buf);
            if(ret==false)
            {
                new_sock.Close();
                break;
            }
        }
    }
    // 6.关闭套接字
    lsn_sock.Close();
    return 0;
}