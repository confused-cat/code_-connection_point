#include"udp_socket.hpp"
#include<cassert>

int main(int argc,char* argv[])
{
    //参数传递进来的地址，是要请求的服务器地址
    if(argc!=3)
    {
        std::cout<<"usage: ./udp_cli 192.168.153.2 9000\n";
        return -1;
    }
    //从传递进来的参数中获取到服务器的IP和port
    std::string srv_ip=argv[1];
    uint16_t srv_port=std::stoi(argv[2]);

    UdpSocket cli_sock;
    //1.创建套接字
    assert(cli_sock.Socket()==true);
    //2.为套接字绑定地址信息（不推荐）
    while(1)
    {
        //3.发送数据
        std::string data;
        std::cout<<"client say: ";
        fflush(stdout);
        std::cin>>data;
        assert(cli_sock.Send(data,srv_ip,srv_port)==true);
        //4.接收数据
        data.clear();
        assert(cli_sock.Recv(&data,&srv_ip,&srv_port)==true);
        std::cout<<"server say:"<<data<<std::endl;
    }
    //5.关闭套接字
    cli_sock.Close();
    return 0;
}