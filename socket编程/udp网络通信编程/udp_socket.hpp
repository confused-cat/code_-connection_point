#include<iostream>
#include <unistd.h>
#include <string>
#include <sys/socket.h> //套接字接口头文件
#include <netinet/in.h> //地质结构类型以及协议类型宏文件
#include <arpa/inet.h>  //字节序转换接口文件

class UdpSocket
{
private:
    int _sockfd;

public:
    UdpSocket()
        : _sockfd(-1) {}
    ~UdpSocket()
    {
        Close();
    }
    bool Socket()
    {
        _sockfd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (_sockfd < 0)
        {
            perror("socket error");
            return false;
        }
        return true;
    }
    bool Bind(const std::string &ip, uint16_t port)
    {
        struct sockaddr_in addr;
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.s_addr = inet_addr(ip.c_str());
        socklen_t len = sizeof(struct sockaddr_in);
        int ret = bind(_sockfd, (struct sockaddr *)&addr, len);
        if (ret < 0)
        {
            perror("bind error");
            return false;
        }
        return true;
    }
    bool Recv(std::string *body, std::string *peer_ip, uint16_t *peer_port)
    {
        // ssize_t recvfrom(int sockfd,void* buf,size_t dlen,int flag,struct sockaddr* peer,socklen_t *alen);
        struct sockaddr_in peer;
        socklen_t len = sizeof(struct sockaddr_in);
        char temp[4096] = {0};
        ssize_t ret = recvfrom(_sockfd, temp, 4096, 0, (struct sockaddr *)&peer, &len);
        if (ret < 0)
        {
            perror("recvfrom error");
            return false;
        }
        if(peer_ip!=NULL)
        {
            *peer_ip = inet_ntoa(peer.sin_addr);
        }
        if(peer_port!=NULL)
        {
            *peer_port = ntohs(peer.sin_port);
        }
        body->assign(temp, ret); // 从temp中取出ret长度的数据放到body中
        return true;
    }
    bool Send(const std::string &body,const std::string &peer_ip,uint16_t peer_port)
    {
        struct sockaddr_in addr;
        addr.sin_family=AF_INET;
        addr.sin_port=htons(peer_port);
        addr.sin_addr.s_addr=inet_addr(peer_ip.c_str());
        socklen_t len=sizeof(struct sockaddr_in);
        ssize_t ret=sendto(_sockfd,body.c_str(),body.size(),0,(struct sockaddr*)&addr,len);
        if(ret<0)
        {
            perror("sendto error");
            return false;
        }
        return true;
    }
    bool Close()
    {
        if(_sockfd!=-1)
        {
            close(_sockfd);
            _sockfd=-1;
        }
        return true;
    }
};