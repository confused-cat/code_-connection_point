#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>//套接字接口头文件
#include<netinet/in.h>//地质结构类型以及协议类型宏文件
#include<arpa/inet.h>//字节序转换接口文件

int main(int argc,char *argv[])
{
    //要求程序运行需要输入三个参数，IP地址，端口，程序命令自身
    if(argc!=3)
    {
        printf("./udp_srv 192.168.153.2 9000\n");
        return -1;
    }
    uint64_t port=atoi(argv[2]);
    char* ip=argv[1];
    //1.创建套接字
    //int socket(int domain,int type,int protocol)
    int sockfd=socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
    if(sockfd<0)
    {
        perror("socket error");
        return -1;
    }
    //2.为套接字绑定地址信息
    //int bind(int sockfd,struct sockaddr *addr,socklen_t len)
    struct sockaddr_in addr;
    addr.sin_family=AF_INET;
    addr.sin_port=htons(port);
    addr.sin_addr.s_addr=inet_addr(ip);
    socklen_t len=sizeof(struct sockaddr_in);
    int ret=bind(sockfd,(struct sockaddr*)&addr,len);
    if(ret==-1)
    {
        perror("bind error");
        return -1;
    }
    //3.循环接收发送数据
    while(1)
    {
        //3 接收数据
        //ssize_t recvfrom(int scokfd,coid* buf,int len,int flag,struct sockaddr* peer,socklen_t *len)
        char buf[1024]={0};
        struct sockaddr_in peer;
        socklen_t len=sizeof(struct sockaddr_in);
        //peer中地址信息是系统进行设置的
        ssize_t ret=recvfrom(sockfd,buf,1023,0,(struct sockaddr*)&peer,&len);
        if(ret<0)
        {
            perror("recvfrom error");
            return -1;
        }
        //const char* inet_ntoa(struct in_addr addr)
        char* peerip=inet_ntoa(peer.sin_addr);//将网络字节序转换为字符串
        uint64_t peerport=ntohs(peer.sin_port);//将网络字节序端口转换为主机字节序端口
        printf("client[%s:%d] say: %s\n",peerip,peerport,buf);
        //4 发送数据
        //ssize_t sendto(int scokfd,void* buf,int len,int flag,struct sockaddr* peer,socklen_t len)
        //键盘获取数据到data中
        char data[1024]={0};
        printf("server say:");
        fflush(stdout);
        scanf("%s",data);
        //将data中的数据，发送给peer地址信息对应的对端主机
        ret=sendto(sockfd,data,strlen(data),0,(struct sockaddr*)&peer,len);
        if(ret<0)
        {
            perror("sendto error");
            return -1;
        }
    }
    //5.关闭套接字
    close(sockfd);
    return 0;
}