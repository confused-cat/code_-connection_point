﻿#pragma once 

#include<iostream>
#include<vector>
#include<string>
#include<assert.h>
#include"Common.h"

using namespace std;

//哈希桶改造，在原本实现的哈希桶上增加迭代器和兼容unordered_map和unordered_set数据的模板参数
template<class T>
struct HashNode
{
	T _data;
	HashNode<T>* _next;

	HashNode(const T& data = T())
		:_data(data)
		, _next(nullptr)
	{}
};

//解决问题，采用除留余数法来作为哈希函数，那当数据类型不为int时又如何处理
//数据转换，处理T是整型家族的类型数据：char/short/int/long/long long
//将其设置为默认的模板参数，当用户未传递时，则调用此方法
template<class T>
class TtoIntDef
{
public:
	size_t operator()(const T& data)
	{
		return data;
	}
};


template<class T, class KOFT,class TtoInt = TtoIntDef<T>>
class HashBucket;

//template<class T, class KOFT, class Ref, class Ptr, class TtoInt = TtoIntDef<T>>
//class HashBucketIterator;

//❤❤❤哈希桶改造第一点：
//增加迭代器

//❤❤❤哈希桶改造第二点：
//由于unordered_map和unordered_set中存储元素不同，unordered_map中存储<k,v>,unordered_set中存储<k>
//在进行元素比较的时候我们是用k来进行元素的比较，所以我们得让比较操作同时兼容unordered_map和unordered_set
//同map和set实现时思想一致
//增加参数模板：KOFT--->表示从存储元素T中将K提取出来

//哈希桶迭代器
//其实模板列表可以给成：template<class T,class Ref,class Ptr> 
//                      但是我们在进行迭代器的遍历的时候，需要调用哈希桶类中的哈希函数
//                      所以得有哈希桶的一个对象，当然参数列表也得有哈希桶的模板参数
template<class T, class KOFT, class Ref, class Ptr, class TtoInt = TtoIntDef<T>>
class HashBucketIterator
{
public:
	typedef HashNode<T> Node;
	typedef HashBucketIterator<T, KOFT, Ref, Ptr, TtoInt> Self;
public:
	HashBucketIterator(HashBucket<T, KOFT, TtoInt>* ht = nullptr, Node* pNode = nullptr)
		:_ht(ht)
		, _pNode(pNode)
	{}

	//迭代器具有指针的行为
	//1 解引用* + 指向->(解引用的结果是指针指向的值；->的结果是下一个数据的地址) Ref(int&)对象的别名
	Ref operator*()
	{
		return _pNode->_data;
	}
	Ptr operator->()
	{
		return &(operator*());
	}

	//迭代器可进行移动
	//++a;
	Self& operator++()
	{
		Next();
		return *this;
	}
	//a++
	Self operator++(int)
	{
		Self temp(*this);
		Next();
		return temp;
	}

	//迭代器可进行比较
	bool operator==(const Self& s)const
	{
		return _pNode == s._pNode;
	}
	bool operator!=(const Self& s)const
	{
		return _pNode != s._pNode;
	}

private:
	void Next()
	{
		//_pNode不是当前链表的最后一个节点
		if (_pNode->_next)
		{
			_pNode = _pNode->_next;
		}
		else
		{
			//_pNode为当前桶的最后一个节点，找下一个非空桶的第一个节点
			//1 首先计算_pNode在哪个桶中
			size_t bucketNo = _ht->HashFunc(_pNode->_data);
			//2 从当前开始往后找非空桶
			for (size_t i = bucketNo + 1; i < _ht->BucketCount(); ++i)
			{
				if (_ht->_table[i])
				{
					_pNode = _ht->_table[i];
					return;
				}
			}
			_pNode = nullptr;
		}
	}
private:
	Node* _pNode;      //节点指针
	HashBucket<T, KOFT, TtoInt>* _ht;  //哈希桶对象指针
};

//模板参数列表：1.T->存储元素类型
//              2.KOFT->如map中将k提取出来进行元素比较
//              3.TtoInt->将存储的元素转化为整型可以哈希函数进行取模操作
template<class T, class KOFT,class TtoInt>
class HashBucket
{
	friend class HashBucketIterator<T, KOFT, T&, T*, TtoInt>;
public:
	typedef HashBucketIterator<T, KOFT, T&, T*, TtoInt> iterator;

	typedef HashNode<T> Node;

public:
	HashBucket(size_t capacity = 10)
		:_table(capacity)
		, _size(0)
	{}

	~HashBucket()
	{
		Clear();
	}

	iterator Begin()
	{
		for (size_t i = 0; i < _table.capacity(); ++i)
		{
			if (_table[i])
				return iterator(this, _table[i]);
		}
		return End();
	}
	iterator End()
	{
		return iterator(this, nullptr);
	}

	//哈希桶中元素唯一
	//元素插入
	pair<iterator,bool> InsertUnique(const T& data)
	{
		//首先检测一下空间是否需要扩容
		CheckCapacity();

		//1.计算哈希地址（即桶号）
		size_t bucketNo = HashFunc(data);
		//2.确保桶中元素唯一，若桶中已有元素data则错误返回，没有的话插入
		KOFT koft;
		Node* cur = _table[bucketNo];
		while (cur)
		{
			if (koft(data) == koft(cur->_data))
				return make_pair(iterator(this, cur), false);
			cur = cur->_next;
		}
		//3.进行新节点的插入
		cur = new Node(data);
		cur->_next = _table[bucketNo];
		_table[bucketNo] = cur;
		++_size;
		return make_pair(iterator(this, cur), true);
	}
	//元素删除
	size_t EraseUnique(const T& data)
	{
		//1.计算哈希地址（即桶号）
		size_t bucketNo = HashFunc(data);
		//2.在此桶中找到元素进行删除
		KOFT koft;
		Node* cur = _table[bucketNo];
		Node* prev = nullptr;
		while (cur)
		{
			if (koft(data) == koft(cur->_data))
			{
				if (cur == _table[bucketNo])
				{
					//删除元素为桶中首节点
					_table[bucketNo] = cur->_next;
				}
				else
				{
					prev->_next = cur->_next;
				}
				delete cur;
				--_size;
				return 1;
			}
			else
			{
				prev = cur;
				cur = cur->_next;
			}
		}
		return 0;
	}

	//哈希桶中元素不唯一
	//元素插入
	pair<iterator, bool> InsertEqual(const T& data)
	{
		//首先检测一下空间是否需要扩容
		CheckCapacity();

		//1.计算哈希地址（即桶号）
		size_t bucketNo = HashFunc(data);

		//2.直接进行新节点的插入
		Node* cur = new Node(data);
		cur->_next = _table[bucketNo];
		_table[bucketNo] = cur;
		++_size;
		return make_pair(iterator(this, cur), true);
	}
	//元素删除(返回值为删除元素个数)
	size_t EraseEqual(const T& data)
	{
		//1.计算哈希地址（即桶号）
		size_t bucketNo = HashFunc(data);
		size_t oldsize = _size;
		//2.在此桶中找到值为data的元素全部进行删除
		KOFT koft;
		Node* cur = _table[bucketNo];
		Node* prev = nullptr;
		while (cur)
		{
			if (koft(data) == koft(cur->_data))
			{
				if (prev == nullptr)
				{
					//删除元素为桶中首节点
					_table[bucketNo] = cur->_next;
					delete cur;
					--_size;
					cur = _table[bucketNo];
				}
				else
				{
					prev = > _next = cur->_next;
					delete cur;
					--_size;
					cur = prev->_next;
				}
			}
			else
			{
				prev = cur;
				cur = cur->_next;
			}
		}
		return oldsize - _size;
	}

	//计算桶的个数
	size_t BucketCount()const
	{
		return _table.capacity();
	}

	//计算某个桶中的元素个数
	size_t BucketSize(size_t bucketNo)const
	{
		assert(bucketNo < BucketCount());
		Node* cur = _table[bucketNo];
		size_t size = 0;
		while (cur)
		{
			size++;
			cur = cur->_next;
		}
		return size;
	}

	//计算某个元素所在的桶号
	size_t BucketNo(const T& data)
	{
		return HashFunc(data);
	}

	//查找元素是否存在在桶中
	iterator Find(const T& data)
	{
		//1.计算元素所在的桶号
		size_t bucketNo = HashFunc(data);
		//2.检测元素是否存在
		KOFT koft;
		Node* cur = _table[bucketNo];
		while (cur)
		{
			if (koft(cur->_data) == koft(data))
				//迭代器，两个参数，一个表示当前哈希桶对象，一个是当前节点
				return iterator(this,cur);
			cur = cur->_next;
		}
		return End();
	}

	//表中有效元素个数
	size_t Size()const
	{
		return _size;
	}
	//判空
	bool Empty()
	{
		return _size == 0;
	}

	void Clear()
	{
		for (size_t i = 0; i < _table.capacity(); ++i)
		{
			Node* cur = _table[i];//获取桶中首节点
			while (cur)
			{
				_table[i] = cur->_next;
				delete cur;
				cur = _table[i];
			}
		}
		_size = 0;
	}

	void Swap(HashBucket<T, KOFT,TtoInt>& ht)
	{
		_table.swap(ht._table);
		std::swap(_size, ht._size);
	}

	void PrintHash()const
	{
		for (size_t i = 0; i < _table.capacity(); ++i)
		{
			cout << "table[" << i << "]:";
			Node* cur = _table[i];
			while (cur)
			{
				cout << cur->_data << "----->";
				cur = cur->_next;
			}
			cout << "NULL" << endl;
		}
		cout << endl;
	}
private:
	//哈希函数(除留余数法)
	size_t HashFunc(const T& data)const
	{
		TtoInt t2int;
		KOFT koft;
		return t2int(koft(data)) % _table.capacity();
	}
	//空间检测
	void CheckCapacity()
	{
		//如果表中有效元素等于空间容量，进行空间2倍数扩容
		if (_size == _table.capacity())
		{
			HashBucket<T, KOFT,TtoInt> newHT(GetNextPrime(_size));

			//将旧桶中元素往新桶中进行插入（不可直接搬移，因为扩容哈希函数也改变了）
			for (size_t i = 0; i < _table.capacity(); ++i)
			{
				Node* cur = _table[i];
				//将i号桶对应的链表中每个节点插入到新桶中去
				while (cur)
				{
					//1.将cur从桶上取出来
					_table[i] = cur->_next;
					//2.将cur插入到新桶中去，先计算cur在新桶中的哈希地址再进行插入
					size_t newBucketNo = newHT.HashFunc(cur->_data);
					cur->_next = newHT._table[newBucketNo];
					newHT._table[newBucketNo] = cur;
					newHT._size++;
					//3.从_table[i]中取下一个节点
					cur = _table[i];
					_size--;
				}
			}
			this->Swap(newHT);
		}
	}
private:
	vector<Node*> _table;//哈希表中存储哈希节点
	size_t _size;
};


//从整型int数据中将数据提取出来
struct KOFI
{
	int operator()(int data)
	{
		return data;
	}
};

void TestHashBucket1()
{
	cout << "测试1：" << endl;
	HashBucket<int, KOFI> ht; //第三个参数使用默认的哈希转换算法，即整型家族数据转换为int类型的数据
	ht.InsertUnique(1);
	ht.InsertUnique(2);
	ht.InsertUnique(3);
	ht.InsertUnique(4);
	ht.InsertUnique(11);
	ht.InsertUnique(12);
	ht.InsertUnique(13);
	ht.InsertUnique(14);
	ht.InsertUnique(44);
	cout << "元素个数为：" << ht.Size() << endl;
	ht.PrintHash();

	//HashBucketIterator<int, KOFI, int&, int*, TtoIntDef<int>> it = ht.Begin();
	auto it = ht.Begin();
	cout << "迭代器遍历哈希桶为：" << endl;
	while(it != ht.End())
	{
		cout << *it << "  ";
		++it;
	}
	cout << endl;
}

//数据转换，处理T为string类型的数据，字符串哈希算法来进行处理
class StrtoInt
{
public:
	size_t operator()(const string& s)
	{
		const char* str = s.c_str();
		unsigned int seed = 131;
		unsigned int hash = 0;
		while (*str)
		{
			hash = hash*seed + (*str++);
		}
		return (hash & 0x7FFFFFFF);
	}
};
//从string中将数据提取出来
class KOFTStr
{
public:
	const string& operator()(const string& s)const
	{
		return s;
	}
};
void TestHashBucket2()
{
	cout << "测试2：" << endl;
	HashBucket<string, KOFTStr,StrtoInt> ht;
	ht.InsertUnique("chinese");
	ht.InsertUnique("math");
	ht.InsertUnique("science");
	ht.InsertUnique("art");

	cout << "元素个数：" << ht.Size() << endl;
	ht.PrintHash();

	auto it = ht.Begin();
	cout << "迭代器遍历哈希桶为：" << endl;
	while (it != ht.End())
	{
		cout << *it << "  ";
		++it;
	}
	cout << endl;
}