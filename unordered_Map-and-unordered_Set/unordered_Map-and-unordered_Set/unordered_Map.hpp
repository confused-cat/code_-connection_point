#pragma once

#include<iostream>
#include"HashBucket.hpp"

using namespace std;

namespace map
{
	//字符串转换为整型int的结构体（作为模板参数）
	template<class K>
	class StrtoInt
	{
	public:
		size_t operator()(const K& s)
		{
			const char* str = s.c_str();
			unsigned int seed = 131;
			unsigned int hash = 0;
			while (*str)
			{
				hash = hash*seed + (*str++);
			}
			return (hash & 0x7FFFFFFF);
		}
	};
	//模板参数：
	//K->key
	//V->value
	//KtoI->将K值转换为int类型数据

	template<class K, class V, class KtoI = StrtoInt<K>>
	class unordered_map
	{
		typedef pair<K, V> DataType;

		//从<k,v>中将k提取出来
		struct KOFT
		{
			const K& operator()(const DataType& data)const
			{
				return data.first;
			}
		};

		typedef HashBucket<DataType, KOFT, KtoI> HB;   //构造一个哈希桶对象
	public:
		typename typedef HB::iterator iterator;    //哈希桶的迭代器

	public:
		unordered_map()
			:_hb()
		{}
		//////////////unordered_map中操作///////////////
		//begin() & end()
		iterator begin()
		{
			return _hb.Begin();
		}
		iterator end()
		{
			return _hb.End();
		}
		size_t size()const
		{
			return _hb.Size();
		}
		bool empty()const
		{
			return _hb.Empty();
		}
		V& operator[](const K& data)
		{
			return (*((_hb.InsertUnique(make_pair(data, V()))).first)).second;
			//其实就是先构造一个键值对进行键值对的插入
			//插入成功后返回一个插入成功的键值对<iterator,true>
			//然后键值对进行.first操作取出插入元素所在的迭代器iterator
			//而迭代器中存储着关键值key为data的键值对
			//再进行.second操作获取data对应的value值,即为下标运算符的结果
		}
		
		//find 返回查找成功元素位置的迭代器
		iterator find(const K& data)
		{
			return _hb.Find(make_pair(data, V()));
		}
		pair<iterator, bool> insert(const DataType& kv)
		{
			return _hb.InsertUnique(kv);
		}
		size_t erase(const K& data)
		{
			return _hb.EraseUnique(make_pair(data, V()));
		}
		void clear()
		{
			return _hb.Clear();
		}
		void swap(unordered_map<K, V, KtoI>& m)
		{
			_hb.Swap(m._hb);
		}
		//哈希桶个数
		size_t bucket_count()const
		{
			return _hb.BucketCount();
		}
		//某个桶中的元素个数
		size_t bucket_size(size_t bucketNo)const
		{
			return _hb.BucketSize(bucketNo);
		}
		//某个元素data所在的桶号
		size_t bucket(const K& data)
		{
			//构造键值对作为参数，使用默认的V()值
			return _hb.BucketNo(make_pair(data, V()));
		}
	private:
		HB _hb;
	};
}
void Test_unordered_map()
{
	map::unordered_map<string, string> m;
	m.insert(make_pair("Chinese", "汉语"));
	m.insert(make_pair("English", "英语"));
	m.insert(make_pair("Japanese", "日语"));
	m.insert(make_pair("Korean", "韩语"));
	m.insert(make_pair("Russian", "俄语"));

	cout << "1 元素个数为：" << m.size() << endl;
	cout <<"2 Chinese对应语言为："<< m["Chinese"] << endl;
	cout << "3 桶的个数为：" << m.bucket_count() << endl;
	cout << "4 0号桶中元素个数为：" << m.bucket_size(0) << endl;
	cout << "5 Japanese所在的桶号为：" << m.bucket("Japanese") << endl;

	auto it = m.begin();
	cout << "6 迭代器遍历哈希桶：" << endl;
	while (it != m.end())
	{
		cout << it->first << "--->" << it->second << endl;
		++it;
	}
	cout << endl;
	if (m.find("Japanese")!=nullptr)
	{
		cout << "Japanese is in the unordered_map." << endl;
	}
	else
	{
		cout << "Japanese is not in the unordered_map.";
	}
	m.erase("Japanese");
	cout << "从表中将Japanese删除后再进行查找：" << endl;
	if (m.find("Japanese")!=nullptr)
	{
		cout << "Japanese is in the unordered_map." << endl;
	}
	else
	{
		cout << "Japanese is not in the unordered_map.";
	}
	m.clear();
}