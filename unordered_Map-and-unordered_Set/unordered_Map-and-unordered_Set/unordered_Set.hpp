#pragma once

#include<iostream>
#include"HashBucket.hpp"

using namespace std;

namespace set
{
	//字符串转换为整型int的结构体（作为模板参数）
	template<class K>
	class StrtoInt
	{
	public:
		size_t operator()(const K& s)
		{
			const char* str = s.c_str();
			unsigned int seed = 131;
			unsigned int hash = 0;
			while (*str)
			{
				hash = hash*seed + (*str++);
			}
			return (hash & 0x7FFFFFFF);
		}
	};
	//模板参数：
	//K->key
	//KtoI->将K值转换为int类型数据

	template<class K, class KtoI = StrtoInt<K>>
	class unordered_set
	{
		typedef K DataType;

		//从<k,v>中将k提取出来
		struct KOFT
		{
			const K& operator()(const DataType& data)const
			{
				return data;
			}
		};

		typedef HashBucket<DataType, KOFT, KtoI> HB;   //构造一个哈希桶对象
	public:
		typename typedef HB::iterator iterator;    //哈希桶的迭代器

	public:
		unordered_set()
			:_hb()
		{}
		//////////////unordered_set中操作///////////////
		//begin() & end()
		iterator begin()
		{
			return _hb.Begin();
		}
		iterator end()
		{
			return _hb.End();
		}
		size_t size()const
		{
			return _hb.Size();
		}
		bool empty()const
		{
			return _hb.Empty();
		}
		//find 返回查找成功元素位置的迭代器
		iterator find(const K& data)
		{
			return _hb.Find(data);
		}
		pair<iterator, bool> insert(const DataType& k)
		{
			return _hb.InsertUnique(k);
		}
		size_t erase(const K& data)
		{
			return _hb.EraseUnique(data);
		}
		void clear()
		{
			return _hb.Clear();
		}
		void swap(unordered_set<K, KtoI>& s)
		{
			_hb.Swap(s._hb);
		}
		//哈希桶个数
		size_t bucket_count()const
		{
			return _hb.BucketCount();
		}
		//某个桶中的元素个数
		size_t bucket_size(size_t bucketNo)const
		{
			return _hb.BucketSize(bucketNo);
		}
		//某个元素data所在的桶号
		size_t bucket(const K& data)
		{
			//构造键值对作为参数，使用默认的V()值
			return _hb.BucketNo(data);
		}
	private:
		HB _hb;
	};
}
void Test_unordered_set()
{
	set::unordered_set<string> s;
	s.insert("Chinese");
	s.insert("English");
	s.insert("Japanese");
	s.insert("Korean");
	s.insert("Russian");

	cout << "1 元素个数为：" << s.size() << endl;
	cout << "2 桶的个数为：" << s.bucket_count() << endl;
	cout << "3 0号桶中元素个数为：" << s.bucket_size(0) << endl;
	cout << "4 Japanese所在的桶号为：" << s.bucket("Japanese") << endl;

	auto it = s.begin();
	cout << "5 迭代器遍历哈希桶：" << endl;
	while (it != s.end())
	{
		cout << *it<< endl;
		++it;
	}
	cout << endl;
	if (s.find("Japanese") != nullptr)
	{
		cout << "Japanese is in the unordered_map." << endl;
	}
	else
	{
		cout << "Japanese is not in the unordered_map.";
	}
	s.erase("Japanese");
	cout << "从表中将Japanese删除后再进行查找：" << endl;
	if (s.find("Japanese") != nullptr)
	{
		cout << "Japanese is in the unordered_map." << endl;
	}
	else
	{
		cout << "Japanese is not in the unordered_map.";
	}
	s.clear();
}