//内存操作函数：memcpy/memmove/memset/memcmp
//memcpy:内存数据拷贝
//memmove:内存数据拷贝，允许空间重叠覆盖
//memset:空间内容初始化
//memcmp:内存比较函数

//内存操作函数的模拟实现
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>

//1.memcpy 在src指向的空间拷贝num字节的数据到dest指向的空间中，不允许重叠
void* my_memcpy(void* dest, const void* src, size_t num)
{
	void* temp = dest;
	//断言：两空间都不为空，为空则报错返回
	assert(dest != NULL);
	assert(src != NULL);
	while (num--)
	{
		*(char*)dest = *(char*)src;
		dest = (char*)dest + 1;
		src = (char*)src + 1;
	}
	return temp;
}
//my_memcpy测试函数
void test_my_memcpy()
{
	char* str1[10] = { 0 };
	char* str2 = "mbcd---";
	char* ret = my_memcpy(str1, str2, strlen(str2));
	printf("%s\n", ret);
}
//2.memmove 将src指向的空间前num个字节的数据拷贝到dest指向的空间中，允许重叠
void* my_memmove(void* dest, const void* src, size_t num)
{
	assert(dest != NULL);
	assert(src != NULL);
	void* temp = dest;
	if (dest <= src || (char*)dest >= ((char*)src+num))
	{
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest + 1;
			src = (char*)src + 1;
		}
	}
	else
	{
		dest = (char*)dest + num - 1;
		src = (char*)src + num - 1;
		while (num--)
		{
			*(char*)dest = *(char*)src;
			dest = (char*)dest - 1;
			src = (char*)src - 1;
		}
	}
	return temp;
}
//my_memmove测试函数
void test_my_memmove()
{
	char str[]= "abcdefgh";
	//char* ret1 = my_memmove(str + 1, str + 5, 3);
	//printf("%s\n", ret1);
	//char* ret2 = my_memmove(str + 5, str + 1, 3);
	//printf("%s\n", ret2);
	//char* ret3 = my_memmove(str + 1, str + 3, 4);
	//printf("%s\n", ret3);
	char* ret4 = my_memmove(str + 3, str + 1, 4);
	printf("%s\n", ret4);
}

//3.memset 将ptr中前num个字节用value填充，并返回ptr
void* my_memset(void* ptr, int value, size_t num)
{
	assert(ptr != NULL);
	void* temp = ptr;
	while(num--)
	{
		*(char*)ptr = value;
		ptr = (char*)ptr + 1;
	}
	return temp;
}
//my_memset测试函数
void test_my_memset()
{
	char str[]= "abcdef";
	char* ret = my_memset(str, '*', 3);
	printf("%s\n", ret);
}
//4.memcmp ptr1与ptr2的前num个字节数据进行比较
int my_memcmp(const void* ptr1, const void* ptr2, size_t num)
{
	assert(ptr1);
	assert(ptr2);
	char* temp1 = (char*)ptr1;
	char* temp2 = (char*)ptr2;
	while (num--)
	{
		if (*temp1 == *temp2)
		{
			temp1++;
			temp2++;
		}
		else
		{
			if (*temp1 > *temp2)
				return 1;
			else
				return -1;
		}
	}
	return 0;
}
//my_memcmp测试函数
void test_my_memcmp()
{
	char* str1 = "ab";
	char* str2 = "abcdef";
	int n1 = my_memcmp(str1, str2, 2);
	if (n1 > 0)
		printf("str1>str2\n");
	else if (n1 < 0)
		printf("str1<str2\n");
	else
		printf("str1=str2\n");

}
int main()
{
	//test_my_memcpy();
	//test_my_memmove();
	//test_my_memset();
	test_my_memcmp();

	system("pause");
	return 0;
}