//求字符串长度：strlen
//长度不受限制的字符串函数：strcpy/strcat/strcmp
//长度受限制的字符串函数：strncpy/strncat/strncmp
//字符串查找：strstr/strtok
//错误信息报告：strerror
//字符操作


//字符串函数和内存操作函数的模拟实现
#define _CRT_INSECURE_DEPRECATE(fopen_s)
#include<stdio.h>
#include<assert.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>

//1.strlen 求字符串长度
//方式一：计数器方式
int my_strlen1(const char* str)
{
	int count = 0;
	while (*str)
	{
		count++;
		str++;
	}
	return count;
}
//方式二：不创建临时变量计数器
int my_strlen2(const char* str)
{
	if (*str == '\0')
		return 0;
	else
		return my_strlen2(str + 1) + 1;
}
//方式三：指针-指针方式
int my_strlen3(char* str)
{
	char* p = str;
	while (*p != '\0')
	{
		p++;
	}
	return p - str;
}

void test_my_strlen()
{
	char* str1 = "hello world";
	char* str2 = "abcd";
	char* str3 = "abcdef";
	int len1 = my_strlen1(str1);
	int len2 = my_strlen2(str2);
	int len3 = my_strlen3(str3);
	printf("str1=%d\n", len1);
	printf("str2=%d\n", len2);
	printf("str3=%d\n", len3);
}

//2.strcpy 复制字符串src到字符数组dest中去
char* my_strcpy(char* dest, char* src)
{
	char* ret = dest;
	//断言：dest和src不为空，为空的话则报错返回
	assert(dest != NULL);
	assert(src != NULL);
	while (*dest++ = *src++)
	{
		;
	}
	return ret;
}
void test_my_strcpy()
{
	char str1[] = "abc";
	char str[10] = { 0 };
	char* ret = my_strcpy(str, str1);
	printf("%s\n", ret);
}

//3.strcat 将src字符串追加到dest后面去
char* my_strcat(char* dest, const char* src)
{
	char* ret = dest;
	assert(dest != NULL);
	assert(src != NULL);
	while (*dest)
	{
		dest++;
	}
	while (*dest++ = *src++)
	{
		;
	}
	return ret;
}
void test_my_strcat()
{
	char str[10] = "ab";
	char str1[]= "+abcd";
	//在str后面追加str1
	char* ret = my_strcat(str, str1);
	printf("%s\n", ret);
}

//4.strcmp 比较两字符串大小
int my_strcmp(const char* str1, const char* str2)
{
	assert(str1 != NULL);
	assert(str2 != NULL);
	while (*str1 == *str2)
	{
		if (*str1 == '\0')
			return 0;
		str1++;
		str2++;
	}
	if (*str1 > *str2)
		return 1;
	else if (*str1 < *str2)
		return -1;
}
void test_my_strcmp()
{
	char str1[10] = "abc";
	char str2[10] = "abcd";
	int ret = my_strcmp(str1, str2);
	if (ret == 0)
		printf("str1==str2\n");
	else if (ret>0)
		printf("str1>str2\n");
	else
		printf("str1<str2\n");
}

//5.strncpy 从src中复制num个字符到dest中
char* my_strncpy(char* dest, const char* src, int num)
{
	char* temp = dest;
	assert(dest != NULL);
	assert(src != NULL);
	while (num && *src)
	{
		*dest++ = *src++;
		num--;
	}
	//当num大于字符串的长度时，用\0来进行填充
	while (num--)
	{
		*dest++ = '\0';
	}
	return temp;
}
//my_strncpy测试函数
void test_my_strncpy()
{
	char str1[10] = { 0 };
	char* str2 = "abcd";
	char* ret1 = my_strncpy(str1, str2, 2);
	printf("ret1=%s\n", ret1);
	char* ret2 = my_strncpy(str1, str2, 6);
	printf("ret2=%s\n", ret2);
}

//6. strncat 从src开头拷贝num个字符追加到dest尾部
char* my_strncat(char* dest, const char* src, size_t num)
{
	char* temp = dest;
	assert(dest != NULL);
	assert(src != NULL);
	while (*dest)
	{
		dest++;
	}
	while(num--)
	{
		*dest++ = *src++;
	}
	*dest = '\0';
	return temp;
}
//my_strncat测试函数
void test_my_strncat()
{
	char str1[10] = "ab";
	char *str2 = "cdef";
	char* ret1 = my_strncat(str1, str2, 1);
	printf("ret1=%s\n", ret1);
	char* ret2 = my_strncat(str1, str2, strlen(str2));
	printf("ret2=%s\n", ret2);
}

//7.strncmp 比较两字符串的前num个字符
int my_strncmp(const char* str1, const char* str2, int num)
{
	assert(str1 != NULL);
	assert(str2 != NULL);
	//当两字符串中字符不相等或num为0时跳出循环
	while (*str1 == *str2 && num)
	{
		if (*str1 == '\0')
			return 0;
		str1++;
		str2++;
		num--;
	}
	if (*str1 > *str2)
		return 1;
	else if (*str1 < *str2)
		return -1;
}
//strncmp测试函数
void test_my_strncmp()
{
	char* st1 = "ab";
	char* st2 = "abc";
	int ret = strncmp(st1, st2, 2);
	printf("%d\n", ret);
}

//8.strstr 判断str2是否为str1的子串
const char* my_strstr(const char* str1, const char* str2)
{
	assert(str1 != NULL);
	assert(str2 != NULL);
	char* temp1 =(char*) str1;
	char* temp2 =(char*) str2;
	char* s = NULL;

	if (*str2 == '\0')
		return NULL;
	while (*temp1)
	{
		s = temp1;
		temp2 = str2;
		while (*s && *temp2 && (*s == *temp2))
		{
			s++;
			temp2++;
		}
		if (*temp2 == '\0')
			return temp1;
		temp1++;
	}
}
void test_my_strstr()
{
	char* str2 = "cdef";
	char* str1 = "abcdefg";
	char* ret = my_strstr(str1, str2);
	if (ret!=NULL)
		printf("str2是str1 的子串\n");
	else
		printf("str2不是str1 的子串\n");
}

//9.strtok 将含有特殊字符的字符串进行分割
char* my_strtok(char* str, const char* sep)
{
	//使用静态成员temp记录每次切割的起始位置
	static char* temp = NULL;
	if (str != NULL)
		temp = str;
	if (temp == NULL)
		return NULL;
	if (sep == NULL)
		return temp;

	//将分隔符字符串中字符进行保存到一数组中
	char table[256] = { 0 };
	while(*sep != '\0')
	{
		table[*sep++] = 1;
	}
	//在temp中找第一个没在table数组中的元素
	while (*temp != '\0' && table[*temp] == 1)
	{
		temp++;
	}
	//找到了，但循环跳出可能为temp走到结束标志\0的位置，故判断一下
	char* ret = *temp != '\0' ? temp : NULL;
	while (*temp != '\0' && table[*temp] == 0)
	{
		temp++;
	}
	if (*temp != '\0')
		*temp++ = '\0';
	return ret;
}
void test_my_strtok()
{
	char str1[] = "ab@cde&a%gh*tu";
	char* str2;
	printf("Splitting string \"%s\" into tokens:\n", str1);
	str2 = my_strtok(str1, "@&%*");
	while (str2 != NULL)
	{
		printf("%s\n", str2);
		str2 = my_strtok(NULL, "@&%*");
	}
}

//10.strerror 通过错误信息标号获得错误的描述字符串
void test_strerror()
{
	FILE* p = fopen("m.txt", "r");
	if (p == NULL)
	{
		printf("Error opening file m.txt: %s\n", strerror(errno));
	}
}

int main()
{
	//test_my_strlen();
	//test_my_strcpy();
	//test_my_strcat();
	//test_my_strcmp();
	//test_my_strncpy();
	//test_my_strncat();
	//test_my_strncmp();
	//test_my_strstr();
	//test_my_strtok();
	//test_strerror();

	system("pause");
	return 0;
}

