
#include"Sort.h"
#include"Stack.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>

void Swap(int *left, int *right)
{
	int temp = *left;
	*left = *right;
	*right = temp;
}

//插入排序
//时间复杂度：O（N^2）
//空间复杂度：O（1）
//算法稳定性：稳定
//使用场景：数据接近有序或是数据较少
void InsertSort(int array[], int size)
{
	for (int i = 1; i < size; ++i)
	{
		int key = array[i];//依次将关键值获取到
		int end = i - 1;
		//找插入元素在array中的位置
		while (end >= 0 && key < array[end])
		{
			array[end + 1] = array[end];
			end--;
		}
		//进行元素key的插入
		array[end + 1] = key;
	}
}


//希尔排序
//时间复杂度：O（N^1.25）-O（1.6*N^1.25）
//空间复杂度：O（1）
//算法稳定性：不稳定
//使用场景：数据杂乱或数据量较大时
void ShellSort(int array[], int size)
{
	int grap = size;
	while (grap > 1)
	{
		grap = grap / 3 + 1;
		for (int i = grap; i < size; ++i)
		{
			//单个元素的互换
			int key = array[i];
			int end = i - grap;

			//找待插入元素在array中的位置
			while (end >= 0 && key < array[end])
			{
				//将元素往后搬移到当前分组的下一个位置
				array[end + grap] = array[end];
				//获取当前分组的前一个元素位置
				end -= grap;
			}
			//进行元素的插入
			array[end + grap] = key;
		}
	}
}

//选择排序
//时间复杂度：O（N^2）
//空间复杂度：O（1）
//算法稳定性：不稳定
void SelectSort(int array[], int size)
{
	//控制循环的趟数
	for (int i = 0; i < size; ++i)
	{
		//遍历数组找到最大元素的下标
		int maxPos = 0;
		for (int j = 1; j < size-i; ++j)
		{
			if (array[j]>array[maxPos])
				maxPos = j;
		}
		//每一趟后，将最大元素和区间的最后一个位置
		if (maxPos != size - i - 1)
		{
			Swap(&array[maxPos], &array[size - i - 1]);
		}
	}
}
//选择排序优化
//时间复杂度：O（N^2）
//空间复杂度：O（1）
//算法稳定性：不稳定
//虽然选择的趟数减少了一半，但元素的比较次数并未减少
void SelectSortOP(int array[], int size)
{
	int begin = 0;
	int end = size - 1;
	while (begin < end)
	{
		int maxPos = begin;
		int minPos = begin;
		int index = begin + 1;

		//找出区间中最大和最小的元素
		while (index <= end)
		{
			if (array[index]>array[maxPos])
				maxPos = index;
			if (array[index]<array[minPos])
				minPos = index;
			++index;
		}
		//找到最大最小的元素后，将最大元素往区间最末尾存放
		if (maxPos != end)
		{
			Swap(&array[maxPos], &array[end]);
		}
		//若最小元素恰好在end的位置，此时交换结束后得对minPos进行更新
		if (minPos == end)
		{
			minPos = maxPos;
		}
		if (minPos != begin)
		{
			Swap(&array[minPos], &array[begin]);
		}
		begin++;
		end--;
	}
}

//选择排序再优化:堆排序
//时间复杂度：O（NlogN）
//空间复杂度：O（1）
//算法稳定性：不稳定
//使用场景：部分排序或搭配其他排序使用

//堆的向下调整
//时间复杂度：O（logN）
void HeapAdjust(int array[], int size, int parent)
{
	int child = 2 * parent + 1;
	while (child < size)
	{
		//右孩子存在的情况下需要找到左右孩子中较大的孩子
		if (child + 1 < size && array[child + 1] > array[child])
			child += 1;
		//检测parent是否符合堆的性质
		if (array[parent] < array[child])
		{
			Swap(&array[parent], &array[child]);
			parent = child;
			child = parent * 2 + 1;
		}
		else
			return;
	}
}
//堆排序
void HeapSort(int array[], int size)
{
	//1.对排序数组进行建堆，升序建大堆，降序建小堆
	//建大堆
	for (int root = (size - 2) / 2; root >= 0; --root)
	{
		HeapAdjust(array, size, root);
	}
	//2.堆删除思想进行排序
	//将堆顶元素和堆中最后一个元素进行交换，有效元素-1，堆顶元素向下进行调整
	int end = size - 1;
	while (end)
	{
		Swap(&array[0], &array[end]);
		HeapAdjust(array, end, 0);
		end--;
	}
}

//冒泡排序
//时间复杂度：O（N^2）
//空间复杂度：O（1）
//算法稳定性：稳定
void BubbleSort(int array[], int size)
{
	for (int i = 0; i < size - 1; ++i)
	{
		int cn = 0;
		for (int j = 0; j < size - i - 1; ++j)
		{
			if (array[j]>array[j + 1])
			{
				Swap(&array[j], &array[j + 1]);
				cn = 1;
			}
		}
		if (!cn)
			return;
	}
}

//快速排序
//时间复杂度：O（N*logN）
//空间复杂度：O（logN）
//算法稳定性：不稳定

//基准值选取优化
//三数取中法
//区间为左闭右开
int GetMiddleIndex(int array[], int left, int right)
{
	int mid = left + ((right - left) >> 1);
	//array[left]
	//array[right-1]
	//array[mid]
	if (array[left] < array[right - 1])
	{
		if (array[mid] < array[left])
			return left;
		else if (array[mid]>array[right - 1])
			return right - 1;
		else
			return mid;
	}
	else
	{
		if (array[mid] > array[left])
			return left;
		else if (array[mid] < array[right - 1])
			return right - 1;
		else
			return mid;
	}
}

//划分方式1：hoare版本
//时间复杂度：O（N）
int Partion1(int array[], int left, int right)
{
	//三数取中法选取基准值
	int keyIndex = GetMiddleIndex(array, left, right);
	//将基准值和区间最后一个元素进行交换
	Swap(&array[keyIndex], &array[right - 1]);

	int begin = left;//注意：begin不能从0开始
	int end = right - 1;
	int key = array[end];
	while (begin < end)
	{
		//begin从前往后找比基准值大的元素
		while (begin < end && array[begin] <= key)
			begin++;
		//end从后往前找比基准值小的元素
		while (begin < end && array[end] >= key)
			end--;
		if (begin < end)
		{
			Swap(&array[begin], &array[end]);
		}
	}
	if (begin != right - 1)
	{
		Swap(&array[begin], &array[right - 1]);
	}
	return begin;
}
//划分方式2：挖坑法
//时间复杂度：O（N）
int Partion2(int array[], int left, int right)
{
	int keyIndex = GetMiddleIndex(array, left, right);
	Swap(&array[keyIndex], &array[right - 1]);
	
	int begin = left;//注意：begin不能从0开始
	int end = right - 1;
	int key = array[end];
	while (begin < end)
	{
		//初始end为坑，让begin从前往后找比基准值大的元素
		while (begin < end && array[begin] <= key)
			begin++;
		//找到比基准值大的元素
		if (begin < end)
		{
			//将begin位置元素覆盖end位置元素
			array[end] = array[begin];
			end--;
		}
		//begin成为新的坑位
		//让end从后往前找比基准值小的元素去填begin位置的坑
		while (begin < end && array[end] >= key)
			end--;
		if (begin < end)
		{
			//将end位置元素去填begin位置元素
			array[begin] = array[end];
			begin++;
		}
		//end位置又成为新的坑位
	}
	//最后用key值将最后一个坑位进行填充
	array[begin] = key;
	return begin;
}
//划分方式3：前后指针法
//时间复杂度：O（N）
int Partion3(int array[], int left, int right)
{
	int cur = left;
	int prev = cur - 1;

	int keyIndex = GetMiddleIndex(array, left, right);
	Swap(&array[keyIndex], &array[right - 1]);
	int key = array[right - 1];
	while (cur < right)
	{
		//cur从前往后找比基准值小的元素
		//找到后，若prev的下一个位置和cur不等则进行交换
		if (array[cur] < key && ++prev != cur)
		{
			Swap(&array[prev], &array[cur]);
		}
		++cur;
	}
	if (++prev != right - 1)
	{
		Swap(&array[prev], &array[right - 1]);
	}
	return prev;
}

//快速排序（递归实现）
void QuickSort(int array[], int left, int right)
{
	if (right - left <16 || right-left==16)
	{
		InsertSort(array + left, right - left);
	}
	else
	{
		//1.在区间[left,right)找一基准值，按照基准值将区间划分为两部分
		int div = Partion1(array, left, right);
		//div基准值在数组中的下标
		//2.递归排序基准值左侧
		QuickSort(array, left, div);
		//3.递归排序基准值右侧
		QuickSort(array, div + 1, right);
	}
}

//快速排序（非递归实现）
void QuickSortNor(int array[], int size)
{
	Stack s;
	StackInit(&s);

	StackPush(&s, size);
	StackPush(&s, 0);

	while (!StackEmpty(&s))
	{
		//获取区间左边界
		int left = StackTop(&s);
		StackPop(&s);
		//获取区间右边界
		int right = StackTop(&s);
		StackPop(&s);

		//对区间[left,right)进行区间划分
		if (right - left <= 1)
			continue;
		int div = Partion3(array, left, right);
		//基准值左侧[left,div)
		//基准值右侧[div+1,right)

		//将基准值右侧压栈，先压右侧再压左侧
		StackPush(&s, right);
		StackPush(&s, div+1);
		//将基准值左侧压栈，先压右侧再压左侧
		StackPush(&s, div);
		StackPush(&s, left);
	}
	StackDestroy(&s);
}

//归并排序
//时间复杂度：O（N*logN）  
//空间复杂度：O(N)
//算法稳定性：稳定

//将两有序区间进行合并
void MergeData(int array[], int left, int mid,int right, int* temp)
{
	//左半侧
	int begin1 = left;
	int end1 = mid;

	//右半侧
	int begin2 = mid;
	int end2 = right;

	int index = left;
	//将两区间中元素从前往后进行比较，将较小元素往temp中搬移
	while (begin1 < end1 && begin2 < end2)
	{
		if (array[begin1] <= array[begin2])
			temp[index++] = array[begin1++];
		else
			temp[index++] = array[begin2++];
	}
	//将较长区间的剩余数据往temp中进行搬移
	while (begin1<end1)
		temp[index++] = array[begin1++];
	while (begin2<end2)
		temp[index++] = array[begin2++];
}

void _MergeSort(int array[], int left, int right, int* temp)
{
	if (right - left <= 1)
	{
		return;
	}
	//1.将区间分为两部分
	int mid = left + ((right - left) >> 1);

	//2.递归排序左半侧[left,mid)
	_MergeSort(array, left, mid, temp);

	//3.递归排序右半侧[mid+1,right)
	_MergeSort(array, mid , right, temp);

	//4.将排序好的左半侧和右半侧进行合并
	MergeData(array, left, mid, right, temp);
	memcpy(array + left, temp + left, (right - left)*sizeof(int));
}
//归并排序（递归实现）
void MergeSort(int array[], int size)
{
	int* temp = (int*)malloc(sizeof(int)*size);
	if (temp == NULL)
	{
		assert(0);
		return;
	}
	_MergeSort(array, 0, size, temp);
	free(temp);
}

//归并排序（非递归实现）
void MergeSortNor(int array[], int size)
{
	int* temp = (int*)malloc(sizeof(int)*size);
	if (temp == NULL)
	{
		assert(0);
		return;
	}
	int gap = 1;
	while (gap < size)
	{
		for (int i = 0; i < size; i += 2 * gap)
		{
			//每个区间都有gap个元素[left,mid) [mid,right)
			int left = i;
			int mid = left + gap;
			int right = mid + gap;

			if (mid>size)
				mid = size;
			if (right > size)
				right = size;
			//将两区间进行归并
			MergeData(array, left, mid, right, temp);
		}
		memcpy(array, temp, sizeof(int)*size);
		gap <<= 1;
	}
	free(temp);
}

//计数排序
//时间复杂度：O（N）N表示元素的个数  
//空间复杂度：O(M)  M表示区间中元素的个数
//算法稳定性：稳定
//使用场景：数据密集几种在某个范围内
void CountSort(int array[], int size)
{
	//1.需统计区间中数据的范围，若事先知道数据的范围则不需要统计数据的区间范围
	int minValue = array[0];
	int maxValue = array[0];
	for (int i = 0; i < size; ++i)
	{
		if (array[i] < minValue)
			minValue = array[i];
		if (array[i]>maxValue)
			maxValue = array[i];
	}
	//2.计算需要多少个保存计数的空间
	int range = maxValue - minValue + 1;
	int* countArray = (int*)calloc(range, sizeof(int));
	//3.统计每个元素出现的次数
	for (int i = 0; i < size; ++i)
	{
		countArray[array[i] - minValue]++;
	}
	//4.按照统计的结果对数据进行回收
	int index = 0;
	for (int i = 0; i < range; ++i)
	{
		while (countArray[i]>0)
		{
			array[index] = i + minValue;
			countArray[i] --;
			index++;
		}
	}
	free(countArray);
}

void PrintArray(int array[], int size)
{
	for (int i = 0; i < size; ++i)
	{
		printf("%d ", array[i]);
	}
	printf("\n");
}
void TestSort()
{
	int arr[] = { 3, 1, 8, 6, 0, 2, 7, 9, 4, 5 };
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));

	InsertSort(arr, sizeof(arr) / sizeof(arr[0]));
	printf("插入排序为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));

	ShellSort(arr, sizeof(arr) / sizeof(arr[0]));
	printf("希尔排序为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));

	SelectSort(arr, sizeof(arr) / sizeof(arr[0]));
	printf("选择排序为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));

	SelectSortOP(arr, sizeof(arr) / sizeof(arr[0]));
	printf("优化选择排序为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));
	
	HeapSort(arr, sizeof(arr) / sizeof(arr[0]));
	printf("选择排序再优化->堆排序为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));
	
	BubbleSort(arr, sizeof(arr) / sizeof(arr[0]));
	printf("冒泡排序为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));

	QuickSort(arr, 0, sizeof(arr) / sizeof(arr[0]));
	printf("hoare版本快速排序（递归）为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));

	QuickSort(arr, 0, sizeof(arr) / sizeof(arr[0]));
	printf("hoare版本快速排序（非递归）为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));

	MergeSort(arr, sizeof(arr) / sizeof(arr[0]));
	printf("归并（递归）为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));

	MergeSortNor(arr, sizeof(arr) / sizeof(arr[0]));
	printf("归并（非递归）为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));

	CountSort(arr, sizeof(arr) / sizeof(arr[0]));
	printf("计数排序为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));

	/*QuickSort(arr, 0, sizeof(arr) / sizeof(arr[0]));
	printf("挖坑法快速排序（递归）为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));*/

	/*QuickSort(arr, 0, sizeof(arr) / sizeof(arr[0]));
	printf("前后指针法快速排序（递归）为：\n");
	PrintArray(arr, sizeof(arr) / sizeof(arr[0]));*/

}
