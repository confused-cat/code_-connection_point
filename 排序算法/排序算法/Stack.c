#include"Stack.h"
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<malloc.h>
#include<string.h>

// 1.初始化栈 
void StackInit(Stack* ps)
{
	assert(ps);
	ps->array = malloc(sizeof(DataType)* 3);
	if (ps->array == NULL)
	{
		assert(0);
		printf("StackInit:malloc申请空间失败！");
		return;
	}
	ps->capacity = 3;
	ps->size = 0;
}

void StackCheckCapacity(Stack* ps)
{
	assert(ps);
	if (ps->size == ps->capacity)
	{
		//1.开辟新空间
		int newCapacity = ps->capacity * 2;
		DataType* temp = malloc(sizeof(DataType)*newCapacity);
		if (temp == NULL)
		{
			assert(0);
			printf("StackCheckCapacity:mallo失败！\n");
			return;
		}
		
		//2.将旧空间元素拷贝到新空间
		memcpy(temp, ps->array, sizeof(DataType)*ps->size);
		
		//3.释放旧空间
		free(ps->array);
		
		//4.使用新空间
		ps->array = temp;
		ps->capacity = newCapacity;
	}
}
// 2.入栈 
void StackPush(Stack* ps, DataType data)
{
	//检测空间是否需要扩容
	StackCheckCapacity(ps);
	ps->array[ps->size] = data;
	ps->size++;
}
// 3.出栈 
void StackPop(Stack* ps)
{
	if (StackEmpty(ps))
		return;
	ps->size--;
}

// 4.获取栈顶元素 
DataType StackTop(Stack* ps)
{
	assert(ps);
	return ps->array[ps->size - 1];
}

// 5.获取栈中有效元素个数 
int StackSize(Stack* ps)
{
	assert(ps);
	return ps->size;
}

// 6.检测栈是否为空，如果为空返回非零结果，如果不为空返回0 
int StackEmpty(Stack* ps)
{
	assert(ps);
	return ps->size == 0;
}

// 7.销毁栈 
void StackDestroy(Stack* ps)
{
	assert(ps);
	free(ps->array);
	ps->array = NULL;
	ps->capacity = 0;
	ps->size = 0;
}