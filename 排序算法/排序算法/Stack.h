#pragma once


typedef int DataType;//将int用STDataType代替
//栈的结构定义
typedef struct Stack
{
	DataType* array;
	int capacity;		//栈的容量
	int size;  // 栈顶（栈中有效元素）
}Stack;

// 1.初始化栈 
void StackInit(Stack* ps);

// 2.入栈 
void StackPush(Stack* ps, DataType data);

// 3.出栈 
void StackPop(Stack* ps);

// 4.获取栈顶元素 
DataType StackTop(Stack* ps);

// 5.获取栈中有效元素个数 
int StackSize(Stack* ps);

// 6.检测栈是否为空，如果为空返回非零结果，如果不为空返回0 
int StackEmpty(Stack* ps);

// 7.销毁栈 
void StackDestroy(Stack* ps);