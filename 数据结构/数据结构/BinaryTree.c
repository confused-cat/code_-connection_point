#include"BinaryTree.h"
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"Queue.h"


// 通过前序遍历的数组"ABD##E#H##CF##G##"构建二叉树
BTNode* BinaryTreeCreate(BTDataType* a, int n, int* pi)
{
	//BTDataType* a数组首元素地址
	//n数组元素个数
	BTNode* root = NULL;
	if ((*pi) < n && a[*pi] != '#')
	{
		//创建根节点
		BTNode* root = (BTNode*)malloc(sizeof(BTNode));
		root->_data = a[*pi];
		//递归创建根的左子树
		++(*pi);
		root->_left = BinaryTreeCreate(a, n, pi);
		//递归创建根右子树
		++(*pi);
		root->_right = BinaryTreeCreate(a, n, pi);
	}
	return root;
}

// 二叉树销毁
void BinaryTreeDestory(BTNode** root)
{
	assert(root);
	//如果根结点的指向为空时则返回
	if (*root == NULL)
		return;
	//在进行空间销毁时，得先将左右空间给销毁掉，最后释放根节点空间
	BinaryTreeDestory(&(*root)->_left);
	BinaryTreeDestory(&(*root)->_right);
	free(*root);
	*root == NULL;
}

// 二叉树节点个数
int BinaryTreeSize(BTNode* root)
{
	//采用递归思想 二叉树的概念
	//1.空树
	if (root == NULL)
	{
		return 0;
	}
	//2.不为空树 根节点+根节点的左子树+根节点的右子树
	return 1 + BinaryTreeSize(root->_left) + BinaryTreeSize(root->_right);
}

// 二叉树叶子节点个数
int BinaryTreeLeafSize(BTNode* root)
{
	//二叉树叶子节点：无左右孩子树的节点
	//1.空树
	if (root == NULL)
		return 0;
	//2.不为空树 且节点左右孩子也为空 （root->_left==NULL）&&（root->_right==NULL）
	if ((root->_left == NULL)&&(root->_right == NULL))
		return 1;
	return BinaryTreeLeafSize(root->_left) + BinaryTreeLeafSize(root->_right);
}

// 二叉树第k层节点个数
int BinaryTreeLevelKSize(BTNode* root, int k)
{
	if ((root==NULL)&&k < 1)
		return 0;
	if (k == 1)
		return 1;
	return BinaryTreeLevelKSize(root->_left, k - 1) + BinaryTreeLevelKSize(root->_right, k - 1);
}

// 二叉树查找值为x的节点
BTNode* BinaryTreeFind(BTNode* root, BTDataType x)
{
	//数空则返回空
	if (root == NULL)
		return NULL;
	//先查找根结点 再在左右子树进行查找
	if(x == root->_data)
		return root;
	BTNode* ret;
	ret =BinaryTreeFind(root->_left, x);
	if (ret != NULL)
		return ret;
	return BinaryTreeFind(root->_right, x);

}

// 二叉树前序遍历 
void BinaryTreePrevOrder(BTNode* root)
{
	if (root == NULL)
		return;
	printf("%d ", root->_data);
	BinaryTreePrevOrder(root->_left);
	BinaryTreePrevOrder(root->_right);
}

// 二叉树中序遍历
void BinaryTreeInOrder(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreeInOrder(root->_left);
	printf("%d ", root->_data);
	BinaryTreeInOrder(root->_right);
}

// 二叉树后序遍历
void BinaryTreePostOrder(BTNode* root)
{
	if (root == NULL)
		return;
	BinaryTreePostOrder(root->_left);
	BinaryTreePostOrder(root->_right);
	printf("%d ", root->_data);
	
}

// 层序遍历
void BinaryTreeLevelOrder(BTNode* root)
{
	Queue q;
	if (root == NULL)
		return;
	//借助队列进行二叉树层序遍历
	
	QueueInit(&q);

	//将树的根节点放入队列，当队列不空时，循环继续
	QueuePush(&q, root);
	while (!QueueEmpty(&q))
	{
		//从对头取一个节点
		struct BTNode* cur = QueueFront(&q);//只是获取对头元素，并未将其进行删除，当遍历完节点时，得将该节点出队列，即删除
		//将该节点进行遍历
		printf("%d ", root->_data);
		//若cur有左右孩子，则将左右孩子入队列
		if (cur->_left)
			Queue(&q, cur->_left);
		if (cur->_right)
			Queue(&q, cur->_right);
		QueuePop(&q);//将节点从队列中删除
	}
	QueueDestroy(&q);
	printf("\n");
}

// 判断二叉树是否是完全二叉树
int BinaryTreeComplete(BTNode* root)
{
	Queue(&q);
	QueueInit(&q);
	QueuePush(&q, root);

	int flag = 0;//标记第一个不饱和节点的位置
	ret = 1;
	//空树也是完全二叉树
	if (root == NULL)
		return 1;
	//按照层序遍历找到第一个不饱和的点（要么没孩子，要么只有一个左孩子）
	
	while (!QueueEmpty(&q))
	{
		BTNode* cur = QueueFront(&q);
		QueuePop(&q);
		if (flag)
		{
			if (cur->_left || cur->_right);
			ret = 0;
			break;
		}
		else
		{

			//检测cur是否为饱和的节点
			if (cur->_left && cur->_right)
			{
				//分别将左右孩子入队
				QueuePush(&q, cur->_left);
				QueuePush(&q, cur->_right);
			}
			else if (cur->_left)//只有左孩子
			{
				QueuePush(&q, cur->_left);
				flag = 1;
			}
			else if (cur->_right)//只有右孩子没有左孩子
			{
				ret = 0;
				break;
			}
			else
			{
				//表示cur左右孩子都不存在则该节点为第一个不饱和的节点
				flag = 1;
			}
		}
	}
	QueueDestroy(&q);
	return ret;
}


//求二叉树高度
int BinaryTreeHeight(BTNode* root)
{
	if (root == NULL)
		return 0;
	int _leftHeight=BinaryTreeHeight(root->_left);
	int _rightHeight= BinaryTreeHeight(root->_right);
	return _leftHeight > _rightHeight ? _leftHeight + 1 : _rightHeight + 1;
}






#include "BTree.h"
#include "queue.h" //参考之前的代码
#include "stack.h"

BTNode *BinaryTreeCreate(BTDataType * src, int n, int* pi)
{
	if (src[*pi] == '#' || *pi >= n)
	{
		(*pi)++;
		return NULL;
	}

	BTNode * cur = (BTNode *)malloc(sizeof(BTNode));
	cur->_data = src[s_n];
	(*pi)++;

	cur->_left = BinaryTreeCreateExe(src);
	cur->_right = BinaryTreeCreateExe(src);

	return cur;
}

void BinaryTreePrevOrder(BTNode* root)
{
	if (root)
	{
		putchar(root->_data);
		BinaryTreePrevOrder(root->_left);
		BinaryTreePrevOrder(root->_right);
	}
}

void BinaryTreeInOrder(BTNode* root)
{
	if (root)
	{
		BinaryTreeInOrder(root->_left);
		putchar(root->_data);
		BinaryTreeInOrder(root->_right);
	}
}

void BinaryTreePostOrder(BTNode* root)
{
	if (root)
	{
		BinaryTreePostOrder(root->_left);
		BinaryTreePostOrder(root->_right);
		putchar(root->_data);
	}
}

void BinaryTreeDestory(BTNode** root)
{
	if (*root)
	{
		BinaryTreeDestory(&(*root)->_left);
		BinaryTreeDestory(&(*root)->_right);
		free(*root);
		*root = NULL;
	}
}

void BinaryTreeLevelOrder(BTNode* root)
{
	Queue qu;
	BTNode * cur;

	QueueInit(&qu);

	QueuePush(&qu, root);

	while (!QueueIsEmpty(&qu))
	{
		cur = QueueTop(&qu);

		putchar(cur->_data);

		if (cur->_left)
		{
			QueuePush(&qu, cur->_left);
		}

		if (cur->_right)
		{
			QueuePush(&qu, cur->_right);
		}

		QueuePop(&qu);
	}

	QueueDestory(&qu);
}

int BinaryTreeComplete(BTNode* root)
{
	Queue qu;
	BTNode * cur;
	int tag = 0;

	QueueInit(&qu);

	QueuePush(&qu, root);

	while (!QueueIsEmpty(&qu))
	{
		cur = QueueTop(&qu);

		putchar(cur->_data);

		if (cur->_right && !cur->_left)
		{
			return 0;
		}

		if (tag && (cur->_right || cur->_left))
		{
			return 0;
		}

		if (cur->_left)
		{
			QueuePush(&qu, cur->_left);
		}

		if (cur->_right)
		{
			QueuePush(&qu, cur->_right);
		}
		else
		{
			tag = 1;
		}

		QueuePop(&qu);
	}

	QueueDestory(&qu);
	return 1;
}

