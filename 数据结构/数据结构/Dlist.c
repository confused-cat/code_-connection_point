#include"Dlist.h"
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<assert.h>


// 创建返回链表的头结点.
ListNode* ListCreate(LTDataType data)
{
	ListNode* newNode = (ListNode*)malloc(sizeif(ListNode));
	if (newNode == NULL)
	{
		assert(0);
		return NULL;
	}
	newNode->_data = data;
	newNode->_next = NULL;
	newNode->_prev = NULL;
	return newNode;

}

// 双向链表销毁
void ListDestory(ListNode* pHead);
{
	
}

// 双向链表打印
void ListPrint(ListNode* pHead)
{
	ListNode* cur = pHead->next;
	while (cur != pHead)
	{
		printf("%d ", cur->_data);
		cur = cur->_next;
	}
	printf("\n");
}


// 双向链表尾插
void ListPushBack(ListNode* pHead, LTDataType x)
{
	//phead后面插入x
	ListInsert(phead, x);
}

// 双向链表尾删
void ListPopBack(ListNode* pHead)
{
	//双向链表为空则返回
	assert(phead);
	if (phead->next = phead)
		return;
	if (phead == NULL)
		return;
	ListErase(phead->prev);//循环双向链表 头结点的前一个节点也是双向链表的后一个节点
}

// 双向链表头插
void ListPushFront(ListNode* pHead, LTDataType x)
{
	ListNode* newNode = NULL;
	if (phead->prev == NULL)
		return;
	ListInsert(phead->next, x);

}

// 双向链表头删
void ListPopFront(ListNode* pHead)
{
	//将phead给删掉
	//双向链表为空则返回
	assert(phead);
	if (phead->next = phead)
		return;
	if (phead->next== NULL)
		return;
	ListErase(phead->next);

}

// 双向链表查找
ListNode* ListFind(ListNode* pHead, LTDataType x)
{
	ListNode* cur = pHead->_next;
	while (cur != pHead)
	{
		if (cur->_data == data)
			return cur;
		cur = cur->_next;
	}
	return NULL;
}

// 双向链表在pos的前面进行插入
void ListInsert(ListNode* pos, LTDataType x)
{
	ListNode* newNode = NULL;
	if (pos == NULL)
		return;
	newNode = ListNode(x);
	//将新节点连接到pos之前
	newNode->next = pos;
	newNode->prev = pos->prev;
	//断开原链表
	newNode->prev->next = newNode;
	pos->prev = newNode;
}

// 双向链表删除pos位置的节点
void ListErase(ListNode* pos)
{
	if (pos == NULL)
		return;
	pos->prev->next = pos->next;
	pos->next->prev = pos->prev;
	free(pos);
}