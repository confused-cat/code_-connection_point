#include"Heao.h"
#include<stdio.h>
#include<assert.h>
#include<string.h>
#include<stdlib.h>

//此代码适用于小堆情况
void CheckCapacity(Heap *hp)//对空间进行扩容
{
	assert(hp);
	if (hp->size == hp->capacity)
	{
		//1.进行新空间的申请
		int newcapacity = hp->capacity * 2;
		HPDataType* temp = (HPDataType*)malloc(sizeof(HPDataType)*newcapacity);
		if (temp = NULL)
		{
			assert(0);
			printf("CheckCapacity:malloc空间失败！！！\n");
			return;
		}
		//2.将旧空间元素拷贝到新空间中
		memcpy(temp, hp->a, sizeof(HPDataType)*hp->size);
		//3.释放旧空间
		free(hp->a);
		hp->a = temp;
		hp->capacity = newcapacity;

	}
}

void Swap(HPDataType *left, HPDataType *right)//在对堆进行向上向下调整时 我们得进行数据的交换
{
	int temp = *left;
	*left = *right;
	*right = temp;
}
void AdjustDown(Heap *hp, int parent)//对堆进行向下调整（现在我们默认堆为小堆）
{
	int *a = hp->a;
	int size = hp->size;

	//parent和child在数组中下标为：child=parent*2+1
	//默认child先标记左孩子
	int child = parent * 2 + 1;
	while (child < size)
	{
		if (child + 1 < size && (hp->a[child] > hp->a[child + 1])
		{
			child = child + 1;
			if (hp->a[parent]>hp->a[child])
			{
				Swap(&a[child], &a[parent]);
				parent = child;
				child = parent * 2 + 1;
			}
			else
				return;
		}
	}
}


void AdjustUp(Heap *hp, int child)//对堆进行向上调整（现在我们默认堆为小堆）
{
	int* a = hp->a;
	int parent = (child - 1) / 2;

	while (child)
	{
		if (hp->a[child] < hp->a[parent])
		{
			Swap(&a[child], &a[parent]);
			child = parent;
			parent = (child - 1) / 2;
		}
		else
			return;
	}

}
// 堆的构建
void HeapCreate(Heap* hp, HPDataType* _a, int n);
{
	hp->a = (HPDataType*)malloc(sizeof(HPDataType)*n);
	if (hp->a == NULL)
	{
		assert(0);
		return;
	}
	hp->capacity = n;

	memcpy(hp->a, _a, sizeof(HPDataType)*n);
	hp->size = n;
	hp->Compare = Compare;
	//将元素弄成堆结构后 其元素排序并不遵循堆元素的特性 故需将二叉树调整为堆
	//从倒数第一个非叶子节点进行向上调整
	for (int root = (n - 2) / 2; root >= 0; ++root)

	{
		AdjustUp(hp, root);
	}
}
// 堆的销毁
void HeapDestory(Heap* hp);
{
	assert(hp);
	free(hp->a);
	hp->a = NULL;
	hp->size = 0;
	hp->capacity = 0;

}



// 堆的插入
void HeapPush(Heap* hp, HPDataType x)
{
	//首先对数组进行元素检查是否空间已满得考虑是否扩容
	CheckCapacity(hp);
	hp->a[hp->size] = x;
	hp->size++;//数组有效元素进行增加

	//因为将一元素直接插入到堆的最后，可能造成堆中元素不再满足堆的元素特征 故得向上进行元素的调整
	AdjustUp(hp,hp->size-1);

}

// 堆的删除
void HeapPop(Heap* hp)
{
	//若堆本来就为空 则直接返回
	if (HeapEmpty(hp))
		return;
	//当堆不为空时
	//将堆顶元素与堆中最后一个元素进行交换，将堆顶元素往下调整
	Swap(&a[0], &a[hp->(size - 1)]);
	hp->size--;
	//将堆顶元素往下调整
	AdjustDown(hp, 0);
}

// 取堆顶的数据
HPDataType HeapTop(Heap* hp)
{
	assert(hp);
	
	reurn hp->a[0];
}

// 堆的数据个数
int HeapSize(Heap* hp);
{
	assert(hp);
	return hp->size;
}

// 堆的判空
int HeapEmpty(Heap* hp);
{
	assert(hp);//意思是不为空的话我们就把hp->size置为空吗
	return hp->size == 0;
}

// TopK问题：找出N个数里面最大/最小的前K个问题。
// 比如：未央区排名前10的泡馍，西安交通大学王者荣耀排名前10的韩信，全国排名前10的李白。等等问题都是Topk问题，
// 需要注意：
// 找最大的前K个，建立K个数的小堆
// 找最小的前K个，建立K个数的大堆

void PrintTopK(int* a, int n, int k)
{
	//1.将a中前k个元素建堆
	HeapCreate(Heap* h, int* a, int k)
	{
		h->a = (int*)malloc(sizeof(int)*k);
		if (h->a == NULL)
		{
			assert(0);
			return;
		}
		hp->capacity = k;

		memcpy(hp->a, _a, sizeof(int)*k);
		hp->size = k;
		hp->Compare = Compare;
		//将元素弄成堆结构后 其元素排序并不遵循堆元素的特性 故需将二叉树调整为堆
		//从倒数第一个非叶子节点进行向上调整
		for (int root = (n - 2) / 2; root >= 0; ++root)

		{
			AdjustUp(h, root);
		}
	}
	//2.将剩余n-k个元素依次与堆顶元素进行比较 比堆顶元素大的话则替换 将堆向下调整 重复此步骤

	int m = HeapTop(Heap* h);
	for (int j = k; k < n; ++j)
	{
		if (m < a[j])
		{
			h->a[0] = a[j];
			AdjustDown(h, root);
		}
		else
			continue;
	}
}

void TestTopk()
{
	int n = 10000;
	int *a = (int *)malloc(sizeof(int)*n);
	srand(time(0));
	for (int i = 0; i < n; ++i)
	{
		a[i] = rand() % 10000000;
	}




	PrintTopK(a, n, 10)
}

//对数组进行堆排序
void HeapSort(int* a, int n)
{
	assert(a);
	for (int i = (n - 1) / 2; i >= 0; --i)
	{
		AdjustDown(a, n, i);
	}
}
void PrintHeap(int *a, int n)
{
	assert(a);
	for (int i = 0; i < n; ++i)
	{
		printf("%d ", a[i]);
	}
	printf("\n");
}
