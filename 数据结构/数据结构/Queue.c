#include"Queue.h"
#include<stdio.h>
#include<assert.h>

// 初始化队列 
void QueueInit(Queue* q);
{
	assert(q);
	q->front == 0;
	q->rear == 0;
}
// 队尾入队列 
void QueuePush(Queue* q, QDataType data);
{
	QNode *p;
	p = (QNode*)malloc(sizeof(QNode));
	if (p == NULL)
		return;
	p->data = data;
	p->next = NULL;
	q->rear->next = p;
	q->rear = p;

}
// 队头出队列 
void QueuePop(Queue* q)
{
	QNode* p;
	if (q->front == q->rear)
		return;
	p = q->front->next;
	q->front->next = p->next;
	free(p);
	q->front = NULL;
	q->rear = NULL;
}
// 获取队列头部元素 
QDataType QueueFront(Queue* q)
{
	assert(q);
	if (QueueEmpty(q))
		return;
	return q->front->data;
}
// 获取队列队尾元素 
QDataType QueueBack(Queue* q)
{
	assert(q);
	if (QueueEmpty(q))
		return;
	return q->rear->data;
}
// 获取队列中有效元素个数 
int QueueSize(Queue* q)
{
	if (q->front == q->rear)
		return 0;
	Queue* p;
	int count = 0;
	p = q->rear->next;
	while (p)
	{
		count++;
		p = p->next;
	}
	return count;
}
// 检测队列是否为空，如果为空返回非零结果，如果非空返回0 
int QueueEmpty(Queue* q)
{
	if (q->front = NULL && q->rear = NULL)
		return;
	return -1;
		
}
// 销毁队列 
void QueueDestroy(Queue* q)
{
	while (q->front)
	{
		q->rear = q->front->next;
		free(q->front);
		q->front = q->rear;
	}
	return;
}

