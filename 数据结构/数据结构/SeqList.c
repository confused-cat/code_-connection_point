#include"SeqList.h"
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

//1.顺序表的初始化
void SeqListInit(SeqList* ps)
{
	assert(ps);
	//对顺序表设置初始的容量
	ps->a = (DataType*)malloc(3 * sizeof(DataType));
	if (ps->a == NULL)
	{
		printf("SeqList:malloc空间失败！");
		exit(0);//正常运行程序并退出程序  exit（1）非正常运行导致退出程序
	}
	//否则申请空间成功
	ps->capacity = 3;
	ps->size = 0;
}

//2.顺序表的销毁
void SeqListDestroy(SeqList* ps)
{
	assert(ps);
	if (ps->a)
	{
		free(ps->a);//释放空间后得将结构体内空间置为0 防止出现野指针
		ps->a = NULL;
		ps->capacity = 0;
		ps->size = 0;
	}
}

//3.顺序表的打印
void SeqListPrint(SeqList* ps)
{
	assert(ps);
	for (int i = 0; i < ps->size; ++i)
	{
		printf("%d", ps->a[i]);
	}
}

//4.顺序表尾插
void SeqListPushBack(SeqList* ps, DateType x)
{
	//如果空间已满的话得考虑是否扩容
	assert(ps);
	CheckCapacity(ps);

	//直接插入
	ps->a[ps->size] = x;
	//多了个元素 更新size
	ps->size++;
}

//5.顺序表头插
void SeqListPushFront(SeqList* ps, DateType x)
{
	assert(ps);
	CheckCapacity(ps);
	//将位置空出来 原来元素的下标+1
	for (int i = ps->size-1; i >= 0; --i)
	{
		ps->a[i + 1] = ps->a[i];

	}
	ps->a[0] = x;
	ps->size++;

}

//6.获取顺序表起始位置元素
void SeqListPopFront(SeqList* ps)
{
	assert(ps);
	return ps->a[0];
}

//7.获取顺序表最后一个元素
void SeqListPopBack(SeqList* ps);
{
	assert(ps);
	return ps->a[ps->isze - 1];
}


// 顺序表查找任意位置元素x
int SeqListFind(SeqList* ps, DateType x)
{
	assert(ps);
	//我们对顺序表进行遍历 逐一与x进行比较
	for (int i = 0; i < ps->size; ++i)
	{
		if (x == ps->a[i])
			return i;
	}
	return -1;
}

// 顺序表在pos位置插入x pos为数字下标
void SeqListInsert(SeqList* ps, int pos, DateType x)
{
	assert(ps);
	//进行参数的检测 看是否造成数组越界
	if (pos<0 || pos>a->size)
	{
		printf("SeqListInset pos越界了！");
		exit(0);
	}
	//参数未越界 当插入元素时 若数组已满的情况下得进行数组的扩容
	CheckCapacity(ps);

	//元素的插入 将x插入到pos后 则原数组的pos后的元素得后移一个位置 即下标+1
	//先进行后续元素的搬移
	for (int i = ps->size-1; i >= pos; --i)
	{
		ps->a[i + 1] = ps->a[i];
	}
	//进行元素的插入
	ps->a[pos] = x;
	//插入元素之后 size=size+1
	ps->size++;

}

// 顺序表删除pos位置的值
void SeqListErase(SeqList* ps, int pos)
{
	assert(ps);
	//进行参数的检测
	if (pos > ps->size)
	{
		printf("SeqListErase中参数pos非法！");
	}
	//元素删除
	for (int i = pos + 1; i < ps->size; ++i)
	{
		ps->a[i - 1] = ps->a[i];
	}
	ps->size--;
}

//数组越界时进行空间的扩容
void CheckCapacity(ps)
{
	//空间不足 进行空间的扩容
	if (ps->capacity == ps->size)
	{
		int newcapacity = ps->capacity * 2;
		//开辟新空间
		DataType* temp = (DataType*)malloc(newcapacity* sizeof(DataType));
		//DataType* temp = (DataType*)malloc(DataType* sizeof(DataType)*2);
		if (NULL == temp)
		{
			printf("申请空间失败！");
			exit(0);
		}
		//新空间申请成功 将旧空间的元素拷贝到新空间
		for (int i = 0; i < ps->size; ++i)
		{
			temp[i] = ps->a[i];
		}

		//元素拷贝完全之后 释放旧空间
		free(ps->a);
		//使用新空间 更新数据
		ps->a = temp;
		ps->capacity = newcapacity;
	}
}

void TestSeqList()
{
	int a[10] = { 1 2 3 4 5 6 7 8 9 0 };
	SeqList ps;
	SeqListInit(ps);


}