
#pragma once
#include <stdio.h>
#include <assert.h>
#include <stdlib.h>

typedef int DateType;
typedef struct SeqList
{
	DateType* a;
	DateType size;
	DateType capacity; // unsigned int
}SeqList;

// 对数据的管理:增删查改 
//1.顺序表的初始化
void SeqListInit(SeqList* ps);

//2.顺序表的销毁
void SeqListDestroy(SeqList* ps);

//3.顺序表的打印
void SeqListPrint(SeqList* ps);

//4.顺序表尾插
void SeqListPushBack(SeqList* ps, SLDateType x);

//5.顺序表头插
void SeqListPushFront(SeqList* ps, SLDateType x);

//6.顺序表头删
void SeqListPopFront(SeqList* ps);

//7.顺序表头插
void SeqListPopBack(SeqList* ps);


// 顺序表查找元素x
int SeqListFind(SeqList* ps, DateType x);

// 顺序表在pos位置插入x
void SeqListInsert(SeqList* ps, size_t pos, SLDateType x);

// 顺序表删除pos位置的值
void SeqListErase(SeqList* ps, size_t pos);

void TestSeqList( );
