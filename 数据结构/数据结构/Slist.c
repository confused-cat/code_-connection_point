#include"Slist.h"
#include<stdio.h>
#include<assert.h>
#include<string.h>
#include<stdlib.h>


// 动态申请一个节点
SListNode* BuySListNode(SLTDateType x)
{
	SListNode* newNode;
	SListNode* newNode = (SListNode*)malloc(sizeof(SListNode));
	if (newNode == NULL)
	{
		printf("SListNode:malloc空间失败！！！\n");
		return;
	}
	newNode->data = x;
	newNode->next = NULL;
}

// 单链表打印
void SListPrint(SListNode* plist)
{
	assert(plist);
	SListNode* cur = plist; 
	while (cur)
	{
		printf("%d \n", cur->data);
		cur = cur->next;
	}
	printf("\n");
}

// 单链表尾插
void SListPushBack(SListNode** pplist, SLTDateType x)
{
	assert(pplist);
	SListNode* newNode;
	SListNode *newNode = BuySListNode(x);
	newNode->data = x;
	newNode->next = NULL;
	if (*pplist == NULL)
	{
		*pplist = newNode;
	}
	else
	{
		newNode = *pplist;
		while (newNode->next != NULL)
		{
			newNode = newNode->next;
		}
		newNode->next = newNode;
	}
}

// 单链表的头插
void SListPushFront(SListNode** pplist, SLTDateType x)
{
	assert(pplist);
	SListNode *newNode = BuySListNode(x);
	if (NULL == *pplist)
	{
		*pplist = newNode;
	}
	else
	{
		newNode->next = *pplist;
		*pplist = newNode;
	}
	SlistNode* newNode = BuySlistNode(x);
	newNode->next = *pplist;
	*pplist = newNode;
}

// 单链表的尾删
void SListPopBack(SListNode** pplist)
{
	assert(pplist);//判断链表是否存在 pplist为空，链表不存在
	//cur为最后一个节点 删除最后一个节点同时得把前一个节点保存下来
	while (cur->next->next)
	{
		cur = cur->next;
	}
	//删除最后一个节点，cur刚好是最后一个节点的前一个节点
	free(cur);
	cur->next = NULL;
}

// 单链表头删
void SListPopFront(SListNode** pplist)
{
	assert(*pplist);
	//空直接返回
	if (NULL == *pplist)
	{
		return;
	}
	//只有一个节点 删除节点直接返回
	else if ((*pplist)->next == NULL)
	{
		free(*pplist);
		*pplist->next = NULL;
	}
	//将第一个节点作为删除节点 *pplist保存下一个节点 持续删除 最后next域判空
	else
	{
		SlistNode* delNode = *pplist;
		*pplist = delNode->next;
		free(delNode);
	}
	SlistNode* delNode = NULL;
	if (NULL == *pplist)
	{
		return;
	}
	delNode = *pplist;
	*pplist = delNode->next;
	free(delNode);
}

// 单链表查找
SListNode* SListFind(SListNode* plist, SLTDateType x)
{
	SListNode* cur = plist;
	while (cur)
	{
		if (cur->data == x)
		{
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}

// 单链表在pos位置之后插入x
// 分析思考为什么不在pos位置之前插入？
//回答：链表为单链表，无法直接知道pos的前一个节点，而函数未提供*pplist,无法知道链表的第一个节点，无法通过遍历链表找到pos的前一个节点
//故在pos的前面进行节点插入方法不可取，故只能pos之后插入
void SListInsertAfter(SListNode* pos, SLTDateType x)
{
	SlistNode* newNode = NULL;
	if (NULL == pos)
	{
		return;
	}
	newNode = BuySlistNode(x);
	newNode->next = pos->next;
	pos->next = newNode;
}

// 单链表删除pos位置之后的值
// 分析思考为什么不删除pos位置？
//回答：当删除了pos位置后，pos->next就找不到了，故在删除pos之前，我们得将pos->next保存下来
void SListEraseAfter(SListNode* pos)
{
	SlistNode* delNode = NULL;
	if (pos == NULL || pos->next == NULL)
		return;

	delNode = pos->next;
	pos->next = delNode->next;
	free(delNode);
}

// 单链表的销毁
void SListDestroy(SList* plist)
{
	SlistNode* cur = plist;
	int count = 0;
	whlie(cur)
	{
		count++;
		cur = cur->next;
	}
	return count;

}


//单链表测试
void SlistTest()
{

}
