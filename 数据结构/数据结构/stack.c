#include"stack.h"
#include<stdio.h>
#include<assert.h>
#include<math.h>


// 1.初始化栈 
void StackInit(Stack* ps);//首先得动态申请一块空间
{
	//1.栈不为空
	assert(ps);
	//申请至少四个空间的位置
	ps->_a = (STDataType*)sizeof(STDataType)* 4;
	//2.栈为空
	if (ps->_a == NULL)
	{
		assert(0);
		printf("申请空间失败！！！");
			return;
	}
	ps->_top = 0;
	ps->_capacity = 4;

}

//入栈时空间不够时申请新的空间函数
void StackChackCapacity(Stack *ps)
{
	assert(ps);
	if (ps->_top == ps->_capacity)
	{
		//说明空间不够需要开辟新空间
		//1.开辟新空间
		int newcapacity = ps->_capacity * 2;
		STDataType* temp =(STDataType*)malloc(sizeof(STDataType)*newcapacity);
		if (NULL == temp)
		{

			printf("申请空间失败！！！");
			return;
		}
		//2.空间拷贝
		memcpy(temp, ps->_a, sizeof(STDataType)*ps->_top);
		//3.把原来空间给释放掉
		free(ps->_a);
		//4.使用新空间
		ps->_a = temp;
		ps->_capacity = newcapacity;
	}
}
// 2.入栈 
void StackPush(Stack* ps, STDataType data)
{
	//如果空间不够得扩容
	StackChackCapacity(ps);

	ps->_a[ps->_top] = data;
	ps->_top++;
}


// 3.出栈 
void StackPop(Stack* ps)
{
	//1.判空
	if (StackEmpty(ps))
		return;
	ps->_top--;
}

// 4.获取栈顶元素 
STDataType StackTop(Stack* ps)
{
	assert(ps);
	return ps->_a[ps->_top - 1];
}

// 5.获取栈中有效元素个数 
int StackSize(Stack* ps)
{
	assert(ps);
	return ps->_top;
}


// 6.检测栈是否为空，如果为空返回非零结果，如果不为空返回0 
int StackEmpty(Stack* ps)
{
	assert(ps);
	return 0 == ps->_top;
}

// 7.销毁栈 
void StackDestroy(Stack* ps)
{
	assert(ps);
	free(ps->_a);
	ps->_a == NULL;
	ps->_capacity = 0;
	ps->_top = 0;
}




